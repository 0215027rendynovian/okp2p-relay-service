@include('inc.header')

<div class="container">
<div class="section">
   <div class="p-5"></div>
   <div class="row center">
      <div class="col s12">
      <img class="mitra-img" style="border-radius: 50%; border-color: #223887;border-style: solid; width:230px; height:230px"  src="{{asset('img/logo.png')}}">
      </div>
      <div class="col s12 center">
      <h6 class="bold text-blue">AGENT SADULUR</h6>
      <h6 class="text-yellow">Hubungi agent sadulur untuk pelayanan terbaik</h6>
      <textarea style="display: none;" class="form-control" rows="5" id="messagewhatsapp" name="messagewhatsapp">Terima kasih telah menghubungi *SADULUR HOMECARE*, silahkan berikan data anda untuk order layanan *_Massage_* di aplikasi *Sadulur Homecare* 

Nama : {{$name}}
Gender Permintaan : {{$gender}}
Treatment/Layanan : {{$layanan}}
Durasi / Deskripsi : {{$description}}
Jam Booking : -
Alamat Peta: {{$adress}}
Detail Alamat : {{$detail_alamat}}
Nomor HP : {{$nomor_hp}}

Data lengkap yang anda berikan, membantu kami dalam melayani anda sebaik mungkin dan selalu profesional

      </textarea>
      </div>
   </div>
</div>
<div class="gap"></div>

   <a class="waves-effect waves-light btn w-100 bg-blue" onclick="myFunction()">HUBUNGI</a>
      <div class="p-5"></div>
    <a class="waves-effect waves-light btn w-100 bg-yellow" href="{{url('home')}}">KEMBALI KE HOME</a>


</div>
</a>

<script>
function myFunction() {
   debugger
  var uri =  document.getElementById('messagewhatsapp').value;
  var res = encodeURI(uri);
  window.location.href = "whatsapp://send?phone=6282299161933&text="+res;
  location.reload();

  
}

</script>




@include('inc.footer')

