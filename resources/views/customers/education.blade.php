@include('inc/header')

<!-- nav -->
<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a href="#" class="" id="nav-title">EDUCATION</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#"><b>SADULUR HOMECARE<b></a></li>
        <li><a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
      </ul>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          <!-- @csrf -->
          {{ csrf_field() }}
      </form>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <a href="#" data-target="#" class="sidenav-trigger right"><i class="material-icons">settings</i></a>
    </div>
  </nav>

<div class="container">
<div class="section">

<h6>Education</h6>
        @foreach($Education as $key => $Education)    
              <a href="{{url('home/detail/'."$Education->id")}}">
                        <div class="card-panel">
                        <div class="cardh1">
                        {{$Education->title}}
                        <span>{{$Education->name_service}}</span>
                  </div>
                  </div>
               </a>
          @endforeach
</div>
</div>

<div class="gap"></div><div class="gap"></div>
<div class="bottomnavbar">
  <a href="{{url('home')}}" class="font-11 active"><img class="ic-bottomnav" src="./img/ic-home.png">Home</a>
  <a href="{{url('massage')}}" class="font-11 active"><img class="ic-bottomnav" src="./img/ic-mas.png">Massage</a>
  <a href="{{url('cleaning')}}" class="font-11"><img class="ic-bottomnav" src="./img/ic-cle.png">Cleaning</a>
  <a href="{{url('education')}}" class="font-11"><img class="ic-bottomnav" src="./img/ic-edu-b.png">Education</a>
</div>


@include('inc/footer')

