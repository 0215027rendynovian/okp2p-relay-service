@include('inc.header')
<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a href="#" class="" id="nav-title">DETAIL RINCIAN</a> 
    </div>
</nav>
<div class="container">
  <input type="hidden" name="latitude" id="latitude" value='{{$latitude}}'>
  <input type="hidden" name="latitude" id="longitude" value='{{$longitude}}'>
  <input type="hidden" name="id_order" id="id_order" value='{{$id}}'>
  <input type="hidden" name="duration" id="duration" value='{{$duration}}'>
  <input type="hidden" name="description" id="description" value='{{$description}}'>
  <input type="hidden" name="payment_method" id="payment_method" value='{{$payment_method}}'>
  <input type="hidden" name="adress" id="adress" value='{{$adress}}'>
  <input type="hidden" name="gender" id="gender" value='{{$gender}}'>
  <input type="hidden" name="detail_alamat" id="detail_alamat" value='{{$detail_alamat}}'>
  <input type="hidden" name="nomor_hp" id="nomor_hp" value='{{$nomor_hp}}'>

<div class="section">
  <div class="text-yellow">Rincian</div>
    <div class="row">
      <div class="col s6">Layanan :</div>
      <div class="col s6">{{$ServiceMitra->title}}{{$ServiceMitra->name_service}}</div>
      <div class="col s6">Deskripsi :</div>
      <div class="col s6">{{$description}}</div>
      <div class="col s6">Pembayaran :</div>
      <div class="col s6">
          @if($payment_method == 1)
            Cash
          @else
            Transfer
          @endif
      </div>
      <div class="col s6">Nomor Hp:</div>
      <div class="col s6">{{$nomor_hp}}</div>
      
      <div class="col s6">Gender :</div>
      <div class="col s6">{{$gender}}</div>

      <div class="col s6">Alamat Maps :</div>
      <div class="col s6">{{$adress}}</div>

      <div class="col s6">Detail Alamat :</div>
      <div class="col s6">{{$detail_alamat}}</div>
      </div>

      <a class="waves-effect waves-light btn w-100 bg-blue" onclick="myFunction()">PESAN SEKARANG</a>
      <div class="p-5"></div>
      <a class="waves-effect waves-light btn w-100 bg-red"  onclick="goBack()" href="#">BATAL</a>

    </div>

</div>

@include('inc.footer')

<script>

    function myFunction() {
      debugger
      
            var idorder = document.getElementById("id_order").value;
            var duration = document.getElementById("duration").value;
            var payment_method = document.getElementById("payment_method").value;
            var alamat = document.getElementById("adress").value;
            var lat = document.getElementById("latitude").value;
            var lng = document.getElementById("longitude").value;
            var gender = document.getElementById("gender").value;
            var detail_alamat = document.getElementById("detail_alamat").value;
            var nomor_hp  = document.getElementById("nomor_hp").value;
            window.location="/home/pencarian/"+idorder+"/"+duration+"/"+payment_method+"/"+alamat+"/"+lat+"/"+lng+"/"+gender+"/"+detail_alamat+"/"+nomor_hp;
     };
</script>


