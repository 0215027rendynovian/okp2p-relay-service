@include('inc.header')

<div class="container">
<div class="section">
   <div class="p-5"></div>
   <div class="row center">
      <div class="col s12">
      @if(empty($data->chooseFileSelfie))
         <img class="mitra-img" style="border-radius: 50%; border-color: #223887;border-style: solid; width:230px; height:230px"  src="{{ asset('mitra/img/happy-customer.png')}}">
      @else
         <img class="mitra-img" style="border-radius: 50%; border-color: #223887;border-style: solid; width:230px; height:230px"  src="{{$data->chooseFileSelfie}}">
      @endif
      </div>
      <div class="col s12 center">
      <input  id="uid_mitra" name="uid_mitra" type="hidden" value="{{$uid_mitra}}">
      <h6 class="bold text-blue">{{$user->name}}</h6>
      <h6 class="text-yellow">{{$data->no_mitra}}</h6>
      </div>
   </div>
   <div class="gap"></div>
   <div class="row center">
   <div class="text-yellow">Waktu Perkiraan tiba :</div>
   <div class="text-blue">30 menit</div>
   </div>
</div>
<div class="gap"></div>

   <a class="waves-effect waves-light btn w-100 bg-blue" onclick="Batalkan()">BATALKAN PESAN</a>
      <div class="p-5"></div>
    <a class="waves-effect waves-light btn w-100 bg-yellow" href="{{url('home')}}">KEMBALI KE HOME</a>


</div>
</a>

<script>

function Batalkan(){



var uid_mitra = document.getElementById('uid_mitra').value;

if(uid_mitra == '' || uid_mitra == undefined){
   return alert('UID tidak ada');
}


$.ajaxSetup({
             headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
             });
             $.ajax({
                     type:'POST',
                     url:'/api-batalkan-pesanan',
                     data:{uid_mitra:uid_mitra},
                       success:function(data){
                     //     if(data.response=="Status Berhasil"){
                     // window.location='saldo#tarikSaldo'
                     alert('Status Berhasil');
                      }
                
             });


};
</script>

@include('inc.footer')

