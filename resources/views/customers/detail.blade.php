
@include('inc/header')
<style>
#map {
  margin: auto;
  width: 100%;
  height: 100%;
  z-index:100;
  border:1;
  padding-bottom:300px;
}
#mapSearchContainer{
  
  position:fixed;
  top:20px;
  right: 40px;
  height:30px;
  width:180px;
  z-index:110;
  font-size:10pt;
  color:#5d5d5d;
  border:solid 1px #bbb;
  background-color:#f8f8f8;
}
.pointer{
  position:absolute;
  top:86px;
  left:60px;
  z-index:99999;
}
</style>

<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a href="#" class="" id="nav-title">DETAIL</a>
      <ul class="right hide-on-med-and-down">
      
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Reservation List</a></li>
      </ul>
      <a href="#" onclick="goBack()" class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>
    </div>
  </nav>
    <div class="container">
    <input type="hidden" name="idserviceMitra" id="idserviceMitra" value='{{$Id}}'>
    <div class="section">
    <div class="text-yellow">Layanan dipilih :</div>
    <div class="text-blue bold">{{$ServiceMitra->title}} {{$ServiceMitra->name_service}}</div>
    <br>
    <div class="text-yellow">Info :</div>
    <div class="">{{$ServiceMitra->description}}</div>
    <!-- <br> -->
    <!-- <div class="text-yellow">Open Order :</div>
    <div class="">{{$ServiceMitra->open_service}}</div> -->
    <br>
    <div class="text-yellow">Referensi Mitra :</div>
    <div class="">{{$ServiceMitra->reference}}</div>
    <br>
    <div class="text-yellow">Masukan Alamat Anda :</div>
    <br>
    <textarea id="alamat" name="alamat" rows="5" cols="5"  style="background-color: rgb(54 78 164);color:#fff;"  disabled>Konfirmasi Alamat di bawah berdasarkan peta lokasi  
    </textarea>
    <div id="map">
    <!-- << Cari Alamat -->
    </div>
<br>
<div class="text-yellow">Pilih durasi :</div>
<form action="#">
<div class="row">
@foreach($detailServiceMitra as $key => $data)
  <div class="col s6">
  <label>
    <input name="group{{$key+1}}" id="duration_{{$key+1}}" onclick="check('duration_{{$key+1}}')"value='{{$data->duration}}' type="radio"/>
    <span>{{$data->description}}</span>
  </label>
</div>
@endforeach
</div>

<div class="text-yellow">Pilihan layanan gender :</div>
<form action="#">
<div class="row">

  <div class="col s6">
  <label>
    <input name="gender1" id="gender1" name="gender1" onclick="checkg('gender1')" type="radio"/>
    <span>Pria</span>
  </label>
</div>
<div class="col s6">
  <label>
    <input name="gender2" id="gender2" name="gender2"  onclick="checkg('gender2')"  type="radio"/>
    <span>Wanita</span>
  </label>
</div>

</div>


</form>
<br>
<div class="text-yellow">Detail Alamat :</div>
<textarea id="detail_alamat" name="detail_alamat" rows="4" cols="80">
</textarea>
<br>
<br>

<div class="text-yellow">Masukan Nomor Ponsel :</div>
<input type="text" id="nomor_hp" name="nomor_hp" placeholder="085624282xxx">
<br>


<div class="text-yellow">Cara Pembayaran :</div>
<div class=""></div>
<div class="input-field col s12">
    <select id="payment_method">
      <option value="" disabled selected>Pilih Cara Pembayaran</option>
      <option value="1">Cash</option>
      <!-- <option value="2">Transfer</option> -->
    </select>  
  </div>
  <br><br>
  
  <a class="waves-effect waves-light btn w-100 bg-blue"  onclick="myFunction()">LANJUT</a>

</div>

</div>


@include('inc/footer')

<script>

      var greenIcon = L.icon({
          iconUrl: 'https://lh3.googleusercontent.com/42bUknoaAVlBtBP2d4yJ9tMEVHgg10MHsshXx-UYGnnLXwZ6xGese_6cHtK4GqhWsQ',
          shadowUrl: 'https://lh3.googleusercontent.com/42bUknoaAVlBtBP2d4yJ9tMEVHgg10MHsshXx-UYGnnLXwZ6xGese_6cHtK4GqhWsQ',

          iconSize:     [38, 95], // size of the icon
          shadowSize:   [50, 64], // size of the shadow
          iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
          shadowAnchor: [4, 62],  // the same for the shadow
          popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
      });
        var map = L.map('map', {
            // Set latitude and longitude of the map center (required)
            center: [-6.914744, 107.609810],
            // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
            zoom: 13,
            icon: greenIcon
        });
        map.invalidateSize();

        L.control.scale().addTo(map);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var alamat;
        var lat;
        var lng;

        var searchControl = new L.esri.Controls.Geosearch().addTo(map);

        var results = new L.LayerGroup().addTo(map);

        searchControl.on('results', function(data){

      
            results.clearLayers();
            for (var i = data.results.length - 1; i >= 0; i--) {
            //alamat = data.results[0].address;
            alamat = data.results[0].text;
            lat = data.results[0].latlng.lat;
            lng = data.results[0].latlng.lng
            results.addLayer(L.marker(data.results[i].latlng));
            }
        
            var x = document.getElementById("alamat").value = alamat;

        });
        setTimeout(function(){$('.pointer').fadeOut('slow');},3400);

        function myFunction() {

            var idserviceMitra = document.getElementById("idserviceMitra").value;
            var duration_1 = document.getElementById("duration_1");
            var duration_2 = document.getElementById("duration_2");
            var duration_3 = document.getElementById("duration_3");
            var gender1 = document.getElementById("gender1");
            var gender2 = document.getElementById("gender2");
            var detail_alamat = document.getElementById("detail_alamat").value;
            var nomor_hp = document.getElementById("nomor_hp").value;

            if(gender1 != null){
                if(gender1.checked == true){
                  gender = 'Pria'
                }
            } 
            if(gender2 != null){
                if(gender2.checked == true){
                  gender = 'Wanita'
                }
            } 

            if(duration_1 != null){
                if(duration_1.checked == true){
                   duration = '1'
                }
            } 

            if(duration_2 != null){
                if(duration_2.checked == true){
                   duration = '2'
                }
            } 

            if(duration_3 != null){
                if(duration_3.checked == true){
                   duration = '3'
                }
            } 
            

            var payment_method = document.getElementById("payment_method").value;
            if(payment_method == undefined || payment_method == ''){
                 alert('Harap Pilih Pembayaran');
                 return location.reload();
            }

            if(gender == undefined || gender == ''){
                 alert('Harap Pilih Gender');
                 return location.reload();
            }

            if(detail_alamat == undefined || detail_alamat == ''){
                 alert('Harap masukan Detail Alamat');
                 return location.reload();
            }

            if(nomor_hp == undefined || nomor_hp == ''){
                 alert('Harap masukan Nomor Hp Anda');
                 return location.reload();
            }

            if(duration == undefined || duration == ''){
                 alert('Harap Pilih');
                 return location.reload();
            }


            if(alamat == undefined || alamat == ''){
              alert('Harap isi alamat untuk menemukan lokasi anda');
              return location.reload();
            }
          
            window.location="/home/rincian/"+idserviceMitra+"/"+duration+"/"+payment_method+"/"+alamat+"/"+lat+"/"+lng+"/"+gender+"/"+detail_alamat+"/"+nomor_hp;
        };

        function check(duration) {
  
          if(document.getElementById("duration_1") != null){
            document.getElementById("duration_1").checked = false;
          }

          if(document.getElementById("duration_2") != null){
            document.getElementById("duration_2").checked = false;
          }

          if(document.getElementById("duration_3") != null){
            document.getElementById("duration_3").checked = false;
          }
       
          if(duration){
            document.getElementById(duration).checked = true;
          }
         
        }


        function checkg(gender) {
              if(document.getElementById("gender1") != null){
                document.getElementById("gender1").checked = false;
              }

              if(document.getElementById("gender2") != null){
                document.getElementById("gender2").checked = false;
              }

              if(gender){
                document.getElementById(gender).checked = true;
              }

      }
        
</script>

