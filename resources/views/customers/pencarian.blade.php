@include('inc.header')
<?php

    header('Set-Cookie: cross-site-cookie=name; SameSite=None; Secure');

?>

<div class="container">
  <input type="hidden" name="latitude" id="latitude" value='{{$latitude}}'>
  <input type="hidden" name="latitude" id="longitude" value='{{$longitude}}'>
  <input type="hidden" name="id_order" id="id_order" value='{{$id}}'>
  <input type="hidden" name="duration" id="duration" value='{{$duration}}'>
  <input type="hidden" name="payment_method" id="payment_method" value='{{$payment_method}}'>
  <input type="hidden" name="adress" id="adress" value='{{$adress}}'>
  <input type="hidden" name="gender" id="gender" value='{{$gender}}'>
  <input type="hidden" name="detail_alamat" id="detail_alamat" value='{{$detail_alamat}}'>
  <input type="hidden" name="nomor_hp" id="nomor_hp" value='{{$nomor_hp}}'>
    <div class="section">

            <div class="row center">
                        <div class="col s12">
                            <img class="pvh-10" src="{{asset('img/ic-search.png')}}">
                        </div>

                        <div class="col s12 center">
                            <h6 class="bold text-blue">SEDANG MELAKUKAN PENCARIAN</h6>
                            <h6 id="mohontunggu" class="text-grey">mohon tunggu</h6>
                            <h6 id="pesan"  style="display:none;" class="text-grey">Ops, Mitra masih belum tersedia di area anda / Mitra belum menerima orderan anda, Hubungi melalui Agent sadulur</h6>
                    <!-- {{$mitra_maps}} -->
                        </div>
                        <br><br><br><br><br>
                        <div id="countdown"> </div>
                        <div id="download" style="display:none;"><a class="waves-effect waves-light btn w-50 bg-yellow" onclick="sadulursend()" href='#'>Hubungi Agents Sadulur</a></div>
            </div>
    </div>

</div>

@include('inc.pusher-pencarian-order')
@include('inc.footer')

