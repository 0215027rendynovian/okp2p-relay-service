<!-- Main Footer -->
<footer class="main-footer">
    <strong>Hak Cipta &copy; {{ date('Y') }} <a href="http://www.okp2p.co.id">OKP2P</a>.</strong>
    Seluruh hak cipta.
    <div class="float-right d-none d-sm-inline-block">
      <b>Versi</b> 1.0.0
    </div>
</footer>
