

<script src="{{asset('AdminLTE-3.0.5/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('AdminLTE-3.0.5/dist/js/adminlte.min.js')}}"></script>
<script src="/AdminLTE-3.0.5/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="/AdminLTE-3.0.5/plugins/toastr/toastr.min.js"></script>

@include('js.nocopy')
@include('js.alert-toast')

<script>
    init()

    function init()
    {
        var email = localStorage.getItem('email')
        var password = localStorage.getItem('password')
        var rememberme = localStorage.getItem('rememberMe')

        if (rememberme == 'false') {
            document.getElementById('email').value = email
            document.getElementById('password').value = ''
            document.getElementById('rememberMe').checked = false
        }else{
            document.getElementById('email').value = email
            document.getElementById('password').value = password
            document.getElementById('rememberMe').checked = rememberme
        }
    }

    function remember()
    {
        var email = document.getElementById('email').value
        var password = document.getElementById('password').value
        var checkRemember = document.getElementById('rememberMe').checked

        if (checkRemember == false) {
            // localStorage.clear()
            localStorage.setItem('email', email)
            localStorage.setItem('password', null)
            localStorage.setItem('rememberMe', false)
        } else {
            localStorage.setItem('email', email)
            localStorage.setItem('password', password)
            localStorage.setItem('rememberMe', checkRemember)
        }
    }

    async function forgot(){
        // alert("Silahkan hubungi Admin Atau Tim IT");
        Swal.fire({
            icon: 'warning',
            title: 'Oops...',
            text: 'Silahkan hubungi Admin Atau Tim IT!'
        })
    }

    async function block(_this) {

        const { value: password } = await Swal.fire({
            icon: 'question',
            title: 'Masukan Passowrd untuk memiliki akses',
            input: 'password',
            inputLabel: 'Password',
            inputPlaceholder: 'Masukan Passowrd untuk memiliki akses',
            inputAttributes: {
                maxlength: 20,
                autocapitalize: 'off',
                autocorrect: 'off'
            }
        })

        if (password) {
            if (password=="userganteng1234") {
                window.location.href = _this
            }
            else
            {
                // document.getElementById("msg").value = "test";
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Silahkan hubungi Admin Atau Tim IT!'
                })
            }
        }
    }

    function showPwd(id, el) {
        let x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
            el.className = 'fa fa-eye-slash showpwd';
        } else {
            x.type = "password";
            el.className = 'fa fa-eye showpwd';
        }
    }
</script>
