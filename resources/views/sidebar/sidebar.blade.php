  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4 back-ops-okp2p" style="">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{asset('AdminLTE-3.0.5/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">

          <img src="{{asset('img/icon-okp2p.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/" class="d-block">{{{ Auth::user()->name }}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a class="nav-link active" style="background-color: #2c3e50">
                    <i class="nav-icon fas fa-th-list"></i>
                    <p>
                        Pilih Menu
                        <i class="right fas fa-angle-down"></i>
                    </p>
                </a>
            </li>
            <?php
                $sidebar = SidebarLayout::sidebar();
                $parent_menu = $sidebar['parent_menu'];
                $child_menu = $sidebar['child_menu'];
                $permission = $sidebar['permission'];
                $is_es_system = auth()->user()->es_system;
            ?>
            @foreach ($parent_menu as $parent)
                @if ($parent->role_id == $permission)
                    @if ($parent->isParent)
                        <li class="nav-item has-treeview {{ $parent->menuopen == $activeSidebar ? 'menu-open' : null }}">
                            <a class="nav-link {{ $parent->menuopen == $activeSidebar ? 'active' : null }}" >
                                <i class="nav-icon fas {{ $parent->icon }}"></i>
                                <p>{{ $parent->name }}
                                <i class="right fas fa-angle-left"></i></p>
                            </a>
                            <ul class="nav nav-treeview">
                                @foreach ($child_menu as $child)
                                    @if ($child->role_id == $permission && $parent->parent == $child->parent)
                                        <li class="nav-item">
                                            <a href="{{url($child->url)}}"  class="nav-link {{ Request::segment(1) === $child->url ? 'active' : null }}" >
                                                <i class="fas fa-angle-right nav-icon"></i>
                                                <p>{{ $child->name }}</p>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="{{url($parent->url)}}"  class="nav-link {{ Request::segment(1) === $parent->url ? 'active' : null }}" >
                                <i class="fas {{ $parent->icon }} nav-icon"></i>
                                <p>{{ $parent->name }}</p>
                            </a>
                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<script type="text/javascript">
    function load_main_content()
    {
        $('#admin-dashboard').load('/admin/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
        $('#identity-check').load('/identitycheck/');
    }
</script>
