	<!-- Import Excel -->
    <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/sigap_import_excel" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="IziModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
							{{ csrf_field() }}
                            <div class="form-group">
                                <label>Tanggal Execute:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    {{-- <input type="text" name="execute_date" id="execute_date" class="form-control" placeholder="24/11/1993" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask> --}}
                                    <input readonly type="text" name="execute_date" id="execute_date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy H:i:s" value="{{ date('d-m-Y H:i:s') }}" data-mask>
                                </div>
                            </div>

							<label>Pilih file excel <code>*Format Harus Sesuai</code></label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Upload</button>
						</div>
					</div>
				</form>
			</div>
</div>
