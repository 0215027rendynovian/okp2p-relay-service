	<!-- REPORT DATA TERDUGA SIGAP -->
    <div class="modal fade" id="ajaxModel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="productForm" name="productForm" class="form-horizontal" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="IziModalLabel">Update Data</h5>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <input type="hidden" name="ref_idsigap" id="ref_idsigap" class="form-control">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label>ALASAN :</label>
                            <div class="input-group">
                                <textarea class="form-control" name="reason" id="reason"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Status :</label>
                            <div class="input-group">
                                <select class="form-control" name="status_report" id="status_report">
                                    <option selected disabled value="">-- Pilih --</option>
                                    <option value="1">Nihil</option>
                                    <option value="2">Pemblokiran</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="saveBtn" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
