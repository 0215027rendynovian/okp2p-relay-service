
<!-- RDL -->
<div class="modal fade" id="modal-rdl">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Informasi Detail (RDL)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>home phone number : <b>{{ $getRdl['home_phone_number'] }}</b></li>
                    <li>office phone number : <b>{{ $getRdl['office_phone_number'] }}</b></li>
                    <li>mobilephone number : <b>{{ $getRdl['mobile_phone_number'] }}</b></li>
                    <li>fax number : <b>{{ $getRdl['fax_number'] }}</b></li>
                    <li>account status : <b>{{ $getRdl['account_status'] }}</b></li>
                    <li>title : <b>{{ $getRdl['title'] }}</b></li>
                    <li>first name : <b>{{ $getRdl['first_name'] }}</b></li>
                    <li>middle name : <b>{{ $getRdl['middle_name'] }}</b></li>
                    <li>last name : <b>{{ $getRdl['last_name'] }}</b></li>
                    <li>npwp number : <b>{{ $getRdl['npwp_number'] }}</b></li>
                    <li>nationality : <b>{{ $getRdl['nationality'] }}</b></li>
                    <li>domicile country : <b>{{ $getRdl['domicile_country'] }}</b></li>
                    <li>religion : <b>{{ $getRdl['religion'] }}</b></li>
                    <li>birth place : <b>{{ $getRdl['birth_place'] }}</b></li>
                    <li>birth date : <b>{{ date('d M Y', strtotime($getRdl['birth_date'])) }}</b></li>
                    <li>gender : <b>{{ $getRdl['gender'] }}</b></li>
                    <li>marital status : <b>{{ $getRdl['marital_status'] }}</b></li>
                    <li>mother maiden name : <b>{{ $getRdl['mother_maiden_name'] }}</b></li>
                    <li>job code : <b>{{ $getRdl['job_code'] }}</b></li>
                    <li>education : <b>{{ $getRdl['education'] }}</b></li>
                    <li>id number : <b>{{ $getRdl['id_number'] }}</b></li>
                    <li>id issuing city : <b>{{ $getRdl['id_issuing_city'] }}</b></li>
                    <li>id expiry date : <b>{{ date('d M Y', strtotime($getRdl['id_expiry_date'])) }}</b></li>
                    <li>address street : <b>{{ $getRdl['address_street'] }}</b></li>
                    <li>address rt rw perum : <b>{{ $getRdl['address_rt_rw_perum'] }}</b></li>
                    <li>address kelurahan : <b>{{ $getRdl['address_kelurahan'] }}</b></li>
                    <li>address kecamatan : <b>{{ $getRdl['address_kecamatan'] }}</b></li>
                    <li>zip code : <b>{{ $getRdl['zip_code'] }}</b></li>
                    <li>branch opening location code : <b>{{ $getRdl['branch_opening_location_code'] }}</b></li>
                    <li>email : <b>{{ $getRdl['email'] }}</b></li>
                    <li>account type : <b>{{ $getRdl['account_type'] }}</b></li>
                    <li>open account reason : <b>{{ $getRdl['open_account_reason'] }}</b></li>
                    <li>source of fund : <b>{{ $getRdl['source_of_fund'] }}</b></li>
                    <li>cif number : <b>{{ $getRdl['cif_number'] }}</b></li>
                    <li>account number : <b>{{ $getRdl['account_number'] }}</b></li>
                    <li>account balance : <b>{{ number_format($getRdl['account_balance'],0,',','.') }}</b></li>
                </ul>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
            </div>
          </div>
    </div>
</div>

<!-- Account Lender -->
<div class="modal fade" id="modal-acc-lender">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Informasi Detail (Account Lender)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>given name : <b>{{ $getAccLender['given_name'] }}</b></li>
                    <li>surname : <b>{{ $getAccLender['surname'] }}</b></li>
                    <li>mobile number : <b>{{ $getAccLender['mobile_number'] }}</b></li>
                    <li>email : <b>{{ $getAccLender['email'] }}</b></li>
                    <li>ktp number : <b>{{ $getAccLender['ktp_number'] }}</b></li>
                    <li>npwp number : <b>{{ $getAccLender['npwp_number'] }}</b></li>
                    <li>type : <b>{{ $getAccLender['type'] }}</b></li>
                    <li>external id : <b>{{ $getAccLender['external_id'] }}</b></li>
                    <li>customer accounts :
                        <table class="table">
                            <thead>
                                <th>account code</th>
                                <th>account number</th>
                                <th>is preferred</th>
                                <th>account type</th>
                            </thead>
                            <tbody>
                                @foreach ($getAccLender['customer_accounts'] as $item)
                                    <tr>
                                        <td>{{ $item['account_code'] }}</td>
                                        <td>{{ $item['account_number'] }}</td>
                                        <td>{{ $item['is_preferred'] }}</td>
                                        <td>{{ $item['account_type'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </li>
                    <li>current balance : <b>Rp. {{ number_format($getAccLender['current_balance'],0,',','.') }}</b></li>
                    <li>customer escrow id : <b>{{ $getAccLender['customer_escrow_id'] }}</b></li>
                </ul>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
            </div>
          </div>
    </div>
</div>


<!-- ESSCROW P2P -->
<div class="modal fade" id="modal-esscrow-fee">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Informasi Detail (ESSCROW P2P)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
            </div>
          </div>
    </div>
</div>



<!-- BALANCE AMOUNT -->
<div class="modal fade" id="modal-balance-amount">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Informasi Detail (BALANCE AMOUNT)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
            </div>
          </div>
    </div>
</div>



<!-- JUMLAH PENGAJUAN -->
<div class="modal fade" id="modal-jml-pengajuan">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Informasi Detail (JUMLAH PENGAJUAN)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
            </div>
          </div>
    </div>
</div>
