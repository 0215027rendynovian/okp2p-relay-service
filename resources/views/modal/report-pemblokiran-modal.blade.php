	<!-- REPORT DATA TERDUGA SIGAP -->
    <div class="modal fade" id="reportPemblokiran" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form method="post" action="/form_report_pemblokiran" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="IziModalLabel">Report Pemblokiran</h5>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="ref_id_backoffice" id="ref_id_backofficePemblokiran" class="form-control">
                        <input type="hidden" name="ref_id_pengguna" id="ref_id_penggunaPemblokiran" class="form-control">
                        <div class="form-group">
                            <label>Nama PT :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nama_pt" id="nama_pt" placeholder="input nama pt...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Alamat PT :</label>
                            <div class="input-group">
                                <textarea class="form-control" name="alamat_pt" id="alamat_pt" placeholder="input alamat pt..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nama Pemblokir :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nama_pemblokir" id="nama_pemblokir" placeholder="input nama pemblokir...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Jabatan Pemblokir :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="jabatan_pemblokir" id="jabatan_pemblokir" placeholder="input jabatan pemblokir...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Alamat Pemblokir :</label>
                            <div class="input-group">
                                <textarea class="form-control" name="alamat_pemblokir" id="alamat_pemblokir" placeholder="input alamat pemblokir..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Hari Pemblokiran :</label>
                            <div class="input-group">
                                <select class="form-control" name="hari_pemblokiran" id="hari_pemblokiran">
                                    <option selected disabled value="">-- Pilih Hari --</option>
                                    <option value="Senin">Senin</option>
                                    <option value="Selasa">Selasa</option>
                                    <option value="Rabu">Rabu</option>
                                    <option value="Kamis">Kamis</option>
                                    <option value="Jumat">Jumat</option>
                                    <option value="Sabtu">Sabtu</option>
                                    <option value="Minggu">Minggu</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Pemblokiran :</label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="tanggal_pemblokiran" id="tanggal_pemblokiran" placeholder="input tanggal pemblokiran...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nomor Polisi :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nomor_polisi" id="nomor_polisi" placeholder="input nomor polisi...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Polisi :</label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="tanggal_polisi" id="tanggal_polisi" placeholder="input tanggal polisi...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nomor DTTOT :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="no_dttot" id="no_dttot" placeholder="input no dttot...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nama Teroris :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nama_terorist" id="nama_terorist" placeholder="input nama terorist...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Jabatan Teroris :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="jabatan_terorist" id="jabatan_terorist" placeholder="input jabatan terorist...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nama Rekening :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nama_rekening" id="nama_rekening" placeholder="input nama rekening...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tempat Tanggal Lahir :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="tempat_tanggal_lahir" id="tempat_tanggal_lahir" placeholder="input tempat tanggal lahir...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Pekerjaan :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="pekerjaan" id="pekerjaan" placeholder="input pekerjaan...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Alamat :</label>
                            <div class="input-group">
                                <textarea class="form-control" name="alamat" id="alamat" placeholder="input alamat..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nomor Rekening :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nomor_rekening" id="nomor_rekening" placeholder="input nomor rekening...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Saldo Terakhir :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="saldo_terakhir" id="saldo_terakhir" placeholder="input saldo terakhir...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Jenis Identitas :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="jenis_identitas" id="jenis_identitas" placeholder="input jenis identitas...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nama Saksi :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nama_saksi" id="nama_saksi" placeholder="input nama saksi...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Lampiran :</label>
                            <div class="input-group">
                                <textarea class="form-control" name="lampiran" id="lampiran" placeholder="input lampiran..."></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
