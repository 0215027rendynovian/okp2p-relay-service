	<!-- Import Excel -->
    <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/izi_import_excel" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="IziModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
 
							{{ csrf_field() }}
                            <div class="form-group">
                                <label>Select Batch</label>
                                <select class="form-control" name="batch" id="batch">
                                <option value='1'>Batch 1</option>
                                <option value='2'>Batch 2</option>
                                <option value='3'>Batch 3</option>
                                <option value='4'>Batch 4</option>
                                <option value='5'>Batch 5</option>
                                <option value='6'>Batch 6</option>
                                <option value='7'>Batch 7</option>
                                <option value='8'>Batch 8</option>
                                <option value='9'>Batch 9</option>
                                <option value='10'>Batch 10</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Tanggal Execute:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input type="text" name="execute_date" id="execute_date" class="form-control" placeholder="24/11/1993" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                                </div>
                            </div>

							<label>Pilih file excel *Format Harus Sesuai</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Upload</button>
						</div>
					</div>
				</form>
			</div>
        </div>
</script>