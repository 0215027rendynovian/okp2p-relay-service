	<!-- REPORT DATA TERDUGA SIGAP -->
    <div class="modal fade" id="reportNihil" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form method="post" action="/form_report_nihil" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="IziModalLabel">Report Nihil</h5>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="ref_id_backoffice" id="ref_id_backofficeNihil" class="form-control">
                        <input type="hidden" name="ref_id_pengguna" id="ref_id_penggunaNihil" class="form-control">
                        <div class="form-group">
                            <label>Nama :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="nama_pt" name="nama_pt" placeholder="input nama...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Alamat :</label>
                            <div class="input-group">
                                <textarea class="form-control" name="alamat_pt" id="alamat_pt" placeholder="input alamat..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nomor :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="nomor" name="nomor" placeholder="input nomor...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Lampiran :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="lampiran" name="lampiran" placeholder="input lampiran...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nomor Polisi :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="nomor_polisi" name="nomor_polisi" placeholder="input nomor polisi...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Polisi :</label>
                            <div class="input-group">
                                <input type="date" class="form-control" id="tanggal_polisi" name="tanggal_polisi" placeholder="input tanggal polisi...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nomor DTTOT :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="nomor_dttot" name="nomor_dttot" placeholder="input nomor dttot...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Hari :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="hari_nihil" name="hari_nihil" placeholder="input hari nihil...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Nihil :</label>
                            <div class="input-group">
                                <input type="date" class="form-control" id="tanggal_nihil" name="tanggal_nihil" placeholder="input tanggal nihil...">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nama PJK :</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="nama_pjk" name="nama_pjk" placeholder="input nama pjk...">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
