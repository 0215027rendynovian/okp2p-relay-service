	<!-- REPORT DATA TERDUGA SIGAP -->
    <div class="modal fade" id="reportTerdugaSigap" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="/update_flag_analysisi" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="IziModalLabel">Report Data Terduga</h5>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="ref_idsigap" id="ref_idsigap" class="form-control">
                        <input type="hidden" name="ref_idbackoffice" id="ref_idbackoffice" class="form-control">
                        <div class="form-group">
                            <label>ALASAN :</label>
                            <div class="input-group">
                                <textarea class="form-control" name="reason" id="reason"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Status :</label>
                            <div class="input-group">
                                <select class="form-control" name="status_report" id="status_report">
                                    <option selected disabled value="">-- Pilih --</option>
                                    <option value="1">Nihil</option>
                                    <option value="2">Pemblokiran</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
