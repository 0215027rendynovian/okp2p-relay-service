
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css" />
<script src="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/0.0.1-beta.5/esri-leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<style>
#map {
  margin: auto;
  width: 100%;
  height: 100%;
  z-index:100;
  border:1;
}
#mapSearchContainer{
  
  position:fixed;
  top:20px;
  right: 40px;
  height:30px;
  width:180px;
  z-index:110;
  font-size:10pt;
  color:#5d5d5d;
  border:solid 1px #bbb;
  background-color:#f8f8f8;
}
.pointer{
  position:absolute;
  top:86px;
  left:60px;
  z-index:99999;
}
</style>

<!--Grid row-->
<style>
.map-container-5{
overflow:hidden;
padding-bottom:56.25%;
position:relative;
height:0;
}
.map-container-5 iframe{
left:0;
top:0;
height:100%;
width:100%;
position:absolute;
}
</style>
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm"><div class="container"><a href="http://127.0.0.1:8000" class="navbar-brand">Sadurlur
</a> <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"></span></button> <div id="navbarSupportedContent" class="collapse navbar-collapse"><ul class="navbar-nav mr-auto"></ul> <ul class="navbar-nav ml-auto"><li class="nav-item dropdown"><a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a> <div aria-labelledby="navbarDropdown" class="dropdown-menu"><a href="http://127.0.0.1:8000/logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item">Logout</a> <form id="logout-form" action="http://127.0.0.1:8000/logout" method="POST" style="display: none;"><input type="hidden" name="_token" value="CqNgjqmdR94jZGtW1X3FoIbR54r9jkNMpcOgYRtq"></form></div></li></ul></div></div></nav>
      <div class="row">
          <div class="col-md-12 mb-4">
            <div class="card card-cascade narrower">
              <div class="view view-cascade gradient-card-header blue-gradient">
              </div>
              <div class="card-body card-body-cascade text-center">
                <h3>Cari Alamat Anda</h3>
                <div id="map" style="height: 300px;">
                </div>
              </div>
            </div>
          </div>
          
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 mb-4">
            <div class="card card-cascade narrower">
              <div class="view view-cascade gradient-card-header blue-gradient">
              </div>
              <div class="card-body card-body-cascade text-center">
                <h3>Cari Alamat Anda</h3>
                <div id="map" style="height: 300px">
                </div>
              </div>
            </div>
          </div>
          
          </div>
        </div>
    <script type="text/javascript">
       var greenIcon = L.icon({
          iconUrl: 'https://lh3.googleusercontent.com/42bUknoaAVlBtBP2d4yJ9tMEVHgg10MHsshXx-UYGnnLXwZ6xGese_6cHtK4GqhWsQ',
          shadowUrl: 'https://lh3.googleusercontent.com/42bUknoaAVlBtBP2d4yJ9tMEVHgg10MHsshXx-UYGnnLXwZ6xGese_6cHtK4GqhWsQ',

          iconSize:     [38, 95], // size of the icon
          shadowSize:   [50, 64], // size of the shadow
          iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
          shadowAnchor: [4, 62],  // the same for the shadow
          popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
      });
        var map = L.map('map', {
            // Set latitude and longitude of the map center (required)
            center: [-6.914744, 107.609810],
            // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
            zoom: 13,
            icon: greenIcon
        });
        

        L.control.scale().addTo(map);
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var searchControl = new L.esri.Controls.Geosearch().addTo(map);

        var results = new L.LayerGroup().addTo(map);

        searchControl.on('results', function(data){
            debugger
            results.clearLayers();
            for (var i = data.results.length - 1; i >= 0; i--) {
            results.addLayer(L.marker(data.results[i].latlng));
            }
        });
        setTimeout(function(){$('.pointer').fadeOut('slow');},3400);
</script>