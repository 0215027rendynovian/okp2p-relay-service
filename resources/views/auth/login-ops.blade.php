<!DOCTYPE html>
<html>
@include('header.header-login-ops')
  <body class="hold-transition login-page" style="background-image: linear-gradient(to right top, #c2e59c, #83e1b4, #42d7d4, #24c8ed, #64b3f4);">
    <div class="login-box">
        <div class="login-logo">
          <a href="{{asset('AdminLTE-3.0.5/index2.html')}}" style="color:white; font-family: 'Droid Sans';"><b>{{ config('app.name', 'Laravel') }}</b></a>
        </div>
          <div class="card">
              <div class="card-body login-card-body">
                  <center><img src="/img/logo-okp2p.png" alt="User Avatar"  width="260" height="100"></center>
                  <p class="login-box-msg">Sistem Informasi Internal Office OKP2P</p>
                  <form method="POST" action="{{ route('login') }}">
                    @csrf
                      <div class="input-group mb-3">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" id="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                          <input type="hidden" name="status_login" id="status_login" value = "sistem-ops">
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                              </div>
                            </div>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                      </div>
                      <div class="input-group mb-3">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" id="password" value="{{ old('password') }}" required autocomplete="current-password">
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-eye" onClick="showPwd('password', this)"></span>
                              </div>
                          </div>
                          @error('password')
                          <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <div class="row">
                        <div class="col-8">
                          <div class="icheck-primary">
                            <input type="checkbox" value="lsRememberMe" id="rememberMe">
                            <label for="rememberMe">
                              Ingatkan Saya
                            </label>
                          </div>
                        </div>
                        <div class="col-4">
                          <button type="submit" class="btn btn-primary btn-block" style="background-image: linear-gradient(to right top, #c2e59c, #83e1b4, #42d7d4, #24c8ed, #64b3f4);" onclick="remember()">Masuk</button>
                        </div>
                      </div>
                  </form>
                  <div class="button-login-ops-sistem">
                  <p class="mb-2">
                    <a onclick="forgot()" style="color:green">Lupa Kata Sandi Saya</a>
                  </p>
                  <p class="mb-0">
                  <a onclick="block('/register')" class="text-center" style="color:green">Daftar Menjadi Anggota Baru</a>
                </p>
              </div>
          </div>
    </div>
  </body>
</html>
@include('footer.footlogin-ops')