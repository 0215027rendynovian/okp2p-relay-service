<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('img/favicon.ico') }}" rel="icon">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page" style="background-image: linear-gradient(to right top, #c2e59c, #83e1b4, #42d7d4, #24c8ed, #64b3f4);">
<div class="login-box">
  <div class="login-logo">

  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <center><img src="{{'img/logo-okp2p.png'}}" alt="User Avatar"  width="260" height="100"></center>
      <p class="login-box-msg">Pendaftaran Pengguna</p>

       <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
        @csrf


        <div class="input-group mb-3">
          <input type="name" class="form-control" placeholder="Nama Pengguna" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          @error('email')
          <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
          </span>
        @enderror
        </div>

        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
          <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
          </span>
        @enderror
        </div>

        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Kata Sandi" name="password" id="password" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-eye" onClick="showPwd('password', this)"></span>
            </div>
          </div>
          @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>


        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="password_confirmation" name="password_confirmation" id="password_confirmation" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-eye" onClick="showPwdCfm('password_confirmation', this)"></span>
            </div>
          </div>
          @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>

        <div class="input-group mb-3">
        <select class="form-control" id="permission" name="permission" onchange="permissionChange('permission')" required autocomplete="permission">
                <option value="" disabled selected>--Pilih Akses--</option>
                <option value="1">Admin</option>
                <option value="2">Finance</option>
                <option value="3">CSS</option>
                <option value="4">Analyst</option>
                <option value="5">HR</option>
                <option value="6">Super Admin</option>
         </select>


          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-adjust"></span>
            </div>
          </div>
          @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>


        <div class="input-group mb-3">
        <select class="form-control" id="dept_id" name="dept_id" onchange="permissionDep('dept_id')" required autocomplete="dept_id">
                <option value="" disabled selected>--Pilih Departemen--</option>
                <option value="1">OKP2P</option>
                <option value="2">RETAIL</option>
                <option value="3">ASSET</option>
         </select>


          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-adjust"></span>
            </div>
          </div>
          @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>




          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block" style="background-image: linear-gradient(to right top, #c2e59c, #83e1b4, #42d7d4, #24c8ed, #64b3f4);">Daftar</button>
            <a href="javascript:history.go(-1)"  class="btn btn-primary btn-block" style="background-image: linear-gradient(to right top, #c2e59c, #83e1b4, #42d7d4, #24c8ed, #64b3f4);">Kembali</a>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('AdminLTE-3.0.5/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-3.0.5/dist/js/adminlte.min.js')}}"></script>

</body>
</html>

@include('js.nocopy')


<script>

    function showPwd(id, el) {
        let x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
            el.className = 'fa fa-eye-slash showpwd';
        } else {
            x.type = "password";
            el.className = 'fa fa-eye showpwd';
        }
    }

    function showPwdCfm(id, el) {
        let x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
            el.className = 'fa fa-eye-slash showpwd';
        } else {
            x.type = "password";
            el.className = 'fa fa-eye showpwd';
        }
    }
</script>
