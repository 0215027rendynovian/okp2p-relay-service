<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Ubah Passsord</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('img/favicon.ico') }}" rel="icon">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="AdminLTE-3.0.5/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="AdminLTE-3.0.5/plugins/toastr/toastr.min.css">
</head>
<body class="hold-transition login-page" style="background-image: linear-gradient(to right top, #c2e59c, #83e1b4, #42d7d4, #24c8ed, #64b3f4);">
<div class="login-box">
  <div class="login-logo">
    <a href="{{asset('AdminLTE-3.0.5/index2.html')}}" style="color:white; font-family: 'Droid Sans';
"><b>{{ config('app.name', 'Laravel') }}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <center><img src="{{'img/logo-okp2p.png'}}" alt="User Avatar"  width="260" height="100"></center>
      <p class="login-box-msg">Sistem Informasi Internal Office OKP2P</p>

      <form method="POST" action="{{ route('change.password') }}">
        @csrf


        @foreach ($errors->all() as $error)
              <p class="text-danger">{{ $error }}</p>
        @endforeach

        <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Kata Sandi lama" name="current_password" id="current_password" required autocomplete="current_password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-eye" onClick="showCurrentPwd('current_password', this)"></span>
                </div>
            </div>
        </div>

        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Kata Sandi Baru" name="new_password" id="new_password" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-eye" onClick="showNewPwd('new_password', this)"></span>
            </div>
          </div>
        </div>

        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Konfirmasi Kata Sandi Baru" name="new_confirm_password" id="new_confirm_password" required autocomplete="current-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-eye" onClick="showCfmPwd('new_confirm_password', this)"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" value="lsRememberMe" id="rememberMe">
              <label for="remember">
                Ingatkan Saya
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block" style="background-image: linear-gradient(to right top, #c2e59c, #83e1b4, #42d7d4, #24c8ed, #64b3f4);" onclick="remember()">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <br><br>
      <p class="mb-2">
        <a onclick="forgot()" style="color:green">Lupa Kata Sandi Saya</a>
      </p>
      <p class="mb-0">

      <a onclick="block('/register')" class="text-center" style="color:green">Daftar Menjadi Anggota Baru</a>

      <!-- <a href="{{ route('register') }}" class="text-center" style="color:green">Daftar Menjadi Anggota Baru</a>    -->
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('AdminLTE-3.0.5/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-3.0.5/dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="AdminLTE-3.0.5/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="AdminLTE-3.0.5/plugins/toastr/toastr.min.js"></script>

</body>
</html>

@include('js.nocopy')


<script>

    function remember()
    {
        var password = document.getElementById('new_password').value

        if (checkRemember == false) {
            localStorage.setItem('password', null)
        } else {
            localStorage.setItem('password', password)
        }
    }

    async function forgot(){
        // alert("Silahkan hubungi Admin Atau Tim IT");
        Swal.fire({
            icon: 'warning',
            title: 'Oops...',
            text: 'Silahkan hubungi Admin Atau Tim IT!'
        })
    }

    async function block(_this) {

        const { value: password } = await Swal.fire({
            icon: 'question',
            title: 'Masukan Kata Sandi untuk memiliki akses',
            input: 'password',
            inputLabel: 'Password',
            inputPlaceholder: 'Masukan Kata Sandi untuk memiliki akses',
            inputAttributes: {
                maxlength: 20,
                autocapitalize: 'off',
                autocorrect: 'off'
            }
        })

        if (password) {
            if (password=="userganteng1234") {
                window.location.href = _this
            }
            else
            {
                // document.getElementById("msg").value = "test";
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Silahkan hubungi Admin Atau Tim IT!'
                })
            }
        }
    }

    function showCurrentPwd(id, el) {
        let x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
            el.className = 'fa fa-eye-slash showpwd';
        } else {
            x.type = "password";
            el.className = 'fa fa-eye showpwd';
        }
    }

    function showNewPwd(id, el) {
        let x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
            el.className = 'fa fa-eye-slash showpwd';
        } else {
            x.type = "password";
            el.className = 'fa fa-eye showpwd';
        }
    }

    function showCfmPwd(id, el) {
        let x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
            el.className = 'fa fa-eye-slash showpwd';
        } else {
            x.type = "password";
            el.className = 'fa fa-eye showpwd';
        }
    }
</script>
