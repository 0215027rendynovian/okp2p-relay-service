<!DOCTYPE html>
<html lang="en">
@include('header.header')
<style>

.container {
  margin-top: 15px;
}
.container .details-row td {
  padding: 0;
  margin: 0;

}

.details-container {
  width: 100%;
  height: 100%;
  background-color: #FFF;
  padding-top: 5px;
}

.details-table {
  width: 100%;
  background-color: #FFF;
  margin: 5px;
}

.title {
  font-weight: bold;
}

.iconSettings, tr.shown td.details-control:before, td.details-control:before {
  margin-top: 5px;
  margin-bottom: 10px;
  font-size: 12px;
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: "Glyphicons Halflings";
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
}

td.details-control {
  cursor: pointer;
  text-align: left;
}
td.details-control:before {
  content: "+";
}

tr.shown td.details-control:before {
  content: "−";
}

</style>
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

            <div class="content-wrapper">
              <div id="loading"></div>
              <section class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1><b>Pusdafill Log</b></h1>
                    </div>
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
                        <li class="breadcrumb-item active">Pusdafill Log</li>
                      </ol>
                    </div>
                  </div>
                </div>
              </section>
                    <div class ="row">
                      <div class="col-md-12">
                              <!-- Main content -->
                        <section class="content">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-12">

                                <div class="card">
                                    <div class="card-header bg-danger back-ops-okp2p">
                                        <h3 class="card-title"><b>Log Pusdafil</b></h3>
                                    </div>

                                    <div class="container">
                                                      <div class="row">
                                                        <form class="col-md4"></form>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <table id="pusdafill_log" class="table table-striped table-hover datatables">
                                                        <thead>
                                                              <tr>
                                                                  <th></th>
                                                                  <th>CRON DATE</th>
                                                                  <th>CREATED AT</th>
                                                                  <th>UPDATED AT</th>
                                                                  <th style="visibility: none;">REG PENGGUNA</th>
                                                                  <th style="visibility: none;">REG BORROWER</th>
                                                                  <th style="visibility: none;">REG LENDER</th>
                                                                  <th style="visibility: none;">pengajuan_pinjaman</th>
                                                                  <th style="visibility: none;">pengajuan_pemberian_pinjaman</th>
                                                                  <th style="visibility: none;">transaksi_pinjam_meminjam</th>
                                                                  <th style="visibility: none;">pembayaran_pinjaman</th>
                                                              </tr>
                                                            </thead>
	                                                          <tbody>
	                                              </tbody>
                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                       </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </section>
              </div>

            <aside class="control-sidebar control-sidebar-dark">
            </aside>
            @include('footer.tag-footer')
          </div>
    @include('footer.footer')
</body>
</html>

<script type="text/javascript">

      $(document.ready).ready(function(){

              var dTable = $('#pusdafill_log').DataTable({
                        lengthChange: true,
                        fixedHeader: true,
                        searching: true,
                        ordering: true,
                        paging: true,
                        info: true,
                        autoWidth: false,
                        scrollY: 300,
                        scrollX: true,
                        processing: true,
                        ajax: "{{ route('pusdafillog') }}",
                        columns: [
                          {
        className      : 'details-control',
        defaultContent : '',
        orderable      : false
      },

                        { data: "date_cron" },
                        { data: "created_at" },
                        { data: "updated_at" },
                        { data: "reg_pengguna"},
                        { data: "reg_borrower"},
                        { data: "reg_lender"},
                        { data: "pengajuan_pinjaman"},
                        { data: "pengajuan_pemberian_pinjaman"},
                        { data: "transaksi_pinjam_meminjam"},
                        { data: "pembayaran_pinjaman"}
                        ],
                        columnDefs: [
            {
                "targets": [ 4 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 5 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 6 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 9 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
            }
        ]
                   });

                   $('.datatables tbody').on('click', 'td.details-control', function() {
                      var tr  = $(this).closest('tr'),
                      row = dTable.row(tr);

                      //console.log(row.data()['reg_pengguna']);


                  if (row.child.isShown()) {
                    tr.next('tr').removeClass('details-row');
                    row.child.hide();
                    tr.removeClass('shown');
                  }
                  else {
                    row.child(showDetail(row.data())).show();
                    tr.next('tr').addClass('details-row');
                    tr.addClass('shown');
                  }

                });

                function showDetail (data) {
      return '<div class="details-container">'+
          '<table cellpadding="5" cellspacing="0" border="0" class="details-table">'+
              '<tr>'+
                  '<td class="title">REG PENGGUNA:</td>'+
                  '<td>'+data['reg_pengguna']+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">REG BORROWER:</td>'+
                  '<td>'+data['reg_borrower']+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">REG LENDER:</td>'+
                  '<td>'+data['reg_lender']+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">PENGAJUAN PINJAMAN:</td>'+
                  '<td>'+data['pengajuan_pinjaman']+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">PENGAJUAN PEMBERIAN PINJAMAN</td>'+
                  '<td>'+data['pengajuan_pemberian_pinjaman']+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">TRANSAKSI PINJAMAN MEMINJAM:</td>'+
                  '<td>'+data['transaksi_pinjam_meminjam']+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">PEMBAYARAN PINJAMAN:</td>'+
                  '<td>'+data['pembayaran_pinjaman']+'</td>'+
              '</tr>'


          '</table>'+
        '</div>';
  };

      });



</script>
