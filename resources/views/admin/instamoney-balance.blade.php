<!DOCTYPE html>
<html lang="en">
@include('header.header')
<style>
.elementID {
    color: #dc3545;
    /* text-shadow: 1px 1px 1px #ccc; */
    font-size: 1.5em;
}

</style>
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

            <div class="content-wrapper">

              <div id="loading"></div>
              <section class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1><b>INSTAMONEY</b></h1>
                    </div>
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
                        <li class="breadcrumb-item active">Instamoney</li>
                      </ol>
                    </div>
                  </div>
                </div>
              </section>

                <!-- Main content -->
                <section class="content">
                  <div id="download_pdf_data" class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-danger">
                                <div class="card-header back-ops-okp2p">
                                    <h3 class="card-title">SALDO INSTAMONEY</h3>
                                </div>
                                <div class="card-body">
                                    {{-- <div class="col-md-6">
                                        <a type='button' href='#' onclick="SaldoRDL()"  class="btn btn-lg btn-primary"> <span><i class="fas fa-sync-alt"></i>&nbsp; Check Balance</span></a>
                                    </div> --}}
                                    <hr style="margin-top:20px;">
                                    <section class="content">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-lg-4 col-6">
                                                    <div class="small-box bg-success">
                                                        <div class="inner">
                                                            <h3>Rp. {{ number_format($getRdl['account_balance'],0,',','.') }}</h3>

                                                            <p>Saldo RDL</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="ion ion-stats-bars"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-rdl">Lihat Detil <i class="fas fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-6">
                                                    <div class="small-box bg-warning">
                                                        <div class="inner">
                                                            <h3>Rp. {{ number_format($getAccLender['current_balance'],0,',','.') }}</h3>

                                                            <p>Saldo Rekening Pemberi Pinjaman</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="ion ion-stats-bars"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-acc-lender">Lihat Detil <i class="fas fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-6">
                                                    <div class="small-box bg-maroon">
                                                        <div class="inner">
                                                            <h3>Rp. 0</h3>

                                                            <p>ESSCROW FEE OKP2P BALANCE</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="ion ion-stats-bars"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-esscrow-fee">Lihat Detil <i class="fas fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-6">
                                                    <div class="small-box bg-secondary">
                                                        <div class="inner">
                                                            <h3>Rp. 0</h3>
                                                            <p>Jumlah Saldo Pengajuan</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="ion ion-pie-graph"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-balance-amount">Lihat Detil <i class="fas fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-6">
                                                    <div class="small-box bg-info">
                                                        <div class="inner">
                                                            <h3>Rp. 0</h3>

                                                            <p>Jumlah Pengajuan</p>
                                                        </div>
                                                        <div class="icon">
                                                        <i class="ion ion-person-add"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-jml-pengajuan">Lihat Detil <i class="fas fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="card-footer">
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- List Bunga Tetap -->
                    <div class ="row">
                      <div class="col-md-12">
                              <!-- Main content -->
                        <section class="content">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-12">

                                <div class="card">
                                    <div class="card-header bg-danger back-ops-okp2p">
                                        <h3 class="card-title"><b>DAFTAR BUNGA TETAP DAN ADMIN FEE </b></h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-md-6">
                                            <code style="font-size: 30px"><span class="badge badge-success"><i class="fas fa-hand-holding-usd elementID"></i>&nbsp;<b id="bungatetap"></b></span>
                                                <span class="badge badge-warning"><i class="fas fa-dollar-sign elementID"></i>&nbsp;<b id="adminfee"></b></span></code>
                                        </div>
                                        <hr style="margin-top:20px;">
                                        <table id="databungatetapdanadminfee" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>NO_PINJAMAN</th>
                                                    <th>NAMA_PEMINJAM</th>
                                                    <th>BUNGA_TETAP <i class="fas fa-hand-holding-usd elementID"></i></th>
                                                    <th>ADMIN_FEE <i class="fas fa-dollar-sign elementID"></i></th>
                                                    <th>TANGGAL_PINJAM</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header bg-danger back-ops-okp2p">
                                        <h3 class="card-title"><b>LIST REPAYMENT </b></h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-md-6">
                                            <code style="font-size: 30px"><span class="badge badge-info"><i class="fas fa-money-check-alt"></i>&nbsp;<b id="nominalrepayment"></b></span></code>
                                        </div>
                                        <hr style="margin-top:20px;">
                                        <table id="datarepayment" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>VIRTUAL_ACCOUNT_NO</th>
                                                    <th>NOMOR_CUSTOMER</th>
                                                    <th>NAMA_CUSTOMER</th>
                                                    <th>NOMINAL <i class="fas fa-money-check-alt elementID"></i></th>
                                                    <th>rgsTrnDh</th>
                                                    <th>INSTAR_CUST_ID</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </section>
              </div>

            <aside class="control-sidebar control-sidebar-dark">
            </aside>
            @include('footer.tag-footer')
          </div>
    @include('footer.footer')
    @include('modal.instamoney-modal')
</body>
</html>

@include('js.alert-toast')

<script type="text/javascript">
$(function () {
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })


    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

  })

//   DAFTAR BUNGA TETAP DAN ADMIN FEE

    //fungsi untuk filtering data berdasarkan tanggal
    var start_date;
    var end_date;

    // get range date in a month
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    // set value in datapicker
    startDateDefault = mm + '/' + 01 + '/' + yyyy;
    endDateDefault = mm + '/' + dd + '/' + yyyy;
    // initial value for default data
    start_date = yyyy + mm + '01';
    end_date = yyyy + mm + dd;

    var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
        var dateStart = parseDateValue(start_date);
        var dateEnd = parseDateValue(end_date);
        //Kolom tanggal yang akan kita gunakan
        var evalDate= parseDateValue(aData[27]);
            if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
                ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
                ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
                ( dateStart <= evalDate && evalDate <= dateEnd ) )
            {
                return true;
            }
            return false;
    });

    // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
    function parseDateValue(rawDate) {
        var dateArray= rawDate.split("/");
        var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11
        return parsedDate;
    }

    $(document).ready(function(){

        var dTable = $('#databungatetapdanadminfee').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: true,
            ordering: true,
            paging: true,
            info: true,
            autoWidth: true,
            scrollY: 300,
            scrollX: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('get-bunga-admin-fee') }}",
                data: function (data) {
                    data.fromDate = start_date
                    data.toDate = end_date
                },
                delay: 1000,
                timeout: 10000,
                error: handleAjaxError
            },
            drawCallback: function (data) {
                var sumBungaTetap = dTable.ajax.json().sumBungaTetap
                var sumAdminFee = dTable.ajax.json().sumAdminFee
                $('#bungatetap').html(sumBungaTetap);
                $('#adminfee').html(sumAdminFee);
            },
            columns: [
                { data: 'NO_PINJAMAN' },
                { data: 'NAMA_PEMINJAM' },
                { data: 'BUNGA_TETAP' },
                { data: 'ADMIN_FEE' },
                { data: 'TANGGAL_PINJAM' },
            ]
        });

        //menambahkan daterangepicker di dalam datatables
        $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span> </div> <input type="text" class="form-control pull-right" id="datesearch" placeholder="Cari berdasarkan rentang tanggal.."> </div>');

        document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";

        //konfigurasi daterangepicker pada input dengan id datesearch
        $('#datesearch').daterangepicker({
            autoUpdateInput: true
        });

        //change the selected date range of that picker
        $("#datesearch").data('daterangepicker').setStartDate(startDateDefault);
        $("#datesearch").data('daterangepicker').setEndDate(endDateDefault);

        //menangani proses saat apply date range
        $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            start_date=picker.startDate.format('YYYYMMDD');
            end_date=picker.endDate.format('YYYYMMDD');
            $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
            dTable.draw();
        });

        $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            // start_date='';
            // end_date='';
            $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
            dTable.draw();
        });
    });

    // DAFTAR DATA REPAYMENT

    //fungsi untuk filtering data berdasarkan tanggal
    var start_date1;
    var end_date1;

    // initial value for default data
    start_date1 = yyyy + mm + '01';
    end_date1 = yyyy + mm + dd;

    var DateFilterFunction1 = (function (oSettings, aData, iDataIndex) {
        var dateStart = parseDateValue1(start_date1);
        var dateEnd = parseDateValue1(end_date1);
        //Kolom tanggal yang akan kita gunakan
        var evalDate= parseDateValue1(aData[27]);
            if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
                ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
                ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
                ( dateStart <= evalDate && evalDate <= dateEnd ) )
            {
                return true;
            }
            return false;
    });

    // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
    function parseDateValue1(rawDate) {
        var dateArray= rawDate.split("/");
        var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11
        return parsedDate;
    }

    $(document).ready(function(){

        var dTable1 = $('#datarepayment').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: true,
            ordering: true,
            paging: true,
            info: true,
            autoWidth: true,
            scrollY: 300,
            scrollX: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox1'>><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('getrepayment') }}",
                data: function (data) {
                    data.fromDate = start_date1
                    data.toDate = end_date1
                },
                delay: 1000,
                timeout: 10000,
                error: handleAjaxError
            },
            drawCallback: function (data) {
                var totalPay = dTable1.ajax.json().totalPay
                $('#nominalrepayment').html(totalPay);
            },
            columns: [
                { data: 'VIRTUAL_ACCOUNT_NO' },
                { data: 'NOMOR_CUSTOMER' },
                { data: 'NAMA_CUSTOMER' },
                { data: 'NOMINAL' },
                { data: 'rgsTrnDh' },
                { data: 'INSTAR_CUST_ID' },
            ]
        });

        //menambahkan daterangepicker di dalam datatables
        $("div.datesearchbox1").html('<div class="input-group"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span> </div> <input type="text" class="form-control pull-right" id="datesearch1" placeholder="Cari berdasarkan rentang tanggal.."> </div>');

        document.getElementsByClassName("datesearchbox1")[0].style.textAlign = "right";

        //konfigurasi daterangepicker pada input dengan id datesearch
        $('#datesearch1').daterangepicker({
            autoUpdateInput: true
        });

        //change the selected date range of that picker
        $("#datesearch").data('daterangepicker').setStartDate(startDateDefault);
        $("#datesearch").data('daterangepicker').setEndDate(endDateDefault);

        //menangani proses saat apply date range
        $('#datesearch1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            start_date1=picker.startDate.format('YYYYMMDD');
            end_date1=picker.endDate.format('YYYYMMDD');
            $.fn.dataTableExt.afnFiltering.push(DateFilterFunction1);
            dTable1.draw();
        });

        $('#datesearch1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            // start_date1='';
            // end_date1='';
            $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction1, 1));
            dTable1.draw();
        });
    });

</script>
