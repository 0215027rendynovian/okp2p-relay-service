<table>
    <thead>
    <tr>
        <th>NOMOR PINJAMAN</th>
        <th>NAMA PEMINJAM</th>
        <th>STATUS CODE</th>
        <th>STATUS NAME</th>
        <th>TANGGAL PERSETUJUAN</th>
        <th>DENDA</th>
        <th>BUNGA</th>
        <th>POKOK</th>
    </tr>
    </thead>
    <tbody>
    @foreach($datadenda as $denda)
        <tr>
            <td>{{ $denda->NoPinjaman }}</td>
            <td>{{ $denda->NamaPinjaman }}</td>
            <td>{{ $denda->stsCd }}</td>
            <td>{{ $denda->Status }}</td>
            <td>{{ $denda->TanggalPersetujuan }}</td>
            <td>RP {{ number_format($denda->Denda, 0,',','.') }}</td>
            <td>RP {{ number_format($denda->BungaTetap, 0,',','.') }}</td>
            <td>RP {{ number_format($denda->Pokok, 0,',','.') }}</td>
       </tr>
    @endforeach
    </tbody>
</table>
