<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pefindo</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Pefindo</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
          <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                    <a type='button' href="#" class="btn btn-lg btn-primary" onclick="exportCsv('/exportsubjectrelation')"> <span><i class="fas fa-file-csv"></i>  Download CSV</span></a>
                {{-- <a type='button' href='#' onclick="PrintPreview()"  class="btn btn-lg btn-danger"> <span><i class="fas fa-file"></i>  Pratinjau</span></a>
                <a type='button' href='#' onclick="submit(2)"  class="btn btn-lg btn-danger"> <span><i class="fas fa-database"></i>  Cek Log</span></a>
                <a type='button' href='#' onclick="ExcelDownload(2)"  class="btn btn-lg btn-danger"> <span><i class="fas fa-file-excel"></i> Unduh Excel</span></a> --}}
                </div>
                <div class="card-footer">
                  <!-- <a type='button' href='#' onclick="downloadpdf()" class="btn btn-success">Download Pdf</a> -->
                </div>
              </form>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>DAFTAR DATA INDIVIDUAL</b></h3>
                        </div>

                        <div class="card-body">
                                <table id="datapefindo" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Customer_Code_Of_Primary_Subject</th>
                                            <th>Relation_Type</th>
                                            <th>Customer_Code_Of_Secondary_Subject</th>
                                            <th>Subject_Type</th>
                                            <th>NPWP</th>
                                            <th>KTP</th>
                                            <th>Full_Name</th>
                                            <th>Gender</th>
                                            <th>Parts_Of_Guranteed</th>
                                            <th>Ownership_Share</th>
                                            <th>Category</th>
                                            <th>Street</th>
                                            <th>City</th>
                                            <th>District</th>
                                            <th>Parish</th>
                                            <th>Adress_Line</th>
                                        </tr>
                                    </thead>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<<script type="text/javascript">
    //fungsi untuk filtering data berdasarkan tanggal
    var start_date;
    var end_date;

    // get range date in a month
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    // set value in datapicker
    startDateDefault = mm + '/' + 01 + '/' + yyyy;
    endDateDefault = mm + '/' + dd + '/' + yyyy;
    // initial value for default data
    start_date = yyyy + mm + '01';
    end_date = yyyy + mm + dd;

    var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
        var dateStart = parseDateValue(start_date);
        var dateEnd = parseDateValue(end_date);
        //Kolom tanggal yang akan kita gunakan
        var evalDate= parseDateValue(aData[10]);
            if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
                ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
                ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
                ( dateStart <= evalDate && evalDate <= dateEnd ) )
            {
                return true;
            }
            return false;
    });

    // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
    function parseDateValue(rawDate) {
        var dateArray= rawDate.split("/");
        var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11
        return parsedDate;
    }

    $(document).ready(function(){

        var dTable = $('#datapefindo').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: true,
            ordering: true,
            paging: true,
            info: true,
            autoWidth: true,
            scrollY: 300,
            scrollX: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('getsubjectrelation') }}",
                data: function (data) {
                    data.fromDate = start_date
                    data.toDate = end_date
                },
                delay: 1000,
                timeout: 10000,
                error: handleAjaxError
            },
            columns: [
                { data: 'Customer_Code_Of_Primary_Subject' },
                { data: 'Relation_Type' },
                { data: 'Customer_Code_Of_Secondary_Subject' },
                { data: 'Subject_Type' },
                { data: 'NPWP' },
                { data: 'KTP' },
                { data: 'Full_Name' },
                { data: 'Gender' },
                { data: 'Parts_Of_Guranteed' },
                { data: 'Ownership_Share' },
                { data: 'Category' },
                { data: 'Street' },
                { data: 'City' },
                { data: 'District' },
                { data: 'Parish' },
                { data: 'Adress_Line' },
            ]
        });

        //menambahkan daterangepicker di dalam datatables
        $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span> </div> <input type="text" class="form-control pull-right" id="datesearch" placeholder="Cari berdasarkan rentang tanggal.."> </div>');

        document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";

        //konfigurasi daterangepicker pada input dengan id datesearch
        $('#datesearch').daterangepicker({
            autoUpdateInput: true
        });

        //change the selected date range of that picker
        $("#datesearch").data('daterangepicker').setStartDate(startDateDefault);
        $("#datesearch").data('daterangepicker').setEndDate(endDateDefault);

        //menangani proses saat apply date range
        $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            start_date=picker.startDate.format('YYYYMMDD');
            end_date=picker.endDate.format('YYYYMMDD');
            $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
            dTable.draw();
        });

        $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            // start_date='';
            // end_date='';
            $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
            dTable.draw();
        });
    });

    function exportCsv(_this) {
        if (typeof start_date === 'undefined' || start_date === null) {
            window.location.href = _this
        }else{
            window.location.href = _this+'?fromDate='+start_date+'&toDate='+end_date;
        }
    }

    function ExcelDownload(){

    }

    function PrintPreview(){

    }
</script>
