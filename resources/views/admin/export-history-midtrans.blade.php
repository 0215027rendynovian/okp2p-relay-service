<table>
    <tbody>
        <tr>
            <td>Departement : </td>
            <td>@foreach ($departement as $dept)
                {{ $dept->description }}<br/>
            @endforeach</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td>Periode :</td>
            <td>Tanggal Awal : {{ $startDate }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Akhir : {{ $endDate }}</td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>COUNT_HIT</th>
    </tr>
    </thead>
    <tbody>
    @foreach($midtranshistory as $midtrans)
        <tr>
            <td>{{ $midtrans->id }}</td>
            <td>{{ $midtrans->name }}</td>
            <td>{{ $midtrans->COUNT_HIT ?? 0 }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
