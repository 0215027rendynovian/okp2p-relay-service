<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Master User Privilege</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Master User Privilege</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                    <a type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="create-new-privilege"> <span><i class="fas fa-university"></i>  Tambah Master User Privilege</span></a>
                    <a id="toolhapus" type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="hapus-privilege" onclick="deleteprivilege($('#id').val())"> <span><i class="fas fa-trash-alt"></i>  Hapus Master User Privilege</span></a>
                </div>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tableprivilege" class="col-md-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Master User Privilege </b></h3>
                        </div>

                        <div class="card-body">
                                <table id="listprivilege" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Code</th>
                                            <th>Role ID</th>
                                            <th>Parent</th>
                                            <th>Child</th>
                                            <th>Status</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listprivilege as $privilege)
                                        <tr>
                                            <td>{{ $privilege->id }}</td>
                                            <td>{{ $privilege->code }}</td>
                                            <td>{{ $privilege->role_id }}</td>
                                            <td>{{ $privilege->parent }}</td>
                                            <td>{{ $privilege->child }}</td>
                                            <td>{{ $privilege->status }}</td>
                                            <td>{{ $privilege->description }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                <!-- left column -->
                <div class="col-md-4" id="detailprivilege">
                    <div class="card card-danger">
                    <div class="card-header back-ops-okp2p">
                        <h3 class="card-title">Detail Master User Privilege</h3>
                    </div>
                    <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form-horizontal" method="POST" action="{{ route('privilege-edit') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" name="id" id="id">

                                <div class="form-group">
                                    <label for="code" class="col-sm-12 control-label">Code</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('code') is-invalid @enderror" id="code" name="code" placeholder="Enter Code" value="" maxlength="300" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="role_id" class="col-sm-12 control-label">Role ID</label>
                                    <div class="col-sm-12">
                                        <select id="role_id" name="role_id" class="form-control" required="">
                                            <option value="">--- Pilih Role ---</option>
                                            @foreach ($listRoles as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>

                                        @error('role_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="parent" class="col-sm-12 control-label">Nama Menu</label>
                                    <div class="col-sm-12">
                                        <select id="parent" name="parent" class="form-control" required="">
                                            <option value="">--- Pilih Menu ---</option>
                                            @foreach ($listMenu as $k => $v)
                                                <option value="{{ $k }}">{{ $v }}</option>
                                            @endforeach
                                        </select>

                                        @error('parent')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Display Menu</label>
                                    <div class="col-sm-12">
                                        <input type="text" readonly class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter name" value="" maxlength="300" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="url" class="col-sm-12 control-label">URL</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('url') is-invalid @enderror" id="url" name="url" placeholder="Enter URL" value="" maxlength="300" required="">
                                    </div>
                                </div> --}}

                                <div class="form-group">
                                    <label for="child" class="col-sm-12 control-label">Child Menu</label>
                                    <div class="col-sm-12">
                                        <select id="child" name="child" class="form-control" required="">
                                            <option value="">--- Pilih Child Menu ---</option>
                                            @foreach ($listChildMenu as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>

                                        @error('child')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="col-sm-12 control-label">Description</label>
                                    <div class="col-sm-12">
                                        <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Enter Description" value="" maxlength="300"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" value="create">Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--/.col (left) -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    {{-- MODALS --}}
    <div class="modal fade" id="ajax-privilege-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="privilegeCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <form id="privilegeForm" name="privilegeForm" class="form-horizontal" method="POST" action="{{ route('privilege-store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="code" class="col-sm-12 control-label">Code</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('code') autofocus is-invalid @enderror" name="code" placeholder="Enter Code" value="{{ old('code') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role_id" class="col-sm-12 control-label">Role ID</label>
                            <div class="col-sm-12">
                                <select name="role_id" class="form-control" required="">
                                    <option value="">--- Pilih Role ---</option>
                                    @foreach ($listRoles as $kr => $vr)
                                        <option value="{{ $kr }}">{{ $vr }}</option>
                                    @endforeach
                                </select>

                                @error('role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parent" class="col-sm-12 control-label">Nama Menu</label>
                            <div class="col-sm-12">
                                <select name="parent" class="form-control" required="">
                                    <option value="">--- Pilih Menu ---</option>
                                    @foreach ($listMenu as $km => $vm)
                                        <option value="{{ $km }}">{{ $vm }}</option>
                                    @endforeach
                                </select>

                                @error('parent')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="child" class="col-sm-12 control-label">Child</label>
                            <div class="col-sm-12">
                                <select name="child" class="form-control" required="">
                                    <option value="">--- Pilih Child Menu ---</option>
                                    @foreach ($listChildMenu as $kr => $vr)
                                        <option value="{{ $kr }}">{{ $vr }}</option>
                                    @endforeach
                                </select>

                                @error('child')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-12 control-label">Description</label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Enter Description"  maxlength="300" required="">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                         <button type="submit" class="btn btn-primary" id="btn-save" value="create">Tambah
                         </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script>
    var table

    $(document).ready( function () {
        $('#detailprivilege').hide()
        $('#toolhapus').hide()
        document.getElementById("tableprivilege").className = "col-md-12";

        table = $('#listprivilege').DataTable();

        $('#listprivilege tbody').on( 'click', 'tr', function () {

            var datauser = table.row( this ).data()
            $('#detailprivilege').show()
            $('#toolhapus').show()
            document.getElementById("tableprivilege").className = "col-md-8";
            $('#id').val(datauser[0])
            $('#code').val(datauser[1])
            $('#role_id').val(datauser[2])
            $('#parent').val(datauser[3])
            $('#child').val(datauser[4])
            $('#description').val(datauser[5])

        } );

        $('#listprivilege tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('row_selected') ) {
                $(this).removeClass('row_selected')

                $('#detailprivilege').hide()
                $('#toolhapus').hide()
                document.getElementById("tableprivilege").className = "col-md-12";
                $('#id').val('')
                $('#code').val('')
                $('#role_id').val('')
                $('#parent').val('')
                $('#child').val('')
                $('#description').val('')
            }
            else {
                table.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected')
            }
        } );

        /*  When user click add user button */
        $('#create-new-privilege').click(function () {
            $('#btn-save').val("create-privilege")
            $('#id').val('')
            $('#privilegeForm').trigger("reset")
            $('#privilegeCrudModal').html("Tambah Master User Privilege")
            $('#ajax-privilege-modal').modal('show')
        });
    });

    function deleteprivilege(id)
    {
        var code = $('#code').val()
        var role_id = $('#role_id').val()
        Swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            html:
            "Code : <b>"+code+"</b> </br> " +
            "Role ID : <b>"+role_id+"</b> </br>  </br> " +
            "Anda tidak akan dapat mengembalikan ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "privilege/delete/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Master User Privilege berhasil terhapus',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal menghapus Master User Privilege',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

</script>
