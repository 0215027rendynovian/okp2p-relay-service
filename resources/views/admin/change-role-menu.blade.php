<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Privilege Menu untuk role <b>{{ Str::upper($selectedRole['name']) }}</b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Privilege Menu </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tableroles" class="col-md-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Privilege Menu </b></h3>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" method="POST" action="{{ route('config-role-update', $idrole) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <nav class="navbar navbar-light bg-light" style="margin-bottom: 20px">
                                    <div class="container-fluid">
                                        <a href="{{ route('config-role') }}" type="button" class="btn btn-lg btn-warning"><span><i class="fas fa-undo-alt"></i>&nbsp;Kembali</span></a>
                                        <button type="submit" class="btn btn-lg btn-primary" value="create"><span><i class="far fa-save"></i>&nbsp;Simpan Perubahan</span></button>
                                    </div>
                                </nav>
                                <table id="listroles" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 2%">No</th>
                                            <th>Nama Menu</th>
                                            <th>Akses</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($configRole as $confr)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $confr->name_master }} {{ $confr->name_child !== null ? ' - ' : '' }} {{ $confr->name_child }}</td>
                                            <td>
                                                <input type="checkbox" name="checkconfigrole[]" id="config-role-{{ $confr->code_master }}-{{ $confr->code_child }}"
                                                    @foreach ($privilege as $priv)
                                                        @if ($priv->role_id == $idrole)
                                                            @if ($confr->code_master == $priv->parent && $confr->code_child == $priv->child)
                                                                {{ 'checked' }}
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                data-bootstrap-switch data-off-color="danger" value="{{ $idrole }}+{{ $confr->code_master }}+{{ $confr->code_child }}+{{ $idrole }}{{ $confr->code_master }}{{ $confr->code_child }}+{{ $confr->isParent }}" data-on-color="success">
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                <!--/.col (left) -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script>
    var table

    $(document).ready( function () {

        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });

        // table = $('#listroles').DataTable();

    });

</script>
