<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

    @include('navbar.navbar')
    @include('sidebar.sidebar')

    <div class="content-wrapper">

        <div id="loading"></div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Config Roles Menu</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
                            <li class="breadcrumb-item active">Config Roles Menu</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <form class="form-horizontal" method="POST" action="{{ route('config-role-reset') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- Main content -->
            <section class="content">
                <div id="download_pdf_data" class="container-fluid">
                    <div class="row">
                        <!-- right column -->
                        <div class="col-md-12">
                        <div class="card card-danger">
                            <div class="card-header back-ops-okp2p">
                            <h3 class="card-title">Kotak Informasi</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <a href="{{ route('config-role') }}" type="button" class="btn btn-lg btn-warning"> <span><i class="fas fa-undo-alt"></i>&nbsp;Cancel</span></a>
                                <button type='submit' value="create" class="btn btn-lg btn-primary"> <span><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</span></button>
                            </div>
                        </div>
                        </div>

                        <!--/.col (right) -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-danger">
                                <div class="card-header bg-danger back-ops-okp2p">
                                    <h3 class="card-title"><b>Config Roles Menu </b></h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <div class="col-md-12">
                                        <table id="listconfigrolemenu" class="table table-bordered table-striped table-hover text-nowrap">
                                            <thead>
                                                <tr>
                                                    <th>no</th>
                                                    <th>Role/Menu</th>
                                                    @foreach ($listroles as $role)
                                                        <th>{{ $role->name }}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <form id="configroleForm" name="configroleForm" class="form-horizontal" method="POST" action="{{ route('privilege-store') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                @foreach ($configRole as $conro)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td><i class="fas fa-lg {{ $conro->icon }}">&nbsp;</i>{{ $conro->name_master }}&nbsp; {{ $conro->name_child !== null ? ' - ' : '' }}&nbsp;{{ $conro->name_child }}</td>
                                                        @foreach ($listroles as $ro)
                                                            <td>
                                                                <input type="checkbox" name="checkconfigrole[]" id="config-role-{{ $ro->id }}-{{ $conro->code_master }}-{{ $conro->code_child }}"
                                                                    @foreach ($privilege as $priv)
                                                                        @if ($ro->code == $priv->role_id)
                                                                            @if ($conro->code_master == $priv->parent && $conro->code_child == $priv->child)
                                                                                {{ 'checked' }}
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                data-bootstrap-switch data-off-color="danger" value="{{ $ro->code }}+{{ $conro->code_master }}+{{ $conro->code_child }}+{{ $ro->code }}{{ $conro->code_master }}{{ $conro->code_child }}+{{ $conro->isParent }}" data-on-color="success">
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                                </form>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <!-- /.content -->

        </form>

    </div>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script>
    var table

    $(document).ready( function () {
        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });
    });

</script>
