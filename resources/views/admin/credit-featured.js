
function submit(){



    var logs;
  
  var myTable = $('#my_logs').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "data": [],
          "columns": [
            {
              "title": "creditscore",
              "data": "creditscore"
          },
          {
              "title": "isfacebook",
              "data": "isfacebook"
          }, 
          {
              "title": "iswhatsapp",
              "data": "iswhatsapp"
          }, {
              "title": "whatsapp_avatar",
              "data": "whatsapp_avatar"
          }, {
              "title": "whatsapp_company_account",
              "data": "whatsapp_company_account"
          }, {
              "title": "whatsapp_signature",
              "data": "whatsapp_signature"
          }, {
              "title": "whatsapp_updatestatus_time",
              "data": "whatsapp_updatestatus_time"
          },
          {
              "title": "multiphone_idinfo",
              "data": "multiphone_idinfo"
          },
          {
              "title": "multiphone_phoneinfo_id",
              "data": "multiphone_phoneinfo_id"
          },
          {
              "title": "multiphone_phoneinfo_id_phone",
              "data": "multiphone_phoneinfo_id_phone"
          },
          {
              "title": "multiphone_status",
              "data": "multiphone_status"
          },
          {
              "title": "phoneage",
              "data": "phoneage"
          },
  
          {
              "title": "phoneage_status",
              "data": "phoneage_status"
          },
          {
              "title": "phonealive_id_num",
              "data": "phonealive_id_num"
          },
          {
              "title": "phonealive_phone_num",
              "data": "phonealive_phone_num"
          },
          {
              "title": "phonealive_status",
              "data": "phonealive_status"
          },
  
          {
              "title": "phoneinquiries_14d",
              "data": "phoneinquiries_14d"
          },
  
         {
              "title": "phoneinquiries_180d",
              "data": "phoneinquiries_180d"
          },
  
          {
              "title": "phoneinquiries_21d",
              "data": "phoneinquiries_21d"
          },
  
          {
              "title": "phoneinquiries_30d",
              "data": "phoneinquiries_30d"
          },
  
  
          {
              "title": "phoneinquiries_360d",
              "data": "phoneinquiries_360d"
          },
  
  
          {
              "title": "phoneinquiries_3d",
              "data": "phoneinquiries_3d"
          },
  
          {
              "title": "phoneinquiries_60d",
              "data": "phoneinquiries_60d"
          },
  
  
          {
              "title": "phoneinquiries_90d",
              "data": "phoneinquiries_90d"
          },
  
  
          {
              "title": "phoneinquiries_status",
              "data": "phoneinquiries_status"
          },
  
  
          {
              "title": "phoneinquiries_total",
              "data": "phoneinquiries_total"
          },
  
  
          {
              "title": "phoneinquiries_3d",
              "data": "phoneinquiries_3d"
          },
  
  
          {
              "title": "phoneinquiriesuname_14d",
              "data": "phoneinquiriesuname_14d"
          },
  
          {
              "title": "phoneinquiriesuname_180d",
              "data": "phoneinquiriesuname_180d"
          },
  
          {
              "title": "phoneinquiriesuname_21d",
              "data": "phoneinquiriesuname_21d"
          },
  
          {
              "title": "phoneinquiriesuname_30d",
              "data": "phoneinquiriesuname_30d"
          },
  
          {
              "title": "phoneinquiriesuname_360d",
              "data": "phoneinquiriesuname_360d"
          },
  
          {
              "title": "phoneinquiriesuname_3d",
              "data": "phoneinquiriesuname_3d"
          },
  
          {
              "title": "phoneinquiriesuname_21d",
              "data": "phoneinquiriesuname_21d"
          },
  
          {
              "title": "phoneinquiriesuname_60d",
              "data": "phoneinquiriesuname_60d"
          },
  
          {
              "title": "phoneinquiriesuname_7d",
              "data": "phoneinquiriesuname_7d"
          },
  
          {
              "title": "phoneinquiriesuname_90d",
              "data": "phoneinquiriesuname_90d"
          },
          {
              "title": "phoneinquiriesuname_status",
              "data": "phoneinquiriesuname_status"
          },
          {
              "title": "phoneinquiriesuname_total",
              "data": "phoneinquiriesuname_total"
          },
  
          {
              "title": "phonescore",
              "data": "phonescore"
          },
          {
              "title": "phonescore_status",
              "data": "phonescore_status"
          },
          {
              "title": "phoneverify",
              "data": "phoneverify"
          }
          ]
      });
  
  
  
  
  // table 2
  
  var myTable = $('#my_logs').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "data": [],
          "columns": [
            {
              "title": "A_II_14d",
              "data": "A_II_14d"
          },
          {
              "title": "A_II_180d",
              "data": "A_II_180d"
          }, 
          {
              "title": "A_II_21d",
              "data": "A_II_21d"
          }, {
              "title": "A_II_30d",
              "data": "A_II_30d"
          }, {
              "title": "A_II_360d",
              "data": "A_II_360d"
          }, {
              "title": "A_II_3d",
              "data": "A_II_3d"
          }, {
              "title": "A_II_60d",
              "data": "A_II_60d"
          },
          {
              "title": "A_II_7d",
              "data": "A_II_7d"
          },
          {
              "title": "A_II_90d",
              "data": "A_II_90d"
          },
          {
              "title": "A_PI_14d",
              "data": "A_PI_14d"
          },
          {
              "title": "A_PI_180d",
              "data": "A_PI_180d"
          },
          {
              "title": "A_PI_21d",
              "data": "A_PI_21d"
          },
  
          {
              "title": "A_PI_30d",
              "data": "A_PI_30d"
          },
          {
              "title": "A_PI_360d",
              "data": "A_PI_360d"
          },
          {
              "title": "A_PI_3d",
              "data": "A_PI_3d"
          },
          {
              "title": "A_PI_60d",
              "data": "A_PI_60d"
          },
  
          {
              "title": "A_PI_7d",
              "data": "A_PI_7d"
          },
  
         {
              "title": "A_PI_90d",
              "data": "A_PI_90d"
          },
  
          {
              "title": "B_II_14d",
              "data": "B_II_14d"
          },
  
          {
              "title": "B_II_180d",
              "data": "B_II_180d"
          },
  
  
          {
              "title": "B_II_21d",
              "data": "B_II_21d"
          },
  
  
          {
              "title": "B_II_30d",
              "data": "B_II_30d"
          },
  
          {
              "title": "B_II_360d",
              "data": "B_II_360d"
          },
  
  
          {
              "title": "B_II_3d",
              "data": "B_II_3d"
          },
  
  
          {
              "title": "B_II_60d",
              "data": "B_II_60d"
          },
  
  
          {
              "title": "B_II_7d",
              "data": "B_II_7d"
          },
  
  
          {
              "title": "B_II_90d",
              "data": "B_II_90d"
          },
  
  
          {
              "title": "B_PI_14d",
              "data": "B_PI_14d"
          },
  
          {
              "title": "B_PI_180d",
              "data": "B_PI_180d"
          },
  
          {
              "title": "B_PI_21d",
              "data": "B_PI_21d"
          },
  
          {
              "title": "B_PI_30d",
              "data": "B_PI_30d"
          },
  
          {
              "title": "B_PI_360d",
              "data": "B_PI_360d"
          },
  
          {
              "title": "B_PI_3d",
              "data": "B_PI_3d"
          },
  
          {
              "title": "B_PI_60d",
              "data": "B_PI_60d"
          },
  
          {
              "title": "B_PI_7d",
              "data": "B_PI_7d"
          },
  
          {
              "title": "B_PI_90d",
              "data": "B_PI_90d"
          },
  
          {
              "title": "C_II_14d",
              "data": "C_II_14d"
          },
          {
              "title": "C_II_180d",
              "data": "C_II_180d"
          },
          {
              "title": "C_II_21d",
              "data": "C_II_21d"
          },
  
          {
              "title": "C_II_30d",
              "data": "C_II_30d"
          },
          {
              "title": "C_II_360d",
              "data": "C_II_360d"
          },
          {
              "title": "C_II_3d",
              "data": "C_II_3d"
          },
  
  
  
          {
              "title": "C_II_60d",
              "data": "C_II_60d"
          },
          {
              "title": "C_II_7d",
              "data": "C_II_7d"
          },
          {
              "title": "C_II_90d",
              "data": "C_II_90d"
          },
          {
              "title": "C_PI_14d",
              "data": "C_PI_14d"
          },
          {
              "title": "C_PI_180d",
              "data": "C_PI_180d"
          },
          {
              "title": "C_PI_21d",
              "data": "C_PI_21d"
          },
          {
              "title": "C_PI_30d",
              "data": "C_PI_30d"
          },
          {
              "title": "C_PI_3d",
              "data": "C_PI_3d"
          },
          {
              "title": "C_PI_60d",
              "data": "C_PI_60d"
          },
          {
              "title": "C_PI_7d",
              "data": "C_PI_7d"
          },
          {
              "title": "C_PI_90d",
              "data": "C_PI_90d"
          },
          {
              "title": "II_v4_feature_status",
              "data": "II_v4_feature_status"
          },
          {
              "title": "pI_v4_feature_status",
              "data": "pI_v4_feature_status"
          },
          {
              "title": "Identity_address",
              "data": "Identity_address"
          },
          {
              "title": "Identity_date_of_birth",
              "data": "Identity_date_of_birth"
          },
          {
              "title": "Identity_district",
              "data": "Identity_district"
          },
          {
              "title": "Identity_gender",
              "data": "Identity_gender"
          },
          {
              "title": "Identity_city",
              "data": "Identity_city"
          },
          {
              "title": "Identity_marital_status",
              "data": "Identity_marital_status"
          },
          {
              "title": "Identity_match",
              "data": "Identity_match"
          },
          {
              "title": "Identity_name",
              "data": "Identity_name"
          },
          {
              "title": "Identity_nationnality",
              "data": "Identity_nationnality"
          },
          {
              "title": "Identity_place_of_birth",
              "data": "Identity_place_of_birth"
          }, 
  
  
          {
              "title": "Identity_province",
              "data": "Identity_province"
          },  
          {
              "title": "Identity_religion",
              "data": "Identity_religion"
          },   
          {
              "title": "Identity_rt",
              "data": "Identity_rt"
          },  
          {
              "title": "Identity_rw",
              "data": "Identity_rw"
          },  
          {
              "title": "Identity_status",
              "data": "Identity_status"
          },  
          {
              "title": "Identity_village",
              "data": "Identity_village"
          },  
          {
              "title": "Identity_work",
              "data": "Identity_work"
          },  
          {
              "title": "Idinquiries_14d",
              "data": "Idinquiries_14d"
          },  
          {
              "title": "Idinquiries_180d",
              "data": "Idinquiries_180d"
          },  
          {
              "title": "Idinquiries_21d",
              "data": "Idinquiries_21d"
          },  
          {
              "title": "Idinquiries_30d",
              "data": "Idinquiries_30d"
          },  
          {
              "title": "Idinquiries_360d",
              "data": "Idinquiries_360d"
          },  
          {
              "title": "Idinquiries_3d",
              "data": "Idinquiries_3d"
          },  
          {
              "title": "Idinquiries_60d",
              "data": "Idinquiries_60d"
          },  
          {
              "title": "Idinquiries_7d",
              "data": "Idinquiries_7d"
          },  
          {
              "title": "Idinquiries_90d",
              "data": "Idinquiries_90d"
          },  
          {
              "title": "Idinquiries_status",
              "data": "Idinquiries_status"
          },  
          {
              "title": "Idinquiries_total",
              "data": "Idinquiries_total"
          },  
          {
              "title": "Idinquiriesuname_14",
              "data": "Idinquiriesuname_14"
          },  
          {
              "title": "Idinquiriesuname_21d",
              "data": "Idinquiriesuname_21d"
          },  
          {
              "title": "Idinquiriesuname_30d",
              "data": "Idinquiriesuname_30d"
          },  
          {
              "title": "Idinquiriesuname_360d",
              "data": "Idinquiriesuname_360d"
          },  
          {
              "title": "Idinquiriesuname_3d",
              "data": "Idinquiriesuname_3d"
          },  
  
  
  
          {
              "title": "Idinquiriesuname_60d",
              "data": "Idinquiriesuname_60d"
          },  
          {
              "title": "Idinquiriesuname_7d",
              "data": "Idinquiriesuname_7d"
          },  
          {
              "title": "Idinquiriesuname_90d",
              "data": "Idinquiriesuname_90d"
          },  
          {
              "title": "Idinquiriesuname_status",
              "data": "Idinquiriesuname_status"
          },  
          {
              "title": "Idinquiriesuname_total",
              "data": "Idinquiriesuname_total"
          },  
          {
              "title": "Idinquiriesuname_3d",
              "data": "Idinquiriesuname_3d"
          },  
          {
              "title": "preference_bank_180d",
              "data": "preference_bank_180d"
          },  
          {
              "title": "preference_bank_270d",
              "data": "preference_bank_270d"
          },  
          {
              "title": "preference_bank_60d",
              "data": "preference_bank_60d"
          },  
          {
              "title": "preference_bank_90d",
              "data": "preference_bank_90d"
          },  
          {
              "title": "preference_ecommerce_180d",
              "data": "preference_ecommerce_180d"
          },  
          {
              "title": "preference_ecommerce_270d",
              "data": "preference_ecommerce_270d"
          },  
          {
              "title": "preference_ecommerce_60d",
              "data": "preference_ecommerce_60d"
          },          
          {
              "title": "preference_ecommerce_90d",
              "data": "preference_ecommerce_90d"
          },  
  
  
          {
              "title": "preference_game_180d",
              "data": "preference_game_180d"
          },  
          {
              "title": "preference_game_270d",
              "data": "preference_game_270d"
          },  
          {
              "title": "preference_game_60d",
              "data": "preference_game_60d"
          },  
          {
              "title": "preference_game_90d",
              "data": "preference_game_90d"
          },  
          {
              "title": "preference_lifestyle_180d",
              "data": "preference_lifestyle_180d"
          },  
          {
              "title": "preference_lifestyle_270d",
              "data": "preference_lifestyle_270d"
          }, 
          {
              "title": "preference_lifestyle_60",
              "data": "preference_lifestyle_60"
          },  
          {
              "title": "preference_lifestyle_90d",
              "data": "preference_lifestyle_90d"
          },  
          {
              "title": "preference_status",
              "data": "preference_status"
          } 
          ]
      });
  
  
      var myTable = $('#my_logs3').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "data": [],
          "columns": [
            {
              "title": "Top_status",
              "data": "Top_status"
          },
          {
              "title": "Top_0_180_avg",
              "data": "Top_0_180_avg"
          }, 
          {
              "title": "Top_0_180_max",
              "data": "Top_0_180_max"
          }, {
              "title": "Top_0_180_min",
              "data": "Top_0_180_min"
          }, {
              "title": "Top_0_180_times",
              "data": "Top_0_180_times"
          }, {
              "title": "Top_0_30_avg",
              "data": "Top_0_30_avg"
          }, {
              "title": "Top_0_30_max",
              "data": "Top_0_30_max"
          },
          {
              "title": "Top_0_30_min",
              "data": "Top_0_30_min"
          },
          {
              "title": "Top_0_30_times",
              "data": "Top_0_30_times"
          },
          {
              "title": "Top_0_360_avg",
              "data": "Top_0_360_avg"
          },
          {
              "title": "Top_0_360_max",
              "data": "Top_0_360_max"
          },
          {
              "title": "Top_0_360_min",
              "data": "Top_0_360_min"
          },
  
          {
              "title": "Top_0_360_times",
              "data": "Top_0_360_times"
          },
          {
              "title": "Top_0_60_avg",
              "data": "Top_0_60_avg"
          },
          {
              "title": "Top_0_60_max",
              "data": "Top_0_60_max"
          },
          {
              "title": "Top_0_60_min",
              "data": "Top_0_60_min"
          },
  
          {
              "title": "Top_0_60_times",
              "data": "Top_0_60_times"
          },
  
         {
              "title": "Top_0_90_avg",
              "data": "Top_0_90_avg"
          },
  
          {
              "title": "Top_0_90_max",
              "data": "Top_0_90_max"
          },
  
          {
              "title": "Top_0_90_min",
              "data": "Top_0_90_min"
          },
  
  
          {
              "title": "Top_0_90_times",
              "data": "Top_0_90_times"
          },
  
  
          {
              "title": "Top_180_360_avg",
              "data": "Top_180_360_avg"
          },
  
          {
              "title": "Top_180_360_max",
              "data": "Top_180_360_max"
          },
  
  
          {
              "title": "Top_180_360_min",
              "data": "Top_180_360_min"
          },
  
  
          {
              "title": "Top_180_360_times",
              "data": "Top_180_360_times"
          },
  
  
          {
              "title": "Top_30_60_avg",
              "data": "Top_30_60_avg"
          },
  
  
          {
              "title": "Top_30_60_max",
              "data": "Top_30_60_max"
          },
  
  
          {
              "title": "Top_30_60_min",
              "data": "Top_30_60_min"
          },
  
          {
              "title": "Top_30_60_times",
              "data": "Top_30_60_times"
          },
  
          {
              "title": "Top_360_720_avg",
              "data": "Top_360_720_avg"
          },
  
          {
              "title": "Top_360_720_max",
              "data": "Top_360_720_max"
          },
  
          {
              "title": "Top_360_720_min",
              "data": "Top_360_720_min"
          },
  
          {
              "title": "Top_360_720_times",
              "data": "Top_360_720_times"
          },
  
          {
              "title": "Top_60_90_avg",
              "data": "Top_60_90_avg"
          },
  
          {
              "title": "Top_60_90_max",
              "data": "Top_60_90_max"
          },
  
          {
              "title": "Top_60_90_min",
              "data": "Top_60_90_min"
          },
  
          {
              "title": "Top_60_90_times",
              "data": "Top_60_90_times"
          },
          {
              "title": "Top_90_180_avg",
              "data": "Top_90_180_avg"
          },
          {
              "title": "Top_90_180_max",
              "data": "Top_90_180_max"
          },
  
          {
              "title": "Top_90_180_min",
              "data": "Top_90_180_min"
          },
          {
              "title": "Top_90_180_times",
              "data": "Top_90_180_times"
          },
          {
              "title": "ece",
              "data": "ece"
          },
          {
              "title": "eco",
              "data": "eco"
          },
          {
              "title": "ecp",
              "data": "ecp"
          },
          {
              "title": "ece",
              "data": "ece"
          },
  
          {
              "title": "ect",
              "data": "ect"
          },
          {
              "title": "multiphoneinquiries_7d",
              "data": "multiphoneinquiries_7d"
          },
          {
              "title": "multiphoneinquiries_14d",
              "data": "multiphoneinquiries_14d"
          },
          {
              "title": "multiphoneinquiries_21d",
              "data": "multiphoneinquiries_21d"
          },
          {
              "title": "multiphoneinquiries_30d",
              "data": "multiphoneinquiries_30d"
          },
          {
              "title": "multiphoneinquiries_60d",
              "data": "multiphoneinquiries_60d"
          },
          {
              "title": "multiphoneinquiries_90d",
              "data": "multiphoneinquiries_90d"
          },
          {
              "title": "multiphoneinquiries_total",
              "data": "multiphoneinquiries_total"
          },
          {
              "title": "multiidinquiries_7d",
              "data": "multiidinquiries_7d"
          },
          {
              "title": "multiidinquiries_14d",
              "data": "multiidinquiries_14d"
          },
          {
              "title": "multiidinquiries_21d",
              "data": "multiidinquiries_21d"
          },
          {
              "title": "multiidinquiries_30d",
              "data": "multiidinquiries_30d"
          },
  
  
          {
              "title": "multiidinquiries_60d",
              "data": "multiidinquiries_60d"
          },
          {
              "title": "multiidinquiries_90d",
              "data": "multiidinquiries_90d"
          },
          {
              "title": "multiidinquiries_total",
              "data": "multiidinquiries_total"
          },
          {
              "title": "B_class",
              "data": "B_class"
          },
          {
              "title": "B_isopen",
              "data": "B_isopen"
          },
          {
              "title": "B_membership",
              "data": "B_membership"
          },
          {
              "title": "B_members",
              "data": "B_members"
          },
          {
              "title": "B_activeStatus",
              "data": "B_activeStatus"
          },
          {
              "title": "B_activeYear",
              "data": "B_activeYear"
          }
          ]
      });
  
  
  
  
  
  
  
  
  
  
  
  
  var ktp = document.getElementById('ktp').value;
  var phone_number = document.getElementById('phone_number').value;
  var nama_lengkap = document.getElementById('nama_lengkap').value;
  
  
  $.ajaxSetup({
       headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
       $.ajax({
                  type:'POST',
                  url:'/creditFeatured',
                  data:{ktp:ktp, phone_number:phone_number, nama_lengkap:nama_lengkap},
                  success:function(data){
                    $("#overlay").fadeOut(300);　
                    str = data.toString().replace(/\'/g, '"');
                    var parsed = JSON.parse(str);
                    if(parsed.status == "INVALID_INPUT"){
                        return alert(parsed.message);
                    }
            var data = {'creditscore' : parsed.message.creditscore,
            'isfacebook' : parsed.message.featurelist.isfacebook,
            'iswhatsapp' : parsed.message.featurelist.iswhatsapp,
            'whatsapp_avatar' : parsed.message.featurelist.whatsapp_avatar,
            'whatsapp_company_account' : parsed.message.featurelist.whatsapp_company_account,
            'whatsapp_signature' : parsed.message.featurelist.whatsapp_signature,
            'whatsapp_updatestatus_time' : parsed.message.featurelist.whatsapp_updatestatus_time,
            'multiphone_idinfo' : parsed.message.featurelist.multiphone_idinfo,
            'multiphone_phoneinfo_id' : parsed.message.featurelist.multiphone_phoneinfo_id,
            'multiphone_phoneinfo_id_phone' : parsed.message.featurelist.multiphone_phoneinfo_id_phone,
            'multiphone_status' : parsed.message.featurelist.multiphone_status,
            'phoneage' : parsed.message.featurelist.phoneage,
            'phoneage_status' : parsed.message.featurelist.phoneage_status,
            'phonealive_id_num' : parsed.message.featurelist.phonealive_id_num,
            'phonealive_phone_num' : parsed.message.featurelist.phonealive_phone_num,
            'phonealive_status' : parsed.message.featurelist.phonealive_status,
            'phoneinquiries_14d' : parsed.message.featurelist.phoneinquiries_14d,
            'phoneinquiries_180d' : parsed.message.featurelist.phoneinquiries_180d,
            'phoneinquiries_21d' : parsed.message.featurelist.phoneinquiries_21d,
            'phoneinquiries_30d' : parsed.message.featurelist.phoneinquiries_30d,
            'phoneinquiries_360d' : parsed.message.featurelist.phoneinquiries_360d,
            'phoneinquiries_3d' : parsed.message.featurelist.phoneinquiries_3d,
            'phoneinquiries_60d' : parsed.message.featurelist.phoneinquiries_60d,
            'phoneinquiries_90d' : parsed.message.featurelist.phoneinquiries_90d,
            'phoneinquiries_status' : parsed.message.featurelist.phoneinquiries_status,
            'phoneinquiries_total' : parsed.message.featurelist.phoneinquiries_total,
            'phoneinquiries_3d' : parsed.message.featurelist.phoneinquiries_3d,
            'phoneinquiriesuname_14d' : parsed.message.featurelist.phoneinquiriesuname_14d,
            'phoneinquiriesuname_180d' : parsed.message.featurelist.phoneinquiriesuname_180d,
            'phoneinquiriesuname_21d' : parsed.message.featurelist.phoneinquiriesuname_21d,
            'phoneinquiriesuname_30d' : parsed.message.featurelist.phoneinquiriesuname_30d,
            'phoneinquiriesuname_360d' : parsed.message.featurelist.phoneinquiriesuname_360d,
            'phoneinquiriesuname_3d' : parsed.message.featurelist.phoneinquiriesuname_3d,
            'phoneinquiriesuname_21d' : parsed.message.featurelist.phoneinquiriesuname_21d,
            'phoneinquiriesuname_60d' : parsed.message.featurelist.phoneinquiriesuname_60d,
            'phoneinquiriesuname_7d' : parsed.message.featurelist.phoneinquiriesuname_7d,
            'phoneinquiriesuname_90d' : parsed.message.featurelist.phoneinquiriesuname_90d,
            'phoneinquiriesuname_status' : parsed.message.featurelist.phoneinquiriesuname_status,
            'phoneinquiriesuname_total' : parsed.message.featurelist.phoneinquiriesuname_total,
            'phonescore' : parsed.message.featurelist.phonescore,
            'phonescore_status' : parsed.message.featurelist.phonescore_status,
            'phoneverify' : parsed.message.featurelist.phoneverify
            };
  
            var data1 = [];
            data1.push(data);
  
            myTable.clear();
            $.each(data1, function(index, value) {
                myTable.row.add(value);
            });
            myTable.draw();
  
          
         
  
      $('#izidatatables1').DataTable( {
          "scrollX": true
      } );
          
          
          
          
          
          }
      });
  }

  