
@include('header.header')
<style>

.body {
  background: rgb(204,204,204);
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {
  width: 21cm;
  height: 29.7cm;
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;
}
page[size="A3"] {
  width: 34cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;
}
th {
        font-size: 14px;
        text-transform: uppercase;
    }
td {
        font-size: 14px;
        text-transform: uppercase;
    }


@page {
    size: 25cm 35.7cm;
    margin: 5mm 5mm 5mm 5mm; /* change the margins as you want them to be. */
}

@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
  #printPageButton {
    display: none;
  }

}

* {
  box-sizing: border-box;
}

.fab-wrapper {
  position: fixed;
  bottom: 3rem;
  right: 3rem;
}
.fab-checkbox {
  display: none;
}
.fab {
  position: absolute;
  bottom: -1rem;
  right: -1rem;
  width: 4rem;
  height: 4rem;
  background: green;
  border-radius: 50%;
  background: green;
  box-shadow: 0px 5px 20px #81a4f1;
  transition: all 0.3s ease;
  z-index: 1;
  border-bottom-right-radius: 6px;
  border: 1px solid #0c50a7;
}

.fab:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  border-radius: 50%;
  background-color: rgba(255, 255, 255, 0.1);
}
.fab-checkbox:checked ~ .fab:before {
  width: 90%;
  height: 90%;
  left: 5%;
  top: 5%;
  background-color: rgba(255, 255, 255, 0.2);
}
.fab:hover {
  background: green;
  box-shadow: 0px 5px 20px 5px green;
}

.fab-dots {
  position: absolute;
  height: 8px;
  width: 8px;
  background-color: white;
  border-radius: 50%;
  top: 50%;
  transform: translateX(0%) translateY(-50%) rotate(0deg);
  opacity: 1;
  animation: blink 3s ease infinite;
  transition: all 0.3s ease;
}

.fab-dots-1 {
  left: 15px;
  animation-delay: 0s;
}
.fab-dots-2 {
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  animation-delay: 0.4s;
}
.fab-dots-3 {
  right: 15px;
  animation-delay: 0.8s;
}

.fab-checkbox:checked ~ .fab .fab-dots {
  height: 6px;
}

.fab .fab-dots-2 {
  transform: translateX(-50%) translateY(-50%) rotate(0deg);
}

.fab-checkbox:checked ~ .fab .fab-dots-1 {
  width: 32px;
  border-radius: 10px;
  left: 50%;
  transform: translateX(-50%) translateY(-50%) rotate(45deg);
}
.fab-checkbox:checked ~ .fab .fab-dots-3 {
  width: 32px;
  border-radius: 10px;
  right: 50%;
  transform: translateX(50%) translateY(-50%) rotate(-45deg);
}

@keyframes blink {
  50% {
    opacity: 0.25;
  }
}

.fab-checkbox:checked ~ .fab .fab-dots {
  animation: none;
}

.fab-wheel {
  position: absolute;
  bottom: 0;
  right: 0;
  border: 1px solid #;
  width: 10rem;
  height: 10rem;
  transition: all 0.3s ease;
  transform-origin: bottom right;
  transform: scale(0);
}

.fab-checkbox:checked ~ .fab-wheel {
  transform: scale(1);
}
.fab-action {
  position: absolute;
  background: green;
  width: 3rem;
  height: 3rem;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: White;
  box-shadow: 0 0.1rem 1rem rgba(24, 66, 154, 0.82);
  transition: all 1s ease;

  opacity: 0;
}

.fab-checkbox:checked ~ .fab-wheel .fab-action {
  opacity: 1;
}

.fab-action:hover {
  background-color: green;
}

.fab-wheel .fab-action-1 {
  right: -1rem;
  top: 0;
}

.fab-wheel .fab-action-2 {
  right: 3.4rem;
  top: 0.5rem;
}
.fab-wheel .fab-action-3 {
  left: 0.5rem;
  bottom: 3.4rem;
}
.fab-wheel .fab-action-4 {
  left: 0;
  bottom: -1rem;
}

.nihil-content{
    margin-left:50px; font-size:18px;
}

.nihil-footer-pageone{
    padding-left:50px; font-size:11px;
    line-height: 0.7;
}

.column {
  float: left;
  width: 50%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

</style>

<page size="A3" id="page-content">
        <body>
        <div style="padding-top:20px;">
            <center>
                <h5><b>FORMAT BERITA ACARA DAN LAPORAN </b></h5>
                <h5><b>PEMBLOKIRAN SECARA SERTA MERTA </b></h5>
            </center>
            {{-- <a class="word-export" href="javascript:void(0)"> Export as .doc </a>  --}}


            <section class="content" style="padding-top:30px;">
                <p class="nihil-content">[{{ $data['nama_pt'] }}]</p>
                <p class="nihil-content">[{{ $data['alamat_pt'] }}]</p>

                <center>
                    <h5><b>BERITA ACARA  </b></h5>
                    <h5><b>PEMBLOKIRAN SECARA SERTA MERTA</b></h5>
               </center>
                <p class="nihil-content">Yang bertandatangan di bawah ini :</p>
                <p class="nihil-content">Nama	: {{ $data['nama_pemblokir'] }}</p>
                <p class="nihil-content">Jabatan	: {{ $data['jabatan_pemblokir'] }}</p>
                <p class="nihil-content">Alamat	: {{ $data['alamat_pemblokir'] }}</p>
                <p align="justify" style="padding-left:50px; padding-right:30px; font-size:18px;">
                dalam hal ini bertindak untuk dan atas nama [{{ $data['nama_pt'] }}], dengan ini menyatakan bahwa pada hari ini [{{ $data['hari_pemblokiran'] }}, {{ date('d, m, Y', strtotime($data['tanggal_pemblokiran'])) }}], pukul ...... WIB/WITA/WIT  telah melakukan pemblokiran secara serta merta berdasarkan surat permintaan Kepala Kepolisian Republik Indonesia Nomor {{ $data['nomor_polisi'] }} tanggal {{ date('d, m, Y', strtotime($data['tanggal_polisi'])) }} atas Daftar Terduga Teroris dan Organisasi Teroris (DTTOT) Nomor {{ $data['no_dttot'] }}, di hadapan saksi:
                </p>
                <p class="nihil-content">Nama	: {{ $data['nama_terorist'] }}</p>
                <p class="nihil-content">Jabatan	: {{ $data['jabatan_terorist'] }}</p>
                <p class="nihil-content">
                terhadap rekening/aset/Dana yang dimiliki atau dikuasai oleh Nasabah sebagai berikut:
                </p>
                <p class="nihil-content">Nama	: {{ $data['nama_rekening'] }}</p>
                <p class="nihil-content">Tempat, Tanggal Lahir	: {{ $data['tempat_tanggal_lahir'] }}</p>
                <p class="nihil-content">Pekerjaan	: {{ $data['pekerjaan'] }}</p>
                <p class="nihil-content">Alamat	: {{ $data['alamat'] }}</p>
                <p class="nihil-content">Nomor rekening 	: {{ $data['nomor_rekening'] }}</p>
                <p class="nihil-content">Saldo Terakhir / Nilai Aset	: {{ $data['saldo_terakhir'] }}</p>
                <p class="nihil-content">Jenis dan identitas aset 	: {{ $data['jenis_identitas'] }}</p>
                <p class="nihil-content">
                    Demikian Berita Acara Pemblokiran Secara Serta Merta ini dibuat rangkap 1 (satu) dan
                    dibuatkan 1 (satu) salinan.
                </p>
                <p class="nihil-content">Demikian kami sampaikan, atas perhatiannya kami ucapkan terima kasih.</p>


                <br>

                <!-- membuat 2 sign  -->
                <div class="row">
                    <div class="column">
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;">Yang melakukan Pemblokiran</p>
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;">secara serta merta,</p>
                        <br><br>
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;">{{ $data['nama_pemblokir'] }}</p>
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;">{{ $data['jabatan_pemblokir'] }}</p>
                    </div>

                    <div class="column">
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;">Saksi</p>
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;"></p>
                        <br><br>
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;">{{ $data['nama_saksi'] }}</p>
                        <p style="margin-left:30px; font-size:18px; padding-left:30px;">{{ $data['jabatan_pemblokir'] }}</p>
                    </div>
                </div>
                <br>
                <p class="nihil-content">Tembusan</p>
                <p class="nihil-content">Yth. Ketua Dewan Komisioner Otoritas Jasa Keuangan</p>

                <br><br>
                <p class="nihil-footer-pageone">----------------------------------</p>
                <p class="nihil-footer-pageone">1. Nama PJK</p>
                <p class="nihil-footer-pageone">2. Nama, jabatan, dan alamat PJK. Dalam hal ini tidak harus dilakukan di kantor pusat, dapat dilakukan oleh kantor operasional/cabang</p>
                <p class="nihil-footer-pageone">3. Zona waktu disesuaikan</p>
                <p class="nihil-footer-pageone">4. Disamakan dengan nomor rekening, antara lain adalah nomor rekening tabungan, nomor rekening giro, atau nomor lain yang disesuaikan dengan kebutuhan dan karakteristik industri.</p>
                <p class="nihil-footer-pageone">5. Dalam hal aset atau Dana yang dilakukan pemblokiran serta merta bukan rekening</p>
               </section>
            </div>
        </body>
</page>

<br><br><br>

<page size="A3">
        <body>
        <div style="padding-top:100px;">
            <center>
                <h5><b>FORMAT LAPORAN PEMBLOKIRAN SECARA SERTA MERTA  </b></h5>
                <h5><b>ATAS DANA NASABAH DI SEKTOR JASA KEUANGAN </b></h5>
                <h5><b>YANG IDENTITASNYA TERCANTUM </b></h5>
                <h5><b>DALAM DAFTAR TERDUGA TERORIS DAN ORGANISASI TERORIS</b></h5>
            </center>


            <section class="content" style="padding-top:30px;">
                <p class="nihil-content">[{{ $data['nama_pt'] }}]</p>
                <p class="nihil-content">[{{ $data['alamat_pt'] }}]</p>
                <p class="nihil-content">Nomor 	: {{ $data['nomor_polisi'] }}                           [{{ $data['alamat_pemblokir'] }}], [{{ date('d, m, Y', strtotime($data['tanggal_pemblokiran'])) }}]</p>
                <p class="nihil-content">Lampiran	: {{ $data['lampiran'] }}</p>
                <p class="nihil-content">Perihal 	: Laporan Pemblokiran Secara Serta Merta</p>

                <p class="nihil-content">Kepada</p>
                <p class="nihil-content">Yth. Kepala Kepolisian Negara Republik Indonesia</p>
                <p class="nihil-content">c.q. Kepala Detasemen 88</p>
                <p class="nihil-content">Kepolisian Negara Republik Indonesia</p>
                <p class="nihil-content">Di-</p>
                <p class="nihil-content">Jakarta</p>

                <p align="justify" style="padding-left:50px; padding-right:30px; font-size:18px;">Menunjuk Pasal 28 ayat (3) Undang-Undang Nomor 9 Tahun 2013 tentang Pencegahan dan Pemberantasan Tindak Pidana Pendanaan Terorisme dan berdasarkan surat permintaan Kepala Kepolisian Republik Indonesia Nomor {{ $data['nomor_polisi'] }} tanggal {{ $data['tanggal_polisi'] }} atas Daftar Terduga Teroris dan Organisasi Teroris (DTTOT) Nomor {{ $data['no_dttot'] }}, dengan ini kami laporkan bahwa pada [{{ $data['hari_pemblokiran'] }}, {{ date('d, m, Y', strtotime($data['tanggal_pemblokiran'])) }}), pukul ...... WIB/WITA/WIT  telah dilakukan pemblokiran secara serta merta atas seluruh rekening atau aset/Dana atas nama {{ $data['nama_terorist'] }} , [{{ $data['nomor_rekening'] }} , {{ $data['saldo_terakhir'] }}), sebagaimana Berita Acara Pemblokiran Secara Serta Merta terlampir.</p>
                <p class="nihil-content">Demikian kami sampaikan, atas perhatiannya kami ucapkan terima kasih.</p>
                <p align="right" style="margin-left:30px; font-size:22px; padding-right:70px;">[{{ $data['nama_pemblokir'] }}]</p>
                <br><br>
                <p align="right" style="margin-left:30px; font-size:22px; padding-right:60px;">[Tanda Tangan]</p>

                <p class="nihil-content">Tembusan:</p>
                <p class="nihil-content">Yth. Ketua Dewan Komisioner Otoritas Jasa Keuangan</p>
                <br><br>
                <p class="nihil-footer-pageone">----------------------------------</p>
                <p class="nihil-footer-pageone">6. Nama PJK</p>
                <p class="nihil-footer-pageone">7. Zona waktu disesuaikan</p>
                <p class="nihil-footer-pageone">8. Nama Nasabah</p>
                <p class="nihil-footer-pageone">9. Disamakan dengan nomor rekening, antara lain adalah nomor rekening tabungan, nomor rekening giro, atau nomor lain yang disesuaikan dengan kebutuhan dan karakteristik industri</p>
                <p class="nihil-footer-pageone">10. Nama dan Jabatan pimpinan PJK. Dalam hal ini tidak harus dilakukan di kantor pusat, dapat dilakukan oleh kantor operasional/cabang
</p>


               </section>
            </div>
        </body>
</page>


<!-- Floating Button -->

<div class="fab-wrapper" id="printPageButton">
  <input id="fabCheckbox" type="checkbox" class="fab-checkbox" />
  <label class="fab" for="fabCheckbox">
    <span class="fab-dots fab-dots-1"></span>
    <span class="fab-dots fab-dots-2"></span>
    <span class="fab-dots fab-dots-3"></span>
  </label>
  <div class="fab-wheel">
    <a class="fab-action fab-action-1">
      <i class="fas fa-file" onclick="window.print()"></i>
    </a>
    <a onclick="back()"class="fab-action fab-action-2">
      <i class="fas fa-arrow-left"></i>
    </a>
        <!-- <a class="fab-action fab-action-3">
      <i class="fas fa-arrow-left"></i> -->
    <!-- </a>
        <a class="fab-action fab-action-4">
      <i class="fas fa-info"></i>
    </a> -->
  </div>
</div>





@include('footer.footer')

<script>
    jQuery(document).ready(function($) {
        $("a.word-export").click(function(event) {
            $("#page-content").wordExport();
        });
    });

</script>

