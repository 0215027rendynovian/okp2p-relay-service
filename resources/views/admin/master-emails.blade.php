<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Master Emails</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Master Emails</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                    <a type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="create-new-mails"> <span><i class="fas fa-envelope"></i>  Tambah Master Emails</span></a>
                    {{-- <a id="toolhapus" type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="hapus-mails" onclick="deletemails($('#id').val())"> <span><i class="fas fa-trash-alt"></i>  Hapus Master Emails</span></a> --}}
                </div>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tablemails" class="col-md-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Master Emails </b></h3>
                        </div>

                        <div class="card-body">
                                <table id="listemails" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th style="display: none">ID</th>
                                            <th>Email</th>
                                            <th>Nama</th>
                                            <th style="display: none">Status</th>
                                            <th>Status</th>
                                            <th>keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; ?>
                                        @foreach ($listemails as $mails)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td style="display: none">{{ $mails->id }}</td>
                                            <td>{{ $mails->email }}</td>
                                            <td>{{ $mails->name }}</td>
                                            <td style="display: none">{{ $mails->status }}</td>
                                            <td><?php
                                                if ($mails->status) {
                                                    echo '<h6><span class="badge badge-success">Enabled</span></h6>';
                                                } else {
                                                    echo '<h6><span class="badge badge-danger">Disabled</span></h6>';
                                                }
                                            ?></td>
                                            <td>{{ $mails->keterangan }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                <!-- left column -->
                <div class="col-md-4" id="detailmails">
                    <div class="card card-danger">
                    <div class="card-header back-ops-okp2p">
                        <h3 class="card-title">Detail Master Emails</h3>
                    </div>
                    <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form-horizontal" method="POST" action="{{ route('mails-edit') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" name="id" id="id">
                                <div class="form-group">
                                    <label for="email" class="col-sm-12 control-label">Email</label>
                                    <div class="col-sm-12">
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Enter Email" value="" maxlength="50" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Name</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter Name" value="" maxlength="300" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="status" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select id="status" name="status" class="form-control">
                                            <option value="">--- Pilih Status ---</option>
                                            <option value="0">Disabled</option>
                                            <option value="1">Enabled</option>
                                        </select>

                                        @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="keterangan" class="col-sm-12 control-label">Description</label>
                                    <div class="col-sm-12">
                                        <textarea type="text" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan" placeholder="Enter keterangan" value="" maxlength="300"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" value="create">Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--/.col (left) -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    {{-- MODALS --}}
    <div class="modal fade" id="ajax-mails-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mailsCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <form id="mailsForm" name="mailsForm" class="form-horizontal" method="POST" action="{{ route('mails-store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-sm-12 control-label">Email</label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control @error('email') autofocus is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Enter email" value="{{ old('email') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-12 control-label">Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter Name" value="{{ old('name') }}" maxlength="300" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-sm-12 control-label">Status</label>
                            <div class="col-sm-12">
                                <select name="status" class="form-control">
                                    <option value="">--- Pilih Status ---</option>
                                    <option value="0">Disabled</option>
                                    <option value="1">Enabled</option>
                                </select>

                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="keterangan" class="col-sm-12 control-label">Description</label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan" placeholder="Enter keterangan"  maxlength="300">{{ old('keterangan') }}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                         <button type="submit" class="btn btn-primary" id="btn-save" value="create">Tambah
                         </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

@include('js.toast-info')

<script>
    var table

    $(document).ready( function () {
        $('#detailmails').hide()
        $('#toolhapus').hide()
        document.getElementById("tablemails").className = "col-md-12";

        table = $('#listemails').DataTable();

        $('#listemails tbody').on( 'click', 'tr', function () {

            var datauser = table.row( this ).data()
            $('#detailmails').show()
            $('#toolhapus').show()
            document.getElementById("tablemails").className = "col-md-8";
            $('#id').val(datauser[1])
            $('#email').val(datauser[2])
            $('#name').val(datauser[3])
            $('#status').val(datauser[4])
            $('#keterangan').val(datauser[6])

        } );

        $('#listemails tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('row_selected') ) {
                $(this).removeClass('row_selected')

                $('#detailmails').hide()
                $('#toolhapus').hide()
                document.getElementById("tablemails").className = "col-md-12";
                $('#id').val('')
                $('#email').val('')
                $('#name').val('')
                $('#status').val('')
                $('#keterangan').val('')
            }
            else {
                table.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected')
            }
        } );

        /*  When user click add user button */
        $('#create-new-mails').click(function () {
            $('#btn-save').val("create-mails")
            $('#id').val('')
            $('#mailsForm').trigger("reset")
            $('#mailsCrudModal').html("Tambah Master Emails")
            $('#ajax-mails-modal').modal('show')
        });
    });

    function deletemails(id)
    {
        var email = $('#email').val()
        var name = $('#name').val()
        Swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            html:
            "email : <b>"+email+"</b> </br> " +
            "Name : <b>"+name+"</b> </br>  </br> " +
            "Anda tidak akan dapat mengembalikan ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "mails/delete/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Master Emails berhasil terhapus',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal menghapus Master Emails',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

</script>
