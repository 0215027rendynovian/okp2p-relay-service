<!DOCTYPE html>
<html lang="en">
@include('header.header')
<style>
table.dataTable thead tr {
  background-color: green;
}
th {
        font-size: 9px;
        text-transform: uppercase;
    }
td {
        font-size: 9px;
        text-transform: uppercase;
    }
</style>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>FINTECH DATA CENTER</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">FINTECH DATA CENTER</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">FINTECH DATA</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">KTP</label>
                    <input class="form-control" type="text" id="ktp" name="ktp" placeholder="Masukan Nomor KTP">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">ALASAN</label>
                    <select class="form-control" name="reason" id="reason">
                        <option value="1">Pengajuan Pinjaman melalui Platform</option>
                        <option value="2">Monitor Saldo Pinjaman dari Peminjam</option>
                    </select>
                  </div>
                </div>
                <div class="card-footer">
                  <a type='button' href='#' onclick="submit(1)"  class="btn btn-success">Kirim</a>
                  <a type='button' href='#' onclick="ClearStorage()"  class="btn btn-secondary">Hapus Formulir</a>

                  <!-- <a type='button' href='#' onclick="downloadpdf()" class="btn btn-success">Download Pdf</a> -->
                </div>
              </form>
            </div>
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">
          <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                <a type='button' href='#' onclick="PrintPreview()"  class="btn btn-lg btn-primary"> <span><i class="fas fa-file"></i>  Pratinjau</span></a>
                <a type='button' href='#' onclick="submit(2)"  class="btn btn-lg btn-primary"> <span><i class="fas fa-database"></i>  Cek Log</span></a>
                <a type='button' href='#' onclick="ExcelDownload(2)"  class="btn btn-lg btn-primary"> <span><i class="fas fa-file-excel"></i> Unduh Excel</span></a>
                </div>
                <div class="card-footer">
                  <!-- <a type='button' href='#' onclick="downloadpdf()" class="btn btn-success">Download Pdf</a> -->
                </div>
              </form>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>DAFTAR DATA</b></h3>
                        </div>

                        <div class="card-body">
                                <table id="izidatatables1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

<script>
function ExcelDownload(){
    var ktp = document.getElementById('ktp').value;
    var reason = document.getElementById('reason').value;

    if(ktp == undefined || ktp == '' || ktp == null){
        return alert('Harap Isi KTP data terlebih dahulu');
    }
    if(reason == undefined || reason == '' || reason == null){
        return alert('Harap Pilih ALASAN Terlebih dahulu');
    }

    localStorage.setItem("ktp", ktp);
    localStorage.setItem("reason", reason);

    var logs;

    var uri = '/FDCDataExcel';

    $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} });
	  $.ajax({
		type:'POST',
        url : uri,
		data:{ktp:ktp, reason:reason},
		success:function(data){
            if(data[0] == '0000'){
                return alert('KTP Tidak ditemukan pada sistem');
            }
            var parsed = JSON.parse(data);
            if(parsed.status == "Not Found"){
                return alert("Data Tidak Ditemukan");
            }

            var createXLSLFormatObj = [];

            /* XLS Title */
            var xlsTitle = ['Report Fintech Data Center'];
            var xlsTitle1 = ['KTP : ', parsed.noIdentitas];
            var xlsTitle2 = ['ALASAN : ', parsed.inquiryReason];

            /* XLS Head Columns */
            var xlsHeader = ["ID Penyelenggara", "ID Pengguna", "Nama Borrower", "No Identitas", "No Npwp", "Tgl Perjanjian Borrower", "Tgl Penyaluran Dana", "Nilai Pendanaan", "Tgl Pelaporan Data", "Sisa Pelaporan Berjalan", "Tgl Jatuh Tempo Pinjaman", "Kualitas Pinjaman", "DPD Terakhir", "DPD Terlama", "Status Pinjaman", "Jenis Pengguna", "Keterangan Kualitas Pinjaman", "Keterangan Status Pinjaman"];

            var xlsRows = parsed.pinjaman

            var xlsRows = JSON.parse(JSON.stringify( xlsRows, ["id_penyelenggara","jenis_pengguna","nama_borrower","no_identitas", "no_npwp", "tgl_perjanjian_borrower", "tgl_penyaluran_dana", "nilai_pendanaan", "tgl_pelaporan_data", "sisa_pinjaman_berjalan","tgl_jatuh_tempo_pinjaman","kualitas_pinjaman","dpd_terakhir","dpd_max","status_pinjaman","jenis_pengguna_ket","kualitas_pinjaman_ket","status_pinjaman_ket"] , 18));

            createXLSLFormatObj.push(xlsTitle);
            createXLSLFormatObj.push(xlsTitle1);
            createXLSLFormatObj.push(xlsTitle2);
            createXLSLFormatObj.push(xlsHeader);

            $.each(xlsRows, function(index, value) {
                var innerRowData = [];
                $.each(value, function(ind, val) {
                    innerRowData.push(val);
                });
                createXLSLFormatObj.push(innerRowData);
            });

            /* File Name */
            var filename = parsed.noIdentitas+".xlsx";

            /* Sheet Name */
            var ws_name = parsed.noIdentitas;

            if (typeof console !== 'undefined') console.log(new Date());
            var wb = XLSX.utils.book_new(),
                ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

            /* Add worksheet to workbook */
            XLSX.utils.book_append_sheet(wb, ws, ws_name);

            /* Write workbook and Download */
            if (typeof console !== 'undefined') console.log(new Date());
            XLSX.writeFile(wb, filename);
            if (typeof console !== 'undefined') console.log(new Date());
        }
	});
}

function PrintPreview(){
    var ktp = document.getElementById('ktp').value;
    var reason = document.getElementById('reason').value;

    if(ktp == undefined || ktp == '' || ktp == null){
        return alert('Harap Isi KTP data terlebih dahulu');
    }

    if(reason == undefined || reason == '' || reason == null){
        return alert('Harap Pilih ALASAN Terlebih dahulu');
    }

    window.open("/FDCDataReport/"+ktp+"/"+reason, "_blank");
}

function init(){

    var ktp_data = localStorage.getItem('ktp');
    var reason_data = localStorage.getItem('reason');

    if(ktp_data == null){

    }else{
        document.getElementById('ktp').value = ktp_data;
    }

    if(reason_data == null){

    }else{
        document.getElementById('reason').value = reason_data;
    }

}
init();

function ClearStorage(){
    localStorage.clear();
    location.reload();
}

function submit(submit_type){
    // debugger
    var ktp = document.getElementById('ktp').value;
    var reason = document.getElementById('reason').value;

    if(ktp == undefined || ktp == '' || ktp == null){
        return alert('Harap Isi KTP data terlebih dahulu');
    }
    if(reason == undefined || reason == '' || reason == null){
        return alert('Harap Pilih ALASAN Terlebih dahulu');
    }

    localStorage.clear();

    localStorage.setItem("ktp", ktp);
    localStorage.setItem("reason", reason);

    var logs;
    var myTable1 = $('#izidatatables1').DataTable({
        "paging": false,
        "lengthChange": true,
        "fixedHeader": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": 'Bfrtip',
        "buttons": [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "data": [],
        "columns": [
        {
            "title": "ID Penyelenggara",
            "data": "id_penyelenggara"
        }, {
            "title": "ID Pengguna",
            "data": "jenis_pengguna"
        }, {
            "title": "NAMA PEMINJAM",
            "data": "nama_borrower"
        }, {
            "title": "No Identitas",
            "data": "no_identitas"
        }, {
            "title": "No Npwp",
            "data": "no_npwp"
        }, {
            "title": "TGL PERJANJIAN PEMINJAM",
            "data": "tgl_perjanjian_borrower"
        }, {
            "title": "Tgl Penyaluran Dana",
            "data": "tgl_penyaluran_dana"
        }, {
            "title": "Nilai Pendanaan",
            "data": "nilai_pendanaan"
        }, {
            "title": "Tgl Pelaporan Data",
            "data": "tgl_pelaporan_data"
        }, {
            "title": "Sisa Pelaporan Berjalan",
            "data": "sisa_pinjaman_berjalan"
        }, {
            "title": "Tgl Jatuh Tempo Pinjaman",
            "data": "tgl_jatuh_tempo_pinjaman"
        }, {
            "title": "Kualitas Pinjaman",
            "data": "kualitas_pinjaman"
        }, {
            "title": "DPD Terakhir",
            "data": "dpd_terakhir"
        }, {
            "title": "DPD Terlama",
            "data": "dpd_max"
        }, {
            "title": "Status Pinjaman",
            "data": "status_pinjaman"
        }, {
            "title": "Jenis Pengguna",
            "data": "jenis_pengguna_ket"
        }, {
            "title": "Keterangan Kualitas Pinjaman",
            "data": "kualitas_pinjaman_ket"
        }, {
            "title": "Keterangan Status Pinjaman",
            "data": "status_pinjaman_ket"
        }
        ]
    });

    myTable1.destroy();

    if(submit_type == 1){
        // buat variable di .env untuk API FDC. ex. : URI_FDC=/api/fdc-internserve
        var uri = '{{ env('URI_FDC') }}';
    }else{
        var uri = '/FDCDataPostLog';
    }

    $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} });
	$.ajax({
		type:'POST',
        url : uri,
		data:{ktp:ktp, reason:reason},
		    success:function(data){


                if(data[0] == '0000'){
                    return alert('KTP Tidak ditemukan pada sistem');
                }

                var parsed = JSON.parse(data);
                if(parsed.status == "Not Found"){
                    return alert("Data Tidak Ditemukan");
                }

                $('#overlay').fadeIn();

                var data1 = [];
                for(var k in parsed.pinjaman) {
                    var status_pinjaman_ket_translate
                    if (parsed.pinjaman[k].status_pinjaman_ket === 'Fully Paid') {
                        status_pinjaman_ket_translate = 'LUNAS'
                    } else if (parsed.pinjaman[k].status_pinjaman_ket === 'Outstanding') {
                        status_pinjaman_ket_translate = 'AKTIF'
                    } else {
                        status_pinjaman_ket_translate = parsed.pinjaman[k].status_pinjaman_ket
                    }
                    var fdc1 = {
                        'id_penyelenggara' : parsed.pinjaman[k].id_penyelenggara,
                        'jenis_pengguna' : parsed.pinjaman[k].jenis_pengguna,
                        'nama_borrower' : parsed.pinjaman[k].nama_borrower,
                        'no_identitas' : parsed.pinjaman[k].no_identitas,
                        'no_npwp' : parsed.pinjaman[k].no_npwp,
                        'tgl_perjanjian_borrower' : parsed.pinjaman[k].tgl_perjanjian_borrower,
                        'tgl_penyaluran_dana' : parsed.pinjaman[k].tgl_penyaluran_dana,
                        'nilai_pendanaan' : parsed.pinjaman[k].nilai_pendanaan,
                        'tgl_pelaporan_data' : parsed.pinjaman[k].tgl_pelaporan_data,
                        'sisa_pinjaman_berjalan' : parsed.pinjaman[k].sisa_pinjaman_berjalan,
                        'tgl_jatuh_tempo_pinjaman' : parsed.pinjaman[k].tgl_jatuh_tempo_pinjaman,
                        'kualitas_pinjaman' : parsed.pinjaman[k].kualitas_pinjaman,
                        'dpd_terakhir' : parsed.pinjaman[k].dpd_terakhir,
                        'dpd_max' : parsed.pinjaman[k].dpd_max,
                        'status_pinjaman' : parsed.pinjaman[k].status_pinjaman,
                        'jenis_pengguna_ket' : parsed.pinjaman[k].jenis_pengguna_ket,
                        'kualitas_pinjaman_ket' : parsed.pinjaman[k].kualitas_pinjaman_ket,
                        'status_pinjaman_ket' : status_pinjaman_ket_translate,
                    };
                    data1.push(fdc1);
                }

                myTable1.clear();
                $.each(data1, function(index, value) {
                    myTable1.row.add(value);
                });
                myTable1.draw();

                $('#overlay').fadeOut();
            }
	});
}
</script>
