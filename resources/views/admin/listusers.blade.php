<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Daftar Pengguna</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Daftar Pengguna</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                    <a type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="create-new-users"> <span><i class="fas fa-user-plus"></i>  Tambah User</span></a>
                    <a id="toolreset" type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="reset-password" onclick="resetpassword($('#id').val())"> <span><i class="fas fa-undo"></i>  Reset Password</span></a>
                    <a id="toolhapus" type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="hapus-user" onclick="deleteuser($('#id').val())"> <span><i class="fas fa-trash-alt"></i>  Hapus User</span></a>
                </div>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                    <div class="col-12" id="tablelistuser">

                        <div class="card">
                            <div class="card-header bg-danger back-ops-okp2p">
                                <h3 class="card-title"><b>Daftar Pengguna </b></h3>
                            </div>

                            <div class="card-body">
                                    <table id="listusers" class="table table-striped table-bordered table-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th style="display: none">ID</th>
                                                <th>NAMA LENGKAP</th>
                                                <th>EMAIL</th>
                                                <th>HAK AKSES</th>
                                                <th style="display: none">ID AKSES</th>
                                                <th>DEPARTEMEN</th>
                                                <th style="display: none">ID DEPARTEMEN</th>
                                                <th style="display: none">STATUS</th>
                                                <th>STATUS</th>
                                                <th style="display: none">TIPE USER</th>
                                                <th>TIPE USER</th>
                                                <th>UPDATED AT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1; ?>
                                            @foreach ($users as $user)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td style="display: none">{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $privilege[$user->permission] }}</td>
                                                <td style="display: none">{{ $user->permission }}</td>
                                                <td>{{ $departemens[$user->dept_id] }}</td>
                                                <td style="display: none">{{ $user->dept_id }}</td>
                                                <td style="display: none">{{ $user->is_active }}</td>
                                                <td><?php
                                                    if ($user->is_active) {
                                                        echo '<h6><span class="badge badge-success">Enabled</span></h6>';
                                                    } else {
                                                        echo '<h6><span class="badge badge-danger">Disabled</span></h6>';
                                                    }
                                                ?></td>
                                                <td style="display: none">{{ $user->es_system }}</td>
                                                <td><?php
                                                    if ($user->es_system) {
                                                        echo '<h6><span class="badge badge-primary">ES System</span></h6>';
                                                    } else {
                                                        echo '<h6><span class="badge badge-warning">OPS System</span></h6>';
                                                    }
                                                ?></td>
                                                <td>{{ date('d M Y H:i:s', strtotime($user->updated_at)) }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                            </div>

                        </div>

                    </div>
                      <!-- /.card-body -->

                      <!-- left column -->
                    <div class="col-md-4" id="detailuser">
                        <div class="card card-danger">
                        <div class="card-header back-ops-okp2p">
                            <h3 class="card-title">Detail User</h3>
                        </div>
                        <!-- /.card-header -->
                            <div class="card-body">
                                <form class="form-horizontal" method="POST" action="{{ route('user-edit') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <input type="hidden" name="id" id="id">
                                    <input type="hidden" name="uid" id="uid" value="{{ $uid }}">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-12 control-label">Nama Lengkap</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="email" class="col-sm-12 control-label">Email</label>
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Enter Mail" value="" maxlength="50" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="permission" class="col-sm-12 control-label">Hak Akses</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" id="permission" name="permission" required autocomplete="permission">
                                            <option value="" disabled selected>--Pilih--</option>
                                            <option value="1">Admin</option>
                                            <option value="2">Finance</option>
                                            <option value="3">CSS</option>
                                            <option value="4">Analyst</option>
                                            <option value="5">HR</option>
                                            <option value="6">Super Admin</option>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="permission" class="col-sm-12 control-label">Departemen</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" id="dept_id" name="dept_id" required autocomplete="dept_id">
                                            <option value="" disabled selected>--Pilih--</option>
                                            <option value="0">--Pilih Departemen--</option>
                                            <option value="1">OKP2P</option>
                                            <option value="2">RETAIL</option>
                                            <option value="3">ASSET</option>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="is_active" class="col-sm-12 control-label">Status</label>
                                        <div class="col-sm-12">
                                            <select id="is_active" name="is_active" class="form-control">
                                                <option value="">--- Pilih Status ---</option>
                                                <option value="0">Disabled</option>
                                                <option value="1">Enabled</option>
                                            </select>

                                            @error('is_active')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="es_system" class="col-sm-12 control-label">Tipe User</label>
                                        <div class="col-sm-12">
                                            <select id="es_system" name="es_system" class="form-control">
                                                <option value="">--- Pilih Tipe ---</option>
                                                <option value="0">OPS System</option>
                                                <option value="1">ES System</option>
                                            </select>

                                            @error('es_system')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password" style="color: #dc3545!important" class="col-sm-12 control-label"><i class="fas fa-unlock-alt"></i> Ganti Password? (optional)</label>
                                        <div class="col-sm-12">
                                            <input type="password" class="form-control is-invalid @error('password') is-invalid @enderror" id="password" name="password" placeholder="Enter Password" value="" maxlength="50">
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" value="create">Simpan Perubahan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!--/.col (left) -->

                </div>

                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    {{-- MODALS --}}
    <div class="modal fade" id="ajax-users-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="usersCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <form id="usersForm" name="usersForm" class="form-horizontal" method="POST" action="{{ route('user-store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="col-sm-12 control-label">Nama Lengkap</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter Name" value="{{ old('name') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-12 control-label">Email</label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter Mail" value="{{ old('email') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="permission" class="col-sm-12 control-label">Hak Akses</label>
                            <div class="col-sm-12">
                                <select class="form-control" name="permission" required autocomplete="permission">
                                    <option value="" disabled selected>--Pilih Hak Akses--</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Finance</option>
                                    <option value="3">CSS</option>
                                    <option value="4">Analyst</option>
                                    <option value="5">HR</option>
                                    <option value="6">Super Admin</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="permission" class="col-sm-12 control-label">Departemen</label>
                            <div class="col-sm-12">
                                <select class="form-control" name="dept_id" required autocomplete="dept_id">
                                    <option value="0" disabled selected>--Pilih Departemen--</option>
                                    <option value="1">OKP2P</option>
                                    <option value="2">Retail</option>
                                    <option value="3">Asset</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="is_active" class="col-sm-12 control-label">Status</label>
                            <div class="col-sm-12">
                                <select name="is_active" class="form-control">
                                    <option value="">--- Pilih Status ---</option>
                                    <option value="0">Disabled</option>
                                    <option value="1">Enabled</option>
                                </select>

                                @error('is_active')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="es_system" class="col-sm-12 control-label">Tipe User</label>
                            <div class="col-sm-12">
                                <select name="es_system" class="form-control">
                                    <option value="">--- Pilih Tipe ---</option>
                                    <option value="0">OPS System</option>
                                    <option value="1">ES System</option>
                                </select>

                                @error('es_system')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                         <button type="submit" class="btn btn-primary" id="btn-save" value="create">Tambah
                         </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

@include('js.toast-info')

<script>
    var table

    $(document).ready( function () {
        $('#detailuser').hide()
        $('#toolreset').hide()
        $('#toolhapus').hide()
        document.getElementById("tablelistuser").className = "col-md-12";

        table = $('#listusers').DataTable();

        $('#listusers tbody').on( 'click', 'tr', function () {

            var datauser = table.row( this ).data()
            $('#detailuser').show()
            $('#toolreset').show()
            $('#toolhapus').show()
            document.getElementById("tablelistuser").className = "col-md-8";
            $('#id').val(datauser[1])
            $('#name').val(datauser[2])
            $('#email').val(datauser[3])
            $('#permission').val(datauser[5])
            $('#dept_id').val(datauser[7])
            $('#is_active').val(datauser[8])
            $('#es_system').val(datauser[10])

        } );

        $('#listusers tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('row_selected') ) {
                $(this).removeClass('row_selected')

                $('#detailuser').hide()
                $('#toolreset').hide()
                $('#toolhapus').hide()
                document.getElementById("tablelistuser").className = "col-md-12";
                $('#id').val('')
                $('#name').val('')
                $('#email').val('')
                $('#permission').val('')
                $('#is_active').val('')
                $('#es_system').val('')
            }
            else {
                table.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected')
            }
        } );

        /*  When user click add user button */
        $('#create-new-users').click(function () {
            $('#btn-save').val("create-users")
            $('#id').val('')
            $('#usersForm').trigger("reset")
            $('#usersCrudModal').html("Tambah User Baru")
            $('#ajax-users-modal').modal('show')
        });
    });

    function deleteuser(id)
    {
        var uid = $('#uid').val()
        if (id == uid) {
            rejectdelete()
        } else {
            fixdelete(id)
        }
    }

    function fixdelete(id)
    {
        var name = $('#name').val()
        var email = $('#email').val()
        Swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            html:
            "Nama : <b>"+name+"</b> </br> " +
            "Email : <b>"+email+"</b> </br>  </br> " +
            "Anda tidak akan dapat mengembalikan ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "users-list/delete/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Data User berhasil terhapus',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal menghapus data user',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

    function rejectdelete()
    {
        Swal.fire(
            'Error!',
            'Tidak dapat menghapus akun Anda sendiri!',
            'error'
        )
    }

    function resetpassword(id)
    {
        var name = $('#name').val()
        var email = $('#email').val()
        Swal.fire({
            title: 'Reset password untuk data ini?',
            html:
            "Nama : <b>"+name+"</b> </br> " +
            "Email : <b>"+email+"</b> </br>  </br> " +
            "Password akan kembali ke default!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, reset it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "users-list/resetpassword/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Reset successfully!',
                            'Password User berhasil di reset',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal reset password user',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

</script>
