<!DOCTYPE html>
<html lang="en">
@include('header.header')
<style>
.elementID {
    color: #dc3545;
    /* text-shadow: 1px 1px 1px #ccc; */
    font-size: 1.5em;
}

</style>
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

            <div class="content-wrapper">
              <div id="loading"></div>
              <section class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1><b>REKAPITULASI DATA POKOK BUNGA & DENDA</b></h1>
                    </div>
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
                        <li class="breadcrumb-item active">Instamoney</li>
                      </ol>
                    </div>
                  </div>
                </div>
              </section>

                <!-- Main content -->
                <section class="content">
                  <div id="download_pdf_data" class="container-fluid">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="card card-danger">
                          <div class="card-header back-ops-okp2p">
                            <h3 class="card-title">Kotak Informasi</h3>
                          </div>
                          <form role="form">
                            <div class="card-body">
                              <div class="col-md-6">
                              <div class="form-group">
                                  <div class="input-group">
                                    <a type="button" onclick="exportData()" class="btn btn-lg btn-primary"> <span><i class="fas fa-file-excel"></i> Unduh Excel</span></a>
                                  </div>
                                  <!-- /.input group -->
                                </div>
                              </div>
                            </div>

                          </form>
                        </div>
                      </div>
                    </div>


                    <!-- List Bunga Tetap -->
                    <div class ="row">
                      <div class="col-md-12">
                              <!-- Main content -->
                        <section class="content">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-12">

                                <div class="card">
                                    <div class="card-header bg-danger back-ops-okp2p">
                                        <h3 class="card-title"><b>LIST Pokok Bunga Dan Denda</b></h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-md-8">
                                              <code style="font-size: 30px">
                                              <span class="badge badge-primary"><i class="fas fa-dollar-sign elementID"></i>&nbsp;Total Denda <b id="denda"></b></span>
                                              <span class="badge badge-success"><i class="fas fa-dollar-sign elementID"></i>&nbsp;Total Pokok <b id="pokok"></b></span>
                                              <span class="badge badge-warning"><i class="fas fa-hand-holding-usd elementID"></i>&nbsp;Total Bunga  <b id="bungatetap"></b>
                                              </span>
                                            </code>
                                        </div>
                                        <hr style="margin-top:20px;">
                                        <table id="datapokokbungadenda" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>NO_PINJAMAN</th>
                                                    <th>NAMA_PEMINJAM</th>
                                                    <th>STATUS_CODE</th>
                                                    <th>STATUS_NAME</th>
                                                    <th>TANGGAL_PERSETUJUAN</th>
                                                    <th>DENDA <i class="fas fa-dollar-sign elementID"></i></th>
                                                    <th>BUNGA <i class="fas fa-dollar-sign elementID"></i></th>
                                                    <th>POKOK <i class="fas fa-hand-holding-usd elementID"></i></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>


                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </section>
              </div>

            <aside class="control-sidebar control-sidebar-dark">
            </aside>
            @include('footer.tag-footer')
          </div>
    @include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script type="text/javascript">

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

start_date = yyyy + '' + mm + '' + '01';
end_date = yyyy + '' + mm + '' + dd;

var status = "405001";

$(function () {

    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

  })



//fungsi untuk filtering data berdasarkan tanggal
   // var start_date = moment()
   // var end_date;
   // var status;

    var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
      //debugger
        var dateStart = parseDateValue(start_date);
        var dateEnd = parseDateValue(end_date);
        //Kolom tanggal yang akan kita gunakan

        var evalDate= parseDateValue(aData[27]);
            if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
                ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
                ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
                ( dateStart <= evalDate && evalDate <= dateEnd ) )
            {
                return true;
            }
            return false;
    });

    var dTable = $('#datapokokbungadenda').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: true,
            ordering: true,
            paging: true,
            info: true,
            autoWidth: true,
            scrollY: 300,
            scrollX: true,
            dom: "<'row'<'col-sm-2'l><'col-sm-2' <'selectstatcd'>><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('reportdendabulanan') }}",
                data: function (data) {
                    data.start_date = start_date
                    data.end_date = end_date
                    data.status = status
                },
                // error: handleAjaxError
            },
              drawCallback: function (data) {

                var api = this.api();

                /*
                var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                 };


                var intVal2 = function(i){
                  return typeof i === 'string' ? i.replace("RP ","").replace(/[\$,]/g, '').replace('.', '')*1 : typeof i === 'number' ? i : 0;
                };
                */

                function intVal(x){
	                return parseInt(x.toString().replace("RP ","").replace(/\./g,''));
                }

                var sumBungaTetap = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


                 var sumDenda = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                var sumPokok = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                 //var sumBungaTetap = dTable.ajax.json().sumBungaTetap
                 //var sumAdminFee = dTable.ajax.json().sumAdminFee
                 //var sumPokok = dTable.ajax.json().sumPokok
                 //var sumDenda = dTable.ajax.json().sumDenda

                 $('#bungatetap').html("RP "+new Intl.NumberFormat( 'ID' ).format(sumBungaTetap));
                 $('#denda').html("RP "+new Intl.NumberFormat( 'ID' ).format(sumDenda));
                 $('#pokok').html("RP "+new Intl.NumberFormat( 'ID' ).format(sumPokok));
                 //$('#adminfee').html(sumAdminFee);
             },
            columns: [
                { data: "NoPinjaman" },
                { data: "NamaPinjaman" },
                { data: "stsCd" },
                { data: "Status" },
                { data: "TanggalPersetujuan" },
                { data: "Denda" },
                { data: "BungaTetap" },
                { data: "Pokok" }
            ]
        });

        //menambahkan daterangepicker di dalam datatables
        $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span> </div> <input type="text" class="form-control pull-right" id="datesearch" placeholder="Cari berdasarkan rentang tanggal.."> </div>');

        document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";

        //menambahkan filter select status
        $("div.selectstatcd").html('<div class="input-group"> <div class="input-group-prepend">  </div><select class="form-control" name="statcd" id="statcd"><option value="405001">Normal</option><option value="405002">Ditolak</option></select></div>');

        document.getElementsByClassName("selectstatcd")[0].style.textAlign = "right";


        //konfigurasi daterangepicker pada input dengan id datesearch

        var startDateDefault = moment(today);
        var endDateDefault = moment(yyyy +'-' + mm + '-' + '01');

        $('#datesearch').daterangepicker({
            autoUpdateInput: true,
            dateFormat: 'MMDDYYYY',
        });


        $('#datesearch').data('daterangepicker').setStartDate(endDateDefault);
        $('#datesearch').data('daterangepicker').setEndDate(startDateDefault);

        //menangani proses saat apply date range
        $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
            //$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            start_date=picker.startDate.format('YYYYMMDD');
            end_date=picker.endDate.format('YYYYMMDD');

            $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);

            status = document.getElementById('statcd').selectedOptions[0].value;
            dTable.draw();

        });

        $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            // start_date='';
            // end_date='';
            $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
            dTable.draw();
        });

        $('#statcd').change(function(){
            status = this.value;
            $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
            dTable.draw();
        });

    // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
    function parseDateValue(rawDate) {
        var dateArray= rawDate.split("/");
        var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11
        return parsedDate;
    }

    async function exportData() {
      //window.alert('export-pokok-denda'+'/'+start_date+'/'+end_date+'/'+status);
      window.location.href = 'export-pokok-denda'+'/'+start_date+'/'+end_date+'/'+status;
    }

</script>
