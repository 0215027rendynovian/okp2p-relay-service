<!DOCTYPE html>
<html lang="en">
@include('header.header')
<style>
table.dataTable thead tr {
  background-color: green;
}
  th {
        font-size: 14px;
        text-transform: uppercase;
    }
  td {
        font-size: 14px;
        text-transform: uppercase;
    }
</style>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>MIDTRANS</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Validator Rekening Bank</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Validator Rekening Bank</h3>
              </div>
              <form role="form">
                <div class="card-body">
                  <input type="hidden" value="{{$uid}}" name="uid" id="uid">

                  <div class="form-group">
                  <label for="exampleInputPassword1">Nama Penerima</label>
                        <select class="form-control" name="bank_name" id="bank_name" onchange="onBankDropDownSelected()">
                        @foreach($MasterBankAccount as $key)
                          <option value='{{ $key->bank_code }}' >{{ $key->bank_name }}</option>
                          @endforeach
                          </select>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1"><p id="lbl_account_name">Nomor telepon</p></label>
                    <input class="form-control" type="text" id="account_number" name="account_number" placeholder="Nomor telepon">
                  </div>

                </div>
                <div class="card-footer">
                  <a type='button' href='#' onclick="submit(1)"  class="btn btn-success">Kirim</a>
                  <a type='button' href='#' onclick="ClearStorage()"  class="btn btn-secondary">Hapus Formulir</a>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-6">
          <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Validator Rekening Bank</h3>
              </div>
              <form role="form">
                <div class="card-body">
                <a type='button' href='#' onclick="PrintPreview()"  class="btn btn-lg btn-success"> <span><i class="fas fa-file"></i>  Pratinjau</span></a>
                <a type='button' href='#' onclick="submit(2)"  class="btn btn-lg btn-secondary"> <span><i class="fas fa-database"></i>  Cek Log</span></a>

                </div>
                <div class="card-footer">
                 </div>
              </form>
            </div>
          </div>
        </div>

        <div class="card">
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3"><b>Hasil tanggapan bank</b></h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">DAFTAR DATA</a></li>
                  <!-- <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Data Upload</a></li> -->
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                        <div class ="row">
                        <div class="col-md-12">
                                <section class="content">
                                    <div class="container-fluid">
                                        <div class="row">
                                             <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-header bg-danger back-ops-okp2p">
                                                            <h3 class="card-title"><b>Log Midtrans</b></h3>
                                                        </div>
                                                        <div class="card-body">
                                                                <table id="izidatatables1" class="table table-striped table-bordered" style="width:100%">
                                                                    <thead>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                        </div>
                                                    </div>
                                             </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </div>
                  </div>
                  <div class="tab-pane" id="tab_2">
                  <div class ="row">
                        <div class="col-md-12">
                                <section class="content">
                                    <div class="container-fluid">
                                        <div class="row">
                                             <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-header bg-danger back-ops-okp2p">
                                                            <h3 class="card-title"><b>DAFTAR DATA UPLOAD</b></h3>
                                                        </div>
                                                        <div class="card-body">
                                                                <table id="izidatatables1" class="table table-striped table-bordered" style="width:100%">
                                                                    <thead>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                        </div>
                                                    </div>
                                             </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        <div>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </section>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>

@include('footer.footer')

</body>
</html>
<script>

function onBankDropDownSelected(){
  var selectedBankName = $("#bank_name").val();

  if (selectedBankName == 'ovo'  || selectedBankName == 'gopay'){
    $("#lbl_account_name").text("Nomor telepon");
    $("#account_number").attr('placeholder','Nomor telepon');
  }
  else {
    $("#lbl_account_name").text("Account Number");
    $("#account_number").attr('placeholder','Account Number');
  }
}


function PrintPreview(){
    var bank_name = document.getElementById('bank_name').value;
    var account_number = document.getElementById('account_number').value;
    if(bank_name == undefined || bank_name == '' || bank_name == null){
        return alert('Harap pilih bank name terlebih dahulu');
    }

    if(account_number == undefined || account_number == '' || account_number == null){
        return alert('Harap Isi Account Number terlebih dahulu');
    }

    window.open("/report-midtrans-bank-validator/"+account_number, "_blank");
}


function init(){
    debugger
    var account_number = localStorage.getItem('account_number');
    var bank_name = localStorage.getItem('bank_name');

    if(account_number == null){

    }else{
        document.getElementById('account_number').value = account_number;
    }


    if(bank_name == null){

    }else{
        document.getElementById('bank_name').value = bank_name;
    }


    if (bank_name) {
      $('#bank_name').val(bank_name);
    }else {

    }

    $('#bank_name').on('change', function(e) {
      debugger
      var bank_name = $(this).val();
      localStorage.setItem('bank_name', bank_name);
    });



}
init();


function ClearStorage(){
    localStorage.clear();
    location.reload();
}

function submit(submit_type){
  debugger
    var account_no  = $("#account_number").val();
    var bank = document.getElementById("bank_name").value;
    var uid = $('#uid').val();
    localStorage.setItem("account_number", account_no);
    localStorage.setItem("bank_name", bank);

    var uri = '';

    var myTable1 = $('#izidatatables1').DataTable({
        "paging": false,
        "lengthChange": true,
        "fixedHeader": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": 'Bfrtip',
        "buttons": [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "data": [],
        "columns": [
        {
            "title": "Nama",
            "data": "account_name"
        }, {
            "title": "Bank",
            "data": "bank_name"
        }, {
            "title": "No. Rekening",
            "data": "account_no"
        }]
    });

    myTable1.destroy();


    if (submit_type == 1){
        uri = "/api/midtrans-okp2p-api";
        $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} });
        $.ajax({
        type:'POST',
            url : uri,
            data:{bank:bank, account_no:account_no, uid:uid},
              success:function(data){
                    var parsed = JSON.parse(data);
                    if ("error_message" in parsed){
                        var err_msg = parsed.errors.account;
                        alert("Error: "+err_msg+".");
                    }
                    else {
                        var account_name = parsed.account_name;
                        alert("Account is valid. Account name: "+account_name+".");

                        var record = {
                            "account_no" : parsed.account_no,
                            "bank_name": parsed.bank_name,
                            "account_name": parsed.account_name
                        };

                        var data1 = [];
                        data1.push(record);
                    }

                    myTable1.clear();
                    $.each(data1, function(index, value) {
                        myTable1.row.add(value);
                    });
                    myTable1.draw();

                    $('#overlay').fadeIn();

                    $('#overlay').fadeOut();
                }
      });
    }
    else {
      uri = "/api/saved-midtrans";
      $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} });
        $.ajax({
        type:'POST',
            url : uri,
            data:{bank:bank, account_no:account_no, uid:uid},
              success:function(data){
                    var parsed = JSON.parse(data);
                    if ("error_message" in parsed){
                        var err_msg = parsed.errors.account;
                        alert("Error: "+err_msg+".");
                    }
                    else {
                        var account_name = parsed.account_name;
                        alert("Account is valid. Account name: "+account_name+".");

                        var record = {
                            "account_no" : parsed.account_no,
                            "bank_name": parsed.bank_name,
                            "account_name": parsed.account_name
                        };

                        var data1 = [];
                        data1.push(record);
                    }

                    myTable1.clear();
                    $.each(data1, function(index, value) {
                        myTable1.row.add(value);
                    });
                    myTable1.draw();

                    $('#overlay').fadeIn();

                    $('#overlay').fadeOut();
                }
      });
    }


}





</script>
