<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ubah IP</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Master Departement</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">



                    <div class="card-body">

                        <form class="form-horizontal" method="POST" action="{{ route('set-ping-config') }}" >
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" name="id" id="id" value="{{ $config_data->id ?? '' }}" >
                                <div class="form-group">

                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">IP Server</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="server_ip" name="server_ip" placeholder="IP" value="{{ $config_data->server_ip ?? '' }}" maxlength="30" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="col-sm-12 control-label">Port Server</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('description') is-invalid @enderror" id="server_port" name="server_port" placeholder="Port" value="{{ $config_data->server_port ?? '' }}" maxlength="4">
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" value="create" onclick="simpan_perubahan">Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tabledepartement" class="col-md-12">

                  </div>
                      <!-- /.card-body -->

                <!-- left column -->

                <!--/.col (left) -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    {{-- MODALS --}}
    <div class="modal fade" id="ajax-departement-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="departementCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <form id="departementForm" name="departementForm" class="form-horizontal" method="POST" action="{{ route('departement-store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">

                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-12 control-label">Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter Name" value="{{ old('name') }}" maxlength="300" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-sm-12 control-label">Status</label>
                            <div class="col-sm-12">
                                <select name="status" class="form-control">
                                    <option value="">--- Pilih Status ---</option>
                                    <option value="0">Disabled</option>
                                    <option value="1">Enabled</option>
                                </select>

                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-12 control-label">Description</label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Enter Description"  maxlength="300">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                         <button type="submit" class="btn btn-primary" id="btn-save" value="create">Tambah
                         </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

@include('js.toast-info')

<script>

    function simpan_perubahan(){
        $.ajax({
            type: 'POST',
            data:{server_ip:server_ip, server_port:server_port},
            success:function(data){
                alert('OK');
            }
        });
    }

    $(document).ready( function () {

    });

</script>
