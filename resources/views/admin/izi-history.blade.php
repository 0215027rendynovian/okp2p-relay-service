<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Riwayat Penggunaan API</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Riwayat Penggunaan API</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tablehistory" class="col-md-12">

                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Ringkasan Hitungan Penggunaan API berdasarkan Departemen</b></h3>

                            <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text"> Pilih Departement </span> </div>
                                        <select class="form-control" name="dept_id" id="dept_id">
                                            <option value="">Semua</option>
                                            <option value="1">OKP2P</option>
                                            <option value="2">Retail</option>
                                            <option value="3">Asset</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span> </div>
                                        <input type="text" class="form-control pull-right" id="datesearch" placeholder="Cari berdasarkan rentang tanggal.." value="">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <button type="button" id="btncari" class="btn btn-primary"><i class="fas fa-search"></i>&nbsp;&nbsp;&nbsp;Cari</button>
                                    </div>
                                </div>
                            </div>
                            <table id="table-history-filter" class="table table-striped projects">
                                <thead>
                                    <tr>
                                        <th rowspan="2" style="width: 1%">
                                            #
                                        </th>
                                        <th rowspan="2" style="width: 20%">
                                            nama departemen
                                        </th>
                                        <th rowspan="2" style="width: 40%">
                                            Persentase
                                        </th>
                                        <th colspan="2" style="width: 20%" class="text-center">
                                            status hit
                                        </th>
                                        <th rowspan="2" style="width: 10%" class="text-center">
                                            total hitungan hit
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>OK</th>
                                        <th>Invalid</th>
                                    </tr>
                                </thead>
                                <tfoot style="background: rgb(181, 184, 185)">
                                    <tr>
                                        <td style="text-align: right" colspan="3"><b style="font-size: 20px">RINGKASAN PENGGUNAAN API :</b></td>
                                        <td><span class="badge badge-success" style="font-size: 20px"><b id="counthitok"></b></span></td>
                                        <td><span class="badge badge-danger" style="font-size: 20px"><b id="counthitinvalid"></b></span></td>
                                        <td><span class="badge badge-primary" style="font-size: 20px"><b id="counthitapi"></b></span></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </section>
                    <!-- /.content -->

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Riwayat Penggunaan API</b></h3>
                        </div>

                        <div class="card-body">
                            <a type='button' href="#" class="btn btn-lg btn-primary" onclick="exportData('/exportDataIziHistory/xlsx')"> <span><i class="fas fa-file-excel"></i>  Unduh Riwayat (.xlsx)</span></a>
                            <a type='button' href="#" class="btn btn-lg btn-success" onclick="exportRawData('/exportRawData/xlsx')"> <span><i class="fas fa-file-excel"></i>  Unduh Data Mentah (.xlsx)</span></a>
                            <hr style="margin-top:5px;">
                                <table id="listhistory" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>NAMA</th>
                                            <th>hitungan hit</th>
                                            <th>LOG</th>
                                        </tr>
                                    </thead>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->




      </div><!-- /.container-fluid -->


    <!-- /.content -->




  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script type="text/javascript">

    //fungsi untuk filtering data berdasarkan tanggal
    var start_date;
    var end_date;
    var dept_id;
    var listDept = {!! $listDept !!}

    // get range date in a month
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    // set value in datapicker
    startDateDefault = mm + '/' + 01 + '/' + yyyy;
    endDateDefault = mm + '/' + dd + '/' + yyyy;
    // initial value for default data
    start_date = yyyy + '-' + mm + '-' + 01;
    end_date = yyyy + '-' + mm + '-' + dd;

    var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
        var dateStart = parseDateValue(start_date);
        var dateEnd = parseDateValue(end_date);
        //Kolom tanggal yang akan kita gunakan
        var evalDate= parseDateValue(aData[10]);
            if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
                ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
                ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
                ( dateStart <= evalDate && evalDate <= dateEnd ) )
            {
                return true;
            }
            return false;
    });

    // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
    function parseDateValue(rawDate) {
        var dateArray= rawDate.split("/");
        var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11
        return parsedDate;
    }

    function onRowClicked(id){
        window.open("/history-data/"+id+"/"+start_date+"/"+end_date, "_blank");
    }

    $(document).ready(function(){

        var dTableLog = $('#listhistory').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: true,
            ordering: true,
            paging: true,
            info: true,
            autoWidth: true,
            // scrollY: 300,
            scrollX: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('getHistoryHit') }}",
                data: function (data) {
                    data.fromDate = start_date
                    data.toDate = end_date
                    data.dept_id = dept_id
                },
                delay: 10000,
                timeout: 100000,
                error: handleAjaxError
            },
            columns: [
                { data: 'id' },
                { data: 'name' },
                { data: 'COUNT_HIT' },
                { data: 'log' },
            ]
        });

        var dTable = $('#table-history-filter').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: false,
            ordering: true,
            paging: false,
            info: false,
            autoWidth: true,
            // scrollY: 300,
            // scrollX: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('getCountHistory') }}",
                data: function (data) {
                    data.fromDate = start_date
                    data.toDate = end_date
                    data.dept_id = dept_id
                },
                delay: 10000,
                timeout: 100000,
                error: handleAjaxError
            },
            drawCallback: function (data) {
                // sum all
                var sumCountHit = dTable.ajax.json().countHit
                $('#counthitapi').html(sumCountHit);
                // sum ok
                var sumCountHitOK = dTable.ajax.json().countHitOK
                $('#counthitok').html(sumCountHitOK);
                // sum invalid
                var sumCountHitInvalid = dTable.ajax.json().countHitInvalid
                $('#counthitinvalid').html(sumCountHitInvalid);
            },
            columns: [
                { data: 'dept_id' },
                { data: 'name' },
                { data: 'description' },
                { data: 'ok' },
                { data: 'invalid' },
                { data: 'sum' },
            ]
        });

        //konfigurasi daterangepicker pada input dengan id datesearch
        $('#datesearch').daterangepicker({
            autoUpdateInput: true,
        });

        //change the selected date range of that picker
        $("#datesearch").data('daterangepicker').setStartDate(startDateDefault);
        $("#datesearch").data('daterangepicker').setEndDate(endDateDefault);

        //menangani proses saat apply date range
        $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            start_date=picker.startDate.format('YYYY-MM-DD');
            end_date=picker.endDate.format('YYYY-MM-DD');
            $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
        });

        $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
        });

        $('#dept_id').change(function(){
            dept_id = this.value;
        });

        $('#btncari').on('click', function(){
            dTable.draw();
            dTableLog.draw();
        })
    });

    function exportData(_this) {
        window.location.href = _this+'?fromDate='+start_date+'&toDate='+end_date+'&dept_id='+dept_id
    }

    function exportRawData(_this) {
        window.location.href = _this+'?fromDate='+start_date+'&toDate='+end_date+'&dept_id='+dept_id
    }

</script>
