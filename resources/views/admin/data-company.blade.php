<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pefindo</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Pefindo</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
          <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                    <a type='button' href="#" class="btn btn-lg btn-primary" onclick="exportCsv('/exportcompany')"> <span><i class="fas fa-file-csv"></i>  Download CSV</span></a>
                {{-- <a type='button' href='#' onclick="PrintPreview()"  class="btn btn-lg btn-danger"> <span><i class="fas fa-file"></i>  Pratinjau</span></a>
                <a type='button' href='#' onclick="submit(2)"  class="btn btn-lg btn-danger"> <span><i class="fas fa-database"></i>  Cek Log</span></a>
                <a type='button' href='#' onclick="ExcelDownload(2)"  class="btn btn-lg btn-danger"> <span><i class="fas fa-file-excel"></i> Unduh Excel</span></a> --}}
                </div>
                <div class="card-footer">
                  <!-- <a type='button' href='#' onclick="downloadpdf()" class="btn btn-success">Download Pdf</a> -->
                </div>
              </form>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>DAFTAR DATA COMPANY</b></h3>
                        </div>

                        <div class="card-body">
                                <table id="datapefindo" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Customer_Code</th>
                                            <th>Contract_Code</th>
                                            <th>Role_Of_Customer</th>
                                            <th>Company_Name</th>
                                            <th>Company_Alias</th>
                                            <th>Group_Name</th>
                                            <th>Status</th>
                                            <th>Establishment_Date</th>
                                            <th>Establishment_Location</th>
                                            <th>Category</th>
                                            <th>Market_Listed</th>
                                            <th>Economic_Sector</th>
                                            <th>Registration_Number</th>
                                            <th>Latest_Deed_Number</th>
                                            <th>Latest_Date_Of_Deed</th>
                                            <th>NPWP</th>
                                            <th>Street</th>
                                            <th>City</th>
                                            <th>Postal_Code</th>
                                            <th>District</th>
                                            <th>Parish</th>
                                            <th>Country</th>
                                            <th>Address_Line</th>
                                            <th>Mobile_Phone</th>
                                            <th>Fixed_Line</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script>
    $(document).ready(function(){
        var myTable1 = $('#datapefindo').DataTable({
            "scrollY": 200,
            "scrollX": true,
            "dom": 'Bfrtip',
            "buttons": [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
        });
    });

    function exportCsv(_this) {
        if (typeof start_date === 'undefined' || start_date === null) {
            window.location.href = _this
        }else{
            window.location.href = _this+'?fromDate='+start_date+'&toDate='+end_date;
        }
    }

    function ExcelDownload(){

    }

    function PrintPreview(){

    }
</script>
