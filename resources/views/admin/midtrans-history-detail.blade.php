<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Riwayat Penggunaan API</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Detail Riwayat Penggunaan API</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tablehistory" class="col-md-12">

                <div id="detailhistory">
                    <div class="card card-danger">
                    <div class="card-header back-ops-okp2p">
                        <h3 class="card-title">Detail Riwayat</h3>
                    </div>
                    <!-- /.card-header -->
                        <div class="card-body">
                        <table id="tbldetailhistory" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>ACCOUNT NAME</th>
                                            <th>ACCOUNT NUMBER</th>
                                            <th>BANK NAME</th>
                                            <th>hitungan hit</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($User ?? '' as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->account_name }}</td>
                                            <td>{{ $item->account_no }}</td>
                                            <td>{{ $item->bank_name }}</td>
                                            <td>{{ $item->count_hit }}
                                        </tr>
                                        @endforeach
                                    </tbody>
                         </table>
                        </div>
                    </div>


                    </div>

                  </div>
                      <!-- /.card-body -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->




      </div><!-- /.container-fluid -->


    <!-- /.content -->




  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

  <script>
 var myTable1 = $('#tbldetailhistory').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "columnDefs": [
        { "targets": [0, 3, 4], "searchable": false}
    ]
    });
  </script>

