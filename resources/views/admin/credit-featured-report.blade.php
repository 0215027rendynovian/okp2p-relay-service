
@include('header.header')
<style>

.body {
  background: rgb(204,204,204);
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {
  width: 21cm;
  height: 29.7cm;
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;
}
page[size="A3"] {
  width: 34cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;
}
th {
        font-size: 14px;
        text-transform: uppercase;
    }
td {
        font-size: 14px;
        text-transform: uppercase;
    }


@page {
    size: 25cm 35.7cm;
    margin: 5mm 5mm 5mm 5mm; /* change the margins as you want them to be. */
}

@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
  #printPageButton {
    display: none;
  }

}

* {
  box-sizing: border-box;
}

.fab-wrapper {
  position: fixed;
  bottom: 3rem;
  right: 3rem;
}
.fab-checkbox {
  display: none;
}
.fab {
  position: absolute;
  bottom: -1rem;
  right: -1rem;
  width: 4rem;
  height: 4rem;
  background: green;
  border-radius: 50%;
  background: green;
  box-shadow: 0px 5px 20px #81a4f1;
  transition: all 0.3s ease;
  z-index: 1;
  border-bottom-right-radius: 6px;
  border: 1px solid #0c50a7;
}

.fab:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  border-radius: 50%;
  background-color: rgba(255, 255, 255, 0.1);
}
.fab-checkbox:checked ~ .fab:before {
  width: 90%;
  height: 90%;
  left: 5%;
  top: 5%;
  background-color: rgba(255, 255, 255, 0.2);
}
.fab:hover {
  background: green;
  box-shadow: 0px 5px 20px 5px green;
}

.fab-dots {
  position: absolute;
  height: 8px;
  width: 8px;
  background-color: white;
  border-radius: 50%;
  top: 50%;
  transform: translateX(0%) translateY(-50%) rotate(0deg);
  opacity: 1;
  animation: blink 3s ease infinite;
  transition: all 0.3s ease;
}

.fab-dots-1 {
  left: 15px;
  animation-delay: 0s;
}
.fab-dots-2 {
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  animation-delay: 0.4s;
}
.fab-dots-3 {
  right: 15px;
  animation-delay: 0.8s;
}

.fab-checkbox:checked ~ .fab .fab-dots {
  height: 6px;
}

.fab .fab-dots-2 {
  transform: translateX(-50%) translateY(-50%) rotate(0deg);
}

.fab-checkbox:checked ~ .fab .fab-dots-1 {
  width: 32px;
  border-radius: 10px;
  left: 50%;
  transform: translateX(-50%) translateY(-50%) rotate(45deg);
}
.fab-checkbox:checked ~ .fab .fab-dots-3 {
  width: 32px;
  border-radius: 10px;
  right: 50%;
  transform: translateX(50%) translateY(-50%) rotate(-45deg);
}

@keyframes blink {
  50% {
    opacity: 0.25;
  }
}

.fab-checkbox:checked ~ .fab .fab-dots {
  animation: none;
}

.fab-wheel {
  position: absolute;
  bottom: 0;
  right: 0;
  border: 1px solid #;
  width: 10rem;
  height: 10rem;
  transition: all 0.3s ease;
  transform-origin: bottom right;
  transform: scale(0);
}

.fab-checkbox:checked ~ .fab-wheel {
  transform: scale(1);
}
.fab-action {
  position: absolute;
  background: green;
  width: 3rem;
  height: 3rem;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: White;
  box-shadow: 0 0.1rem 1rem rgba(24, 66, 154, 0.82);
  transition: all 1s ease;

  opacity: 0;
}

.fab-checkbox:checked ~ .fab-wheel .fab-action {
  opacity: 1;
}

.fab-action:hover {
  background-color: green;
}

.fab-wheel .fab-action-1 {
  right: -1rem;
  top: 0;
}

.fab-wheel .fab-action-2 {
  right: 3.4rem;
  top: 0.5rem;
}
.fab-wheel .fab-action-3 {
  left: 0.5rem;
  bottom: 3.4rem;
}
.fab-wheel .fab-action-4 {
  left: 0;
  bottom: -1rem;
}


</style>

<page size="A3">
        <body>
        <div style="padding-top:20px;">
            <center>
                <h5 style="color:green;"><b>OKP2P - IZI DATA</b></h4>
                <h6>Report IZI DATA</h5>
            </center>
            <hr>
            <p style="margin-left:30px;"><b>Nama Lengkap : {{$namalengkap}}</b></p>
            <p style="margin-left:30px;"><b>KTP : {{$ktp}} </b></p>
            <p style="margin-left:30px;"><b>Nomor HP : {{$numberphone}} </b></p>
            <input type="hidden" name="nama" id="nama" value="{{$namalengkap}}">
            <input type="hidden" name="ktp" id="ktp" value="{{$ktp}}">
            <input type="hidden" name="nomorhp" id="nomorhp" value="{{$numberphone}}">


            </div>

        <section class="content" style="padding-top:30px;">
        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header bg-success">
                        <h3 class="card-title"><b>WHATSAPP & Facebook</b></h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                            <table id="izidatatables1" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                      </div>
                      <!-- /.card-body -->
                    </div>


                    <div class="card">
                      <div class="card-header bg-success">
                        <h3 class="card-title"><b>PENGECEKAN TELEPON</b></h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <table id="izidatatables1_2" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                      </div>


                      <div class="card-body">

                        <table id="izidatatables1_3" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                      </div>

                      <div class="card-body">
                        <table id="izidatatables1_4" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                      </div>

                      <div class="card-body">
                        <table id="izidatatables1_5" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                  </div>
                      <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


        </body>
</page>

<br><br><br><br>
<br><br><br><br>
<br><br><br>
<!-- PAGE 2 -->

<page size="A3">
        <body>
        <div style="padding-top:20px;">
            <center>
                <h5 style="color:green;"><b>OKP2P - IZI DATA</b></h4>
                <h6>Report IZI DATA</h5>
            </center>
            <hr>
            </div>

        <section class="content" style="padding-top:30px;">
        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                  <div class="card">
                    <div class="card-header bg-success">
                      <h3 class="card-title"><b>RIWAYAT SKOR</b></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                      <table id="izidatatables2" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>


                    </div>

                    <div class="card-body">
                      <table id="izidatatables2_1" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                        <div class="card-body">
                        <table id="izidatatables2_2" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </body>
</page>


<br><br><br><br>
<br><br><br><br>
<br><br><br><br>

<!-- page 3 -->

<page size="A3">
        <body>
        <div style="padding-top:20px;">
             <center>
                <h5 style="color:green;"><b>OKP2P - IZI DATA</b></h4>
                <h6>Report IZI DATA</h5>
                </center>
            <hr>
            </div>

        <section class="content" style="padding-top:30px;">
        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                  <div class="card">
                   <div class="card-header bg-success">
                         <h3 class="card-title"><b>DETIL IDENTITAS</b></h3>
                     </div>
                     <div class="card-body">
                      <table id="izidatatables2_3" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                    <div class="card-body">
                      <table id="izidatatables2_4" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                  </div>


                  <div class="card">
                     <div class="card-header bg-success">
                         <h3 class="card-title"><b>PENGECEKAN IDENTITAS</b></h3>
                     </div>
                     <div class="card-body">
                      <table id="izidatatables2_5" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                    <div class="card-body">
                      <table id="izidatatables2_6" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables2_6_1" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                  </div>

                </div>
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </body>
</page>





<br><br><br><br>
<br><br><br><br>
<br><br><br><br>



<!-- page 4 -->

<page size="A3">
        <body>
        <div style="padding-top:20px;">
            <center>
                <h5 style="color:green;"><b>OKP2P - IZI DATA</b></h4>
                <h6>Report IZI DATA</h5>
            </center>
            <hr>
            </div>

        <section class="content" style="padding-top:30px;">
        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                  <div class="card">
                     <div class="card-header bg-success">
                         <h3 class="card-title"><b>PREFERENSI BANK</b></h3>
                     </div>
                     <div class="card-body">
                      <table id="izidatatables2_7" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables2_8" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    <div class="card-body">
                        <table id="izidatatables2_9" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </body>
</page>


<br><br><br><br>
<br><br><br><br>
<br><br><br><br>

<!-- page 4 -->

<page size="A3">
        <body>
        <div style="padding-top:20px;">
            <center>
                <h5 style="color:green;"><b>OKP2P - IZI DATA</b></h4>
                <h6>Report IZI DATA</h5>
            </center>
            <hr>
            </div>

        <section class="content" style="padding-top:30px;">
        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                  <div class="card">
                  <div class="card-header bg-success">
                      <h3 class="card-title"><b>PENGECEKAN TOP & MULTI ID</b></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table id="izidatatables3" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>



                    <div class="card-body">
                      <table id="izidatatables3_1" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_1_1" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_2" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_3" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables3_4" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables3_5" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_6" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_7" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables3_8" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>

                </div>
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </body>
</page>




<!-- Floating Button -->

<div class="fab-wrapper" id="printPageButton">
  <input id="fabCheckbox" type="checkbox" class="fab-checkbox" />
  <label class="fab" for="fabCheckbox">
    <span class="fab-dots fab-dots-1"></span>
    <span class="fab-dots fab-dots-2"></span>
    <span class="fab-dots fab-dots-3"></span>
  </label>
  <div class="fab-wheel">
    <a class="fab-action fab-action-1">
      <i class="fas fa-file" onclick="window.print()"></i>
    </a>
    <a onclick="back()"class="fab-action fab-action-2">
      <i class="fas fa-arrow-left"></i>
    </a>
        <!-- <a class="fab-action fab-action-3">
      <i class="fas fa-arrow-left"></i> -->
    <!-- </a>
        <a class="fab-action fab-action-4">
      <i class="fas fa-info"></i>
    </a> -->
  </div>
</div>





@include('footer.footer')
<script>

function back(){
    location.href = '/creditfeature';
}


submit();

function submit(){


var logs;
var myTable1 = $('#izidatatables1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
          {
            "title": "Credit Score",
            "data": "creditscore"
        },
        {
            "title": "Is Facebook",
            "data": "isfacebook"
        },
        {
            "title": "Is Whatsapp",
            "data": "iswhatsapp"
        }, {
            "title": "Whatsapp Avatar",
            "data": "whatsapp_avatar"
        }, {
            "title": "Whatsapp Company Account",
            "data": "whatsapp_company_account"
        }, {
            "title": "Whatsapp Signature",
            "data": "whatsapp_signature"
        }, {
            "title": "Whatsapp Updatestatus Time",
            "data": "whatsapp_updatestatus_time"
        }
        ]
    });


    myTable1.destroy();




    var myTable1_2 = $('#izidatatables1_2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "Multiphone Idinfo",
            "data": "multiphone_idinfo"
        },
        {
            "title": "Multiphone Phoneinfo Id",
            "data": "multiphone_phoneinfo_id"
        },
        {
            "title": "Multiphone Phoneinfo Id Phone",
            "data": "multiphone_phoneinfo_id_phone"
        },
        {
            "title": "Multiphone Status",
            "data": "multiphone_status"
        },
        {
            "title": "Phoneage",
            "data": "phoneage"
        },

        {
            "title": "Phoneage Status",
            "data": "phoneage_status"
        },
        {
            "title": "Phonealive Id_Num",
            "data": "phonealive_id_num"
        },
        {
            "title": "Phonealive phone Num",
            "data": "phonealive_phone_num"
        },
        {
            "title": "Phonealive Status",
            "data": "phonealive_status"
        }
        ]
    });


    myTable1_2.destroy();







    var myTable1_3 = $('#izidatatables1_3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [


        {
            "title": "Phone inquiries 14d",
            "data": "phoneinquiries_14d"
        },

       {
            "title": "Phone inquiries 180d",
            "data": "phoneinquiries_180d"
        },

        {
            "title": "Phone inquiries 21d",
            "data": "phoneinquiries_21d"
        },

        {
            "title": "Phone inquiries 30d",
            "data": "phoneinquiries_30d"
        },


        {
            "title": "Phone inquiries 360d",
            "data": "phoneinquiries_360d"
        },


        {
            "title": "Phone inquiries 3d",
            "data": "phoneinquiries_3d"
        },

        {
            "title": "Phone inquiries 60d",
            "data": "phoneinquiries_60d"
        },


        {
            "title": "Phone inquiries 90d",
            "data": "phoneinquiries_90d"
        },


        {
            "title": "Phone inquiries status",
            "data": "phoneinquiries_status"
        },


        {
            "title": "Phone inquiries total",
            "data": "phoneinquiries_total"
        }

        ]
    });


    myTable1_3.destroy();






    var myTable1_4 = $('#izidatatables1_4').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "Phone inquiriesuname 14d",
            "data": "phoneinquiriesuname_14d"
        },

        {
            "title": "phone inquiriesuname 180d",
            "data": "phoneinquiriesuname_180d"
        },

        {
            "title": "phone inquiriesuname 21d",
            "data": "phoneinquiriesuname_21d"
        },

        {
            "title": "phone inquiriesuname 30d",
            "data": "phoneinquiriesuname_30d"
        },

        {
            "title": "phone inquiriesuname 360d",
            "data": "phoneinquiriesuname_360d"
        },

        {
            "title": "phone inquiriesuname 3d",
            "data": "phoneinquiriesuname_3d"
        },

        {
            "title": "phone inquiriesuname 21d",
            "data": "phoneinquiriesuname_21d"
        },

        {
            "title": "phone inquiriesuname 60d",
            "data": "phoneinquiriesuname_60d"
        }
        ]
    });


    myTable1_4.destroy();




    var myTable1_5 = $('#izidatatables1_5').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "phone inquiriesuname 7d",
            "data": "phoneinquiriesuname_7d"
        },

        {
            "title": "phone inquiriesuname 90d",
            "data": "phoneinquiriesuname_90d"
        },
        {
            "title": "phone inquiriesuname status",
            "data": "phoneinquiriesuname_status"
        },
        {
            "title": "phone inquiriesuname total",
            "data": "phoneinquiriesuname_total"
        },

        {
            "title": "phone score",
            "data": "phonescore"
        },
        {
            "title": "phonescore status",
            "data": "phonescore_status"
        },
        {
            "title": "phone verify",
            "data": "phoneverify"
        }
        ]
    });


    myTable1_5.destroy();




// table 2

var myTable2 = $('#izidatatables2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
          {
            "title": "A II 14d",
            "data": "A_II_14d"
        },
        {
            "title": "A II 180d",
            "data": "A_II_180d"
        },
        {
            "title": "A II 21d",
            "data": "A_II_21d"
        }, {
            "title": "A II 30d",
            "data": "A_II_30d"
        }, {
            "title": "A II 360d",
            "data": "A_II_360d"
        }, {
            "title": "A II 3d",
            "data": "A_II_3d"
        }, {
            "title": "A II 60d",
            "data": "A_II_60d"
        },
        {
            "title": "A II 7d",
            "data": "A_II_7d"
        },
        {
            "title": "A II 90d",
            "data": "A_II_90d"
        },
        {
            "title": "A PI 14d",
            "data": "A_PI_14d"
        },
        {
            "title": "A PI 180d",
            "data": "A_PI_180d"
        },
        {
            "title": "A PI 21d",
            "data": "A_PI_21d"
        },

        {
            "title": "A PI 30d",
            "data": "A_PI_30d"
        },
        {
            "title": "A PI 360d",
            "data": "A_PI_360d"
        },
        {
            "title": "A PI 3d",
            "data": "A_PI_3d"
        },
        {
            "title": "A PI 60d",
            "data": "A_PI_60d"
        },

        {
            "title": "A PI 7d",
            "data": "A_PI_7d"
        },

       {
            "title": "A PI 90d",
            "data": "A_PI_90d"
        }


        ]
    });

    myTable2.destroy();



    var myTable2_1 = $('#izidatatables2_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
          {
            "title": "B II 14d",
            "data": "B_II_14d"
        },

        {
            "title": "B II 180d",
            "data": "B_II_180d"
        },


        {
            "title": "B II 21d",
            "data": "B_II_21d"
        },
        {
            "title": "B II 30d",
            "data": "B_II_30d"
        },

        {
            "title": "B II 360d",
            "data": "B_II_360d"
        },


        {
            "title": "B II 3d",
            "data": "B_II_3d"
        },


        {
            "title": "B II 60d",
            "data": "B_II_60d"
        },


        {
            "title": "B II 7d",
            "data": "B_II_7d"
        },


        {
            "title": "B II 90d",
            "data": "B_II_90d"
        },


        {
            "title": "B PI 14d",
            "data": "B_PI_14d"
        },

        {
            "title": "B PI 180d",
            "data": "B_PI_180d"
        },

        {
            "title": "B PI 21d",
            "data": "B_PI_21d"
        },

        {
            "title": "B PI 30d",
            "data": "B_PI_30d"
        },

        {
            "title": "B PI 360d",
            "data": "B_PI_360d"
        },

        {
            "title": "B PI 3d",
            "data": "B_PI_3d"
        },

        {
            "title": "B PI 60d",
            "data": "B_PI_60d"
        },

        {
            "title": "B PI 7d",
            "data": "B_PI_7d"
        },

        {
            "title": "B PI 90d",
            "data": "B_PI_90d"
        }
        ]
    });

    myTable2_1.destroy();



    var myTable2_2 = $('#izidatatables2_2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "C II 14d",
            "data": "C_II_14d"
        },
        {
            "title": "C II 180d",
            "data": "C_II_180d"
        },
        {
            "title": "C II 21d",
            "data": "C_II_21d"
        },

        {
            "title": "C II 30d",
            "data": "C_II_30d"
        },
        {
            "title": "C II 360d",
            "data": "C_II_360d"
        },
        {
            "title": "C II 3d",
            "data": "C_II_3d"
        },



        {
            "title": "C II 60d",
            "data": "C_II_60d"
        },
        {
            "title": "C II 7d",
            "data": "C_II_7d"
        },
        {
            "title": "C II 90d",
            "data": "C_II_90d"
        },
        {
            "title": "C PI 14d",
            "data": "C_PI_14d"
        },
        {
            "title": "C PI 180d",
            "data": "C_PI_180d"
        },
        {
            "title": "C PI 21d",
            "data": "C_PI_21d"
        },
        {
            "title": "C PI 30d",
            "data": "C_PI_30d"
        },
        {
            "title": "C PI 3d",
            "data": "C_PI_3d"
        },
        {
            "title": "C PI 60d",
            "data": "C_PI_60d"
        },
        {
            "title": "C PI 7d",
            "data": "C_PI_7d"
        },
        {
            "title": "C PI 90d",
            "data": "C_PI_90d"
        }
        ]
    });

    myTable2_2.destroy();



    var myTable2_3 = $('#izidatatables2_3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "II v4 feature_status",
            "data": "II_v4_feature_status"
        },
        {
            "title": "PI v4 feature status",
            "data": "PI_v4_feature_status"
        },
        {
            "title": "identity address",
            "data": "identity_address"
        },
        {
            "title": "identity date of birth",
            "data": "identity_date_of_birth"
        },
        {
            "title": "identity district",
            "data": "identity_district"
        },
        {
            "title": "identity gender",
            "data": "identity_gender"
        },
        {
            "title": "identity city",
            "data": "identity_city"
        },
        {
            "title": "identity marital status",
            "data": "identity_marital_status"
        },
        {
            "title": "identity match",
            "data": "identity_match"
        }
        ]
    });

    myTable2_3.destroy();





    var myTable2_4 = $('#izidatatables2_4').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "identity name",
            "data": "identity_name"
        },
        {
            "title": "identity nationnality",
            "data": "identity_nationnality"
        },
        {
            "title": "identity place of birth",
            "data": "identity_place_of_birth"
        },


        {
            "title": "identity province",
            "data": "identity_province"
        },
        {
            "title": "identity religion",
            "data": "identity_religion"
        },
        {
            "title": "identity rt",
            "data": "identity_rt"
        },
        {
            "title": "identity rw",
            "data": "identity_rw"
        },
        {
            "title": "identity status",
            "data": "identity_status"
        },
        {
            "title": "identity village",
            "data": "identity_village"
        },
        {
            "title": "identity work",
            "data": "identity_work"
        }
        ]
    });

    myTable2_4.destroy();





    var myTable2_5 = $('#izidatatables2_5').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "idinquiries 14d",
            "data": "idinquiries_14d"
        },
        {
            "title": "idinquiries 180d",
            "data": "idinquiries_180d"
        },
        {
            "title": "idinquiries 21d",
            "data": "idinquiries_21d"
        },
        {
            "title": "idinquiries 30d",
            "data": "idinquiries_30d"
        },
        {
            "title": "idinquiries 360d",
            "data": "idinquiries_360d"
        },
        {
            "title": "idinquiries 3d",
            "data": "idinquiries_3d"
        },
        {
            "title": "idinquiries 60d",
            "data": "idinquiries_60d"
        },
        {
            "title": "idinquiries 7d",
            "data": "idinquiries_7d"
        },
        {
            "title": "idinquiries 90d",
            "data": "idinquiries_90d"
        },
        {
            "title": "idinquiries status",
            "data": "idinquiries_status"
        },
        {
            "title": "idinquiries total",
            "data": "idinquiries_total"
        }
        ]
    });

    myTable2_5.destroy();




    var myTable2_6 = $('#izidatatables2_6').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "idinquiriesuname 14d",
            "data": "idinquiriesuname_14d"
        },
        {
            "title": "idinquiriesuname 21d",
            "data": "idinquiriesuname_21d"
        },
        {
            "title": "idinquiriesuname 30d",
            "data": "idinquiriesuname_30d"
        },
        {
            "title": "idinquiriesuname 360d",
            "data": "idinquiriesuname_360d"
        },
        {
            "title": "idinquiriesuname 3d",
            "data": "idinquiriesuname_3d"
        },
        {
            "title": "idinquiriesuname 60d",
            "data": "idinquiriesuname_60d"
        }
        ]
    });

    myTable2_6.destroy();




    var myTable2_6_1 = $('#izidatatables2_6_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "idinquiriesuname 7d",
            "data": "idinquiriesuname_7d"
        },
        {
            "title": "idinquiriesuname 90d",
            "data": "idinquiriesuname_90d"
        },
        {
            "title": "idinquiriesuname status",
            "data": "idinquiriesuname_status"
        },
        {
            "title": "idinquiriesuname total",
            "data": "idinquiriesuname_total"
        },
        {
            "title": "idinquiriesuname 3d",
            "data": "idinquiriesuname_3d"
        }
        ]
    });

    myTable2_6_1.destroy();





    var myTable2_7 = $('#izidatatables2_7').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "preference bank 180d",
            "data": "preference_bank_180d"
        },
        {
            "title": "preference bank 270d",
            "data": "preference_bank_270d"
        },
        {
            "title": "preference bank 60d",
            "data": "preference_bank_60d"
        },
        {
            "title": "preference bank 90d",
            "data": "preference_bank_90d"
        },
        {
            "title": "preference ecommerce 180d",
            "data": "preference_ecommerce_180d"
        },
        {
            "title": "preference ecommerce 270d",
            "data": "preference_ecommerce_270d"
        },
        {
            "title": "preference ecommerce 60d",
            "data": "preference_ecommerce_60d"
        }
        ]
    });

    myTable2_7.destroy();



    var myTable2_8 = $('#izidatatables2_8').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "preference ecommerce 90d",
            "data": "preference_ecommerce_90d"
        },


        {
            "title": "preference game 180d",
            "data": "preference_game_180d"
        },
        {
            "title": "preference game 270d",
            "data": "preference_game_270d"
        },
        {
            "title": "preference game 60d",
            "data": "preference_game_60d"
        },
        {
            "title": "preference game 90d",
            "data": "preference_game_90d"
        },
        {
            "title": "preference lifestyle 180d",
            "data": "preference_lifestyle_180d"
        }
        ]
    });

    myTable2_8.destroy();



    var myTable2_9 = $('#izidatatables2_9').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "preference lifestyle 270d",
            "data": "preference_lifestyle_270d"
        },


        {
            "title": "preference lifestyle 60d",
            "data": "preference_lifestyle_60d"
        },
        {
            "title": "preference lifestyle 90d",
            "data": "preference_lifestyle_90d"
        },
        {
            "title": "preference status",
            "data": "preference_status"
        }
        ]
    });


    myTable2_9.destroy();



    var myTable3 = $('#izidatatables3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "topup 0 180 avg",
            "data": "topup_0_180_avg"
        },
        {
            "title": "topup 0 180 max",
            "data": "topup_0_180_max"
        }, {
            "title": "topup 0 180 min",
            "data": "topup_0_180_min"
        }, {
            "title": "topup 0 180 times",
            "data": "topup_0_180_times"
        }, {
            "title": "topup 0 30 avg",
            "data": "topup_0_30_avg"
        }, {
            "title": "topup 0 30 max",
            "data": "topup_0_30_max"
        },
        {
            "title": "topup 0 30 min",
            "data": "topup_0_30_min"
        },
        {
            "title": "topup 0 30 times",
            "data": "topup_0_30_times"
        }
        ]
    });

    myTable3.destroy();




    var myTable3_1 = $('#izidatatables3_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "topup 0 360 avg",
            "data": "topup_0_360_avg"
        },
        {
            "title": "topup 0 360 max",
            "data": "topup_0_360_max"
        },
        {
            "title": "topup 0 360 min",
            "data": "topup_0_360_min"
        },

        {
            "title": "topup 0 360 times",
            "data": "topup_0_360_times"
        },
        {
            "title": "topup 0 60 avg",
            "data": "topup_0_60_avg"
        },
        {
            "title": "topup 0 60 max",
            "data": "topup_0_60_max"
        },
        {
            "title": "topup 0 60 min",
            "data": "topup_0_60_min"
        },

        {
            "title": "topup 0 60 times",
            "data": "topup_0_60_times"
        }

        ]
    });

    myTable3_1.destroy();




    var myTable3_1_1 = $('#izidatatables3_1_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [


       {
            "title": "topup 0 90 avg",
            "data": "topup_0_90_avg"
        },

        {
            "title": "topup 0 90 max",
            "data": "topup_0_90_max"
        },

        {
            "title": "topup 0 90 min",
            "data": "topup_0_90_min"
        },


        {
            "title": "topup 0 90 times",
            "data": "topup_0_90_times"
        },


        {
            "title": "topup 180 360 avg",
            "data": "topup_180_360_avg"
        },

        {
            "title": "topup 180 360 max",
            "data": "topup_180_360_max"
        },


        {
            "title": "topup 180 360 min",
            "data": "topup_180_360_min"
        },


        {
            "title": "topup 180 360 times",
            "data": "topup_180_360_times"
        }
        ]
    });

    myTable3_1_1.destroy();






    var myTable3_2 = $('#izidatatables3_2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "topup 30 60 avg",
            "data": "topup_30_60_avg"
        },


        {
            "title": "topup 30 60 max",
            "data": "topup_30_60_max"
        },


        {
            "title": "topup 30 60 min",
            "data": "topup_30_60_min"
        },

        {
            "title": "topup 30 60 times",
            "data": "topup_30_60_times"
        },

        {
            "title": "topup 360 720 avg",
            "data": "topup_360_720_avg"
        },

        {
            "title": "topup 360 720 max",
            "data": "topup_360_720_max"
        },

        {
            "title": "topup 360 720 min",
            "data": "topup_360_720_min"
        },

        {
            "title": "topup 360 720 times",
            "data": "topup_360_720_times"
        }
        ]
    });

    myTable3_2.destroy();




    var myTable3_3 = $('#izidatatables3_3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "topup 60 90 avg",
            "data": "topup_60_90_avg"
        },

        {
            "title": "topup 60 90 max",
            "data": "topup_60_90_max"
        },

        {
            "title": "topup 60 90 min",
            "data": "topup_60_90_min"
        },

        {
            "title": "topup 60 90 times",
            "data": "topup_60_90_times"
        },
        {
            "title": "topup 90 180 avg",
            "data": "topup_90_180_avg"
        },
        {
            "title": "topup 90 180 max",
            "data": "topup_90_180_max"
        },

        {
            "title": "topup 90 180 min",
            "data": "topup_90_180_min"
        },
        {
            "title": "topup 90 180 times",
            "data": "topup_90_180_times"
        }
        ]
    });

    myTable3_3.destroy();



    var myTable3_4 = $('#izidatatables3_4').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "eco",
            "data": "eco"
        },
        {
            "title": "ecp",
            "data": "ecp"
        },
        {
            "title": "ece",
            "data": "ece"
        },

        {
            "title": "ect",
            "data": "ect"
        }
        ]
    });

    myTable3_4.destroy();



    var myTable3_5 = $('#izidatatables3_5').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "multiphoneinquiries 7d",
            "data": "multiphoneinquiries_7d"
        },
        {
            "title": "multiphoneinquiries 14d",
            "data": "multiphoneinquiries_14d"
        },
        {
            "title": "multiphoneinquiries 21d",
            "data": "multiphoneinquiries_21d"
        },
        {
            "title": "multiphoneinquiries 30d",
            "data": "multiphoneinquiries_30d"
        },
        {
            "title": "multiphoneinquiries 60d",
            "data": "multiphoneinquiries_60d"
        },
        {
            "title": "multiphoneinquiries 90d",
            "data": "multiphoneinquiries_90d"
        },
        {
            "title": "multiphoneinquiries total",
            "data": "multiphoneinquiries_total"
        }
        ]
    });

    myTable3_5.destroy();



    var myTable3_5_1 = $('#izidatatables3_5_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "multiidinquiries 7d",
            "data": "multiidinquiries_7d"
        },
        {
            "title": "multiidinquiries 14d",
            "data": "multiidinquiries_14d"
        },
        {
            "title": "multiidinquiries 21d",
            "data": "multiidinquiries_21d"
        },
        {
            "title": "multiidinquiries 30d",
            "data": "multiidinquiries_30d"
        },


        {
            "title": "multiidinquiries 60d",
            "data": "multiidinquiries_60d"
        },
        {
            "title": "multiidinquiries 90d",
            "data": "multiidinquiries_90d"
        },
        {
            "title": "multiidinquiries total",
            "data": "multiidinquiries_total"
        }
        ]
    });

    myTable3_5_1.destroy();



    var myTable3_6 = $('#izidatatables3_6').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "BPJS class",
            "data": "b_class"
        },
        {
            "title": "Have BPJS",
            "data": "b_isopen"
        },
        {
            "title": "BPJS membership",
            "data": "b_membership"
        },
        {
            "title": "BPJS members",
            "data": "b_members"
        },
        {
            "title": "BPJS active Status",
            "data": "b_activeStatus"
        },
        {
            "title": "BPJS active Year",
            "data": "b_activeYear"
        }
        ]
    });

    myTable3_6.destroy();






var submit_type = 2;


var ktp = document.getElementById('ktp').value;
var phone_number = document.getElementById('nomorhp').value;
var nama_lengkap = document.getElementById('nama').value;
debugger
if(submit_type == 1){
    var uri = '/creditFeatured';
}else{
    var uri = '/creditFeaturedLog';
}


$.ajaxSetup({
	 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
	 $.ajax({
				type:'POST',
				// url:'/creditFeatured',
                url : uri,
				data:{ktp:ktp, phone_number:phone_number, nama_lengkap:nama_lengkap},
				success:function(data){
          debugger

          if(data[0] == '0000'){
               alert('Opps KTP Tidak ditemukan pada sistem, Mohon dicoba kirim Kembali');
               return  window.close() ;
          }

          str = data.toString().replace(/\'/g, "'");
          var parsed = JSON.parse(str);
          if(parsed.status == "INVALID_INPUT"){
              return alert(parsed.message);
          }

          var izi1 = {'creditscore' : parsed.message.creditscore,
          'isfacebook' : parsed.message.featurelist.isfacebook,
          'iswhatsapp' : parsed.message.featurelist.iswhatsapp,
          'whatsapp_avatar' : parsed.message.featurelist.whatsapp_avatar,
          'whatsapp_company_account' : parsed.message.featurelist.whatsapp_company_account,
          'whatsapp_signature' : parsed.message.featurelist.whatsapp_signature,
          'whatsapp_updatestatus_time' : parsed.message.featurelist.whatsapp_updatestatus_time
          };

          var data1 = [];
          data1.push(izi1);

          myTable1.clear();
          $.each(data1, function(index, value) {
              myTable1.row.add(value);
          });
          myTable1.draw();





          var izi1_2 = {
          'multiphone_idinfo' : parsed.message.featurelist.multiphone_idinfo,
          'multiphone_phoneinfo_id' : parsed.message.featurelist.multiphone_phoneinfo_id,
          'multiphone_phoneinfo_id_phone' : parsed.message.featurelist.multiphone_phoneinfo_id_phone,
          'multiphone_status' : parsed.message.featurelist.multiphone_status,
          'phoneage' : parsed.message.featurelist.phoneage,
          'phoneage_status' : parsed.message.featurelist.phoneage_status,
          'phonealive_id_num' : parsed.message.featurelist.phonealive_id_num,
          'phonealive_phone_num' : parsed.message.featurelist.phonealive_phone_num,
          'phonealive_status' : parsed.message.featurelist.phonealive_status

          };

          var data1_2 = [];
          data1_2.push(izi1_2);

          myTable1_2.clear();
          $.each(data1_2, function(index, value) {
              myTable1_2.row.add(value);
          });
          myTable1_2.draw();






          var izi1_3 = {

          'phoneinquiries_14d' : parsed.message.featurelist.phoneinquiries_14d,
          'phoneinquiries_180d' : parsed.message.featurelist.phoneinquiries_180d,
          'phoneinquiries_21d' : parsed.message.featurelist.phoneinquiries_21d,
          'phoneinquiries_30d' : parsed.message.featurelist.phoneinquiries_30d,
          'phoneinquiries_360d' : parsed.message.featurelist.phoneinquiries_360d,
          'phoneinquiries_3d' : parsed.message.featurelist.phoneinquiries_3d,
          'phoneinquiries_60d' : parsed.message.featurelist.phoneinquiries_60d,
          'phoneinquiries_90d' : parsed.message.featurelist.phoneinquiries_90d,
          'phoneinquiries_status' : parsed.message.featurelist.phoneinquiries_status,
          'phoneinquiries_total' : parsed.message.featurelist.phoneinquiries_total

          };

          var data1_3 = [];
          data1_3.push(izi1_3);

          myTable1_3.clear();
          $.each(data1_3, function(index, value) {
              myTable1_3.row.add(value);
          });
          myTable1_3.draw();




          var izi1_4 = {
          'phoneinquiriesuname_14d' : parsed.message.featurelist.phoneinquiriesuname_14d,
          'phoneinquiriesuname_180d' : parsed.message.featurelist.phoneinquiriesuname_180d,
          'phoneinquiriesuname_21d' : parsed.message.featurelist.phoneinquiriesuname_21d,
          'phoneinquiriesuname_30d' : parsed.message.featurelist.phoneinquiriesuname_30d,
          'phoneinquiriesuname_360d' : parsed.message.featurelist.phoneinquiriesuname_360d,
          'phoneinquiriesuname_3d' : parsed.message.featurelist.phoneinquiriesuname_3d,
          'phoneinquiriesuname_21d' : parsed.message.featurelist.phoneinquiriesuname_21d,
          'phoneinquiriesuname_60d' : parsed.message.featurelist.phoneinquiriesuname_60d

          };

          var data1_4 = [];
          data1_4.push(izi1_4);

          myTable1_4.clear();
          $.each(data1_4, function(index, value) {
              myTable1_4.row.add(value);
          });
          myTable1_4.draw();




          var izi1_5 = {
          'phoneinquiriesuname_7d' : parsed.message.featurelist.phoneinquiriesuname_7d,
          'phoneinquiriesuname_90d' : parsed.message.featurelist.phoneinquiriesuname_90d,
          'phoneinquiriesuname_status' : parsed.message.featurelist.phoneinquiriesuname_status,
          'phoneinquiriesuname_total' : parsed.message.featurelist.phoneinquiriesuname_total,
          'phonescore' : parsed.message.featurelist.phonescore,
          'phonescore_status' : parsed.message.featurelist.phonescore_status,
          'phoneverify' : parsed.message.featurelist.phoneverify
          };

          var data1_5 = [];
          data1_5.push(izi1_5);

          myTable1_5.clear();
          $.each(data1_5, function(index, value) {
              myTable1_5.row.add(value);
          });
          myTable1_5.draw();



          var izi2= {'A_II_14d' : parsed.message.featurelist.A_II_14d,
          'A_II_180d' : parsed.message.featurelist.A_II_180d,
          'A_II_21d' : parsed.message.featurelist.A_II_21d,
          'A_II_30d' : parsed.message.featurelist.A_II_30d,
          'A_II_360d' : parsed.message.featurelist.A_II_360d,
          'A_II_3d' :  parsed.message.featurelist.A_II_3d,
          'A_II_60d' : parsed.message.featurelist.A_II_60d,
          'A_II_7d' :  parsed.message.featurelist.A_II_7d,
          'A_II_90d' : parsed.message.featurelist.A_II_90d,
          'A_PI_14d' : parsed.message.featurelist.A_PI_14d,
          'A_PI_180d' : parsed.message.featurelist.A_PI_180d,
          'A_PI_21d' : parsed.message.featurelist.A_PI_21d,
          'A_PI_30d' : parsed.message.featurelist.A_PI_30d,
          'A_PI_360d' :  parsed.message.featurelist.A_PI_360d,
          'A_PI_3d' : parsed.message.featurelist.A_PI_3d,
          'A_PI_60d' :  parsed.message.featurelist.A_PI_60d,
          'A_PI_7d' :  parsed.message.featurelist.A_PI_7d,
          'A_PI_90d' : parsed.message.featurelist.A_PI_90d

          };


          var data2 = [];
          data2.push(izi2);

          myTable2.clear();
          $.each(data2, function(index, value) {
              myTable2.row.add(value);
          });
          myTable2.draw();




          var izi2_1= {
          'B_II_14d' : parsed.message.featurelist.B_II_14d,
          'B_II_180d' : parsed.message.featurelist.B_II_180d,
          'B_II_21d' : parsed.message.featurelist.B_II_21d,
          'B_II_30d' : parsed.message.featurelist.B_II_30d,
          'B_II_360d' :  parsed.message.featurelist.B_II_360d,
          'B_II_3d' : parsed.message.featurelist.B_II_3d,
          'B_II_60d' :  parsed.message.featurelist.B_II_60d,
          'B_II_7d' :  parsed.message.featurelist.B_II_7d,
          'B_II_90d' : parsed.message.featurelist.B_II_90d,
          'B_PI_14d' : parsed.message.featurelist.B_PI_14d,
          'B_PI_180d' : parsed.message.featurelist.B_PI_180d,
          'B_PI_21d' : parsed.message.featurelist.B_PI_21d,
          'B_PI_30d' : parsed.message.featurelist.B_PI_30d,
          'B_PI_360d' : parsed.message.featurelist.B_PI_360d,
          'B_PI_3d' : parsed.message.featurelist.B_PI_3d,
          'B_PI_60d' : parsed.message.featurelist.B_PI_60d,
          'B_PI_7d' :  parsed.message.featurelist.B_PI_7d,
          'B_PI_90d' : parsed.message.featurelist.B_PI_90d

          };


          var data2_1 = [];
          data2_1.push(izi2_1);

          myTable2_1.clear();
          $.each(data2_1, function(index, value) {
              myTable2_1.row.add(value);
          });
          myTable2_1.draw();




          var izi2_2= {
          'C_II_14d' :  parsed.message.featurelist.C_II_14d,
          'C_II_180d' : parsed.message.featurelist.C_II_180d,
          'C_II_21d' : parsed.message.featurelist.C_II_21d,
          'C_II_30d' : parsed.message.featurelist.C_II_30d,
          'C_II_360d' :  parsed.message.featurelist.C_II_360d,
          'C_II_3d' :parsed.message.featurelist.C_II_3d,
          'C_II_60d' : parsed.message.featurelist.C_II_60d,
          'C_II_7d' :   parsed.message.featurelist.C_II_7d,
          'C_II_90d' :  parsed.message.featurelist.C_II_90d,
          'C_PI_14d' :  parsed.message.featurelist.C_PI_14d,
          'C_PI_180d' :   parsed.message.featurelist.C_PI_180d,
          'C_PI_21d' : parsed.message.featurelist.C_PI_21d,
          'C_PI_30d' : parsed.message.featurelist.C_PI_30d,
          'C_PI_3d' :  parsed.message.featurelist.C_PI_3d,
          'C_PI_60d' :  parsed.message.featurelist.C_PI_60d,
          'C_PI_7d' : parsed.message.featurelist.C_PI_7d,
          'C_PI_90d' : parsed.message.featurelist.C_PI_90d

          };


          var data2_2 = [];
          data2_2.push(izi2_2);

          myTable2_2.clear();
          $.each(data2_2, function(index, value) {
              myTable2_2.row.add(value);
          });
          myTable2_2.draw();



          var izi2_3= {
          'II_v4_feature_status' :  parsed.message.featurelist.II_v4_feature_status,
          'PI_v4_feature_status' : parsed.message.featurelist.PI_v4_feature_status,
          'identity_address' :   parsed.message.featurelist.identity_address,
          'identity_date_of_birth' : parsed.message.featurelist.identity_date_of_birth,
          'identity_district' :  parsed.message.featurelist.identity_district,
          'identity_gender' : parsed.message.featurelist.identity_gender,
          'identity_city' :  parsed.message.featurelist.identity_city,
          'identity_marital_status' : parsed.message.featurelist.identity_marital_status,
          'identity_match' :  parsed.message.featurelist.identity_match
          };


          var data2_3 = [];
          data2_3.push(izi2_3);

          myTable2_3.clear();
          $.each(data2_3, function(index, value) {
              myTable2_3.row.add(value);
          });
          myTable2_3.draw();







          var izi2_4= {
          'identity_name' :parsed.message.featurelist.identity_name,
          'identity_nationnality' :  parsed.message.featurelist.identity_nationnality,
          'identity_place_of_birth' :   parsed.message.featurelist.identity_place_of_birth,
          'identity_province' :   parsed.message.featurelist.identity_province,
          'identity_religion' :  parsed.message.featurelist.identity_religion,
          'identity_rt' :   parsed.message.featurelist.identity_rt,
          'identity_rw' :  parsed.message.featurelist.identity_rw,
          'identity_status' :  parsed.message.featurelist.identity_status,
          'identity_village' :   parsed.message.featurelist.identity_village,
          'identity_work' :  parsed.message.featurelist.identity_work

          };


          var data2_4 = [];
          data2_4.push(izi2_4);

          myTable2_4.clear();
          $.each(data2_4, function(index, value) {
              myTable2_4.row.add(value);
          });
          myTable2_4.draw();




         var izi2_5= {
          'idinquiries_14d' : parsed.message.featurelist.idinquiries_14d,
          'idinquiries_180d' :  parsed.message.featurelist.idinquiries_180d,
          'idinquiries_21d' :   parsed.message.featurelist.idinquiries_21d,
          'idinquiries_30d' :  parsed.message.featurelist.idinquiries_30d,
          'idinquiries_360d' :   parsed.message.featurelist.idinquiries_360d,
          'idinquiries_3d' :  parsed.message.featurelist.idinquiries_3d,
          'idinquiries_60d' :  parsed.message.featurelist.idinquiries_60d,
          'idinquiries_7d' : parsed.message.featurelist.idinquiries_7d,
          'idinquiries_90d' :   parsed.message.featurelist.idinquiries_90d,
          'idinquiries_status' : parsed.message.featurelist.idinquiries_status,
          'idinquiries_total' :  parsed.message.featurelist.idinquiries_total
          };


          var data2_5 = [];
          data2_5.push(izi2_5);

          myTable2_5.clear();
          $.each(data2_5, function(index, value) {
              myTable2_5.row.add(value);
          });
          myTable2_5.draw();




         var izi2_6= {
          'idinquiriesuname_14d' :parsed.message.featurelist.idinquiriesuname_14d,
          'idinquiriesuname_21d' : parsed.message.featurelist.idinquiriesuname_21d,
          'idinquiriesuname_30d' :    parsed.message.featurelist.idinquiriesuname_30d,
          'idinquiriesuname_360d' :   parsed.message.featurelist.idinquiriesuname_360d,
          'idinquiriesuname_3d' :   parsed.message.featurelist.idinquiriesuname_3d,
          'idinquiriesuname_60d' :    parsed.message.featurelist.idinquiriesuname_60d
          };


          var data2_6 = [];
          data2_6.push(izi2_6);

          myTable2_6.clear();
          $.each(data2_6, function(index, value) {
              myTable2_6.row.add(value);
          });
          myTable2_6.draw();




          var izi2_6_1= {
          'idinquiriesuname_7d' :  parsed.message.featurelist.idinquiriesuname_7d,
          'idinquiriesuname_90d' :   parsed.message.featurelist.idinquiriesuname_90d,
          'idinquiriesuname_status' :   parsed.message.featurelist.idinquiriesuname_status,
          'idinquiriesuname_total' :  parsed.message.featurelist.idinquiriesuname_total,
          'idinquiriesuname_3d' : parsed.message.featurelist.idinquiriesuname_3d
          };


          var data2_6_1 = [];
          data2_6_1.push(izi2_6_1);

          myTable2_6_1.clear();
          $.each(data2_6_1, function(index, value) {
              myTable2_6_1.row.add(value);
          });
          myTable2_6_1.draw();




          var izi2_7= {
          'preference_bank_180d' :  parsed.message.featurelist.preference_bank_180d,
          'preference_bank_270d' :    parsed.message.featurelist.preference_bank_270d,
          'preference_bank_60d' :  parsed.message.featurelist.preference_bank_60d,
          'preference_bank_90d' :   parsed.message.featurelist.preference_bank_90d,
          'preference_ecommerce_180d' :  parsed.message.featurelist.preference_ecommerce_180d,
          'preference_ecommerce_270d' :  parsed.message.featurelist.preference_ecommerce_270d,
          'preference_ecommerce_60d' : parsed.message.featurelist.preference_ecommerce_60d
          };


          var data2_7 = [];
          data2_7.push(izi2_7);

          myTable2_7.clear();
          $.each(data2_7, function(index, value) {
              myTable2_7.row.add(value);
          });
          myTable2_7.draw();



          var izi2_8= {
          'preference_ecommerce_90d' :  parsed.message.featurelist.preference_ecommerce_90d,
          'preference_game_180d' : parsed.message.featurelist.preference_game_180d,
          'preference_game_270d' : parsed.message.featurelist.preference_game_270d,
          'preference_game_60d' :parsed.message.featurelist.preference_game_60d,
          'preference_game_90d' : parsed.message.featurelist.preference_game_90d,
          'preference_lifestyle_180d' :    parsed.message.featurelist.preference_lifestyle_180d
          };


          var data2_8 = [];
          data2_8.push(izi2_8);

          myTable2_8.clear();
          $.each(data2_8, function(index, value) {
              myTable2_8.row.add(value);
          });
          myTable2_8.draw();



          var izi2_9= {
          'preference_lifestyle_270d' :    parsed.message.featurelist.preference_lifestyle_270d,
          'preference_lifestyle_60d' :   parsed.message.featurelist.preference_lifestyle_60d,
          'preference_lifestyle_90d' :    parsed.message.featurelist.preference_lifestyle_90d,
          'preference_status' :   parsed.message.featurelist.preference_status
          };


          var data2_9 = [];
          data2_9.push(izi2_9);

          myTable2_9.clear();
          $.each(data2_9, function(index, value) {
              myTable2_9.row.add(value);
          });
          myTable2_9.draw();











          var izi3= {
          'topup_0_180_avg' : parsed.message.featurelist.topup_0_180_avg,
          'topup_0_180_max' : parsed.message.featurelist.topup_0_180_max,
          'topup_0_180_min' : parsed.message.featurelist.topup_0_180_min,
          'topup_0_180_times' : parsed.message.featurelist.topup_0_180_times,
          'topup_0_30_avg' : parsed.message.featurelist.topup_0_30_avg,
          'topup_0_30_max' : parsed.message.featurelist.topup_0_30_max,
          'topup_0_30_min' :  parsed.message.featurelist.topup_0_30_min,
          'topup_0_30_times' : parsed.message.featurelist.topup_0_30_times
          };


          var data3 = [];
          data3.push(izi3);

          myTable3.clear();
          $.each(data3, function(index, value) {
              myTable3.row.add(value);
          });
          myTable3.draw();



          var izi3_1= {
          'topup_0_360_avg' :  parsed.message.featurelist.topup_0_360_avg,
          'topup_0_360_max' : parsed.message.featurelist.topup_0_360_max,
          'topup_0_360_min' : parsed.message.featurelist.topup_0_360_min,
          'topup_0_360_times' :  parsed.message.featurelist.topup_0_360_times,
          'topup_0_60_avg' :  parsed.message.featurelist.topup_0_60_avg,
          'topup_0_60_max' : parsed.message.featurelist.topup_0_60_max,
          'topup_0_60_min' :  parsed.message.featurelist.topup_0_60_min,
          'topup_0_60_times' :  parsed.message.featurelist.topup_0_60_times
          };


          var data3_1 = [];
          data3_1.push(izi3_1);

          myTable3_1.clear();
          $.each(data3_1, function(index, value) {
              myTable3_1.row.add(value);
          });
          myTable3_1.draw();


          var izi3_1_1= {
          'topup_0_90_avg' : parsed.message.featurelist.topup_0_90_avg,
          'topup_0_90_max' : parsed.message.featurelist.topup_0_90_max,
          'topup_0_90_min' :  parsed.message.featurelist.topup_0_90_min,
          'topup_0_90_times' : parsed.message.featurelist.topup_0_90_times,
          'topup_180_360_avg' : parsed.message.featurelist.topup_180_360_avg,
          'topup_180_360_max' :  parsed.message.featurelist.topup_180_360_max,
          'topup_180_360_min' :  parsed.message.featurelist.topup_180_360_min,
          'topup_180_360_times' :   parsed.message.featurelist.topup_180_360_times
          };


          var data3_1_1 = [];
          data3_1.push(izi3_1_1);

          myTable3_1_1.clear();
          $.each(data3_1_1, function(index, value) {
              myTable3_1_1.row.add(value);
          });
          myTable3_1_1.draw();



          var izi3_2= {
          'topup_30_60_avg' :   parsed.message.featurelist.topup_30_60_avg,
          'topup_30_60_max' :  parsed.message.featurelist.topup_30_60_max,
          'topup_30_60_min' :  parsed.message.featurelist.topup_30_60_min,
          'topup_30_60_times' : parsed.message.featurelist.topup_30_60_times,
          'topup_360_720_avg' :  parsed.message.featurelist.topup_360_720_avg,
          'topup_360_720_max' : parsed.message.featurelist.topup_360_720_max,
          'topup_360_720_min' : parsed.message.featurelist.topup_360_720_min,
          'topup_360_720_times' : parsed.message.featurelist.topup_360_720_times
          };


          var data3_2 = [];
          data3_2.push(izi3_2);

          myTable3_2.clear();
          $.each(data3_2, function(index, value) {
              myTable3_2.row.add(value);
          });
          myTable3_2.draw();




          var izi3_3= {
          'topup_60_90_avg' : parsed.message.featurelist.topup_60_90_avg,
          'topup_60_90_max' :  parsed.message.featurelist.topup_60_90_max,
          'topup_60_90_min' :  parsed.message.featurelist.topup_60_90_min,
          'topup_60_90_times' :  parsed.message.featurelist.topup_60_90_times,
          'topup_90_180_avg' : parsed.message.featurelist.topup_90_180_avg,
          'topup_90_180_max' :  parsed.message.featurelist.topup_90_180_max,
          'topup_90_180_min' : parsed.message.featurelist.topup_90_180_min,
          'topup_90_180_times' :   parsed.message.featurelist.topup_90_180_times
          };


          var data3_3 = [];
          data3_3.push(izi3_3);

          myTable3_3.clear();
          $.each(data3_3, function(index, value) {
              myTable3_3.row.add(value);
          });
          myTable3_3.draw();


          var izi3_4= {
          'eco' : parsed.message.featurelist.eco,
          'ecp' : parsed.message.featurelist.ecp,
          'ece' :   parsed.message.featurelist.ece,
          'ect' :  parsed.message.featurelist.ect

          };


          var data3_4 = [];
          data3_4.push(izi3_4);

          myTable3_4.clear();
          $.each(data3_4, function(index, value) {
              myTable3_4.row.add(value);
          });
          myTable3_4.draw();





          var izi3_5= {
          'multiphoneinquiries_7d' :   parsed.message.featurelist.multiphoneinquiries_7d,
          'multiphoneinquiries_14d' :   parsed.message.featurelist.multiphoneinquiries_14d,
          'multiphoneinquiries_21d' : parsed.message.featurelist.multiphoneinquiries_21d,
          'multiphoneinquiries_30d' : parsed.message.featurelist.multiphoneinquiries_30d,
          'multiphoneinquiries_60d' :  parsed.message.featurelist.multiphoneinquiries_60d,
          'multiphoneinquiries_90d' :  parsed.message.featurelist.multiphoneinquiries_90d,
          'multiphoneinquiries_total' : parsed.message.featurelist.multiphoneinquiries_total

          };


          var data3_5 = [];
          data3_5.push(izi3_5);

          myTable3_5.clear();
          $.each(data3_5, function(index, value) {
              myTable3_5.row.add(value);
          });
          myTable3_5.draw();





          var izi3_5_1= {
          'multiidinquiries_7d' : parsed.message.featurelist.multiidinquiries_7d,
          'multiidinquiries_14d' :  parsed.message.featurelist.multiidinquiries_14d,
          'multiidinquiries_21d' :  parsed.message.featurelist.multiidinquiries_21d,
          'multiidinquiries_30d' :    parsed.message.featurelist.multiidinquiries_30d,
          'multiidinquiries_60d' : parsed.message.featurelist.multiidinquiries_60d,
          'multiidinquiries_90d' :   parsed.message.featurelist.multiidinquiries_90d,
          'multiidinquiries_total' :  parsed.message.featurelist.multiidinquiries_total

          };


          var data3_5_1 = [];
          data3_5_1.push(izi3_5_1);

          myTable3_5_1.clear();
          $.each(data3_5_1, function(index, value) {
              myTable3_5_1.row.add(value);
          });
          myTable3_5_1.draw();

          debugger

            var b_isopen = { null :"Data Tidak Ada", "1":"Yes", "0":"No" };
            var a = b_isopen[parsed.message.featurelist.b_isopen];
            b_isopen = a;


            var b_membership = { null :"Data Tidak Ada",
            "1":"PNS Pusat",
            "2": "PNS Pusat Diperbantukan",
            "3" : "PNS Daerah",
            "4" : "PNS Daerah Diperbantukan",
            "5": "Prajurit AD",
            "6" :"Prajurit AL",
            "7" : "Prajurit AU",
            "8" : "Anggota Polri",
            "9" : "Pejabat Negara",
            "10": "Pegawai Pemerintah dengan Perjanjian Kerja",
            "11" : "Pegawai BUMN",
            "12" : "Pegawai BUMD",
            "13" : "Pegawai Swasta",
            "14" : "Pekerja Mandiri",
            "15" : "Penerima Pensiun PNS",
            "16" : "Penerima Pensiun TNI",
            "17" : "Penerima Pensiun Polri",
            "20" : "Veteran",
            "21" :  "PBI (APBN)",
            "22" : "PBI (APBD)",
            "23" : "Investor",
            "24"  : "Pemberi Kerja",
            "25" : "Penerima Pensiun Swasta",
            "26" : "PNS Mabes dan Kemhan",
            "27" : "Dewan Perwakilan Rakyat Daerah",
            "28" : "Kepala Desa dan Perangkat Desa"
            }

            var b = b_membership[parsed.message.featurelist.b_membership];
            b_membership = b;



            var b_activeStatus = {"0" : "Aktif",
            "2" : "Meninggal",
            "3" : "PHK",
            "4" : "Keluar Kemauan Sendiri",
            "5" : "Habis PKS",
            "6" : "Tidak Ditanggung / Masa Berlaku sudah Berakhir",
            "7" : "Penangguhan Peserta",
            "9" : "Non Aktif Karna Premi",
            "14" : "Bu Lapse",
            "15" : "Non Aktif di Akhir Bulan",
            "18" : "Penangguhan Aktivasi Peserta",
            "20" : "Penangguhan Pembayaran",
            "21" : "Non Aktif Karena Data Ganda",
            "24" : "Non Aktif Oleh Pusat",
            "25" : "Usia Anak PPU >21th - 25th",
            "26" : "Usia Anak PPU >25th",
            "27" : "Masa Berlaku Habis",
            "29" : "Usia Veteran >85th"
            }

            var c = b_activeStatus[parsed.message.featurelist.b_activeStatus];
            b_activeStatus = c;





        var izi3_6= {
            'b_class' :   parsed.message.featurelist.b_class,
            'b_isopen' : b_isopen,
            'b_membership' :   b_membership,
            'b_members' : parsed.message.featurelist.b_members,
            'b_activeStatus' :  b_activeStatus,
            'b_activeYear' :    parsed.message.featurelist.b_activeYear
            };


          var data3_6 = [];
          data3_6.push(izi3_6);

          myTable3_6.clear();
          $.each(data3_6, function(index, value) {
              myTable3_6.row.add(value);
          });
          myTable3_6.draw();


        }
	});
}
</script>
