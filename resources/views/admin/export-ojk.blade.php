<table>
    <thead>
    <tr>
        <th>NOMOR_PINJAMAN</th>
        <th>NOMOR_CUSTOMER</th>
        <th>NAMA_PRODUK</th>
        <th>NAMA_CUSTOMER</th>
        <th>KTP</th>
        <th>TANGGAL_LAHIR</th>
        <th>JENIS_KELAMIN</th>
        <th>STATUS_PENGAJUAN</th>
        <th>PERIODE_PINJAMAN</th>
        <th>BULAN_PINJAMAN</th>
        <th>JATUH_TEMPO</th>
        <th>NOMINAL_PINJAMAN</th>
        <th>NOMINAL_PENGEMBALIAN</th>
        <th>SALDO_PINJAMAN</th>
        <th>NOMINAL_PEMBAYARAN</th>
        <th>BIAYA_ADMINISTRASI</th>
        <th>BUNGA_AWAL</th>
        <th>ADM_PLUS_AWAL</th>
        <th>TENOR_PINJAMAN</th>
        <th>SUKU_BUNGA_PINJAMAN</th>
        <th>SUKU_BUNGA_TUNGGAKAN</th>
        <th>TANGGAL_JATUH_TEMPO_BUNGA</th>
        <th>TANGGAL_REPAYMENT_TERAKHIR</th>
        <th>TANGGAL_PERSETUJUAN</th>
        <th>JUMLAH_HARI_TUNGGAKAN_SAAT_INI</th>
        <th>KATEGORI_DPD</th>
        <th>PROVINCE</th>
        <th>USIA</th>
        <th>KATEGORI_USIA</th>
        <th>AREA_USAHA</th>
        <th>KODE_USAHA</th>
        <th>KETERANGAN_TAMBAHAN</th>
        <th>KLASIFIKASI_PINJAMAN</th>
        <th>BULAN</th>
        <th>JUMLAH_HARI</th>
    </tr>
    </thead>
    <tbody>
    @foreach($dataojk as $ojk)
        <tr>
            <td>{{ $ojk->NOMOR_PINJAMAN }}</td>
            <td>{{ $ojk->NOMOR_CUSTOMER }}</td>
            <td>{{ $ojk->NAMA_PRODUK }}</td>
            <td>{{ $ojk->NAMA_CUSTOMER }}</td>
            <td>{{ $ojk->KTP }}</td>
            <td>{{ $ojk->TANGGAL_LAHIR }}</td>
            <td>{{ $ojk->JENIS_KELAMIN }}</td>
            <td>{{ $ojk->STATUS_PENGAJUAN }}</td>
            <td>{{ $ojk->PERIODE_PINJAMAN }}</td>
            <td>{{ $ojk->BULAN_PINJAMAN }}</td>
            <td>{{ $ojk->JATUH_TEMPO }}</td>
            <td>{{ $ojk->NOMINAL_PINJAMAN }}</td>
            <td>{{ $ojk->NOMINAL_PENGEMBALIAN }}</td>
            <td>{{ $ojk->SALDO_PINJAMAN }}</td>
            <td>{{ $ojk->NOMINAL_PEMBAYARAN }}</td>
            <td>{{ $ojk->BIAYA_ADMINISTRASI }}</td>
            <td>{{ $ojk->BUNGA_AWAL }}</td>
            <td>{{ $ojk->ADM_PLUS_AWAL }}</td>
            <td>{{ $ojk->TENOR_PINJAMAN }}</td>
            <td>{{ $ojk->SUKU_BUNGA_PINJAMAN }}</td>
            <td>{{ $ojk->SUKU_BUNGA_TUNGGAKAN }}</td>
            <td>{{ $ojk->TANGGAL_JATUH_TEMPO_BUNGA }}</td>
            <td>{{ $ojk->TANGGAL_REPAYMENT_TERAKHIR }}</td>
            <td>{{ $ojk->TANGGAL_PERSETUJUAN }}</td>
            <td>{{ $ojk->JUMLAH_HARI_TUNGGAKAN_SAAT_INI }}</td>
            <td>{{ $ojk->KATEGORI_DPD }}</td>
            <td>{{ $ojk->PROVINCE }}</td>
            <td>{{ $ojk->USIA }}</td>
            <td>{{ $ojk->KATEGORI_USIA }}</td>
            <td>{{ $ojk->AREA_USAHA }}</td>
            <td>{{ $ojk->KODE_USAHA }}</td>
            <td>{{ $ojk->KETERANGAN_TAMBAHAN }}</td>
            <td>{{ $ojk->KLASIFIKASI_PINJAMAN }}</td>
            <td>{{ $ojk->BULAN }}</td>
            <td>{{ $ojk->JUMLAH_HARI }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
