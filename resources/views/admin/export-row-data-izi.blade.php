<table>
    <tbody>
        <tr>
            <td>Departement : </td>
            <td>@foreach ($departement as $dept)
                {{ $dept->description }}<br/>
            @endforeach</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td>Periode :</td>
            <td>Tanggal Awal : {{ $startDate }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Akhir : {{ $endDate }}</td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>User</th>
        <th>Email</th>
        <th>Departement</th>
        <th>Nama</th>
        <th>KTP</th>
        <th>Phone</th>
        <th>hitungan hit</th>
        <th>Time Hit</th>
        <th>Data Json</th>
    </tr>
    </thead>
    <tbody>
    @foreach($izihistory as $izi)
        <tr>
            <td>{{ $izi->id }}</td>
            <td>{{ $izi->user }}</td>
            <td>{{ $izi->email }}</td>
            <td>{{ $izi->departement }}</td>
            <td>{{ $izi->cust }}</td>
            <td>'{{ $izi->ktp }}</td>
            <td>{{ $izi->phone }}</td>
            <td>{{ $izi->count_hit }}</td>
            <td>{{ $izi->time_hit }}</td>
            <td>{{ $izi->json }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
