<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Blacklist SIGAP - OJK</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Data Blacklist SIGAP - OJK</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">SIGAP IMPORT DATA</h3>
              </div>
              <!-- /.card-header -->
                <form role="form">
                    <div class="card-body">
                        <a type='button' href='#' data-toggle="modal" data-target="#importExcel" class="btn btn-lg btn-primary"> <span><i class="fas fa-upload"></i> Unggah Data</span></a>
                        <a type='button' href='/sigap/format-sigap.xlsx' class="btn btn-lg btn-warning"> <span><i class="fas fa-download"></i> Unduh Template</span></a>
                    </div>
                    <div class="card-footer">
                    </div>
                </form>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tabledatablacklist" class="col-md-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Data Blacklist SIGAP - OJK </b></h3>
                        </div>

                        <div class="card-body">
                                <table id="listblacklistojk" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>NIK</th>
                                            <th>Expected</th>
                                            <th>Code Densus</th>
                                            <th>Birth</th>
                                            <th>Birth Date</th>
                                            <th>Citizen</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;?>
                                        @foreach ($listblacklistojk as $blacklist)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $blacklist->name }}</td>
                                            <td>{{ $blacklist->nik }}</td>
                                            <td>{{ $blacklist->expected }}</td>
                                            <td>{{ $blacklist->code_densus }}</td>
                                            <td>{{ $blacklist->birth }}</td>
                                            <td>{{ (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$blacklist->birth_date)) ? '' : date('d/m/Y', strtotime($blacklist->birth_date)) }}</td>
                                            <td>{{ $blacklist->citizen }}</td>
                                            <td>{{ $blacklist->address }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                <!--/.col (left) -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
@include('modal.sigap-modal')

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script>
    var table

    $(document).ready( function () {
        table = $('#listblacklistojk').DataTable();
    });

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

</script>
