<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
@include('css.loader')
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cari Data Blacklist Sigap</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Cari Data Blacklist Sigap</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tablehistory" class="col-md-12">

                    @if ($checkCreation > 0)

                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Pembuatan Data</b></h3>

                            <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="alert alert-info alert-dismissible">
                                <h5><i class="icon fas fa-info"></i> Data Sigap Belum di Compare!</h5>
                                Klik button <b>Pembuatan Data</b> untuk mendapatkan kecocokan data antara data Sigap dengan data di Backoffice.
                                <hr>
                                <button type="button" id="creation-btn" class="btn btn-default" onclick="creationData()"><i class="fas fa-retweet"></i>&nbsp;&nbsp;&nbsp;Pembuatan Data</button>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </section>
                    <!-- /.content -->

                    @endif

                    @if ($checkCreation === 0)

                    <div class="card" id="filterSigap">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Filter Data</b></h3>

                            <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text"> Nama </span> </div>
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Cari berdasarkan nama (optional) ...">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text"> KTP </span> </div>
                                        <input type="text" class="form-control" id="ktp" name="ktp" placeholder="Cari berdasarkan KTP (optional) ...">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text"> No Seri Pengajuan </span> </div>
                                        <input type="text" class="form-control" id="pengajuan" name="pengajuan" placeholder="No Seri Pengajuan (optional) ...">
                                    </div>
                                </div>
                                {{-- <div class="col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span> </div>
                                        <input type="text" class="form-control pull-right" id="datesearch" placeholder="Cari berdasarkan rentang tanggal.." value="">
                                    </div>
                                </div> --}}
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <button type="button" id="btncari" class="btn btn-primary"><i class="fas fa-search"></i>&nbsp;&nbsp;&nbsp;Cari</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                    </div>

                    <div class="card" id="resultSigap">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Data Hasil Blacklist Sigap</b></h3>
                        </div>

                        <div class="card-body">
                            {{-- <div class="card-body col-md-12">
                                <h3 class="card-title"><b>Data Hasil Blacklist Sigap</b></h3></br>
                                <hr style="margin-top:5px;">
                                <table id="resultdatablacklist" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID Backoffice</th>
                                            <th>NAMA</th>
                                            <th>NIK</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Tempat Lahir</th>
                                            <th>Negara</th>
                                            <th>Alamat</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div> --}}
                            <div class="card-body col-md-12">
                                <h3 class="card-title"><b>Data Hasil Terduga</b></h3></br>
                                <hr style="margin-top:5px;">
                                <table id="resultdataterduga" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Backoffice</th>
                                            <th>NAMA</th>
                                            <th>NIK</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Tempat Lahir</th>
                                            <th>Negara</th>
                                            <th>Alamat</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>

                    @endif

                  </div>
                      <!-- /.card-body -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->




      </div><!-- /.container-fluid -->


    <!-- /.content -->




  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @include('modal.report-sigap-modal')

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script type="text/javascript">

    //fungsi untuk filtering data berdasarkan tanggal
    // var start_date;
    // var end_date;
    var nama;
    var ktp;
    var pengajuan;
    var dTable;
    var dTable1;

    // // get range date in a month
    // var today = new Date();
    // var dd = String(today.getDate()).padStart(2, '0');
    // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy = today.getFullYear();
    // // set value in datapicker
    // startDateDefault = mm + '/' + 01 + '/' + yyyy;
    // endDateDefault = mm + '/' + dd + '/' + yyyy;
    // // initial value for default data
    // start_date = yyyy  + mm  + 01;
    // end_date = yyyy  + mm  + dd;
    nama = null;
    ktp = null;
    pengajuan = null;

    // var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
    //     var dateStart = parseDateValue(start_date);
    //     var dateEnd = parseDateValue(end_date);
    //     //Kolom tanggal yang akan kita gunakan
    //     var evalDate= parseDateValue(aData[10]);
    //         if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
    //             ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
    //             ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
    //             ( dateStart <= evalDate && evalDate <= dateEnd ) )
    //         {
    //             return true;
    //         }
    //         return false;
    // });

    // // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
    // function parseDateValue(rawDate) {
    //     var dateArray= rawDate.split("/");
    //     var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11
    //     return parsedDate;
    // }

    $(document).ready(function(){

        // $('#resultSigap').hide()

        // //konfigurasi daterangepicker pada input dengan id datesearch
        // $('#datesearch').daterangepicker({
        //     autoUpdateInput: true,
        // });

        // //change the selected date range of that picker
        // $("#datesearch").data('daterangepicker').setStartDate(startDateDefault);
        // $("#datesearch").data('daterangepicker').setEndDate(endDateDefault);

        // //menangani proses saat apply date range
        // $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
        //     $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        //     start_date=picker.startDate.format('YYYYMMDD');
        //     end_date=picker.endDate.format('YYYYMMDD');
        //     $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
        // });

        // $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        //     $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
        // });

        // dTable = $('#resultdatablacklist').DataTable({
        //     lengthChange: true,
        //     fixedHeader: true,
        //     searching: false,
        //     ordering: false,
        //     paging: false,
        //     info: false,
        //     autoWidth: true,
        //     // scrollY: 300,
        //     // scrollX: true,
        //     processing: true,
        //     serverSide: true,
        //     ajax: {
        //         url: "{{ route('getDataSigap') }}",
        //         data: function (data) {
        //             // data.fromDate = start_date
        //             // data.toDate = end_date
        //             data.nama = nama
        //             data.ktp = ktp
        //         },
        //         delay: 10000000,
        //         timeout: 10000000,
        //         error: handleAjaxError
        //     },
        //     columns: [
        //         { data: 'id_backoffice' },
        //         { data: 'nama' },
        //         { data: 'nik' },
        //         { data: 'tanggal_lahir' },
        //         { data: 'tempat_Lahir' },
        //         { data: 'warga_negara' },
        //         { data: 'alamat' },
        //     ]
        // });

        dTable1 = $('#resultdataterduga').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: false,
            ordering: false,
            paging: false,
            info: false,
            autoWidth: true,
            // scrollY: 300,
            // scrollX: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('getDataReportTerduga') }}",
                data: function (data) {
                    // data.fromDate = start_date
                    // data.toDate = end_date
                    data.nama = nama
                    data.ktp = ktp
                    data.pengajuan = pengajuan
                },
                delay: 10000000,
                timeout: 10000000,
                error: handleAjaxError
            },
            columns: [
                { data: 'no' },
                { data: 'id_backoffice' },
                { data: 'nama' },
                { data: 'nik' },
                { data: 'tanggal_lahir' },
                { data: 'tempat_Lahir' },
                { data: 'warga_negara' },
                { data: 'alamat' },
                { data: 'aksi' },
            ]
        });

        $('#btncari').on('click', function(){
            // $('#resultSigap').show()
            dTable1.draw()
        })
    });

    $('#nama').on('change', function(){
        nama = this.value;
    })

    $('#ktp').on('change', function(){
        ktp = this.value;
    })

    $('#pengajuan').on('change', function(){
        pengajuan = this.value;
    })

    function modalTerdugaSigap(sigap, backoffice)
    {
        $('#ref_idsigap').val(sigap)
        $('#ref_idbackoffice').val(backoffice)
    }

    function creationData()
    {
        Swal.fire({
            title: 'Pembuatan Data Terduga?',
            html:
            "Proses ini akan melakukan kecocokan data antara data Sigap dengan data pada Backoffice",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Pembuatan Data!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('getCreationData') }}",
                    beforeSend: function() {
                        $('#loader').removeClass('hidden')
                    },
                    success: function (data) {
                        Swal.fire(
                            'Pembuatan Data successfully!',
                            'Berhasil melakukan pencocokan data Sigap dengan data Backoffice',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    complete: function(){
                        $('#loader').addClass('hidden')
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal Pembuatan Data',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

</script>
