<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Master Roles</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Master Roles</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                    <a type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="create-new-roles"> <span><i class="fas fa-university"></i>  Tambah Master Roles</span></a>
                    {{-- <a id="toolhapus" type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="hapus-roles" onclick="deleteroles($('#id').val())"> <span><i class="fas fa-trash-alt"></i>  Hapus Master Roles</span></a> --}}
                </div>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tableroles" class="col-md-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Master Roles </b></h3>
                        </div>

                        <div class="card-body">
                                <table id="listroles" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th style="display: none">ID</th>
                                            <th style="display: none">Code</th>
                                            <th>Name</th>
                                            <th style="display: none">Status</th>
                                            <th>Status</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; ?>
                                        @foreach ($listroles as $roles)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td style="display: none">{{ $roles->id }}</td>
                                            <td style="display: none">{{ $roles->code }}</td>
                                            <td>{{ $roles->name }}</td>
                                            <td style="display: none">{{ $roles->status }}</td>
                                            <td><?php
                                                if ($roles->status) {
                                                    echo '<h6><span class="badge badge-success">Enabled</span></h6>';
                                                } else {
                                                    echo '<h6><span class="badge badge-danger">Disabled</span></h6>';
                                                }
                                            ?></td>
                                            <td>{{ $roles->description }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                <!-- left column -->
                <div class="col-md-4" id="detailroles">
                    <div class="card card-danger">
                    <div class="card-header back-ops-okp2p">
                        <h3 class="card-title">Detail Master Roles</h3>
                    </div>
                    <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form-horizontal" method="POST" action="{{ route('roles-edit') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" name="id" id="id">
                                <div class="form-group">
                                    <label for="code" class="col-sm-12 control-label">Code</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('code') is-invalid @enderror" id="code" name="code" placeholder="Enter Code" value="" maxlength="50" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Name</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter Name" value="" maxlength="300" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="status" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select id="status" name="status" class="form-control">
                                            <option value="">--- Pilih Status ---</option>
                                            <option value="0">Disabled</option>
                                            <option value="1">Enabled</option>
                                        </select>

                                        @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="col-sm-12 control-label">Description</label>
                                    <div class="col-sm-12">
                                        <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Enter Description" value="" maxlength="300"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" value="create">Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--/.col (left) -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    {{-- MODALS --}}
    <div class="modal fade" id="ajax-roles-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="rolesCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <form id="rolesForm" name="rolesForm" class="form-horizontal" method="POST" action="{{ route('roles-store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="code" class="col-sm-12 control-label">Code</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('code') autofocus is-invalid @enderror" name="code" value="{{ $code+1 }}" placeholder="Enter Code" value="{{ old('code') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-12 control-label">Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter Name" value="{{ old('name') }}" maxlength="300" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-sm-12 control-label">Status</label>
                            <div class="col-sm-12">
                                <select name="status" class="form-control">
                                    <option value="">--- Pilih Status ---</option>
                                    <option value="0">Disabled</option>
                                    <option value="1">Enabled</option>
                                </select>

                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-12 control-label">Description</label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Enter Description"  maxlength="300">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                         <button type="submit" class="btn btn-primary" id="btn-save" value="create">Tambah
                         </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

@include('js.toast-info')

<script>
    var table

    $(document).ready( function () {
        $('#detailroles').hide()
        $('#toolhapus').hide()
        document.getElementById("tableroles").className = "col-md-12";

        table = $('#listroles').DataTable();

        $('#listroles tbody').on( 'click', 'tr', function () {

            var datauser = table.row( this ).data()
            $('#detailroles').show()
            $('#toolhapus').show()
            document.getElementById("tableroles").className = "col-md-8";
            $('#id').val(datauser[1])
            $('#code').val(datauser[2])
            $('#name').val(datauser[3])
            $('#status').val(datauser[4])
            $('#description').val(datauser[6])

        } );

        $('#listroles tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('row_selected') ) {
                $(this).removeClass('row_selected')

                $('#detailroles').hide()
                $('#toolhapus').hide()
                document.getElementById("tableroles").className = "col-md-12";
                $('#id').val('')
                $('#code').val('')
                $('#name').val('')
                $('#status').val('')
                $('#description').val('')
            }
            else {
                table.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected')
            }
        } );

        /*  When user click add user button */
        $('#create-new-roles').click(function () {
            $('#btn-save').val("create-roles")
            $('#id').val('')
            $('#rolesForm').trigger("reset")
            $('#rolesCrudModal').html("Tambah Master Roles")
            $('#ajax-roles-modal').modal('show')
        });
    });

    function deleteroles(id)
    {
        var code = $('#code').val()
        var name = $('#name').val()
        Swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            html:
            "Code : <b>"+code+"</b> </br> " +
            "Name : <b>"+name+"</b> </br>  </br> " +
            "Anda tidak akan dapat mengembalikan ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "roles/delete/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Master Roles berhasil terhapus',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal menghapus Master Roles',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

</script>
