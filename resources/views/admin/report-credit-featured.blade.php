<html>
<head>
	<title>OKP2P IZI DATA</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>OKP2P X IZI DATA</h4>
		<h6><a target="_blank" href="https://www.okp2p.co.id/">www.okp2p.co.id</a></h5>
	</center>

    <section class="content">


        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">

                    <div class="card">
                      <div class="card-header back-ops-okp2p">
                        <h3 class="card-title"><b>WHATSAPP & Facebook</b></h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                            <table id="izidatatables1" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                      </div>
                      <!-- /.card-body -->
                    </div>


                    <div class="card">
                      <div class="card-header back-ops-okp2p">
                        <h3 class="card-title"><b>PENGECEKAN TELEPON</b></h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <table id="izidatatables1_2" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                      </div>



                      <div class="card-body">

                        <table id="izidatatables1_3" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                      </div>




                      <div class="card-body">

                        <table id="izidatatables1_4" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                      </div>

                      <div class="card-body">
                        <table id="izidatatables1_5" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                  </div>


                  <div class="card">
                    <div class="card-header back-ops-okp2p">
                      <h3 class="card-title"><b>RIWAYAT SKOR</b></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                      <table id="izidatatables2" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>


                    </div>

                    <div class="card-body">
                      <table id="izidatatables2_1" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables2_2" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    <!-- /.card-body -->
                  </div>

                  <div class="card">
                     <div class="card-header back-ops-okp2p">
                         <h3 class="card-title"><b>DETIL IDENTITAS</b></h3>
                     </div>
                     <div class="card-body">
                      <table id="izidatatables2_3" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                    <div class="card-body">
                      <table id="izidatatables2_4" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                  </div>


                  <div class="card">
                     <div class="card-header back-ops-okp2p">
                         <h3 class="card-title"><b>PENGECEKAN IDENTITAS</b></h3>
                     </div>
                     <div class="card-body">
                      <table id="izidatatables2_5" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                    <div class="card-body">
                      <table id="izidatatables2_6" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables2_6_1" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                  </div>

                  <div class="card">
                    <div class="card-header back-ops-okp2p">
                            <b>PREFERENSI BANK</b>
                    </div>
                    <div class="card-body">
                      <table id="izidatatables2_7" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables2_8" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    <div class="card-body">
                        <table id="izidatatables2_9" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                  </div>


                  <div class="card">
                    <div class="card-header back-ops-okp2p">
                      <h3 class="card-title"><b>PENGECEKAN TOP & MULTI ID</b></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table id="izidatatables3" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>



                    <div class="card-body">
                      <table id="izidatatables3_1" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_1_1" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_2" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_3" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables3_4" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables3_5" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_6" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>


                    <div class="card-body">
                      <table id="izidatatables3_7" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>

                    <div class="card-body">
                      <table id="izidatatables3_8" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                      <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

</body>
</html>


<script>

submit();

function submit(){



var ktp = document.getElementById('ktp').value;
var phone_number = document.getElementById('phone_number').value;
var nama_lengkap = document.getElementById('nama_lengkap').value;


localStorage.setItem("ktp", ktp);
localStorage.setItem("phone_number", phone_number);
localStorage.setItem("nama_lengkap", nama_lengkap);


var logs;
var myTable1 = $('#izidatatables1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
          {
            "title": "Credit Score",
            "data": "creditscore"
        },
        {
            "title": "Is Facebook",
            "data": "isfacebook"
        },
        {
            "title": "Is Whatsapp",
            "data": "iswhatsapp"
        }, {
            "title": "Whatsapp Avatar",
            "data": "whatsapp_avatar"
        }, {
            "title": "Whatsapp Company Account",
            "data": "whatsapp_company_account"
        }, {
            "title": "Whatsapp Signature",
            "data": "whatsapp_signature"
        }, {
            "title": "Whatsapp Updatestatus Time",
            "data": "whatsapp_updatestatus_time"
        }
        ]
    });


    myTable1.destroy();




    var myTable1_2 = $('#izidatatables1_2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "Multiphone Idinfo",
            "data": "multiphone_idinfo"
        },
        {
            "title": "Multiphone Phoneinfo Id",
            "data": "multiphone_phoneinfo_id"
        },
        {
            "title": "Multiphone Phoneinfo Id Phone",
            "data": "multiphone_phoneinfo_id_phone"
        },
        {
            "title": "Multiphone Status",
            "data": "multiphone_status"
        },
        {
            "title": "Phoneage",
            "data": "phoneage"
        },

        {
            "title": "Phoneage Status",
            "data": "phoneage_status"
        },
        {
            "title": "Phonealive Id_Num",
            "data": "phonealive_id_num"
        },
        {
            "title": "Phonealive phone Num",
            "data": "phonealive_phone_num"
        },
        {
            "title": "Phonealive Status",
            "data": "phonealive_status"
        }
        ]
    });


    myTable1_2.destroy();







    var myTable1_3 = $('#izidatatables1_3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [


        {
            "title": "phoneinquiries_14d",
            "data": "phoneinquiries_14d"
        },

       {
            "title": "phoneinquiries_180d",
            "data": "phoneinquiries_180d"
        },

        {
            "title": "phoneinquiries_21d",
            "data": "phoneinquiries_21d"
        },

        {
            "title": "phoneinquiries_30d",
            "data": "phoneinquiries_30d"
        },


        {
            "title": "phoneinquiries_360d",
            "data": "phoneinquiries_360d"
        },


        {
            "title": "phoneinquiries_3d",
            "data": "phoneinquiries_3d"
        },

        {
            "title": "phoneinquiries_60d",
            "data": "phoneinquiries_60d"
        },


        {
            "title": "phoneinquiries_90d",
            "data": "phoneinquiries_90d"
        },


        {
            "title": "phoneinquiries_status",
            "data": "phoneinquiries_status"
        },


        {
            "title": "phoneinquiries_total",
            "data": "phoneinquiries_total"
        }

        ]
    });


    myTable1_3.destroy();






    var myTable1_4 = $('#izidatatables1_4').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "phoneinquiriesuname_14d",
            "data": "phoneinquiriesuname_14d"
        },

        {
            "title": "phoneinquiriesuname_180d",
            "data": "phoneinquiriesuname_180d"
        },

        {
            "title": "phoneinquiriesuname_21d",
            "data": "phoneinquiriesuname_21d"
        },

        {
            "title": "phoneinquiriesuname_30d",
            "data": "phoneinquiriesuname_30d"
        },

        {
            "title": "phoneinquiriesuname_360d",
            "data": "phoneinquiriesuname_360d"
        },

        {
            "title": "phoneinquiriesuname_3d",
            "data": "phoneinquiriesuname_3d"
        },

        {
            "title": "phoneinquiriesuname_21d",
            "data": "phoneinquiriesuname_21d"
        },

        {
            "title": "phoneinquiriesuname_60d",
            "data": "phoneinquiriesuname_60d"
        }
        ]
    });


    myTable1_4.destroy();




    var myTable1_5 = $('#izidatatables1_5').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "phoneinquiriesuname_7d",
            "data": "phoneinquiriesuname_7d"
        },

        {
            "title": "phoneinquiriesuname_90d",
            "data": "phoneinquiriesuname_90d"
        },
        {
            "title": "phoneinquiriesuname_status",
            "data": "phoneinquiriesuname_status"
        },
        {
            "title": "phoneinquiriesuname_total",
            "data": "phoneinquiriesuname_total"
        },

        {
            "title": "phonescore",
            "data": "phonescore"
        },
        {
            "title": "phonescore_status",
            "data": "phonescore_status"
        },
        {
            "title": "phoneverify",
            "data": "phoneverify"
        }
        ]
    });


    myTable1_5.destroy();




// table 2

var myTable2 = $('#izidatatables2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
          {
            "title": "A_II_14d",
            "data": "A_II_14d"
        },
        {
            "title": "A_II_180d",
            "data": "A_II_180d"
        },
        {
            "title": "A_II_21d",
            "data": "A_II_21d"
        }, {
            "title": "A_II_30d",
            "data": "A_II_30d"
        }, {
            "title": "A_II_360d",
            "data": "A_II_360d"
        }, {
            "title": "A_II_3d",
            "data": "A_II_3d"
        }, {
            "title": "A_II_60d",
            "data": "A_II_60d"
        },
        {
            "title": "A_II_7d",
            "data": "A_II_7d"
        },
        {
            "title": "A_II_90d",
            "data": "A_II_90d"
        },
        {
            "title": "A_PI_14d",
            "data": "A_PI_14d"
        },
        {
            "title": "A_PI_180d",
            "data": "A_PI_180d"
        },
        {
            "title": "A_PI_21d",
            "data": "A_PI_21d"
        },

        {
            "title": "A_PI_30d",
            "data": "A_PI_30d"
        },
        {
            "title": "A_PI_360d",
            "data": "A_PI_360d"
        },
        {
            "title": "A_PI_3d",
            "data": "A_PI_3d"
        },
        {
            "title": "A_PI_60d",
            "data": "A_PI_60d"
        },

        {
            "title": "A_PI_7d",
            "data": "A_PI_7d"
        },

       {
            "title": "A_PI_90d",
            "data": "A_PI_90d"
        }


        ]
    });

    myTable2.destroy();



    var myTable2_1 = $('#izidatatables2_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
          {
            "title": "B_II_14d",
            "data": "B_II_14d"
        },

        {
            "title": "B_II_180d",
            "data": "B_II_180d"
        },


        {
            "title": "B_II_21d",
            "data": "B_II_21d"
        },
        {
            "title": "B_II_30d",
            "data": "B_II_30d"
        },

        {
            "title": "B_II_360d",
            "data": "B_II_360d"
        },


        {
            "title": "B_II_3d",
            "data": "B_II_3d"
        },


        {
            "title": "B_II_60d",
            "data": "B_II_60d"
        },


        {
            "title": "B_II_7d",
            "data": "B_II_7d"
        },


        {
            "title": "B_II_90d",
            "data": "B_II_90d"
        },


        {
            "title": "B_PI_14d",
            "data": "B_PI_14d"
        },

        {
            "title": "B_PI_180d",
            "data": "B_PI_180d"
        },

        {
            "title": "B_PI_21d",
            "data": "B_PI_21d"
        },

        {
            "title": "B_PI_30d",
            "data": "B_PI_30d"
        },

        {
            "title": "B_PI_360d",
            "data": "B_PI_360d"
        },

        {
            "title": "B_PI_3d",
            "data": "B_PI_3d"
        },

        {
            "title": "B_PI_60d",
            "data": "B_PI_60d"
        },

        {
            "title": "B_PI_7d",
            "data": "B_PI_7d"
        },

        {
            "title": "B_PI_90d",
            "data": "B_PI_90d"
        }
        ]
    });

    myTable2_1.destroy();



    var myTable2_2 = $('#izidatatables2_2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "C_II_14d",
            "data": "C_II_14d"
        },
        {
            "title": "C_II_180d",
            "data": "C_II_180d"
        },
        {
            "title": "C_II_21d",
            "data": "C_II_21d"
        },

        {
            "title": "C_II_30d",
            "data": "C_II_30d"
        },
        {
            "title": "C_II_360d",
            "data": "C_II_360d"
        },
        {
            "title": "C_II_3d",
            "data": "C_II_3d"
        },



        {
            "title": "C_II_60d",
            "data": "C_II_60d"
        },
        {
            "title": "C_II_7d",
            "data": "C_II_7d"
        },
        {
            "title": "C_II_90d",
            "data": "C_II_90d"
        },
        {
            "title": "C_PI_14d",
            "data": "C_PI_14d"
        },
        {
            "title": "C_PI_180d",
            "data": "C_PI_180d"
        },
        {
            "title": "C_PI_21d",
            "data": "C_PI_21d"
        },
        {
            "title": "C_PI_30d",
            "data": "C_PI_30d"
        },
        {
            "title": "C_PI_3d",
            "data": "C_PI_3d"
        },
        {
            "title": "C_PI_60d",
            "data": "C_PI_60d"
        },
        {
            "title": "C_PI_7d",
            "data": "C_PI_7d"
        },
        {
            "title": "C_PI_90d",
            "data": "C_PI_90d"
        }
        ]
    });

    myTable2_2.destroy();



    var myTable2_3 = $('#izidatatables2_3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "II_v4_feature_status",
            "data": "II_v4_feature_status"
        },
        {
            "title": "PI_v4_feature_status",
            "data": "PI_v4_feature_status"
        },
        {
            "title": "identity_address",
            "data": "identity_address"
        },
        {
            "title": "identity_date_of_birth",
            "data": "identity_date_of_birth"
        },
        {
            "title": "identity_district",
            "data": "identity_district"
        },
        {
            "title": "identity_gender",
            "data": "identity_gender"
        },
        {
            "title": "identity_city",
            "data": "identity_city"
        },
        {
            "title": "identity_marital_status",
            "data": "identity_marital_status"
        },
        {
            "title": "identity_match",
            "data": "identity_match"
        }
        ]
    });

    myTable2_3.destroy();





    var myTable2_4 = $('#izidatatables2_4').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "identity_name",
            "data": "identity_name"
        },
        {
            "title": "identity_nationnality",
            "data": "identity_nationnality"
        },
        {
            "title": "identity_place_of_birth",
            "data": "identity_place_of_birth"
        },


        {
            "title": "identity_province",
            "data": "identity_province"
        },
        {
            "title": "identity_religion",
            "data": "identity_religion"
        },
        {
            "title": "identity_rt",
            "data": "identity_rt"
        },
        {
            "title": "identity_rw",
            "data": "identity_rw"
        },
        {
            "title": "identity_status",
            "data": "identity_status"
        },
        {
            "title": "identity_village",
            "data": "identity_village"
        },
        {
            "title": "identity_work",
            "data": "identity_work"
        }
        ]
    });

    myTable2_4.destroy();





    var myTable2_5 = $('#izidatatables2_5').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "idinquiries_14d",
            "data": "idinquiries_14d"
        },
        {
            "title": "idinquiries_180d",
            "data": "idinquiries_180d"
        },
        {
            "title": "idinquiries_21d",
            "data": "idinquiries_21d"
        },
        {
            "title": "idinquiries_30d",
            "data": "idinquiries_30d"
        },
        {
            "title": "idinquiries_360d",
            "data": "idinquiries_360d"
        },
        {
            "title": "idinquiries_3d",
            "data": "idinquiries_3d"
        },
        {
            "title": "idinquiries_60d",
            "data": "idinquiries_60d"
        },
        {
            "title": "idinquiries_7d",
            "data": "idinquiries_7d"
        },
        {
            "title": "idinquiries_90d",
            "data": "idinquiries_90d"
        },
        {
            "title": "idinquiries_status",
            "data": "idinquiries_status"
        },
        {
            "title": "idinquiries_total",
            "data": "idinquiries_total"
        }
        ]
    });

    myTable2_5.destroy();




    var myTable2_6 = $('#izidatatables2_6').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "idinquiriesuname_14d",
            "data": "idinquiriesuname_14d"
        },
        {
            "title": "idinquiriesuname_21d",
            "data": "idinquiriesuname_21d"
        },
        {
            "title": "idinquiriesuname_30d",
            "data": "idinquiriesuname_30d"
        },
        {
            "title": "idinquiriesuname_360d",
            "data": "idinquiriesuname_360d"
        },
        {
            "title": "idinquiriesuname_3d",
            "data": "idinquiriesuname_3d"
        },
        {
            "title": "idinquiriesuname_60d",
            "data": "idinquiriesuname_60d"
        }
        ]
    });

    myTable2_6.destroy();




    var myTable2_6_1 = $('#izidatatables2_6_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "idinquiriesuname_7d",
            "data": "idinquiriesuname_7d"
        },
        {
            "title": "idinquiriesuname_90d",
            "data": "idinquiriesuname_90d"
        },
        {
            "title": "idinquiriesuname_status",
            "data": "idinquiriesuname_status"
        },
        {
            "title": "idinquiriesuname_total",
            "data": "idinquiriesuname_total"
        },
        {
            "title": "idinquiriesuname_3d",
            "data": "idinquiriesuname_3d"
        }
        ]
    });

    myTable2_6_1.destroy();





    var myTable2_7 = $('#izidatatables2_7').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "preference_bank_180d",
            "data": "preference_bank_180d"
        },
        {
            "title": "preference_bank_270d",
            "data": "preference_bank_270d"
        },
        {
            "title": "preference_bank_60d",
            "data": "preference_bank_60d"
        },
        {
            "title": "preference_bank_90d",
            "data": "preference_bank_90d"
        },
        {
            "title": "preference_ecommerce_180d",
            "data": "preference_ecommerce_180d"
        },
        {
            "title": "preference_ecommerce_270d",
            "data": "preference_ecommerce_270d"
        },
        {
            "title": "preference_ecommerce_60d",
            "data": "preference_ecommerce_60d"
        }
        ]
    });

    myTable2_7.destroy();



    var myTable2_8 = $('#izidatatables2_8').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "preference_ecommerce_90d",
            "data": "preference_ecommerce_90d"
        },


        {
            "title": "preference_game_180d",
            "data": "preference_game_180d"
        },
        {
            "title": "preference_game_270d",
            "data": "preference_game_270d"
        },
        {
            "title": "preference_game_60d",
            "data": "preference_game_60d"
        },
        {
            "title": "preference_game_90d",
            "data": "preference_game_90d"
        },
        {
            "title": "preference_lifestyle_180d",
            "data": "preference_lifestyle_180d"
        }
        ]
    });

    myTable2_8.destroy();



    var myTable2_9 = $('#izidatatables2_9').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "preference_lifestyle_270d",
            "data": "preference_lifestyle_270d"
        },


        {
            "title": "preference_lifestyle_60d",
            "data": "preference_lifestyle_60d"
        },
        {
            "title": "preference_lifestyle_90d",
            "data": "preference_lifestyle_90d"
        },
        {
            "title": "preference_status",
            "data": "preference_status"
        }
        ]
    });


    myTable2_9.destroy();



    var myTable3 = $('#izidatatables3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "topup_0_180_avg",
            "data": "topup_0_180_avg"
        },
        {
            "title": "topup_0_180_max",
            "data": "topup_0_180_max"
        }, {
            "title": "topup_0_180_min",
            "data": "topup_0_180_min"
        }, {
            "title": "topup_0_180_times",
            "data": "topup_0_180_times"
        }, {
            "title": "topup_0_30_avg",
            "data": "topup_0_30_avg"
        }, {
            "title": "topup_0_30_max",
            "data": "topup_0_30_max"
        },
        {
            "title": "topup_0_30_min",
            "data": "topup_0_30_min"
        },
        {
            "title": "topup_0_30_times",
            "data": "topup_0_30_times"
        }
        ]
    });

    myTable3.destroy();




    var myTable3_1 = $('#izidatatables3_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "topup_0_360_avg",
            "data": "topup_0_360_avg"
        },
        {
            "title": "topup_0_360_max",
            "data": "topup_0_360_max"
        },
        {
            "title": "topup_0_360_min",
            "data": "topup_0_360_min"
        },

        {
            "title": "topup_0_360_times",
            "data": "topup_0_360_times"
        },
        {
            "title": "topup_0_60_avg",
            "data": "topup_0_60_avg"
        },
        {
            "title": "topup_0_60_max",
            "data": "topup_0_60_max"
        },
        {
            "title": "topup_0_60_min",
            "data": "topup_0_60_min"
        },

        {
            "title": "topup_0_60_times",
            "data": "topup_0_60_times"
        }

        ]
    });

    myTable3_1.destroy();




    var myTable3_1_1 = $('#izidatatables3_1_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [


       {
            "title": "topup_0_90_avg",
            "data": "topup_0_90_avg"
        },

        {
            "title": "topup_0_90_max",
            "data": "topup_0_90_max"
        },

        {
            "title": "topup_0_90_min",
            "data": "topup_0_90_min"
        },


        {
            "title": "topup_0_90_times",
            "data": "topup_0_90_times"
        },


        {
            "title": "topup_180_360_avg",
            "data": "topup_180_360_avg"
        },

        {
            "title": "topup_180_360_max",
            "data": "topup_180_360_max"
        },


        {
            "title": "topup_180_360_min",
            "data": "topup_180_360_min"
        },


        {
            "title": "topup_180_360_times",
            "data": "topup_180_360_times"
        }
        ]
    });

    myTable3_1_1.destroy();






    var myTable3_2 = $('#izidatatables3_2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "topup_30_60_avg",
            "data": "topup_30_60_avg"
        },


        {
            "title": "topup_30_60_max",
            "data": "topup_30_60_max"
        },


        {
            "title": "topup_30_60_min",
            "data": "topup_30_60_min"
        },

        {
            "title": "topup_30_60_times",
            "data": "topup_30_60_times"
        },

        {
            "title": "topup_360_720_avg",
            "data": "topup_360_720_avg"
        },

        {
            "title": "topup_360_720_max",
            "data": "topup_360_720_max"
        },

        {
            "title": "topup_360_720_min",
            "data": "topup_360_720_min"
        },

        {
            "title": "topup_360_720_times",
            "data": "topup_360_720_times"
        }
        ]
    });

    myTable3_2.destroy();




    var myTable3_3 = $('#izidatatables3_3').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "topup_60_90_avg",
            "data": "topup_60_90_avg"
        },

        {
            "title": "topup_60_90_max",
            "data": "topup_60_90_max"
        },

        {
            "title": "topup_60_90_min",
            "data": "topup_60_90_min"
        },

        {
            "title": "topup_60_90_times",
            "data": "topup_60_90_times"
        },
        {
            "title": "topup_90_180_avg",
            "data": "topup_90_180_avg"
        },
        {
            "title": "topup_90_180_max",
            "data": "topup_90_180_max"
        },

        {
            "title": "topup_90_180_min",
            "data": "topup_90_180_min"
        },
        {
            "title": "topup_90_180_times",
            "data": "topup_90_180_times"
        }
        ]
    });

    myTable3_3.destroy();



    var myTable3_4 = $('#izidatatables3_4').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [

        {
            "title": "eco",
            "data": "eco"
        },
        {
            "title": "ecp",
            "data": "ecp"
        },
        {
            "title": "ece",
            "data": "ece"
        },

        {
            "title": "ect",
            "data": "ect"
        }
        ]
    });

    myTable3_4.destroy();



    var myTable3_5 = $('#izidatatables3_5').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "multiphoneinquiries_7d",
            "data": "multiphoneinquiries_7d"
        },
        {
            "title": "multiphoneinquiries_14d",
            "data": "multiphoneinquiries_14d"
        },
        {
            "title": "multiphoneinquiries_21d",
            "data": "multiphoneinquiries_21d"
        },
        {
            "title": "multiphoneinquiries_30d",
            "data": "multiphoneinquiries_30d"
        },
        {
            "title": "multiphoneinquiries_60d",
            "data": "multiphoneinquiries_60d"
        },
        {
            "title": "multiphoneinquiries_90d",
            "data": "multiphoneinquiries_90d"
        },
        {
            "title": "multiphoneinquiries_total",
            "data": "multiphoneinquiries_total"
        }
        ]
    });

    myTable3_5.destroy();



    var myTable3_5_1 = $('#izidatatables3_5_1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "multiidinquiries_7d",
            "data": "multiidinquiries_7d"
        },
        {
            "title": "multiidinquiries_14d",
            "data": "multiidinquiries_14d"
        },
        {
            "title": "multiidinquiries_21d",
            "data": "multiidinquiries_21d"
        },
        {
            "title": "multiidinquiries_30d",
            "data": "multiidinquiries_30d"
        },


        {
            "title": "multiidinquiries_60d",
            "data": "multiidinquiries_60d"
        },
        {
            "title": "multiidinquiries_90d",
            "data": "multiidinquiries_90d"
        },
        {
            "title": "multiidinquiries_total",
            "data": "multiidinquiries_total"
        }
        ]
    });

    myTable3_5_1.destroy();



    var myTable3_6 = $('#izidatatables3_6').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "data": [],
        "columns": [
        {
            "title": "b_class",
            "data": "b_class"
        },
        {
            "title": "b_isopen",
            "data": "b_isopen"
        },
        {
            "title": "b_membership",
            "data": "b_membership"
        },
        {
            "title": "b_members",
            "data": "b_members"
        },
        {
            "title": "b_activeStatus",
            "data": "b_activeStatus"
        },
        {
            "title": "b_activeYear",
            "data": "b_activeYear"
        }
        ]
    });

    myTable3_6.destroy();






var submit_type = 2;

var ktp = '32023032411930003';
var phone_number = '085624282247';
var nama_lengkap ='Rendy Novian';

// var ktp = document.getElementById('ktp').value;
// var phone_number = document.getElementById('phone_number').value;
// var nama_lengkap = document.getElementById('nama_lengkap').value;
debugger
if(submit_type == 1){
    var uri = '/creditFeatured';
}else{
    var uri = '/creditFeaturedLog';
}


$.ajaxSetup({
	 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
	 $.ajax({
				type:'POST',
				// url:'/creditFeatured',
                url : uri,
				data:{ktp:ktp, phone_number:phone_number, nama_lengkap:nama_lengkap},
				success:function(data){
          debugger

          if(data[0] == '0000'){
              return alert('KTP Tidak ditemukan pada sistem');
          }

          str = data.toString().replace(/\'/g, '"');
          var parsed = JSON.parse(str);
          if(parsed.status == "INVALID_INPUT"){
              return alert(parsed.message);
          }

          var izi1 = {'creditscore' : parsed.message.creditscore,
          'isfacebook' : parsed.message.featurelist.isfacebook,
          'iswhatsapp' : parsed.message.featurelist.iswhatsapp,
          'whatsapp_avatar' : parsed.message.featurelist.whatsapp_avatar,
          'whatsapp_company_account' : parsed.message.featurelist.whatsapp_company_account,
          'whatsapp_signature' : parsed.message.featurelist.whatsapp_signature,
          'whatsapp_updatestatus_time' : parsed.message.featurelist.whatsapp_updatestatus_time
          };

          var data1 = [];
          data1.push(izi1);

          myTable1.clear();
          $.each(data1, function(index, value) {
              myTable1.row.add(value);
          });
          myTable1.draw();





          var izi1_2 = {
          'multiphone_idinfo' : parsed.message.featurelist.multiphone_idinfo,
          'multiphone_phoneinfo_id' : parsed.message.featurelist.multiphone_phoneinfo_id,
          'multiphone_phoneinfo_id_phone' : parsed.message.featurelist.multiphone_phoneinfo_id_phone,
          'multiphone_status' : parsed.message.featurelist.multiphone_status,
          'phoneage' : parsed.message.featurelist.phoneage,
          'phoneage_status' : parsed.message.featurelist.phoneage_status,
          'phonealive_id_num' : parsed.message.featurelist.phonealive_id_num,
          'phonealive_phone_num' : parsed.message.featurelist.phonealive_phone_num,
          'phonealive_status' : parsed.message.featurelist.phonealive_status

          };

          var data1_2 = [];
          data1_2.push(izi1_2);

          myTable1_2.clear();
          $.each(data1_2, function(index, value) {
              myTable1_2.row.add(value);
          });
          myTable1_2.draw();






          var izi1_3 = {

          'phoneinquiries_14d' : parsed.message.featurelist.phoneinquiries_14d,
          'phoneinquiries_180d' : parsed.message.featurelist.phoneinquiries_180d,
          'phoneinquiries_21d' : parsed.message.featurelist.phoneinquiries_21d,
          'phoneinquiries_30d' : parsed.message.featurelist.phoneinquiries_30d,
          'phoneinquiries_360d' : parsed.message.featurelist.phoneinquiries_360d,
          'phoneinquiries_3d' : parsed.message.featurelist.phoneinquiries_3d,
          'phoneinquiries_60d' : parsed.message.featurelist.phoneinquiries_60d,
          'phoneinquiries_90d' : parsed.message.featurelist.phoneinquiries_90d,
          'phoneinquiries_status' : parsed.message.featurelist.phoneinquiries_status,
          'phoneinquiries_total' : parsed.message.featurelist.phoneinquiries_total

          };

          var data1_3 = [];
          data1_3.push(izi1_3);

          myTable1_3.clear();
          $.each(data1_3, function(index, value) {
              myTable1_3.row.add(value);
          });
          myTable1_3.draw();




          var izi1_4 = {
          'phoneinquiriesuname_14d' : parsed.message.featurelist.phoneinquiriesuname_14d,
          'phoneinquiriesuname_180d' : parsed.message.featurelist.phoneinquiriesuname_180d,
          'phoneinquiriesuname_21d' : parsed.message.featurelist.phoneinquiriesuname_21d,
          'phoneinquiriesuname_30d' : parsed.message.featurelist.phoneinquiriesuname_30d,
          'phoneinquiriesuname_360d' : parsed.message.featurelist.phoneinquiriesuname_360d,
          'phoneinquiriesuname_3d' : parsed.message.featurelist.phoneinquiriesuname_3d,
          'phoneinquiriesuname_21d' : parsed.message.featurelist.phoneinquiriesuname_21d,
          'phoneinquiriesuname_60d' : parsed.message.featurelist.phoneinquiriesuname_60d

          };

          var data1_4 = [];
          data1_4.push(izi1_4);

          myTable1_4.clear();
          $.each(data1_4, function(index, value) {
              myTable1_4.row.add(value);
          });
          myTable1_4.draw();




          var izi1_5 = {
          'phoneinquiriesuname_7d' : parsed.message.featurelist.phoneinquiriesuname_7d,
          'phoneinquiriesuname_90d' : parsed.message.featurelist.phoneinquiriesuname_90d,
          'phoneinquiriesuname_status' : parsed.message.featurelist.phoneinquiriesuname_status,
          'phoneinquiriesuname_total' : parsed.message.featurelist.phoneinquiriesuname_total,
          'phonescore' : parsed.message.featurelist.phonescore,
          'phonescore_status' : parsed.message.featurelist.phonescore_status,
          'phoneverify' : parsed.message.featurelist.phoneverify
          };

          var data1_5 = [];
          data1_5.push(izi1_5);

          myTable1_5.clear();
          $.each(data1_5, function(index, value) {
              myTable1_5.row.add(value);
          });
          myTable1_5.draw();



          var izi2= {'A_II_14d' : parsed.message.featurelist.A_II_14d,
          'A_II_180d' : parsed.message.featurelist.A_II_180d,
          'A_II_21d' : parsed.message.featurelist.A_II_21d,
          'A_II_30d' : parsed.message.featurelist.A_II_30d,
          'A_II_360d' : parsed.message.featurelist.A_II_360d,
          'A_II_3d' :  parsed.message.featurelist.A_II_3d,
          'A_II_60d' : parsed.message.featurelist.A_II_60d,
          'A_II_7d' :  parsed.message.featurelist.A_II_7d,
          'A_II_90d' : parsed.message.featurelist.A_II_90d,
          'A_PI_14d' : parsed.message.featurelist.A_PI_14d,
          'A_PI_180d' : parsed.message.featurelist.A_PI_180d,
          'A_PI_21d' : parsed.message.featurelist.A_PI_21d,
          'A_PI_30d' : parsed.message.featurelist.A_PI_30d,
          'A_PI_360d' :  parsed.message.featurelist.A_PI_360d,
          'A_PI_3d' : parsed.message.featurelist.A_PI_3d,
          'A_PI_60d' :  parsed.message.featurelist.A_PI_60d,
          'A_PI_7d' :  parsed.message.featurelist.A_PI_7d,
          'A_PI_90d' : parsed.message.featurelist.A_PI_90d

          };


          var data2 = [];
          data2.push(izi2);

          myTable2.clear();
          $.each(data2, function(index, value) {
              myTable2.row.add(value);
          });
          myTable2.draw();




          var izi2_1= {
          'B_II_14d' : parsed.message.featurelist.B_II_14d,
          'B_II_180d' : parsed.message.featurelist.B_II_180d,
          'B_II_21d' : parsed.message.featurelist.B_II_21d,
          'B_II_30d' : parsed.message.featurelist.B_II_30d,
          'B_II_360d' :  parsed.message.featurelist.B_II_360d,
          'B_II_3d' : parsed.message.featurelist.B_II_3d,
          'B_II_60d' :  parsed.message.featurelist.B_II_60d,
          'B_II_7d' :  parsed.message.featurelist.B_II_7d,
          'B_II_90d' : parsed.message.featurelist.B_II_90d,
          'B_PI_14d' : parsed.message.featurelist.B_PI_14d,
          'B_PI_180d' : parsed.message.featurelist.B_PI_180d,
          'B_PI_21d' : parsed.message.featurelist.B_PI_21d,
          'B_PI_30d' : parsed.message.featurelist.B_PI_30d,
          'B_PI_360d' : parsed.message.featurelist.B_PI_360d,
          'B_PI_3d' : parsed.message.featurelist.B_PI_3d,
          'B_PI_60d' : parsed.message.featurelist.B_PI_60d,
          'B_PI_7d' :  parsed.message.featurelist.B_PI_7d,
          'B_PI_90d' : parsed.message.featurelist.B_PI_90d

          };


          var data2_1 = [];
          data2_1.push(izi2_1);

          myTable2_1.clear();
          $.each(data2_1, function(index, value) {
              myTable2_1.row.add(value);
          });
          myTable2_1.draw();




          var izi2_2= {
          'C_II_14d' :  parsed.message.featurelist.C_II_14d,
          'C_II_180d' : parsed.message.featurelist.C_II_180d,
          'C_II_21d' : parsed.message.featurelist.C_II_21d,
          'C_II_30d' : parsed.message.featurelist.C_II_30d,
          'C_II_360d' :  parsed.message.featurelist.C_II_360d,
          'C_II_3d' :parsed.message.featurelist.C_II_3d,
          'C_II_60d' : parsed.message.featurelist.C_II_60d,
          'C_II_7d' :   parsed.message.featurelist.C_II_7d,
          'C_II_90d' :  parsed.message.featurelist.C_II_90d,
          'C_PI_14d' :  parsed.message.featurelist.C_PI_14d,
          'C_PI_180d' :   parsed.message.featurelist.C_PI_180d,
          'C_PI_21d' : parsed.message.featurelist.C_PI_21d,
          'C_PI_30d' : parsed.message.featurelist.C_PI_30d,
          'C_PI_3d' :  parsed.message.featurelist.C_PI_3d,
          'C_PI_60d' :  parsed.message.featurelist.C_PI_60d,
          'C_PI_7d' : parsed.message.featurelist.C_PI_7d,
          'C_PI_90d' : parsed.message.featurelist.C_PI_90d

          };


          var data2_2 = [];
          data2_2.push(izi2_2);

          myTable2_2.clear();
          $.each(data2_2, function(index, value) {
              myTable2_2.row.add(value);
          });
          myTable2_2.draw();



          var izi2_3= {
          'II_v4_feature_status' :  parsed.message.featurelist.II_v4_feature_status,
          'PI_v4_feature_status' : parsed.message.featurelist.PI_v4_feature_status,
          'identity_address' :   parsed.message.featurelist.identity_address,
          'identity_date_of_birth' : parsed.message.featurelist.identity_date_of_birth,
          'identity_district' :  parsed.message.featurelist.identity_district,
          'identity_gender' : parsed.message.featurelist.identity_gender,
          'identity_city' :  parsed.message.featurelist.identity_city,
          'identity_marital_status' : parsed.message.featurelist.identity_marital_status,
          'identity_match' :  parsed.message.featurelist.identity_match
          };


          var data2_3 = [];
          data2_3.push(izi2_3);

          myTable2_3.clear();
          $.each(data2_3, function(index, value) {
              myTable2_3.row.add(value);
          });
          myTable2_3.draw();







          var izi2_4= {
          'identity_name' :parsed.message.featurelist.identity_name,
          'identity_nationnality' :  parsed.message.featurelist.identity_nationnality,
          'identity_place_of_birth' :   parsed.message.featurelist.identity_place_of_birth,
          'identity_province' :   parsed.message.featurelist.identity_province,
          'identity_religion' :  parsed.message.featurelist.identity_religion,
          'identity_rt' :   parsed.message.featurelist.identity_rt,
          'identity_rw' :  parsed.message.featurelist.identity_rw,
          'identity_status' :  parsed.message.featurelist.identity_status,
          'identity_village' :   parsed.message.featurelist.identity_village,
          'identity_work' :  parsed.message.featurelist.identity_work

          };


          var data2_4 = [];
          data2_4.push(izi2_4);

          myTable2_4.clear();
          $.each(data2_4, function(index, value) {
              myTable2_4.row.add(value);
          });
          myTable2_4.draw();




         var izi2_5= {
          'idinquiries_14d' : parsed.message.featurelist.idinquiries_14d,
          'idinquiries_180d' :  parsed.message.featurelist.idinquiries_180d,
          'idinquiries_21d' :   parsed.message.featurelist.idinquiries_21d,
          'idinquiries_30d' :  parsed.message.featurelist.idinquiries_30d,
          'idinquiries_360d' :   parsed.message.featurelist.idinquiries_360d,
          'idinquiries_3d' :  parsed.message.featurelist.idinquiries_3d,
          'idinquiries_60d' :  parsed.message.featurelist.idinquiries_60d,
          'idinquiries_7d' : parsed.message.featurelist.idinquiries_7d,
          'idinquiries_90d' :   parsed.message.featurelist.idinquiries_90d,
          'idinquiries_status' : parsed.message.featurelist.idinquiries_status,
          'idinquiries_total' :  parsed.message.featurelist.idinquiries_total
          };


          var data2_5 = [];
          data2_5.push(izi2_5);

          myTable2_5.clear();
          $.each(data2_5, function(index, value) {
              myTable2_5.row.add(value);
          });
          myTable2_5.draw();




         var izi2_6= {
          'idinquiriesuname_14d' :parsed.message.featurelist.idinquiriesuname_14d,
          'idinquiriesuname_21d' : parsed.message.featurelist.idinquiriesuname_21d,
          'idinquiriesuname_30d' :    parsed.message.featurelist.idinquiriesuname_30d,
          'idinquiriesuname_360d' :   parsed.message.featurelist.idinquiriesuname_360d,
          'idinquiriesuname_3d' :   parsed.message.featurelist.idinquiriesuname_3d,
          'idinquiriesuname_60d' :    parsed.message.featurelist.idinquiriesuname_60d
          };


          var data2_6 = [];
          data2_6.push(izi2_6);

          myTable2_6.clear();
          $.each(data2_6, function(index, value) {
              myTable2_6.row.add(value);
          });
          myTable2_6.draw();




          var izi2_6_1= {
          'idinquiriesuname_7d' :  parsed.message.featurelist.idinquiriesuname_7d,
          'idinquiriesuname_90d' :   parsed.message.featurelist.idinquiriesuname_90d,
          'idinquiriesuname_status' :   parsed.message.featurelist.idinquiriesuname_status,
          'idinquiriesuname_total' :  parsed.message.featurelist.idinquiriesuname_total,
          'idinquiriesuname_3d' : parsed.message.featurelist.idinquiriesuname_3d
          };


          var data2_6_1 = [];
          data2_6_1.push(izi2_6_1);

          myTable2_6_1.clear();
          $.each(data2_6_1, function(index, value) {
              myTable2_6_1.row.add(value);
          });
          myTable2_6_1.draw();




          var izi2_7= {
          'preference_bank_180d' :  parsed.message.featurelist.preference_bank_180d,
          'preference_bank_270d' :    parsed.message.featurelist.preference_bank_270d,
          'preference_bank_60d' :  parsed.message.featurelist.preference_bank_60d,
          'preference_bank_90d' :   parsed.message.featurelist.preference_bank_90d,
          'preference_ecommerce_180d' :  parsed.message.featurelist.preference_ecommerce_180d,
          'preference_ecommerce_270d' :  parsed.message.featurelist.preference_ecommerce_270d,
          'preference_ecommerce_60d' : parsed.message.featurelist.preference_ecommerce_60d
          };


          var data2_7 = [];
          data2_7.push(izi2_7);

          myTable2_7.clear();
          $.each(data2_7, function(index, value) {
              myTable2_7.row.add(value);
          });
          myTable2_7.draw();



          var izi2_8= {
          'preference_ecommerce_90d' :  parsed.message.featurelist.preference_ecommerce_90d,
          'preference_game_180d' : parsed.message.featurelist.preference_game_180d,
          'preference_game_270d' : parsed.message.featurelist.preference_game_270d,
          'preference_game_60d' :parsed.message.featurelist.preference_game_60d,
          'preference_game_90d' : parsed.message.featurelist.preference_game_90d,
          'preference_lifestyle_180d' :    parsed.message.featurelist.preference_lifestyle_180d
          };


          var data2_8 = [];
          data2_8.push(izi2_8);

          myTable2_8.clear();
          $.each(data2_8, function(index, value) {
              myTable2_8.row.add(value);
          });
          myTable2_8.draw();



          var izi2_9= {
          'preference_lifestyle_270d' :    parsed.message.featurelist.preference_lifestyle_270d,
          'preference_lifestyle_60d' :   parsed.message.featurelist.preference_lifestyle_60d,
          'preference_lifestyle_90d' :    parsed.message.featurelist.preference_lifestyle_90d,
          'preference_status' :   parsed.message.featurelist.preference_status
          };


          var data2_9 = [];
          data2_9.push(izi2_9);

          myTable2_9.clear();
          $.each(data2_9, function(index, value) {
              myTable2_9.row.add(value);
          });
          myTable2_9.draw();











          var izi3= {
          'topup_0_180_avg' : parsed.message.featurelist.topup_0_180_avg,
          'topup_0_180_max' : parsed.message.featurelist.topup_0_180_max,
          'topup_0_180_min' : parsed.message.featurelist.topup_0_180_min,
          'topup_0_180_times' : parsed.message.featurelist.topup_0_180_times,
          'topup_0_30_avg' : parsed.message.featurelist.topup_0_30_avg,
          'topup_0_30_max' : parsed.message.featurelist.topup_0_30_max,
          'topup_0_30_min' :  parsed.message.featurelist.topup_0_30_min,
          'topup_0_30_times' : parsed.message.featurelist.topup_0_30_times
          };


          var data3 = [];
          data3.push(izi3);

          myTable3.clear();
          $.each(data3, function(index, value) {
              myTable3.row.add(value);
          });
          myTable3.draw();



          var izi3_1= {
          'topup_0_360_avg' :  parsed.message.featurelist.topup_0_360_avg,
          'topup_0_360_max' : parsed.message.featurelist.topup_0_360_max,
          'topup_0_360_min' : parsed.message.featurelist.topup_0_360_min,
          'topup_0_360_times' :  parsed.message.featurelist.topup_0_360_times,
          'topup_0_60_avg' :  parsed.message.featurelist.topup_0_60_avg,
          'topup_0_60_max' : parsed.message.featurelist.topup_0_60_max,
          'topup_0_60_min' :  parsed.message.featurelist.topup_0_60_min,
          'topup_0_60_times' :  parsed.message.featurelist.topup_0_60_times
          };


          var data3_1 = [];
          data3_1.push(izi3_1);

          myTable3_1.clear();
          $.each(data3_1, function(index, value) {
              myTable3_1.row.add(value);
          });
          myTable3_1.draw();


          var izi3_1_1= {
          'topup_0_90_avg' : parsed.message.featurelist.topup_0_90_avg,
          'topup_0_90_max' : parsed.message.featurelist.topup_0_90_max,
          'topup_0_90_min' :  parsed.message.featurelist.topup_0_90_min,
          'topup_0_90_times' : parsed.message.featurelist.topup_0_90_times,
          'topup_180_360_avg' : parsed.message.featurelist.topup_180_360_avg,
          'topup_180_360_max' :  parsed.message.featurelist.topup_180_360_max,
          'topup_180_360_min' :  parsed.message.featurelist.topup_180_360_min,
          'topup_180_360_times' :   parsed.message.featurelist.topup_180_360_times
          };


          var data3_1_1 = [];
          data3_1.push(izi3_1_1);

          myTable3_1_1.clear();
          $.each(data3_1_1, function(index, value) {
              myTable3_1_1.row.add(value);
          });
          myTable3_1_1.draw();



          var izi3_2= {
          'topup_30_60_avg' :   parsed.message.featurelist.topup_30_60_avg,
          'topup_30_60_max' :  parsed.message.featurelist.topup_30_60_max,
          'topup_30_60_min' :  parsed.message.featurelist.topup_30_60_min,
          'topup_30_60_times' : parsed.message.featurelist.topup_30_60_times,
          'topup_360_720_avg' :  parsed.message.featurelist.topup_360_720_avg,
          'topup_360_720_max' : parsed.message.featurelist.topup_360_720_max,
          'topup_360_720_min' : parsed.message.featurelist.topup_360_720_min,
          'topup_360_720_times' : parsed.message.featurelist.topup_360_720_times
          };


          var data3_2 = [];
          data3_2.push(izi3_2);

          myTable3_2.clear();
          $.each(data3_2, function(index, value) {
              myTable3_2.row.add(value);
          });
          myTable3_2.draw();




          var izi3_3= {
          'topup_60_90_avg' : parsed.message.featurelist.topup_60_90_avg,
          'topup_60_90_max' :  parsed.message.featurelist.topup_60_90_max,
          'topup_60_90_min' :  parsed.message.featurelist.topup_60_90_min,
          'topup_60_90_times' :  parsed.message.featurelist.topup_60_90_times,
          'topup_90_180_avg' : parsed.message.featurelist.topup_90_180_avg,
          'topup_90_180_max' :  parsed.message.featurelist.topup_90_180_max,
          'topup_90_180_min' : parsed.message.featurelist.topup_90_180_min,
          'topup_90_180_times' :   parsed.message.featurelist.topup_90_180_times
          };


          var data3_3 = [];
          data3_3.push(izi3_3);

          myTable3_3.clear();
          $.each(data3_3, function(index, value) {
              myTable3_3.row.add(value);
          });
          myTable3_3.draw();


          var izi3_4= {
          'eco' : parsed.message.featurelist.eco,
          'ecp' : parsed.message.featurelist.ecp,
          'ece' :   parsed.message.featurelist.ece,
          'ect' :  parsed.message.featurelist.ect

          };


          var data3_4 = [];
          data3_4.push(izi3_4);

          myTable3_4.clear();
          $.each(data3_4, function(index, value) {
              myTable3_4.row.add(value);
          });
          myTable3_4.draw();





          var izi3_5= {
          'multiphoneinquiries_7d' :   parsed.message.featurelist.multiphoneinquiries_7d,
          'multiphoneinquiries_14d' :   parsed.message.featurelist.multiphoneinquiries_14d,
          'multiphoneinquiries_21d' : parsed.message.featurelist.multiphoneinquiries_21d,
          'multiphoneinquiries_30d' : parsed.message.featurelist.multiphoneinquiries_30d,
          'multiphoneinquiries_60d' :  parsed.message.featurelist.multiphoneinquiries_60d,
          'multiphoneinquiries_90d' :  parsed.message.featurelist.multiphoneinquiries_90d,
          'multiphoneinquiries_total' : parsed.message.featurelist.multiphoneinquiries_total

          };


          var data3_5 = [];
          data3_5.push(izi3_5);

          myTable3_5.clear();
          $.each(data3_5, function(index, value) {
              myTable3_5.row.add(value);
          });
          myTable3_5.draw();





          var izi3_5_1= {
          'multiidinquiries_7d' : parsed.message.featurelist.multiidinquiries_7d,
          'multiidinquiries_14d' :  parsed.message.featurelist.multiidinquiries_14d,
          'multiidinquiries_21d' :  parsed.message.featurelist.multiidinquiries_21d,
          'multiidinquiries_30d' :    parsed.message.featurelist.multiidinquiries_30d,
          'multiidinquiries_60d' : parsed.message.featurelist.multiidinquiries_60d,
          'multiidinquiries_90d' :   parsed.message.featurelist.multiidinquiries_90d,
          'multiidinquiries_total' :  parsed.message.featurelist.multiidinquiries_total

          };


          var data3_5_1 = [];
          data3_5_1.push(izi3_5_1);

          myTable3_5_1.clear();
          $.each(data3_5_1, function(index, value) {
              myTable3_5_1.row.add(value);
          });
          myTable3_5_1.draw();



          var izi3_6= {
          'b_class' :   parsed.message.featurelist.b_class,
          'b_isopen' : parsed.message.featurelist.b_isopen,
          'b_membership' :   parsed.message.featurelist.b_membership,
          'b_members' : parsed.message.featurelist.b_members,
          'b_activeStatus' :  parsed.message.featurelist.b_activeStatus,
          'b_activeYear' :    parsed.message.featurelist.b_activeYear
          };


          var data3_6 = [];
          data3_6.push(izi3_6);

          myTable3_6.clear();
          $.each(data3_6, function(index, value) {
              myTable3_6.row.add(value);
          });
          myTable3_6.draw();


        }
	});
}
</script>
