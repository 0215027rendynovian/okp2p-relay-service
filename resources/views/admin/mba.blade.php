<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Rekening Bank Utama</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Rekening Bank Utama</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="download_pdf_data" class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header back-ops-okp2p">
                <h3 class="card-title">Kotak Informasi</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                    <a type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="create-new-mba"> <span><i class="fas fa-university"></i>  Tambah Master Bank</span></a>
                    <a id="toolhapus" type='button' href="javascript:void(0)" class="btn btn-lg btn-primary" id="hapus-mba" onclick="deletemba($('#id').val())"> <span><i class="fas fa-trash-alt"></i>  Hapus Master Bank</span></a>
                </div>
            </div>
          </div>

          <!--/.col (right) -->
        </div>

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tablemba" class="col-md-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Rekening Bank Utama </b></h3>
                        </div>

                        <div class="card-body">
                                <table id="listmba" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Kode Bank</th>
                                            <th>Nama Bank</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listmba as $mba)
                                        <tr>
                                            <td>{{ $mba->id }}</td>
                                            <td>{{ $mba->bank_code }}</td>
                                            <td>{{ $mba->bank_name }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                <!-- left column -->
                <div class="col-md-4" id="detailmba">
                    <div class="card card-danger">
                    <div class="card-header back-ops-okp2p">
                        <h3 class="card-title">Detil Rekening Bank Utama</h3>
                    </div>
                    <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form-horizontal" method="POST" action="{{ route('mba-edit') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" name="id" id="id">
                                <div class="form-group">
                                    <label for="bank_code" class="col-sm-12 control-label">Kode Bank</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('bank_code') is-invalid @enderror" id="bank_code" name="bank_code" placeholder="Enter Kode Bank" value="" maxlength="50" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="bank_name" class="col-sm-12 control-label">Nama Bank</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control @error('bank_name') is-invalid @enderror" id="bank_name" name="bank_name" placeholder="Enter Nama Bank" value="" maxlength="300" required="">
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" value="create">Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--/.col (left) -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    {{-- MODALS --}}
    <div class="modal fade" id="ajax-mba-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mbaCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <form id="mbaForm" name="mbaForm" class="form-horizontal" method="POST" action="{{ route('mba-store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="bank_code" class="col-sm-12 control-label">Kode Bank</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('bank_code') autofocus is-invalid @enderror" name="bank_code" placeholder="Enter Kode Bank" value="{{ old('bank_code') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="bank_name" class="col-sm-12 control-label">Nama Bank</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control @error('bank_name') is-invalid @enderror" name="bank_name" placeholder="Enter Nama Bank" value="{{ old('bank_name') }}" maxlength="300" required="">
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                         <button type="submit" class="btn btn-primary" id="btn-save" value="create">Tambah
                         </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

@include('js.toast-info')

<script>
    var table

    $(document).ready( function () {
        $('#detailmba').hide()
        $('#toolhapus').hide()
        document.getElementById("tablemba").className = "col-md-12";

        table = $('#listmba').DataTable();

        $('#listmba tbody').on( 'click', 'tr', function () {

            var datauser = table.row( this ).data()
            $('#detailmba').show()
            $('#toolhapus').show()
            document.getElementById("tablemba").className = "col-md-8";
            $('#id').val(datauser[0])
            $('#bank_code').val(datauser[1])
            $('#bank_name').val(datauser[2])

        } );

        $('#listmba tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('row_selected') ) {
                $(this).removeClass('row_selected')

                $('#detailmba').hide()
                $('#toolhapus').hide()
                document.getElementById("tablemba").className = "col-md-12";
                $('#id').val('')
                $('#bank_code').val('')
                $('#bank_name').val('')
            }
            else {
                table.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected')
            }
        } );

        /*  When user click add user button */
        $('#create-new-mba').click(function () {
            $('#btn-save').val("create-mba")
            $('#id').val('')
            $('#mbaForm').trigger("reset")
            $('#mbaCrudModal').html("Tambah Rekening Bank Utama")
            $('#ajax-mba-modal').modal('show')
        });
    });

    function deletemba(id)
    {
        var bank_code = $('#bank_code').val()
        var bank_name = $('#bank_name').val()
        Swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            html:
            "Kode Bank : <b>"+bank_code+"</b> </br> " +
            "Nama Bank : <b>"+bank_name+"</b> </br>  </br> " +
            "Anda tidak akan dapat mengembalikan ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "mba/delete/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Rekening Bank Utama berhasil terhapus',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal menghapus Rekening Bank Utama',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

</script>
