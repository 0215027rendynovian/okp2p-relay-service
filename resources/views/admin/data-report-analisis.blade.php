<!DOCTYPE html>
<html lang="en">
@include('header.header')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" id="Identity-check">
<div class="wrapper">

  @include('navbar.navbar')
  @include('sidebar.sidebar')

  <div class="content-wrapper">

    <div id="loading"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Data Analisis</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">DASBOR</a></li>
              <li class="breadcrumb-item active">Laporan Data Analisis</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class ="row">
          <div class="col-md-12">
                  <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div id="tablehistory" class="col-md-12">

                    <div class="card">
                        <div class="card-header bg-danger back-ops-okp2p">
                            <h3 class="card-title"><b>Laporan Data Analisis</b></h3>
                        </div>

                        <div class="card-body">
                            <div class="card-body col-md-12">
                                <h3 class="card-title"><b>Laporkan Data Nihil</b></h3></br>
                                <hr style="margin-top:5px;">
                                <table id="reportdatanihil" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Backoffice</th>
                                            <th>NAMA</th>
                                            <th>NIK</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Tempat Lahir</th>
                                            <th>Negara</th>
                                            <th>Alamat</th>
                                            <th>Keterangan</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="card-body col-md-12">
                                <h3 class="card-title"><b>Laporkan Data Pemblokiran</b></h3></br>
                                <hr style="margin-top:5px;">
                                <table id="reportdatapemblokiran" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Backoffice</th>
                                            <th>NAMA</th>
                                            <th>NIK</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Tempat Lahir</th>
                                            <th>Negara</th>
                                            <th>Alamat</th>
                                            <th>Keterangan</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>

                  </div>
                      <!-- /.card-body -->

                </div>
                    <!-- /.card -->
                </div>
                  <!-- /.col -->
            </div>
                <!-- /.row -->
        </div>
              <!-- /.container-fluid -->
            </section>
          </div>
        </div>
        <!-- /.row -->




      </div><!-- /.container-fluid -->


    <!-- /.content -->




  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @include('modal.report-analisis-sigap-modal')
  @include('modal.report-nihil-modal')
  @include('modal.report-pemblokiran-modal')

  <!-- Main Footer -->
  @include('footer.tag-footer')
</div>
@include('footer.footer')
</body>
</html>

@include('js.alert-toast')

<script type="text/javascript">

    var dTable;
    var dTable1;

    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        dTable = $('#reportdatanihil').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: false,
            ordering: false,
            paging: false,
            info: false,
            autoWidth: true,
            // scrollY: 300,
            // scrollX: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('getReportNihil') }}",
                data: function (data) {

                },
                delay: 10000000,
                timeout: 10000000,
                error: handleAjaxError
            },
            columns: [
                { data: 'no' },
                { data: 'ref_idbackoffice' },
                { data: 'nama' },
                { data: 'nik' },
                { data: 'tanggal_lahir' },
                { data: 'tempat_Lahir' },
                { data: 'warga_negara' },
                { data: 'alamat' },
                { data: 'reason' },
                { data: 'aksi' },
            ]
        });

        dTable1 = $('#reportdatapemblokiran').DataTable({
            lengthChange: true,
            fixedHeader: true,
            searching: false,
            ordering: false,
            paging: false,
            info: false,
            autoWidth: true,
            // scrollY: 300,
            // scrollX: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('getReportPemblokiran') }}",
                data: function (data) {

                },
                delay: 10000000,
                timeout: 10000000,
                error: handleAjaxError
            },
            columns: [
                { data: 'no' },
                { data: 'ref_idbackoffice' },
                { data: 'nama' },
                { data: 'nik' },
                { data: 'tanggal_lahir' },
                { data: 'tempat_Lahir' },
                { data: 'warga_negara' },
                { data: 'alamat' },
                { data: 'reason' },
                { data: 'aksi' },
            ]
        });

        $('#btncari').on('click', function(){
            dTable.draw()
            dTable1.draw()
        })

        $('body').on('click', '.editProduct', function () {
            var product_id = $(this).data('id');
            $.get('editSigap/'+product_id+'/editReasonSigap', function (data) {
                $('#saveBtn').val("edit-user");
                $('#ajaxModel').modal('show');
                $('#id').val(data.id);
                $('#reason').val(data.reason);
                $('#status_report').val(data.status_report);
            })
        });

        $('#saveBtn').click(function (e) {
            e.preventDefault();

            $.ajax({
                data: $('#productForm').serialize(),
                url: "{{ route('updateSigap') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    dTable.draw()
                    dTable1.draw()
                    toastr.success("Data berhasil terupdate!")

                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Save Changes');
                }
            });
        });
    });

    function deleteReportNihil(id)
    {
        Swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            html:
            "Anda dapat menginputkan report kembali!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "sigap/cancelReportNihil/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Data berhasil terhapus',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                dTable.draw()
                                dTable1.draw()
                                toastr.success("Data Report Nihil berhasil di cancel!")
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal menghapus data',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

    function deleteReportPemblokiran(id)
    {
        Swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            html:
            "Anda dapat menginputkan report kembali!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "sigap/cancelReportPemblokiran/"+id,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Data berhasil terhapus',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                dTable.draw()
                                dTable1.draw()
                                toastr.success("Data Report Pemblokiran berhasil di cancel!")
                            }
                        })
                    },
                    error: function (data) {
                        Swal.fire(
                            'Error!',
                            'Gagal menghapus data',
                            'error'
                        )
                        console.log('Error:', data);
                    }
                });
            }
        })
    }

    function modalReportNihil(ojk, bo)
    {
        $('#ref_id_backofficeNihil').val(bo)
        $('#ref_id_penggunaNihil').val(ojk)
    }

    function modalReportPemblokiran(ojk, bo)
    {
        $('#ref_id_backofficePemblokiran').val(bo)
        $('#ref_id_penggunaPemblokiran').val(ojk)
    }

</script>
