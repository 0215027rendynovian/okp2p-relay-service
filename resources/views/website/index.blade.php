<style>

html,
body {
  min-width: 290px;
  color: #4d4e53;
  background-color: #ffffff;
  font-family: 'Open Sans', Arial, sans-serif;
  line-height: 1.5;
}

#navbar {
  position: fixed;
  min-width: 290px;
  top: 0px;
  left: 0px;
  width: 300px;
  height: 100%;
  border-right: solid;
  border-color: rgba(0, 22, 22, 0.4);
}

header {
  color: black;
  margin: 10px;
  text-align: center;
  font-size: 1.8em;
  font-weight: thin;
}

#main-doc header {
  text-align: left;
  margin: 0px;
}

#navbar ul {
  height: 88%;
  padding: 0;
  overflow-y: auto;
  overflow-x: hidden;
}

#navbar li {
  color: #4d4e53;
  border-top: 1px solid;
  list-style: none;
  position: relative;
  width: 100%;
}

#navbar a {
  display: block;
  padding: 10px 30px;
  color: #4d4e53;
  text-decoration: none;
  cursor: pointer;
}

#main-doc {
  position: absolute;
  margin-left: 310px;
  padding: 20px;
  margin-bottom: 110px;
}

section article {
  color: #4d4e53;
  margin: 15px;
  font-size: 0.96em;
}

section li {
  margin: 15px 0px 0px 20px;
}
table, th, td {
  border: 1px solid black;
}

code {
  display: block;
  text-align: left;
  white-space: pre-line;
  position: relative;
  word-break: normal;
  word-wrap: normal;
  line-height: 2;
  background-color: #f7f7f7;
  padding: 15px;
  margin: 10px;
  border-radius: 5px;
}

@media only screen and (max-width: 815px) {
  /* For mobile phones: */
  #navbar ul {
    border: 1px solid;
    height: 207px;
  }

  #navbar {
    background-color: white;
    position: absolute;
    top: 0;
    padding: 0;
    margin: 0;
    width: 100%;
    max-height: 275px;
    border: none;
    z-index: 1;
    border-bottom: 2px solid;
  }

  #main-doc {
    position: relative;
    margin-left: 0px;
    margin-top: 270px;
  }
}

@media only screen and (max-width: 400px) {
  #main-doc {
    margin-left: -10px;
  }

  code {
    margin-left: -20px;
    width: 100%;
    padding: 15px;
    padding-left: 10px;
    padding-right: 45px;
    min-width: 233px;
  }
}

</style>

<script>
    // coded by @ChaituVR
// eslint-disable-next-line no-unused-vars
const projectName = 'technical-docs-page';

</script>
<nav id="navbar">
  <header><img src="/img/logo-okp2p.png" alt="User Avatar"  width="260" height="100">
<span style="font-size:20px; margin-bottom:20px;">Documentation API</span></header>
  <ul>
    <li>
      <a class="nav-link" href="#Introduction">Introduction</a>
    </li>
    <li>
      <a class="nav-link" href="#What_you_should_already_know">What you should already know</a>
    </li>
    <li>
      <a class="nav-link" href="#Authentication">Authentication</a>
    </li>
    <li>
      <a class="nav-link" href="#Api_FDC">FDC Credit Information</a>
    </li>
    <li>
      <a class="nav-link" href="#Api_SMS">SMS Gateway</a>
    </li>
    <li>
      <a class="nav-link" href="#Api_Iluma">Iluma Bank Validation</a>
    </li>
    <li>
      <a class="nav-link" href="#Api_Creditfeature">Izi Data (Credit Feature)</a>
    </li>
    <li>
      <a class="nav-link" href="#Api_Salary">Izi Data (Salary)</a>
    </li>
    <li>
      <a class="nav-link" href="#Api_Pefindo">Pefindo (Credit Information)</a>
    </li>
    <li>
      <a class="nav-link" href="#Api_AdvanceAi">Advance AI (Credit Score)</a>
    </li>
    <li>
      <a class="nav-link" href="#Check_customers">Check Customers Asset</a>
    </li>
  </ul>
</nav>
<main id="main-doc">

  <!-- Section Introduction -->
  <section class="main-section" id="Introduction">
    <header>Introduction</header>
    <article>
      <p>
      OKP2P Relay Service provides API interfaces such as credit score, credit information, sms gateway, Bank Validation and personal/company credit. Relay Service to verify the identity of the sender of each request method and the number of destination for sending sms which has 2 features, sms marketing and sms OTP.
      </p>
 
    </article>
  </section>

  <!-- Section What you should already know -->
  <section class="main-section" id="What_you_should_already_know">
    <header>What you should already know?</header>
    <article>
      <ul>
        <li>
          Knowledge of Accept Header response and HTTP status code.
        </li>
        <li>
          When you're creating tests for an API, the GET method will likely be the most frequent type of request made by consumers of the service, so it's important to check every known endpoint with a GET request.
        </li>
        <li>
          In web services, POST requests are used to send data to the API server to create or update a resource. The data sent to the server is stored in the request body of the HTTP request
       </li>
        <li>
          Some programming experience. If you are new to programming, try one of
          the tutorials linked on the main page about Http Client and HTTP Status Code.
        </li>
      </ul>
    </article>
  </section>
  <br><br><hr><br><br>



  <!-- Section Authentication -->
  <section class="main-section" id="Authentication">
    <header>Authentication</header>
    <article>
      <p>
      OKP2P API is organized around REST to make it cleaner and easier to understand. All our API responses return JSON. To let you explore our APIs, make sure that you have registered an account. You can obtain and manage your API keys in API Keys Settings. We provide you API keys for both the test and live environments. To authenticate your account, you have to include your secret API key in the request which can be accessed in OKP2P Ops Sistem Dashboard. Here are the steps to authenticate your account: Generate secret API key from Dashboard Obtain your secret API key Select Bearer Token Authentication,  All requests made in the test environment will never hit the relay service networks. Your API keys should be kept private so do not share your secret API keys.
    </p>
    </article>
    <header>Get Token</header>
    <article>
        Include Base64 encoded value in HTTP(s) header
      <code
        >Authorization : Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.xxxxx.xxxxx
      </code>
    </article>
  </section>
  <br><br><hr><br><br>






 <!-- Section FDC -->
  <section class="main-section" id="Api_FDC">
    <header>API FDC (Fintech Data Center)</header>
    <article>
      <p>
        In connection with the Loan Facility, hereby grants approval and authority to OKP2P to obtain data from other parties, with its affiliates, the Indonesian Joint Funding Fintech Association (AFPI) or other parties collaborating with the Operator, data and/or information on Prospective Borrowers and/or Borrowers as permitted under applicable regulations for risk management purposes and/or other purposes deemed reasonable or necessary by OKP2P to obtain investigative data  
     </p>
    </article>
    <header>Inquiry Request</header>
    <article>
      HTTP Request:  
      <code
        >POST http://147.139.165.214:8000/api/api-post
      </code>
      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
       Inquiry Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>ktp</td>
              <td><strong>string (required)</strong> Filled by 16 numerics of KTP (National ID) number for getting
                individual report or Filled by 15 numerics of NPWP (Tax Number) for getting company report
              </td>
            </tr>
            <tr>
              <td>reason</td>
              <td><strong>number (required)</strong><br>Filled by "1" for new loan reason or Filled by "2" for inquiring your existing customer</td>
            </tr>
            <tr>
              <td>reffid</td>
              <td><strong>string (optional)</strong><br>32 characters, for your own purpose</td>
            </tr>
          </tbody>
        </table>
        </code>
      Example Request Body :
      <code> {
              "ktp":"3203032411930006",
              "reason":1,
              "reffid":123
          }
      </code>
      Example Response :
      <code>{
              "noIdentitas": "3175062302970007",
              "userId": "riski.firdaus@okp2p.co.id",
              "userName": "OK!P2P",
              "inquiryReason": "1 - Applying loan via Platform",
              "memberId": "820140",
              "memberName": "OK!P2P",
              "refferenceId": "123",
              "inquiryDate": "2021-09-14",
              "status": "Found",
              "pinjaman": [
                  {
                      "id_penyelenggara": "5",
                      "jenis_pengguna": 1,
                      "nama_borrower": "HIDAYATULLOH",
                      "no_identitas": "3175062302970007",
                      "no_npwp": "000000000000000",
                      "tgl_perjanjian_borrower": "2021-07-14",
                      "tgl_penyaluran_dana": "2021-07-14",
                      "nilai_pendanaan": 1200000.0,
                      "tgl_pelaporan_data": "2021-07-28",
                      "sisa_pinjaman_berjalan": 0.0,
                      "tgl_jatuh_tempo_pinjaman": "2021-08-11",
                      "kualitas_pinjaman": "1",
                      "dpd_terakhir": 0,
                      "dpd_max": 0,
                      "status_pinjaman": "L",
                      "jenis_pengguna_ket": "Individual",
                      "kualitas_pinjaman_ket": "Lancar (<30 hari)",
                      "status_pinjaman_ket": "Fully Paid"
                  }
              ]
            }
      </code>
    </article>
  </section>
  <br><br><hr><br><br>








 <!-- Section SMS GATEWAY -->
  <section class="main-section" id="Api_SMS">
    <header>SMS Gateway</header>
    <article>
      <p>
      SMS sent via relay service is connected to 3rd party Alibaba and TIG to facilitate sending SMS marketing, OTP and notifications
      </p>
    </article>
    <header>SMS Request</header>
    <article>
      HTTP Request:
      <code
        >POST http://147.139.165.214:8000/api/api-sendsms
      </code>


      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
      Inquiry Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>content</td>
              <td><strong>string (required)</strong>This is the text sending to the target phone number
              </td>
            </tr>
            <tr>
              <td>numberphone</td>
              <td><strong>string (required)</strong><br>This is the text sending to the target phone number</td>
            </tr>
            <tr>
              <td>type</td>
              <td><strong>string (required)</strong><br>choose the type of sending sms like : <b>"OTP"</b> for verify code or  <b>"NOTIFY"</b> for notification  <b>"MKT"</b> for marketing</td>
            </tr>
            <tr>
              <td>vendor</td>
              <td><strong>string (required)</strong><br>choose the vendor of sending sms like : <b>"TIG"</b> Service 3rd Party or <b>"ALIBABA"</b> Service 3rd Party  or <b>"NUSASMS" </b>Service 3rd Party</td>
            </tr>
          </tbody>
        </table>
        </code>
      Example Request Body :
      <code> {
              "content" : "Kepada Pelanggan Yth, Mohon maaf, pengajuan pinjaman Anda belum disetujui belum memenuhi kriteria analisa pinjaman kami. Jika ada pertanyaan, silahkan hubungi nomor layanan Customer Service OK P2P  (021) 2410 3010 (ONLY TEST)",
              "numberphone" : "6285624282247",
              "type" : "OTP",
              "vendor"  : "TIG"
            }
      </code>
      Example Response :
      <code>{
                "ResponseCode": "OK",
                "NumberDetail": {
                    "Country": "Indonesia",
                    "Region": "Indonesia",
                    "Carrier": "IM3"
                },
                "RequestId": "60218E45-8216-331D-B0D2-C8F7C81560AE",
                "ResponseDescription": "152311631612761021^0",
                "Segments": "2",
                "To": "6285624282247",
                "From": "OKP2P",
                "MessageId": "152311631612761021"
            }
      </code>
    </article>
  </section>
  <br><br><hr><br><br>





 <!-- Section ILUMA -->
  <section class="main-section" id="Api_Iluma">
    <header>API Iluma</header>
    <article>
      <p>
       The Bank Name Validator can be used to look up the name attached to bank accounts in Indonesia and run a configurable name similarity algorithm betweeen the result and the provided name. It also provides an indication whether the account is a normal bank account or not (e.g a Virtual Account), allowing you to confirm that the account is truly owned by the individual and minimising your exposure to fraud.
      </p>
    </article>
    <header>Bank Validation Request</header>
    <article>
       HTTP Request:
      <code
        >POST http://147.139.165.214:8000/api/api-iluma-bank-validation
      </code>
      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
      Inquiry Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>bank_account_number</td>
              <td>Bank accounts will be inquired strictly as entered. If the account number requires leading 0s, you should ensure these are retained</td>
            </tr>
            <tr>
              <td>bank_code</td>
              <td><strong>string (required)</strong><br>The bank code. See <a href="/bank_codes.html">Bank Codes</a></td>
            </tr>
            <tr>
              <td>given_name</td>
              <td><strong>string (required)</strong><br>The given name that will be used for name matching purposes. Please do not include honorifics.</td>
            </tr>
          </tbody>
        </table>
        </code>
        Errors :
        <code>
        <table>
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>UNSUPPORTED_BANK_CODE_ERROR</td>
                <td>Destination bank is not supported, request is using the wrong bank code.</td>
              </tr>
            </tbody>
          </table>
          </code>
      Example Request Body :
      <code
        >{
          "bank_account_number" : "0559318289",
          "bank_code" : "BCA",
          "given_name" : "Rendy Novian"
        }
      </code>
      Example  Response :
      <code
        >{
              "status": "SUCCESS",
              "bank_account_number": "0559318289",
              "bank_code": "BCA",
              "updated": "2021-08-25T08:27:48.369Z",
              "is_normal_account": true,
              "name_matching_result": "NOT_MATCH",
              "id": "6125ff04bd6b2e53e70584e9"
          }
      </code>
    </article>
  </section>
  <br><br><hr><br><br>







 <!-- Section IZI DATA (Creadit Featured) -->
  <section class="main-section" id="Api_Creditfeature">
    <header>Izi Data (Credit Featured)</header>
    <article>
      <p>
      According to the phone number、 ktp、 name, return custom features that customers need, including credit score, inquiries feature and features that other interfaces can output.      
    </p>
    </article>
    <header>Credit Featured V.2 Request</header>
    <article>
       HTTP Request:
      <code
        >POST http://147.139.165.214:8000/api/api-izi-post
      </code>
      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
       Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>phone</td>
              <td><strong>string (required)</strong>Phone number</td>
            </tr>
            <tr>
              <td>name</td>
              <td><strong>string (required)</strong><br>Username borrower</td>
            </tr>
            <tr>
              <td>ktp</td>
              <td><strong>string (required)</strong><br>Filled by 16 numerics of KTP (National ID) number.</td>
            </tr>
          </tbody>
        </table>
        </code>
          Example Request Body :
          <code
            >{
                  "phone":"081312555467",
                  "name":"Wahyu Saepulloh",
                  "ktp":"3203032411930003"
              }
          </code>
          Example  Response :
          <code
            >{
                "status": "OK",
                "message": {
                    "creditscore": 585,
                    "featurelist": {
                        "A_II_14d": 1,
                        "A_II_180d": 14,
                        "A_II_21d": 2,
                        "A_II_30d": 2,
                        "A_II_360d": 23,
                        "A_II_3d": 0,
                        "A_II_60d": 4,
                        "A_II_7d": 0,
                        "A_II_90d": 8,
                        "A_PI_14d": 2,
                        "A_PI_180d": 15,
                        "A_PI_21d": 2,
                        "A_PI_30d": 2,
                        "A_PI_360d": 23,
                        "A_PI_3d": 0,
                        "A_PI_60d": 2,
                        "A_PI_7d": 0,
                        "A_PI_90d": 6,
                        "B_II_14d": 0,
                        "B_II_180d": 1,
                        "B_II_21d": 0,
                        "B_II_30d": 0,
                        "B_II_360d": 1,
                        "B_II_3d": 0,
                        "B_II_60d": 0,
                        "B_II_7d": 0,
                        "B_II_90d": 0,
                        "B_PI_14d": 0,
                        "B_PI_180d": 0,
                        "B_PI_21d": 0,
                        "B_PI_30d": 0,
                        "B_PI_360d": 0,
                        "B_PI_3d": 0,
                        "B_PI_60d": 0,
                        "B_PI_7d": 0,
                        "B_PI_90d": 0,
                        "C_II_14d": 0,
                        "C_II_180d": 0,
                        "C_II_21d": 0,
                        "C_II_30d": 0,
                        "C_II_360d": 0,
                        "C_II_3d": 0,
                        "C_II_60d": 0,
                        "C_II_7d": 0,
                        "C_II_90d": 0,
                        "C_PI_14d": 0,
                        "C_PI_180d": 0,
                        "C_PI_21d": 0,
                        "C_PI_30d": 0,
                        "C_PI_360d": 0,
                        "C_PI_3d": 0,
                        "C_PI_60d": 0,
                        "C_PI_7d": 0,
                        "C_PI_90d": 0,
                        "II_v4_feature_status": "OK",
                        "PI_v4_feature_status": "OK",
                        "b_activeStatus": 0,
                        "b_activeYear": 4,
                        "b_class": null,
                        "b_isopen": 1,
                        "b_members": 1,
                        "b_membership": 13,
                        "ece": 1,
                        "eco": 1,
                        "ecp": 1,
                        "ect": 1,
                        "identity_address": "KP PANYANDUNGAN",
                        "identity_city": "CIANJUR",
                        "identity_date_of_birth": "24-11-1993",
                        "identity_district": "CAMPAKA",
                        "identity_gender": "LAKI-LAKI",
                        "identity_marital_status": "BELUM KAWIN ",
                        "identity_match": 0,
                        "identity_name": "RENDY NOVIAN",
                        "identity_nationnality": "WNI",
                        "identity_place_of_birth": "BANDUNG",
                        "identity_province": "JAWA BARAT",
                        "identity_religion": "ISLAM",
                        "identity_rt": "003",
                        "identity_rw": "001",
                        "identity_status": "OK",
                        "identity_village": "GIRIMUKTI",
                        "identity_work": "PELAJAR/MAHASISWA",
                        "idinquiries_14d": 1,
                        "idinquiries_180d": 15,
                        "idinquiries_21d": 2,
                        "idinquiries_30d": 2,
                        "idinquiries_360d": 24,
                        "idinquiries_3d": 0,
                        "idinquiries_60d": 4,
                        "idinquiries_7d": 0,
                        "idinquiries_90d": 8,
                        "idinquiries_status": "OK",
                        "idinquiries_total": 30,
                        "idinquiriesuname_14d": 1,
                        "idinquiriesuname_180d": 5,
                        "idinquiriesuname_21d": 1,
                        "idinquiriesuname_30d": 1,
                        "idinquiriesuname_360d": 5,
                        "idinquiriesuname_3d": 0,
                        "idinquiriesuname_60d": 1,
                        "idinquiriesuname_7d": 0,
                        "idinquiriesuname_90d": 1,
                        "idinquiriesuname_status": "OK",
                        "idinquiriesuname_total": 7,
                        "isfacebook": "NO_ACCOUNT",
                        "iswhatsapp": "yes",
                        "multiidinquiries_14d": 0,
                        "multiidinquiries_21d": 0,
                        "multiidinquiries_30d": 0,
                        "multiidinquiries_60d": 0,
                        "multiidinquiries_7d": 0,
                        "multiidinquiries_90d": 0,
                        "multiidinquiries_total": 5,
                        "multiphone_idinfo": "",
                        "multiphone_phoneinfo_id": "3207210104940001",
                        "multiphone_phoneinfo_id_phone": "+6281312555467",
                        "multiphone_status": "OK",
                        "multiphoneinquiries_14d": 0,
                        "multiphoneinquiries_21d": 0,
                        "multiphoneinquiries_30d": 0,
                        "multiphoneinquiries_60d": 0,
                        "multiphoneinquiries_7d": 0,
                        "multiphoneinquiries_90d": 0,
                        "multiphoneinquiries_total": 0,
                        "phoneage": "10-12month",
                        "phoneage_status": "OK",
                        "phonealive_id_num": 7,
                        "phonealive_phone_num": 2,
                        "phonealive_status": "OK",
                        "phoneinquiries_14d": 2,
                        "phoneinquiries_180d": 15,
                        "phoneinquiries_21d": 2,
                        "phoneinquiries_30d": 2,
                        "phoneinquiries_360d": 23,
                        "phoneinquiries_3d": 0,
                        "phoneinquiries_60d": 2,
                        "phoneinquiries_7d": 0,
                        "phoneinquiries_90d": 6,
                        "phoneinquiries_status": "OK",
                        "phoneinquiries_total": 23,
                        "phoneinquiriesuname_14d": 2,
                        "phoneinquiriesuname_180d": 2,
                        "phoneinquiriesuname_21d": 2,
                        "phoneinquiriesuname_30d": 2,
                        "phoneinquiriesuname_360d": 3,
                        "phoneinquiriesuname_3d": 0,
                        "phoneinquiriesuname_60d": 2,
                        "phoneinquiriesuname_7d": 0,
                        "phoneinquiriesuname_90d": 2,
                        "phoneinquiriesuname_status": "OK",
                        "phoneinquiriesuname_total": 3,
                        "phonescore": 32,
                        "phonescore_status": "OK",
                        "phoneverify": "NOT_MATCH",
                        "preference_bank_180d": 11,
                        "preference_bank_270d": 12,
                        "preference_bank_60d": 6,
                        "preference_bank_90d": 7,
                        "preference_ecommerce_180d": 59,
                        "preference_ecommerce_270d": 72,
                        "preference_ecommerce_60d": 18,
                        "preference_ecommerce_90d": 36,
                        "preference_game_180d": 11,
                        "preference_game_270d": 11,
                        "preference_game_60d": 2,
                        "preference_game_90d": 8,
                        "preference_lifestyle_180d": 112,
                        "preference_lifestyle_270d": 180,
                        "preference_lifestyle_60d": 28,
                        "preference_lifestyle_90d": 49,
                        "preference_status": "OK",
                        "topup_0_180_avg": 13,
                        "topup_0_180_max": 150,
                        "topup_0_180_min": 5,
                        "topup_0_180_times": 96,
                        "topup_0_30_avg": 55,
                        "topup_0_30_max": 150,
                        "topup_0_30_min": 5,
                        "topup_0_30_times": 3,
                        "topup_0_360_avg": 16,
                        "topup_0_360_max": 150,
                        "topup_0_360_min": 5,
                        "topup_0_360_times": 160,
                        "topup_0_60_avg": 21,
                        "topup_0_60_max": 150,
                        "topup_0_60_min": 5,
                        "topup_0_60_times": 18,
                        "topup_0_90_avg": 16,
                        "topup_0_90_max": 150,
                        "topup_0_90_min": 5,
                        "topup_0_90_times": 40,
                        "topup_180_360_avg": 20,
                        "topup_180_360_max": 115,
                        "topup_180_360_min": 5,
                        "topup_180_360_times": 64,
                        "topup_30_60_avg": 14,
                        "topup_30_60_max": 150,
                        "topup_30_60_min": 5,
                        "topup_30_60_times": 15,
                        "topup_360_720_avg": 45,
                        "topup_360_720_max": 100,
                        "topup_360_720_min": 10,
                        "topup_360_720_times": 9,
                        "topup_60_90_avg": 11,
                        "topup_60_90_max": 150,
                        "topup_60_90_min": 5,
                        "topup_60_90_times": 22,
                        "topup_90_180_avg": 11,
                        "topup_90_180_max": 150,
                        "topup_90_180_min": 5,
                        "topup_90_180_times": 56,
                        "topup_status": "OK",
                        "whatsapp_avatar": "yes",
                        "whatsapp_company_account": "",
                        "whatsapp_signature": "اللَّهُمَّ اغْفِرْ لِي ، وَارْحَمْنِي ، وَاهْدِني ، وَعَافِني ، وَارْزُقْنِي",
                        "whatsapp_updatestatus_time": 20210608
                    }
                }
            }
         </code>
        </article>
    </section>
    <br><br><hr><br><br>



    <section class="main-section" id="Api_Salary">
    <header>Izi Data (Salary)</header>
    <article>
      <p>
      Query the latest average monthly salary index based on the input ID
      </p>
    </article>
    <header>Salary Request</header>
    <article>
       HTTP Request:
      <code
        >POST http://147.139.165.214:8000/api/api-sallary
      </code>
      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
      Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>id</td>
              <td><strong>string (required)</strong><br>Inquirer's ID number</td>
            </tr>
          </tbody>
        </table>
        </code>
       
      Example Request Body :
      <code>{
            "id" : "3302212007840003"
          }
      </code>
      Example  Response :
      <code>{
              "status": "OK",
              "message": {
                  "salary": 700
              }
          }
      </code>
    </article>
  </section>
  <br><br><hr><br><br>


  <section class="main-section" id="Api_Pefindo">
    <header>Pefindo</header>
    <article>
      <p>
      Web Service is one of some PBK system features which is ready for all members to get consumer report in online mode. This feature also known as Host to Host reporting system. Web Service feature is invisible for member’s users in getting consumer reports from Pefindo Biro
      Kredit for report data is requested and managed by member’s application system, for example Loan Origination System (LOS).
      </p>
    </article>
    <header>Inquiry Request</header>
    <article>
       HTTP Request:
      <code
        >POST http://147.139.165.214:8000/api/relay/requestSmartSearch
      </code>
      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
      Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>ktp</td>
              <td><strong>string (required)</strong><br>KTP number</td>
            </tr>
            <tr>
              <td>DateOfBirth</td>
              <td><strong>string (required)</strong><br>Inquirer's ID number</td>
            </tr>
            <tr>
              <td>fullname</td>
              <td><strong>string (required)</strong><br>full name of the searched subject or it's part (min - 3 chars)</td>
            </tr>
            <tr>
              <td>InquiryReason</td>
              <td><strong>string (required)</strong><br>mandatory, lookup value. (case sensitive)
              <br>Here is the list of possible values:<br>- ProvidingFacilities<br>- MonitoringDebtorOrCustomer<br>- OperationalRiskManagement<br>- FulfilRegulationRequirements<br>- AnotherReason (Not recommended</td>
            </tr>
          </tbody>
        </table>
        </code>
       
      Example Request Body :
      <code>{
              "ktp" : "3203032411930003",
              "DateOfBirth" : "1993-11-24",
              "fullname" : "Rendy Novian",
              "InquiryReason" : "ProvidingFacilities"
            }
      </code>
      Example  Response :
      <code>{
              "sBody": {
                  "GetCustomReportResponse": {
                      "GetCustomReportResult": {
                          "aCIP": {
                              "bRecordList": {
                                  "bRecord": [
                                      {
                                          "bDate": "2021-09-01T00:00:00",
                                          "bGrade": "D3",
                                          "bProbabilityOfDefault": "17.21",
                                          "bReasonsList": {
                                              "bReason": {
                                                  "bCode": "NSL1",
                                                  "bDescription": "Contracts from 3 different subscribers last 6 months"
                                              }
                                          },
                                          "bScore": "598",
                                          "bTrend": "NoChange"
                                      },
                                      {
                                          "bDate": "2021-08-31T00:00:00",
                                          "bGrade": "D3",
                                          "bProbabilityOfDefault": "17.21",
                                          "bReasonsList": {
                                              "bReason": {
                                                  "bCode": "NSL1",
                                                  "bDescription": []
                                              }
                                          },
                                          "bScore": "598",
                                          "bTrend": "NoChange"
                                      },
                                      {
                                          "bDate": "2020-09-30T00:00:00",
                                          "bGrade": "D2",
                                          "bProbabilityOfDefault": "13.61",
                                          "bReasonsList": [],
                                          "bScore": "610",
                                          "bTrend": "Up"
                                      }
                                  ]
                              }
                          },
                          "aCIQ": [],
                          "aCollaterals": [],
                          "aCompany": [],
                          "aContractOverview": {
                              "bContractList": {
                                  "bContract": [
                                      {
                                          "bContractStatus": "GrantedAndActivated",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "10668350.00",
                                              "cValue": "10668350.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Open",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "NonBankingFinancialInstitutions",
                                          "bStartDate": "2021-03-03T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "10668350.00",
                                              "cValue": "10668350.00"
                                          },
                                          "bTypeOfContract": "OtherWithLoanAgreement"
                                      },
                                      {
                                          "bContractStatus": "GrantedAndActivated",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Open",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "NonBankingFinancialInstitutions",
                                          "bStartDate": "2019-11-23T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "600000.00",
                                              "cValue": "600000.00"
                                          },
                                          "bTypeOfContract": "Installment"
                                      },
                                      {
                                          "bContractStatus": "GrantedAndActivated",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "900000.00",
                                              "cValue": "900000.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Open",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "Banks",
                                          "bStartDate": "2021-07-19T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "900000.00",
                                              "cValue": "900000.00"
                                          },
                                          "bTypeOfContract": "ChannelingLoan"
                                      },
                                      {
                                          "bContractStatus": "GrantedAndActivated",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "201298467.00",
                                              "cValue": "201298467.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Open",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "Banks",
                                          "bStartDate": "2021-06-14T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "201298467.00",
                                              "cValue": "201298467.00"
                                          },
                                          "bTypeOfContract": "ChannelingLoan"
                                      },
                                      {
                                          "bContractStatus": "Settled",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Closed",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "Banks",
                                          "bStartDate": "2018-06-06T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "2106040.00",
                                              "cValue": "2106040.00"
                                          },
                                          "bTypeOfContract": "OtherWithLoanAgreement"
                                      },
                                      {
                                          "bContractStatus": "SettledWithDiscount",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Closed",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "NonBankingFinancialInstitutions",
                                          "bStartDate": "2016-06-18T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "20677532.00",
                                              "cValue": "20677532.00"
                                          },
                                          "bTypeOfContract": "Installment"
                                      },
                                      {
                                          "bContractStatus": "Settled",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Closed",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "Banks",
                                          "bStartDate": "2020-12-22T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bTypeOfContract": "OtherWithLoanAgreement"
                                      },
                                      {
                                          "bContractStatus": "Settled",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Closed",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "Banks",
                                          "bStartDate": "2020-12-22T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bTypeOfContract": "OtherWithLoanAgreement"
                                      },
                                      {
                                          "bContractStatus": "Settled",
                                          "bOutstandingAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bPastDueDays": "0",
                                          "bPhaseOfContract": "Closed",
                                          "bRoleOfClient": "MainDebtor",
                                          "bSector": "Banks",
                                          "bStartDate": "2021-06-16T00:00:00",
                                          "bTotalAmount": {
                                              "cCurrency": "IDR",
                                              "cLocalValue": "0.00",
                                              "cValue": "0.00"
                                          },
                                          "bTypeOfContract": "ChannelingLoan"
                                      }
                                  ]
                              }
                          },
                          "aContractSummary": [],
                          "aContracts": [],
                          "aCurrentRelations": [],
                          "aDashboard": [],
                          "aDisputes": [],
                          "aFinancials": [],
                          "aIndividual": {
                              "bContact": {
                                  "bEmail": "0215027rendynovian@gmail.com",
                                  "bFixedLine": "085624282247",
                                  "bMobilePhone": "085624282247"
                              },
                              "bGeneral": {
                                  "bAlias": [],
                                  "bCitizenship": "ID",
                                  "bClassificationOfIndividual": "Individual",
                                  "bDateOfBirth": "1993-11-24T00:00:00",
                                  "bEducation": "Bachelor",
                                  "bEmployerName": "OKE PTOP INDONESIA PT",
                                  "bEmployerSector": "ID_9990",
                                  "bEmployment": "Computer",
                                  "bFullName": "Rendy Novian",
                                  "bFullNameLocal": "Rendy Novian",
                                  "bGender": "Male",
                                  "bMaritalStatus": "Married",
                                  "bMotherMaidenName": "Didah Kurnia",
                                  "bPlaceOfBirth": "BANDUNG",
                                  "bResidency": "Yes",
                                  "bSocialStatus": "Employed"
                              },
                              "bIdentifications": {
                                  "bKTP": "3203032411930003",
                                  "bNPWP": "858917081406000",
                                  "bPassportIssuerCountry": "NotSpecified",
                                  "bPassportNumber": [],
                                  "bPefindoId": "1239743373"
                              },
                              "bMainAddress": {
                                  "bAddressLine": "KP LIO SANTA NO 26 RT 001 RW 006 KEL GEDONG PANJANG KEC CITAMIANG SUKABUMI, 0193, 43142, GEDONG PANJANG, CITAMIANG, ID",
                                  "bCity": "Sukabumi, Kota.",
                                  "bCountry": "ID",
                                  "bDistrict": "Gedong Panjang",
                                  "bParish": "Citamiang",
                                  "bPostalCode": "43142",
                                  "bStreet": "Kp Lio Santa No 26 Rt 001 Rw 006 Kel Gedong Panjang Kec Citamiang Sukabumi"
                              }
                          },
                          "aInquiries": [],
                          "aInvolvements": [],
                          "aOtherLiabilities": [],
                          "aParameters": {
                              "aConsent": "true",
                              "aIDNumber": "1239743373",
                              "aIDNumberType": "PefindoId",
                              "aInquiryReason": "ProvidingFacilities",
                              "aInquiryReasonText": [],
                              "aReportDate": "2021-09-01T00:00:00",
                              "aSections": {
                                  "bstring": [
                                      "CIP",
                                      "SubjectInfo",
                                      "SubjectHistory",
                                      "ContractList"
                                  ]
                              },
                              "aSubjectType": "Individual"
                          },
                          "aReportInfo": {
                              "bCreated": "2021-09-01T13:40:27.1792617+07:00",
                              "bReferenceNumber": "203857840-1239743373",
                              "bReportStatus": "ReportGenerated",
                              "bRequestedBy": "Wahyu",
                              "bVersion": "553"
                          },
                          "aSecurities": [],
                          "aSubjectInfoHistory": {
                              "bAddressList": {
                                  "bAddress": [
                                      {
                                          "bItem": "MainAddress",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2018-06-30T00:00:00",
                                          "bValidTo": "2019-11-30T00:00:00",
                                          "bValue": "Kp Panyandungan Rt 003 Rw 001, Cianjur, Kab. 43263, Girimukti, Campaka, ID"
                                      },
                                      {
                                          "bItem": "MainAddressLine",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2018-06-30T00:00:00",
                                          "bValidTo": "2019-01-31T00:00:00",
                                          "bValue": "KP PANYANDUNGAN RT 003 RW 001, 43263, Campaka, Girimukti, ID"
                                      },
                                      {
                                          "bItem": "MainAddressLine",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-01-31T00:00:00",
                                          "bValidTo": "2019-02-28T00:00:00",
                                          "bValue": "KP PANYANDUNGAN RT 003 RW 001, 0110, 43263, Campaka, Girimukti, ID"
                                      },
                                      {
                                          "bItem": "MainAddressLine",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-02-28T00:00:00",
                                          "bValidTo": "2019-03-31T00:00:00",
                                          "bValue": "KP PANYANDUNGAN RT 003 RW 001, 43263, Campaka, Girimukti, ID"
                                      },
                                      {
                                          "bItem": "MainAddressLine",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-03-31T00:00:00",
                                          "bValidTo": "2019-11-30T00:00:00",
                                          "bValue": "KP PANYANDUNGAN RT 003 RW 001, 0110, 43263, Campaka, Girimukti, ID"
                                      }
                                  ]
                              },
                              "bContactList": {
                                  "bContact": {
                                      "bItem": "MobilePhone",
                                      "bSubscriber": "B02",
                                      "bValidFrom": "2018-06-30T00:00:00",
                                      "bValidTo": "2019-11-30T00:00:00",
                                      "bValue": "6285795044136"
                                  }
                              },
                              "bGeneralList": {
                                  "bGeneral": [
                                      {
                                          "bItem": "FullNameLocal",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-01-31T00:00:00",
                                          "bValidTo": "2019-02-28T00:00:00",
                                          "bValue": "Rendy Novian"
                                      },
                                      {
                                          "bItem": "Banking.MaritalStatus",
                                          "bSubscriber": "B03",
                                          "bValidFrom": "2021-06-30T00:00:00",
                                          "bValidTo": "2021-07-31T00:00:00",
                                          "bValue": "Single"
                                      },
                                      {
                                          "bItem": "Banking.MaritalStatus",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-01-31T00:00:00",
                                          "bValidTo": "2019-02-28T00:00:00",
                                          "bValue": "Single"
                                      },
                                      {
                                          "bItem": "Banking.MaritalStatus",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-03-31T00:00:00",
                                          "bValidTo": "2019-11-30T00:00:00",
                                          "bValue": "Single"
                                      },
                                      {
                                          "bItem": "Banking.SocialStatus",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-01-31T00:00:00",
                                          "bValidTo": "2019-02-28T00:00:00",
                                          "bValue": "Employed"
                                      },
                                      {
                                          "bItem": "Banking.Residency",
                                          "bSubscriber": "B02",
                                          "bValidFrom": "2019-01-31T00:00:00",
                                          "bValidTo": "2019-02-28T00:00:00",
                                          "bValue": "Yes"
                                      }
                                  ]
                              },
                              "bIdentificationsList": []
                          }
                      }
                  }
              }
          }
      </code>
    </article>
  </section>
  <br><br><hr><br><br>

  <section class="main-section" id="Api_AdvanceAi">
    <header>Advance AI (Credit Score)</header>
    <article>
      <p>
         This is API for the Credit Score 3.0. Credit score is used to comprehensively assess the credit risk level of borrowers. In addition to the specific credit score, it also provides significant features for predicting the default probability.      </p>
    </article>
    <header>Inquiry Request</header>
    <article>
       HTTP Request:
      <code
        >POST http://147.139.165.214:8000/api/api-advance-score
      </code>
      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
      Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>name</td>
              <td><strong>string (required)</strong><br>Customer name, should be the full name of the person, the length should be greater than 3</td>
            </tr>
            <tr>
             <td>idNumber</td>
              <td><strong>string (required)</strong><br>Customer ID number (NIK), must be a 16 digit number,cannot be empty, cannot start with 0</td>
            </tr>
            <tr>
             <td>phoneNumber</td>
              <td><strong>string (required)</strong><br>Customer phone number, start with "+628"</td>
            </tr>
          </tbody>
        </table>
        </code>
       
      Example Request Body :
      <code>{
                "name" : "Rendy Novian",
                "idNumber" : "3203032411930003",
                "phoneNumber" : "+6285624282247"
            }
      </code>
      Example  Response :
      <code>{
                "code": "SUCCESS",
                "message": "OK",
                "data": {
                    "score": 515,
                    "features": {
                        "GD_C_V3_5": 3.0,
                        "GD_C_V3_4": 10.775,
                        "GD_C_V3_19": 7.0,
                        "GD_C_V3_7": 4.0,
                        "GD_C_V3_6": 32.56,
                        "GD_C_V3_9": 100000.0,
                        "GD_C_V3_16": 5000.0,
                        "GD_C_V3_8": 1.0,
                        "GD_C_V3_15": 21.0,
                        "GD_C_V3_18": 5000.0,
                        "GD_C_V3_17": 34440.47619047619,
                        "GD_C_V3_12": 0.0,
                        "GD_C_V3_11": 22209.408197878663,
                        "GD_C_V3_14": 2.0,
                        "GD_C_V3_13": 0.0,
                        "GD_C_V3_10": 4.0,
                        "GD_C_V3_20": 0.002075630797171953,
                        "GD_C_V3_1": 1560.0,
                        "GD_C_V3_3": 34625.0,
                        "GD_C_V3_2": 13.0
                    }
                },
                "extra": null,
                "transactionId": "33348ac8c12f417b",
                "pricingStrategy": "PAY"
            }
      </code>
    </article>
  </section>
  <br><br><hr><br><br>
  
  <section class="main-section" id="Check_customers">
    <header>Check Customers Asset</header>
    <article>
      <p>
        The API that is created performs the process of checking data from assets based on asset numbers.
        the data to be displayed consists of the names of customers and bond sellers.
      </p>
    </article>
    <header>Inquiry Request</header>
    <article>
       HTTP Request:
      <code
        >POST http://147.139.165.214:8000/api/check-customers-asset
      </code>
      Setup Value Key Headers :
      <code>
      <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Content-Type :</td>
              <td>application/json</td>
            </tr>
            <tr>
              <td>Authorization :</td>
              <td><strong>Get Token From Dashboard Ops Sistem</strong><br>Bearer xxxx.xxxx.xxxx </td>
            </tr>
            <tr>
              <td>mode_dev :</td>
              <td><strong>boolean (required)</strong><br>if later will do test mode, make value <b>"false"</b> and if use production mode key then use status <b>"true"</b> </td>
            </tr>
          </tbody>
        </table>
        </code>
      Schema :
      <code>
      <table>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
             <td>ktp</td>
              <td><strong>string (required)</strong><br>KTP number (NIK), must be a 16 digit number,cannot be empty, cannot start with 0</td>
            </tr>
          </tbody>
        </table>
        </code>
       
      Example Request Body :
      <code>{
                "ktp" : "3203032411930003"
            }
      </code>
      Example  Response :
      <code>{
                  "success": true,
                  "data": [
                      {
                          "loan_no": "1000007",
                          "npl_id": 19339,
                          "cust_id_no": "3217082012810012",
                          "cust_name": "hilman setiawan",
                          "bond_seller_id": "ADAPUNDI"
                      }
                  ],
                  "message": "Customer retrieved successfully."
              }
      </code>
    </article>
  </section>

</main>
