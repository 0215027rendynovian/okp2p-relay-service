@include('inc-mitra.header')
<style>
#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	100% { 
		transform: rotate(360deg); 
	}
}
.is-hide{
	display:none;
}
</style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/js/materialize.min.js"></script>
         
<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">VIRTUAL ACCOUNT</a>
		<ul class="right hide-on-med-and-down">
			<li><a href="#">Navbar Link</a>
			</li>
		</ul>

		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="{{url('home/saldo')}}" class="sidenav-trigger right"><i class="material-icons">close</i></a>
	</div>
</nav>

<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>
<div class="container">

<div class="pv10 text-grey">Pilih Bank :</div>
	<select class="browser-default"  id="bank_code" name="bank_code">
		<option value="" disabled selected>Pilih Bank</option>
		<option value="BCA" selected>BCA</option>
		<option value="BNI">BNI</option>
		<option value="BRI">BRI</option>
	</select>

  	<form class="col s12 pv10">
		<div class="input-field">
		<br>
		<select id="nominal_saldo" name="nominal_saldo" class="browser-default">
		<option value="0" selected>-- Pilih Nominal --</option>
		<option value="20000">20000</option>
		<option value="25000">25000</option>
		<option value="50000">50000</option>
		<option value="100000">100000</option>
		<option value="150000">150000</option>
		<option value="200000">200000</option>
		<option value="250000">250000</option>
		<option value="300000">300000</option>
		<option value="500000">500000</option>
	</select>
			<label for="first_name" class="active">Min. Isi Saldo Anda Rp 20.000</label>
		</div>
	</form>
<a class=" w-100 btn bg-yellow" onclick="GenerateVa()">BUAT NOMOR VA</a>
<br><br>
<label style="color:green;"><i>*Biaya Belum Termasuk  Admin (BRI / BNI : + Rp 4.950) dan (BCA : + Rp 2.200)</i></label>
  <div class="section">
  <div class="gap"></div>
		<div class="col s12 text-grey"> Detail Virtual Account :</div>
  <div class="row section">
		<div class="col s6 text-blue">No. Pembayaran</div>
		<div class="col s6 black-text">: <span id="no_pembayaran"></span></div>
		<div class="col s6 text-blue">ID</div>
		<div class="col s6 black-text">: <span id="id_va"></span></div>
		<div class="col s6 text-blue">Nominal</div>
		<div class="col s6 black-text">: <span id="nominal"></span></div>
		
		<div class="col s12 text-blue" id="labelvaMitra">Nomor Virtual Account</div>
		<div class="col text-yellow" id="vaMitra"></div>
		<div class="col text-blue" id="vaCopy" onclick="M.toast({html: 'Copy'})"><i class="material-icons center">content_copy</i></div>
</div><div class="divider"></div>
<div class="section">
<div class="col s12 black-text">Mohon lakukan pembayaran sebelum :</div>
		<div class="col s12 text-blue"><span id="expiration_date"></div>
		</div> 
		<br><br>
		
 <a class="waves-effect waves-light btn w-100 bg-red" id="fixedbutton" href="{{url('home/show-berhasil-topup')}}">Konfirmasi Pembayaran</a>
 </div>   
	 
</div>   

@include('inc-mitra.footer')

<script>
debugger


  function GenerateVa(){
	  debugger
	$(document).ajaxSend(function() {
		$("#overlay").fadeIn(300);　
	});
	var nominal_saldo = document.getElementById('nominal_saldo').value;
	var bank_code = document.getElementById('bank_code').value;
	  debugger
	  if(nominal_saldo == 0 || nominal_saldo == undefined){
		  return alert('nominal saldo harus terisi')
	  }

	  if(bank_code == '' || bank_code == undefined){
		  return alert('bank code harus terisi')
	  }

	  if(bank_code == 'BCA'){
		  return alert('Ops, Sistem BCA Virtual Account masih belum hadir, terkendala pada sitem payment')
	  }

	$.ajaxSetup({
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
                    type:'POST',
                    url:'/xendit-create-virtual-account',
                    data:{bank_code:bank_code,nominal_saldo:nominal_saldo},
                    success:function(data){
						debugger
						document.getElementById("no_pembayaran").innerHTML = data.external_id;
						document.getElementById("id_va").innerHTML = data.id;
						document.getElementById("nominal").innerHTML = data.expected_amount;
						document.getElementById("vaMitra").innerHTML = data.account_number;
						document.getElementById("expiration_date").innerHTML = data.expiration_date
						$("#overlay").fadeOut();
					}
        });
   }


   


  


</script>



