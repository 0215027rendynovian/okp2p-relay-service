@include('inc-mitra.header')
<style>
#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	100% { 
		transform: rotate(360deg); 
	}
}
.is-hide{
	display:none;
}
</style>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">SALDO</a>
		<ul class="right hide-on-med-and-down">
			<li><a href="#">Navbar Link</a>
			</li>
		</ul>

		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="#" onclick="goBack()" class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>
	</div>
</nav>
<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>

<div class="section">

	<div class="row center">
		<i class="material-icons text-blue">account_balance_wallet</i>
		<div class="text-red">@currency($SaldoWallet)
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s3"><a href="#isiSaldo">ISI SALDO</a>
				</li>
				<li class="tab col s3"><a href="#tarikSaldo" onclick="alerts()" disabled> 
			

                  TARIK SALDO</a>
				</li>
			</ul>
		</div>

		<div id="isiSaldo" class="col s12">
			<div class="pv10 text-grey">Pilih cara isi saldo :</div>
			<div class="container">
				<a onclick="OpenVirtualAccount()">
					<div class="card-panel">
						<div class="cardh1">
							VIRTUAL ACCOUNT
						</div><span class="right-arrow material-icons">
							keyboard_arrow_right
						</span>
					
					</div>
				</a>

			</div>
		</div>

		<div id="tarikSaldo" class="col s12">
			<div class="pv10 text-grey f12 center">Anda belum memiliki rekening</div>
			<div class="container">
				<a class="w-100 btn bg-yellow" onclick="TambahRekening()"><i class="material-icons center">add</i>TAMBAH REKENING</a>




			</div>

			<!-- Jika sudah ada rekening dan ingin narik -->

			<div class="gap"></div>
			<div class="divider"></div>
			<div class="section">


					<!-- Isi Data Rekening -->
					<div id="isi_data_rekening" style="display:none;">
							<div class="row">
								<div class="col s6 bold text-blue">
									Isi Data Rekening
								</div>
								<div class="col s6 text-red bold text-right">
							
								</div>
							</div>
							<form class="col s12">
			
							<div class="row">

									<div class="input-field">
										<input placeholder="masukan nomor rekening" id="account_number" name="account_number" type="number" class="validate">
										<label for="account_number">Nomor Rekening</label>
										</div>

							</div>

							<div class="row">
									<div class="input-field">
										<input placeholder="masukan nama rekening" id="account_name" name="account_name" type="text" class="validate">
										<label for="account_name">Nama Rekening</label>
									</div>
							</div>

							<div class="row">
							<div class="pv10 text-grey">Pilih Bank :</div>
									<div class="input-field">
									<select class="browser-default" id="bank_code" name="bank_code" autocomplete="bank_code" autofocus>
											<option selected>-- Pilih Bank --</option>
												@foreach($Bankcode as $Bankcode)
													<option value="{{$Bankcode->bank_code}}">{{$Bankcode->bank_name}}</option>
												@endforeach
											</select> 
									
									</div>
							</div>

						
			
								<div class="row">
									<div style="padding-left:30px; padding-right:30px;">
										<button class="waves-effect waves-light w-100 btn bg-yellow" onclick="SimpanRekening()"><i class="material-icons center">save</i>SIMPAN REKENING</button>
									</div>
								</div>
								</form>
							</div>
					  </div>



					  <div id="saldo_penghasilan" style="display:block;">
					  <div class="row">
						<div class="col s6 bold text-blue">
							Saldo Penghasilan
						</div>
						<div class="col s6 text-red bold text-right">
							Rp 0
						</div>
					</div>


					<div class="row">
						<div class="col s6 ">
							Nominal Penarikan
						</div>
						<div class="col s6 text-blue text-right">
							Tarik Semua
						</div>
					</div>

						<div class="row">
							<form class="col s12">

								<div class="input-field">
									<input placeholder="Nominal Penarikan" id="nominaltariksaldo" type="number" class="validate">
									<label for="first_name">Min. penarikan Rp 50.000</label>

							</form>
							</div>
						</div>


						<div class="row">
							<div class="col s12 bold text-blue">
								Rekening Tujuan
							</div>

							
							@foreach($Rekening as $key => $p)	
							<div class="col s12 ">
								@if(isset($RekeningTujuan))
								 <p>
									@if($RekeningTujuan == $p->id)
										<label>
											<input name="group1" id="group{{$key+1}}" type="radio" onclick="check({{$p->id}},'{{$p->BankCode->bank_code}}')" checked/>
											<span>{{$p->BankCode->bank_name}}</span><br>
											<span class="pl10">{{$p->account_number}}- {{$p->account_name}}</span>
										</label>
										@else
										<label>
											<input name="group1" id="group{{$key+1}}" type="radio" onclick="check({{$p->id}},'{{$p->BankCode->bank_code}}')"/>
											<span>{{$p->BankCode->bank_name}}</span><br>
											<span class="pl10">{{$p->account_number}}- {{$p->account_name}}</span>
										</label>
										@endif
								 </p>
								 @else
								 <p>

										<label>
											<input name="group1" id="group{{$key+1}}" type="radio" onclick="check({{$p->id}},'{{$p->BankCode->bank_code}}')"/>
											<span>{{$p->BankCode->bank_name}}</span><br>
											<span class="pl10">{{$p->account_number}}- {{$p->account_name}}</span>
										</label>
								
								 </p>
								 @endif
							</div>
			
							@endforeach
					  </div>
					  <div class="divider"></div>
					  	<!-- end -->
				<div class="pv10 text-grey f12 center">Dengan klik tombol tarik saldo, Anda telah setuju dengan <a href="#">ketentuan penarikan saldo</a>
				</div>
				<a class="waves-effect waves-light btn w-100 bg-red" id="fixedbutton" onclick="TarikSaldo()">TARIK SALDO</a>
			    </div>
			</div>

		</div>

	</div>


</div>


@include('inc-mitra.footer' )
<script>

function alerts(){
	alert('Maaf, sedang maintenance untuk penarikan saldo');
	location.reload();
}

function OpenVirtualAccount(){
	window.location = 'virtual-account';
}

var id_rekening;
var bankcode;
function check(id, Bankcode){
	id_rekening = id;
	bankcode = Bankcode;
}


function TarikSaldo(){

	debugger
	// Show Loading
	$(document).ajaxSend(function() {
		$("#overlay").fadeIn(300);　
	});


	// Validate Nominal Saldo
	var nominaltariksaldo = document.getElementById("nominaltariksaldo").value;
	if(nominaltariksaldo == undefined || nominaltariksaldo == ''){
		return alert('Harap Isi Nominal Saldo')
	}

	// Validate Bank Code
	var GetCodeBank = "<?php echo $BankCode ? $BankCode : 0; ?>";
	
	//Cek Bank Code sudah ada di database
	if(GetCodeBank === "0"){

			//jika bank code baru belum dipilih
			if(bankcode == undefined){
				alert('Harap Pilih bank terlebih dahulu');
			}
	
	}else{

			//jika bank code ada di database tetapi tidak dipilih
			if(bankcode == undefined){
				bankcode = GetCodeBank;
			}
			

	}


	// Validate Id Rekening
	var getIdRekening =  "<?php echo $RekeningTujuan  ? $RekeningTujuan : 0; ?>";
	
	// Cek Rekening sudah ada di database
	if(getIdRekening === "0"){

			// jika rekening baru belum dipilih
			if(id_rekening == undefined){
				alert("Harap pilih kode rekening terlebih dahulu");
			}

	}else{

		//jika rekening ada di database tetapi tidak dipilih
		if(id_rekening == undefined){
		   id_rekening = getIdRekening;
		}
	}


			// Validate  Saldo
			if(<?php echo $SaldoWallet?> === 0){
				return alert('maaf saldo anda tidak mencukupi');
			}


			// Validate  Saldo melebihi jumlah penarikan
			var nominaltariksaldo = document.getElementById("nominaltariksaldo").value;
				if(<?php echo $SaldoWallet?> < parseInt(nominaltariksaldo)){
				return alert('maaf Jumlah Penarikan melebihi jumlah saldo');
			}

	$.ajaxSetup({
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
                    type:'POST',
                    url:'/api-tarik-saldo-mitra',
                    data:{id_rekening:id_rekening,amount:nominaltariksaldo, bank_code:bankcode, uid:<?php echo Auth::user()->id ?>},
                    success:function(data){
						debugger
						$("#overlay").fadeOut();
						location.reload();
					}
        });
   }

$("#saldo_penghasilan").fadeIn();

 function TambahRekening(){
	$("#isi_data_rekening").fadeIn();
	$("#saldo_penghasilan").fadeOut();
 }
 function SimpanRekening(){

	$("#isi_data_rekening").fadeOut();
	$("#saldo_penghasilan").fadeIn();


	var account_number = document.getElementById('account_number').value;
	var account_name = document.getElementById('account_name').value;
	var addBankCode = document.getElementById('bank_code').value;


	if(account_number == ''){
		return alert('Nomor Rekening Harus Terisi');
	}
	if(account_name == ''){
		return alert('Nama Rekening Terisi');
	}
	if(addBankCode == ''){
		return alert('Kode bank harus terisi');
	}


	$.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
                $.ajax({
                        type:'POST',
                        url:'/api-save-rekening-mitra',
                        data:{account_number:account_number,account_name:account_name,bank_code:addBankCode},
                          success:function(data){
                            if(data.response=="Status Berhasil"){
								window.location='saldo#tarikSaldo'
                            }
                            console.log(data)
                          }
                });

	
	};
</script>