@include('inc-mitra.header')
<style>
.img {
  border-radius: 50%;
}
</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- nav -->
<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a href="#" class="" id="nav-title">HOME</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Reservation List</a></li>
        <li><a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
      </ul>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <a href="#" data-target="#" class="sidenav-trigger right"><i class="material-icons">settings</i></a>
    </div>
  </nav>
<div class="container">

<div class="section">

<div class="row">

<div class="col s8" id="user-info"><i class="material-icons">account_balance_wallet</i><span>@currency($SaldoWallet)</span></div>


<div class="col s4 text-right">
<a id="user-notif" onclick="topup()" href="#" class="sidenav-trigger right"><div class="waves-effect waves-light btn-small">TOPUP</div></a></div>
</div>

<div class="row">
    <div class="col s12 center">
    @if(empty($DetailMitra->chooseFileSelfie))
    <img class="mitra-img" style="border-radius: 50%; border-color: #223887;border-style: solid; width:230px; height:230px"  src="{{ asset('mitra/img/happy-customer.png')}}">
    @else
    <img class="mitra-img" style="border-radius: 50%; border-color: #223887;border-style: solid; width:230px; height:230px"  src="{{$DetailMitra->chooseFileSelfie}}">
    @endif
    </div>
    <div class="col s12 center">
   <h5 class="bold text-blue">{{Auth::user()->name}}</h5>
   @if(empty($no_mitra))
   <h6 class="text-yellow">Anda Belum Terverifikasi dengan ID Mitra</h6>
   @else
   <h6 class="text-yellow">{{$no_mitra}}</h6>
   @endif
   </div>
</div>

<div class="divider"></div>
<div class="gap"></div>

      <a onclick="orderan()" href="#">
      <div class="card-panel">
      <div class="cardh1">
      @if(!empty($countOrder))
      <span class="new badge bg-red">{{$countOrder}}</span>
      @endif
      ORDERAN
      </div><span class="right-arrow material-icons">
      keyboard_arrow_right
      </span>
      </div>
      </a>

   <a onclick="aktivitas()" href="#">
    <div class="card-panel">
        <div class="cardh1">
        AKTIVITAS
        </div><span class="right-arrow material-icons">
        keyboard_arrow_right
        </span>
        </div>
    </a>

    <a onclick="akun_saya()" href="#">
        <div class="card-panel">
        <div class="cardh1">
        AKUN SAYA
        </div><span class="right-arrow material-icons">
        keyboard_arrow_right
        </span>
      </div>
    </a>

   
</div>

<div class="gap"></div><div class="gap"></div>

</div>

  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
  </div>

  <textarea style="display: none;" class="form-control" rows="5" id="messagewhatsapp" name="messagewhatsapp">Hallo Tim Agent Sadulur, Saya ingin mendaftar menjadi mitra..
  </textarea>

  <script>

  function MitraCek(){
        Swal.fire({
                        title: 'Akun Anda Belum Terverifikasi',
                        imageUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAACjCAMAAAA3vsLfAAAAY1BMVEX///9axfFSw/H///1PwvBaxfBHwvFZxPJRwvH7/v7y+/1Pw/BuzfNGwvDe8/vZ8fu85/lkyvPH6/qq4fjl9vx30fSF1PXR7/qe3feW2vb0+/6m3ve15fjs+P3J7Pma3PZ/1PSGHBb+AAAI/ElEQVR4nO2dC3PiIBCADRsgkZiYl29t//+vvERtz8cmAoKA9bu5uZmbsS3bBfbNZPLhHQDXP0B4wKbJiyJvph/ZSQKTvP4uhYg7RBK1dZHCR3iPyL8oIxGPjnBKuWB0nX927BiwzDqZ3UFm5eGjcUNAnrF7mZ3UjmXFR24o6boTGh2QWxSxder6J/SRpkwGRXbaqrT5KNwtB8bHpdbt1Fl18yG4+fevAfPHUus36u5KQJu82tXbejdf7v/iBgaYzySEdpIbnD6RVuuSsVgQQkTCGF3M966X8XKWQzfovdzmxy1ZLDqJXeonJ/Esq/6Wzu2FrNQ6uTUwYN1FUUJ3f+iUg1JeahEv83ZYNxO6/DOCWz2wPG4gqKb9wBbpn7hYoWHDNq4GhOZ/QWyTdlR71OGzwx/Qt0L6FpWG3ZrF7we0MnaustzeXd325pWtl1vuel2W2SrYbPJwMXW9MLuUFvZo7zS0rhdmBzj9bazs0Y54/n7HW7pvlofqUOzTnZqpKw+fvVfWC4pVW85YEicxw11LM5Av1ys1BkzyNWMJ4Ub9Ahwav00kqcgYeYHETpDte+zSZjAxZYeZ6wUbYWXzJMNgB9dLfhqYZrbuzEHI2vWqnwQgF1bM2lF4GXqQXD5ZYBIW9l0KbqQWxWEHkBor7vpjxNb1yp8hfZmpdsvC9dKfYfFiw+M/AYdBoHJzsEX9Vep68fqkjg62noDFtnK2RQPWNpjGzqQW8cz18nUBO7kCScK9SV1Kjaxcr16Xg8M9Gona9fJ1cWezHeVWrpeuJaBDOlL6/Qo4YWIVXsrUWj7vRMwkgnhEBFeaP7d5tPFov1nI/F4SFliVpVVbl/XnViUV/4yzoHbqt0WpxV8T6P7IRdu5COlusFPhcRJEed54sJM6QNk8nG43e/con/2vxWpKmSOU7YIpsoysaVt8YcrCZCvTRNPJLRCoLbHx7EpzoKASlw8L5XyztknvklLHtspHhFIWklmSWowYYofHUQMSSCRpYUdq9Bv5XiBhWwdi99Z2zF2yQb+ZxH0q0E/6RmXFucL7DqYy5lscRAhuY8OVJ2t0p8k1vM1CUDcoLdylNMXEJrNFO0gAoUtQ7d6TgRXYd8ol9TqMbJbsauRJ0FJmkPZ+Ual7h/FdimvLl/TdQ1YB2CAyxpQSDBsGolIJFsYuTY168zTGTnRIVb5GGO1Yc5OnG8nQHaaUH4uDONyM+qV4WalaTVM8f7UE1OmUY2HOBsEjZhvJ2StnRAiOQkPNSY20aHz2W61gwv+6e4BCanaRJOhhDqqHJ/G9oAYmB4P3AcW7WpS7xr0Xm9mpCwPLzVRjU96Lba92Vo/DI8yDh62yOe392aYyu+ghuC+Zq/9mfL9J1yajH/EXdomCRmrMb7sNjB5svzn46++h85tJ/PYS1AoZOBl1XtF5KHq9XMLrrJ+aOZVkazESYkI9eL3CYL8jIIqnTtbdu9ngpcgNePA/CK9b6NVyVmLfr6WeDYia7bGl6nUl+T3+SGkqW3x20fGRxZxhE2RgqtUQ7fceVXJ5/tcYwAr5HD6vCPTG5fmdudopnNb84m6DvLz/JBqO1Qx/zrwuf24VVnKzB29Lh9gBO9hUhs5ekPjd0KyQQbhzrZfR5QnXuZCY2EqtLcqp18rWKNyj91G09KJsHvfgZXPwt6CXiz8s5VeFRtEq8aNMAx681sFGfR+Gt5PP9uJhnOk3O/YdDVQI6VWgc+p53Yx0GwcX+GED3U3Zfw3Ug5986d0HzPdBxtJiG6lC7p0tgx58CGNl15JiG4+01gy1TVOt+vPO1/C+L0FW28hYqwBMGvT/tTx4zgJ480m+RW2prAJaHjyJ/A5OnpC/SRka7B5hMxaXG6QN4jkAhUOblEqRHGh1MhScBiC0zhxVMeJZDdKqoJyDP5N4Hfj4AVSWRJMSP/uRr7uf6VVnUryVwTNUx/kL2ea7THcArahD2Kbyd8KJOJPKJ9X6qdcgxsmqFrRQLiSCE8/UnYcxTlYlTnle1+JBUS3IV9EjkEUIu1SjQpyIBwrxXHWE3xHKEwA6eSWGd1OdebI6wu9M3w+ql8KRRAy/irZ5SmgP/F9v0Jt/yhmeOpg8P2XK+7q2I6BZgJpQdDM9Pxk0lNmBCz3TlLMtEuKZPt8HTR3IQAOllp5LRHbvbBl4so44kIEOjXbt7l3fhomndkgAFsgR/fp61l4+HARG3l4byPZ4B2jWUp0Ed+lsKVfRo2JzJwlFQOLd80G5LX5jPZo5+FtcSkKRPNJWFCKq05VqpnPc78q2awDSVl9V+heBJ6amwHnf/XJDJbRnFpOoc7bWZrZocFN405VINBWGs4WpflQW3CPgsKkpI3qSG3/YWx4exnijW4ovI1aEvtja0JTtB6tTeB/hd6vVGE8kUJ6HBeIj3OPwoY5Aom0oLsfZz2Qz2P6hns0yhgjM1r3EodhYEBN6cCwN+JRAuljCR2RLU40TShoBZ+tIbDyMIVpDWHtE/gGzEApQhzE/cVEK/wvrx9k42aSsCqFsd4zs9Q8w9wVagUvNwZ0gSmyWZWBYfr/pHhbcW00oFt+GuYezrAhf1XrmrzNBBMvQbvEAAZtP4/JzozmNOCExW4dtrF1jKFGMIhiLkyRmM9qugku3jKPU56EmtDrdF4eqWuabt7gGrtAtFnwI7z2oXsXeS81+MTiG90ps0fvp2CWpHSMk4FyBHEYHff4SxjMIT6A7IWAcfITDGwGTpcnRzycS9DGitwImhWmrF5/4+XbspZ7GlCeYRw2fZT2+UdU6tpKAs6CKFOWg4DjLcqXHKUKpATcBzMsZFrYkx9DFZmi8J0IYvXvmqFoWcx799hjR/qn4xfLoIS2lzTsWbAmWLjCdf5eMJYIQESeMlYsqPTdcSfemsdr/KTwWSDfFfFdv6928uC4TraTMO3x81N/grGDX7ylPlo/vUx7Su+evAWCPji++gND8IzWEfnzxoMrxvt3jIzUEaNrBm4GV7x71eAJYZlhLA2Fl4KUdloFJs6Ys4b+i45Fg9K0SU5aANN99UyHiDiFoW+foDOMPGNMmL4q8CbIH6MMHA/wD0xl/pBTiHLgAAAAASUVORK5CYII=',
                        imageWidth: 300,
                        imageHeight: 200,
                        showCloseButton: true,
                        showCancelButton: false,
                        showConfirmButton: false,
                        focusConfirm: false,
                        text: 'Kami sedang meningkatkan pelayanan, Harap Hubungi Agent Sadulur untuk Verifikasi ID Mitra',
                        footer: '<div style="padding:3px"><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="HubungAgent()">Hubungi Agent</button></div>'
                    })
  }
  

  function orderan(){
    var no_mitra = '<?php echo $no_mitra ? $no_mitra : 0; ?>';
    if(no_mitra == '0' || no_mitra == null){
      MitraCek();
    }else{
      window.location = 'home/orderan';
    }
     
  }

  function aktivitas(){
    var no_mitra = <?php echo $no_mitra ? $no_mitra : 0; ?>;
    if(no_mitra == 0 || no_mitra == null){
      MitraCek();
    }else{
      window.location = 'home/aktivitas';
    }
  }


  function akun_saya(){
    var no_mitra = <?php echo $no_mitra ? $no_mitra : 0; ?>;
    if(no_mitra == 0 || no_mitra == null){
      MitraCek();
    }else{
      window.location = 'home/profile-mitra';
    }
     
  }

  function topup(){
    var no_mitra = <?php echo $no_mitra ? $no_mitra : 0; ?>;
    if(no_mitra == 0 || no_mitra == null){
      MitraCek();
    }else{
      window.location = 'home/saldo';
    }
     
  }


  function HubungAgent() {
    var uri =  document.getElementById('messagewhatsapp').value;
    var res = encodeURI(uri);
    window.location.href = "whatsapp://send?phone=6282299171933&text="+res;
    location.reload();
  }
 

  </script>


@include('inc-mitra.footer')


