 @include('inc-mitra.header')
 <style>
        /****** CSS SADULUR Unggah Data ******/
        /****** KTP ******/
        .file-upload-ktp{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
        .file-upload-ktp .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload-ktp .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload-ktp .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
        .file-upload-ktp .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-ktp .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-ktp.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-ktp.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-ktp .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
        .file-upload-ktp .file-select.file-select-disabled{opacity:0.65;}
        .file-upload-ktp .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload-ktp .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload-ktp .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}

         /****** SERTIFIKAT ******/
        .file-upload-sertifikat{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
        .file-upload-sertifikat .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload-sertifikat .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload-sertifikat .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
        .file-upload-sertifikat .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-sertifikat .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-sertifikat.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-sertifikat.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-sertifikat .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
        .file-upload-sertifikat .file-select.file-select-disabled{opacity:0.65;}
        .file-upload-sertifikat .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload-sertifikat .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload-sertifikat .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}

        /****** SELFIE ******/
        .file-upload-selfie{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
        .file-upload-selfie .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload-selfie .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload-selfie .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
        .file-upload-selfie .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-selfie .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-selfie.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-selfie.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload-selfie .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
        .file-upload-selfie .file-select.file-select-disabled{opacity:0.65;}
        .file-upload-selfie .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload-selfie .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload-selfie .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}

                #map {
                    margin: auto;
                    width: 100%;
                    height: 100%;
                    z-index:100;
                    border:1;
                    padding-bottom:300px;
                }
                #mapSearchContainer{
                    position:fixed;
                    top:20px;
                    right: 40px;
                    height:30px;
                    width:180px;
                    z-index:110;
                    font-size:10pt;
                    color:#5d5d5d;
                    border:solid 1px #bbb;
                    background-color:#f8f8f8;
                }
                .pointer{
                position:absolute;
                top:86px;
                left:60px;
                z-index:99999;
                }

       </style>



<!-- nav -->
<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a href="#" class="" id="nav-title">PROFIL MITRA</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Reservation List</a></li>
      </ul>
      <a href="{{url('home/profile-mitra')}}" class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>

    </div>
  </nav>

<div class="container">
<div class="section">

    <div class="row">
		<div class="col s4">
       <input type="hidden" name="id_detail_mitra" id="id_detail_mitra" value="{{$DetailMitra->id}}">
            @if(empty($DetailMitra->chooseFileSelfie))
                    <img class="circle" src="{{ asset('mitra/img/happy-customer.png')}}" width="95px" height="95px" alt=""/></div>
            @else
                    <img class="circle" src="{{$DetailMitra->chooseFileSelfie}}" width="95px" height="95px" alt=""/></div>
            @endif
    <div class="col s7">
        <div class="text-blue bold">{{Auth::user()->name}}</div>
        <div class="">{{Auth::user()->email}}</div>
	    <div class="text-blue">{{$DetailMitra->no_hp}}</div>

	</div>
    </div>
   <div class="divider"></div>

</div>
<div class="section">
  <form method="POST" action="{{url('home/profile-mitra/update-data')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="lat" id="lat" value="{{$latitude}}">
                        <input type="hidden" name="lng" id="lng" value="{{$longitude}}">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Username</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{Auth::user()->name}}" required autocomplete="name" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{Auth::user()->email}}" required autocomplete="email" disabled>
                            </div>
                        </div>


                        <div id="div1" style="display:block;">
                        <!-- Mitra Detail Form -->

                        <div class="form-group row">
                            <label for="nik" class="col-md-4 col-form-label text-md-right">No NIK KTP</label>
                            <div class="col-md-6">
                                <input id="nik" type="text" onkeypress="return hanyaAngka(event)" class="form-control @error('nik') is-invalid @enderror" name="nik" maxlength="16" value="{{ $DetailMitra->nik }}"  autocomplete="nik" autofocus>
                                @error('nik')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tgl_lahir" class="col-md-4 col-form-label text-md-right">Tanggal Lahir (Tgl/Bln/Thn)</label>
                            <div class="col-md-6">
                                <div class="input-group date" data-provide="datepicker">
                                        <input class="datepicker" id="tgl_lahir" name="tgl_lahir" value="{{$DetailMitra->tgl_lahir}}" data-date-format="mm/dd/yyyy" autofocus>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="jenis_kelamin" class="col-md-4 col-form-label text-md-right">Jenis Kelamin</label>
                            <div class="col-md-6">

                                <select class="browser-default" id="jenis_kelamin" name="jenis_kelamin" autocomplete="jenis_kelamin" autofocus>
                                    <option value="" disabled selected>--Pilih--</option>
                                    @if($DetailMitra->jenis_kelamin==1)
                                        <option value="1" selected>Pria</option>
                                        <option value="2">Wanita</option>
                                    @else if($DetailMitra->jenis_kelamin==2)
                                        <option value="1">Pria</option>
                                        <option value="2" selected>Wanita</option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="no_hp" class="col-md-4 col-form-label text-md-right">Nomor Whatsapp / Telegram</label>
                            <div class="col-md-6">
                                <input id="no_hp" type="text" onkeypress="return hanyaAngka(event)" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" value="{{ $DetailMitra->no_hp}}" autocomplete="no_hp" autofocus>
                                @error('no_hp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="kota" class="col-md-4 col-form-label text-md-right">Kota</label>
                            <div class="col-md-6">
                                <input id="kota" type="text" class="form-control @error('kota') is-invalid @enderror" name="kota" value="{{ $DetailMitra->kota }}"  autocomplete="kota" autofocus>
                                @error('kota')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="kecamatan" class="col-md-4 col-form-label text-md-right">Kecamatan Tempat Tinggal saat ini</label>
                            <div class="col-md-6">
                                <input id="kecamatan" type="text" class="form-control @error('kecamatan') is-invalid @enderror" name="kecamatan" value="{{$DetailMitra->kecamatan }}" autocomplete="kecamatan" autofocus>
                                @error('kecamatan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat_lengkap" class="col-md-4 col-form-label text-md-right">Alamat Lengkap Tempat Tinggal saat ini</label>
                            <div class="col-md-6">
                                <input id="alamat_lengkap" type="text" class="form-control @error('alamat_lengkap') is-invalid @enderror" name="alamat_lengkap" value="{{ $DetailMitra->alamat_lengkap }}"  autocomplete="alamat_lengkap" autofocus>

                                @error('alamat_lengkap')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                        <label for="cari_alamat" class="col-md-4 col-form-label text-md-right">Cari Alamat Berdasarkan Peta</label>
                        <input type="text" id="address" name="address" class="form-control @error('address') is-invalid @enderror" value="{{$address}}" style='color:blue;' autocomplete="address" readonly>
                            <div style="padding-top:10px;" class="col-md-6">
                                <div id="map">

                                </div>
                            </div>
                        </div>


                        <!-- Management File -->

                        <!-- Upload Sertifikat -->
                        <!-- <div class="form-group row">
                            <label for="upload_sertifikat" class="col-md-4 col-form-label text-md-right">Upload bukti sertifikat sesuai keahlian (bagi calon mitra layanan massage dan cleaning)/ Ijazah terakhir bagi calon mitra layanan edukasi</label>
                            <div class="col-md-6">
                                <div class="file-upload-sertifikat">
                                    <div class="file-select">
                                        <div class="file-select-button" id="fileName">Pilih File</div>
                                        <div class="file-select-name" id="noFileSertifikat">File belum dipilih...</div>
                                        <input type="file" name="chooseFileSertifikat" id="chooseFileSertifikat" onchange="previewFile('sertifikat')">
                                    </div>
                                </div>
                            </div>
                        </div> -->

                         <!-- View Sertifikat -->
                        <!-- <div class="form-group row">
                            <label for="hasil_sertifikat" class="col-md-4 col-form-label text-md-right">Hasil Upload Sertifikat</label>
                            <div class="col-md-6">
                                <div style="display:none;">
                                    <img id="source_image_sertifikat" />
                                    <input type="text" id="base64Sertifikat" name="base64Sertifikat" value="{{$DetailMitra->chooseFileSertifikat}}">
                                    <input id="jpeg_encode_quality" size='3' readonly='true' type="text" value="30" />
                                </div>
                                    <img id="result_compress_sertifikat"  style="border: 2px solid #555; border-color: #223887;" src="{{$DetailMitra->chooseFileSertifikat}}" class='img_container' width="130" height="140"/>
                            </div>
                        </div> -->
                        <input type="hidden" id="base64Sertifikat" name="base64Sertifikat" value="">

                        <!-- Upload KTP -->
                        <!-- <div class="form-group row">
                            <label for="upload_ktp" class="col-md-4 col-form-label text-md-right">Upload foto KTP terbaru (wajib)</label>
                            <div class="col-md-6">
                                <div class="file-upload-ktp">
                                    <div class="file-select">
                                        <div class="file-select-button" id="fileName">Pilih File</div>
                                        <div class="file-select-name" id="noFileKtp">File belum dipilih...</div>
                                        <input type="file" name="chooseFileKtp" id="chooseFileKtp" onchange="previewFile('ktp')">
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <!-- View Ktp -->
                        <!-- <div class="form-group row">
                            <label for="hasil_ktp" class="col-md-4 col-form-label text-md-right">Hasil Upload KTP</label>
                            <div class="col-md-6">
                                    <div style="display:none;">
                                        <img id="source_image_ktp" />
                                        <input type="text" id="base64Ktp" name="base64Ktp" value="{{$DetailMitra->chooseFileKtp}}">
                                    </div>
                                         <img id="result_compress_ktp" style="border: 2px solid #555; border-color: #223887;" src="{{$DetailMitra->chooseFileKtp}}" class='img_container' value="" width="130" height="140"/>
                            </div>
                        </div> -->
                        <input type="hidden" id="base64Ktp" name="base64Ktp" value="">

                        <!-- Upload SELFIE -->
                        <!-- <div class="form-group row">
                            <label for="upload_selfie" class="col-md-4 col-form-label text-md-right">Upload Foto Selfie Sambil Memegang KTP</label>
                            <div class="col-md-6">
                                <div class="file-upload-selfie">
                                    <div class="file-select">
                                        <div class="file-select-button" id="fileName">Pilih File</div>
                                        <div class="file-select-name" id="noFileSelfie">File belum dipilih...</div>
                                        <input type="file" name="chooseFileSelfie" id="chooseFileSelfie" onchange="previewFile('selfie')">
                                    </div>
                                </div>
                             </div>
                        </div> -->

                        <!-- View Selfie -->
                        <!-- <div class="form-group row">
                            <label for="hasil_selfie" class="col-md-4 col-form-label text-md-right">Hasil Upload Selfie</label>
                            <div class="col-md-6">
                                    <div style="display:none;">
                                        <img id="source_image_selfie" />
                                        <input type="text" id="base64Selfie" name="base64Selfie" value="{{$DetailMitra->chooseFileSelfie}}">
                                    </div>
                                    <img id="result_compress_selfie"  style="border: 2px solid #555; border-color: #223887;" class='img_container' src="{{$DetailMitra->chooseFileSelfie}}" width="130" height="140"/>
                            </div>
                        </div> -->
                        <input type="hidden" id="base64Selfie" name="base64Selfie" value="">

                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="waves-effect waves-light btn w-100 bg-blue">
                                   Edit
                                </button>
                            </div>
                        </div>
                    </form>

</section>




</div>

@include('inc-mitra.footer')

<script>

    // Next Page
    function myFunction() {
            var idDetailMitra = document.getElementById("id_detail_mitra").value;
            window.location="/home/profile-mitra/edit/"+idDetailMitra;
     };

     // Map Leaflet

     var address;
     var lat;
     var lng;
     $(document).ready(function() { /* code here */
            debugger
            address = document.getElementById('address').value;
            lat = document.getElementById('lat').value;
            lng = document.getElementById('lng').value;
            var center = [lat,lng];
              //create marker for cordinate here..
              if(lat != undefined  && lng != undefined){
                  debugger
               return L.marker(center).addTo(map);
            }
    });

     var greenIcon = L.icon({
          iconUrl: 'https://lh3.googleusercontent.com/42bUknoaAVlBtBP2d4yJ9tMEVHgg10MHsshXx-UYGnnLXwZ6xGese_6cHtK4GqhWsQ',
          shadowUrl: 'https://lh3.googleusercontent.com/42bUknoaAVlBtBP2d4yJ9tMEVHgg10MHsshXx-UYGnnLXwZ6xGese_6cHtK4GqhWsQ',

          iconSize:     [38, 95], // size of the icon
          shadowSize:   [50, 64], // size of the shadow
          iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
          shadowAnchor: [4, 62],  // the same for the shadow
          popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
      });

        var map = L.map('map', {
            // Set latitude and longitude of the map center (required)
            center: [<?php echo $latitude ? $latitude : -6.914744; ?>, <?php echo $longitude ? $longitude : 107.609810; ?>],
            // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
            zoom: 15,
            icon: greenIcon
        });

        L.control.scale().addTo(map);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var alamat;
        var lat;
        var lng;

        var searchControl = new L.esri.Controls.Geosearch().addTo(map);

        var results = new L.LayerGroup().addTo(map);

        searchControl.on('results', function(data){
            results.clearLayers();
            for (var i = data.results.length - 1; i >= 0; i--) {
            alamat = data.results[0].text;
            lat = data.results[0].latlng.lat;
            lng = data.results[0].latlng.lng
            document.getElementById('address').value = alamat;
            document.getElementById('lat').value = lat;
            document.getElementById('lng').value = lng;

            results.addLayer(L.marker(data.results[i].latlng));
            }
        });
        setTimeout(function(){$('.pointer').fadeOut('slow');},3400);




           // FILE UPLOAD

           $('#chooseFileSertifikat').bind('change', function () {
                var filename = $("#chooseFileSertifikat").val();
                if (/^\s*$/.test(filename)) {
                    $(".file-upload-sertifikat").removeClass('active');
                    $("#noFileSertifikat").text("No file chosen...");
                }else {
                    $(".file-upload-sertifikat").addClass('active');
                    $("#noFileSertifikat").text(filename.replace("C:\\fakepath\\", ""));
                }
                });

                $('#chooseFileKtp').bind('change', function () {
                var filename = $("#chooseFileKtp").val();
                if (/^\s*$/.test(filename)) {
                    $(".file-upload-ktp").removeClass('active');
                    $("#noFileKtp").text("No file chosen...");
                }else {
                    $(".file-upload-ktp").addClass('active');
                    $("#noFileKtp").text(filename.replace("C:\\fakepath\\", ""));
                }
                });

                $('#chooseFileSelfie').bind('change', function () {
                var filename = $("#chooseFileSelfie").val();
                if (/^\s*$/.test(filename)) {
                    $(".file-upload-selfie").removeClass('active');
                    $("#noFileSelfie").text("No file chosen...");
                }else {
                    $(".file-upload-selfie").addClass('active');
                    $("#noFileSelfie").text(filename.replace("C:\\fakepath\\", ""));
                }
                });

                // ONLY NUMBER KTP
                function hanyaAngka(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))

                    return false;
                return true;
                }


                if (!window.console)
                console = {};

                // var console_out = document.getElementById('console_out');
                var output_format = "jpg";

                var encodeButton = document.getElementById('jpeg_encode_button');
                var encodeQuality = document.getElementById('jpeg_encode_quality');

                function previewFile(condition) {
                if(condition=="sertifikat"){
                    var preview = document.getElementById('source_image_sertifikat');
                    var previewCompress =  document.getElementById('result_compress_sertifikat');
                    var file   = document.querySelector('input[name=chooseFileSertifikat]').files[0];
                }
                else if(condition=="ktp"){
                    var preview = document.getElementById('source_image_ktp');
                    var previewCompress =  document.getElementById('result_compress_ktp');
                    var file   = document.querySelector('input[name=chooseFileKtp]').files[0];
                }
                else if(condition == "selfie"){
                    var preview = document.getElementById('source_image_selfie');
                    var previewCompress =  document.getElementById('result_compress_selfie');
                    var file   = document.querySelector('input[name=chooseFileSelfie]').files[0];

                }



                var reader  = new FileReader();
                reader.addEventListener("load", function(e) {
                    preview.src = e.target.result;
                    preview.onload = function() {
                    compressFile(this, previewCompress,condition)
                    };
                }, false);

                if (file) {
                    reader.readAsDataURL(file);
                }
                }


                function compressFile(loadedData, preview,condition) {
                if(condition=="sertifikat"){
                    var result_image = document.getElementById('result_compress_sertifikat');
                }else if(condition=="ktp"){
                    var result_image = document.getElementById('result_compress_ktp');
                }else if(condition=="selfie"){
                    var result_image = document.getElementById('result_compress_selfie');
                }


                var quality = parseInt(encodeQuality.value);
                var time_start = new Date().getTime();

                var mime_type = "image/jpeg";
                if (typeof output_format !== "undefined" && output_format == "png") {
                    mime_type = "image/png";
                }

                var cvs = document.createElement('canvas');
                cvs.width = loadedData.width;
                cvs.height = loadedData.height;
                var ctx = cvs.getContext("2d").drawImage(loadedData, 0, 0);
                var newImageData = cvs.toDataURL(mime_type, quality / 100);
                var result_image_obj = new Image();
                result_image_obj.src = newImageData;
                result_image.src = result_image_obj.src;
                if(condition=="sertifikat"){
                    document.getElementById('base64Sertifikat').value = result_image.src;
                }else if(condition=="ktp"){
                    document.getElementById('base64Ktp').value = result_image.src;
                }
                else if(condition=="selfie"){
                    document.getElementById('base64Selfie').value = result_image.src;
                }

                result_image.onload = function() {}
                var duration = new Date().getTime() - time_start;

            }

</script>


