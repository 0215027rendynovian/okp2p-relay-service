@include('inc-mitra.header')
<?php
header('Set-Cookie: cross-site-cookie=name; SameSite=None; Secure');

?>

<style>
    @media screen {
      #printSection {
          display: none;
      }
    }

    @media print {
      body * {
        visibility:hidden;
      }
      #printSection, #printSection * {
        visibility:visible;
      }
      #printSection {
        position:absolute;
        left:0;
        top:0;
      }
    }
</style>



<!-- nav -->
<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a href="#" class="" id="nav-title">ORDERAN</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Reservation List</a></li>
      </ul>
      <a href="#" onclick="goBack()" class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>
    </div>
  </nav>
  <div class="container">


	<div class="row">
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s3"><a class="active" href="#inbox">Berlangsung</a>
				</li>
				<li class="tab col s3"><a href="#unread">Diterima</a>
				</li>

			</ul>
		</div>

		<div id="inbox" class="col s12">

		@isset($cekDataCusToMitra)
		@if($cekDataCusToMitra->status_order == '0')
			<div class="card-panel">
				<div class="cardh1">
					{{$cekDataCusToMitra->User->name}}
					<span>{{$cekDataCusToMitra->address}}</span>
				</div>
				<div class="row mb-0">
					<div class="pv2"></div>
						<div class="col s6 text-blue">
							<i class="material-icons small pr-5">access_time</i><span class="top">{{$cekDataCusToMitra->created_at}}</span>
						</div>
					<div class="col s6 text-red text-right">
					@if($cekDataCusToMitra->duration == 1)
						<i class="material-icons f22 pr-5">alarm</i><span class="top">{{$DetailServiceMitra->description}}</span>
					@elseif($cekDataCusToMitra->duration == 2)
						<i class="material-icons f22 pr-5">alarm</i><span class="top">{{$DetailServiceMitra->description}}</span>
					@elseif($cekDataCusToMitra->duration == 3)
						<i class="material-icons f22 pr-5">alarm</i><span class="top">{{$DetailServiceMitra->description}}</span>
					@endif
					</div>
				</div>
				<div class="divider"></div>
				<div class="pv5"></div>
				<div class="row mb-0">
					<a class="col s6 waves-effect waves-light btn bg-yellow" onclick="UpdateOrderMitra(1,{{$cekDataCusToMitra->uid_customer}})">TERIMA</a>
					<a class="col s6 waves-effect waves-light btn bg-red" onclick="UpdateOrderMitra(2,{{$cekDataCusToMitra->uid_customer}})">TOLAK</a>
				</div>
			</div>
			@endif
		@endisset

		<!-- History Orderan -->
			<div class="divider"></div>
			<div class="pv10"></div>
			<div class="text-yellow ">1 minggu lalu :</div>
			@foreach($getDataMessage as $key)
			<a href="">
				<div class="row mb-5" id="activity-list">
					<div class="col s12 text-blue" id="small-dates">
						{{$key->created_at}}
					</div>
					<div class="col s6" id="small-service">
					{{$key->message}}
					</div>
					<span class="right-arrow material-icons">keyboard_arrow_right</span>
				</div>
			</a>
			<div class="divider"></div>
			@endforeach
		</div>


		<!-- Orderan Belum dibaca-->

		@isset($cekDataCusToMitra)
		@if($cekDataCusToMitra->status_order == '1')

		<div id="unread" class="col s12">

			<div class="card-panel">
				<div class="cardh1">
	
					<span>{{$cekDataCusToMitra->address}}</span>
				</div>

				<div class="row mb-0">


					<div class="pv2"></div>
					<div class="col s6 text-blue">
						<i class="material-icons f22 pr-5">access_time</i><span class="top">{{$cekDataCusToMitra->created_at}}</span>
					</div>

					<div class="col s6 text-red text-right">
						@if($cekDataCusToMitra->duration == 1)
							<i class="material-icons f22 pr-5">alarm</i><span class="top">{{$DetailServiceMitra->description}}</span>
						@elseif($cekDataCusToMitra->duration == 2)
							<i class="material-icons f22 pr-5">alarm</i><span class="top">{{$DetailServiceMitra->description}}</span>
						@elseif($cekDataCusToMitra->duration == 3)
							<i class="material-icons f22 pr-5">alarm</i><span class="top">{{$DetailServiceMitra->description}}</span>
						@endif
					</div>
				</div>
				<div class="divider"></div>
				<div class="pv5"></div>
				<div class="row mb-0">
					<a class="waves-effect waves-light btn w-100 bg-yellow" href="{{url('home/orderan-detail')}}">MULAI LAYANAN</a>

					<div class="pv5"></div>
					<a class="waves-effect waves-light btn w-100 bg-blue" href="#">KONFIRMASI SELESAI</a>

				</div>
			</div>
			<div class="divider"></div>
		</div>

		@endif
		@endisset


	</div>


@include('inc-mitra.modal-notif-order')

@include('inc-mitra.footer')


