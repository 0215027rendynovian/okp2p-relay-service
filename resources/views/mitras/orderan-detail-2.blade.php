@include('inc-mitra.header')

<style>
#status{
  color: #223887;
}

input, button{
  padding: 10px;
}
</style>

<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">DETAIL ORDERAN</a>


		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="{{url('home')}}" class="sidenav-trigger right"><i class="material-icons">close</i></a>

	</div>
</nav>

<iframe src="https://maps.google.com/maps?q={{$latitude_customers}},{{$longitude_customers}}&hl=es;z=14&amp;output=embed" width="auto" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
<div class="pv5"></div>
<div class="container">

	<div class="row">
    <span id="status"></span><br/>
        <!-- <input id="time" placeholder="Enter Time" va /> -->
        <input type="hidden" id="time" value="{{$real_time}}" />
        <!-- <button id="timeBtn" onclick="setTime()">Set</button> -->
        <div style="text-align:center">
            <h3 style="font-weight:bold; color:#223887;" id="timeLabel">
              <span id="hour">00</span>:<span id="min">00</span>:<span id="sec">00</span>:<span id="sec_det">00</span>
            </h3>
        </div>
	</div>
    <div class="row">
        <a class="waves-effect waves-light btn w-100 bg-success" onclick="AkhiriOrderan()">Konfirmasi Selesai</a>
        <br><br>
    </div>
      
		    
        
</div>



@include('inc-mitra.footer')

<script>


        function AkhiriOrderan(){
            debugger
            localStorage.removeItem("time");
            window.location = 'show-akhiri-layanan';
            
        }

        function $(id){
            debugger
        //this function return the ID of the element parsed
        return document.getElementById(id)
        }
        var status_mulai_layanan = <?php echo $status_mulai_layanan; ?>;
        if(status_mulai_layanan == '0'){
            debugger
            if(localStorage.getItem("time") == null){
                setTime();
            }
            
        }
    
      
  
       
        function setTime(){
            debugger
        //this function ensures that the user enter the correct format
        //var time = $("time").value;
        var time = "1:00:00"
        var fields = time.split(':');
        var hour = fields[0];
        var min = fields[1];
        var sec = fields[2];
        
        if(time === ''){
            $("status").innerHTML = "Kesalahan Formaat Jam. eg 1:50";
        }else{
            if(hour < 61 && min < 61 && sec < 61){
            $("status").innerHTML = "Layanan Menhitung..";
                count(hour,min, sec, sec);
            }else{   
            $("status").innerHTML = "Waktu. eg 1:50";
            }
        }
        }

        function count(hour,min, sec, secLeft){
            debugger
            //this function is the count control is will check if the count down is finish
            if(hour > 0 || min > 0 || secLeft > 0){
                debugger
                if(min == 0){ 
                hour -= 1;
                min = 60;
                }

                if(secLeft == 0){ 
                min -= 1;
                sec = 60;
                }
          

                $("hour").innerHTML = hour;
                $("min").innerHTML = min;
                countDown(hour, min, sec);
            }else{
                debugger
            $("status").innerHTML = "";
            localStorage.removeItem("time");
            window.location = 'show-akhiri-layanan';
            // $("timeBtn").disabled = false;
            $("time").disabled = false;
            }
        }

        function countDown(hour, min, sec){
            debugger
        //this function runs the seconds count
        
            var time = sec;
            $("sec").innerHTML = time;
            var interval = setInterval(function(){
                $("sec").innerHTML = -- time;
                rememberMe(hour, min, time);
                if (time == 0) {
                clearInterval(interval);
                count(hour, min, sec, "00");
                }
            }, 1000);
        }

        function rememberMe(hour,min, sec){
            debugger
        //this function stores the time as a local storage incase the page refresh
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem("time", hour+':'+min+':'+sec);
        }else{
            $("status").innerHTML = "Ooh my, your browser doesn't support web storage";
        }
        }

        function isTimeSet(){
            debugger
            //this function checks if there is a time set
            if(localStorage.getItem("time") != null){
                debugger
                var time = localStorage.getItem("time");
                var fields = time.split(':');
                hour = fields[0];
                min = fields[1];
                sec = fields[2];
                $("status").innerHTML = "Waktu berjalan anda sedang memulai layanan..";
                // $("timeBtn").disabled = true;
                $("time").disabled = true;
                count(hour, min, sec, sec);
            }
        }

        //on page load this call the isTimeSet function to check if there is a set time
        window.onload = isTimeSet();


</script>

