 @include('inc-mitra.header')

<!-- nav -->
<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a href="#" class="" id="nav-title">PROFIL MITRA</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Reservation List</a></li>
      </ul>
      <a href="{{url('home')}}"class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>
      
    </div>
  </nav>

<div class="container">
<div class="section">
  
    <div class="row">
		<div class="col s4"> 
       <input type="hidden" name="id_detail_mitra" id="id_detail_mitra" value="{{$DetailMitra->id}}">
       @if(empty($DetailMitra->chooseFileSelfie))
         <img class="circle" src="{{ asset('mitra/img/happy-customer.png')}}" width="95px" height="95px" alt=""/></div>	
      @else
          <img class="circle" src="{{$DetailMitra->chooseFileSelfie}}" width="95px" height="95px" alt=""/></div>	
      @endif
        
    <div class="col s7"> 
        <div class="text-blue bold">{{Auth::user()->id}}</div>
        <div class="">{{Auth::user()->email}}</div>
		    <div class="text-blue">{{$DetailMitra->no_hp}}</div>
		
	</div>
    </div>
   <div class="divider"></div>
 
</div>

<div class="section">
  
<!--   Konten Info Profil  -->
<div class="row">	
<div class="col s12 text-blue">Alamat
<a href="#" onclick="myFunction()" class="right waves-effect waves-light btn-small bg-yellow"><i class="material-icons">edit</i></a>
<div class="black-text">{{$DetailMitra->alamat_lengkap}}</div>
</div></div>

<div class="row">
<div class="col s6 text-blue">ID Mitra
@if($DetailMitra->no_mitra == NULL)
    <div class="black-text"><span style="color:red;">Anda Belum memiliki No Mitra.</span></div>
@else
    <div class="black-text">{{$DetailMitra->no_mitra}}</div>
@endif
</div>

<div class="col s6 text-blue text-right">Jenis Kelamin
@if($DetailMitra->jenis_kelamin == 1)
    <div class="black-text">Pria</div>
@else
    <div class="black-text">Wanita</div>
@endif




</div>
</div>
<div class="divider"></div>
<div class="gap"></div>

<div class="row">
<div class="col s12 text-blue">Layanan Mitra
@if($DetailMitra->type_layanan==1)
<div class="black-text">Message</div>
@elseif($DetailMitra->type_layanan==2)
<div class="black-text">Cleaning</div>
@elseif($DetailMitra->type_layanan==3)
<div class="black-text">Education</div>
@endif
</div>





</div>
</div>
<div class="divider"></div>

</div>


</div>

@include('inc-mitra.footer')

<script>
    function myFunction() {
      debugger
            var idDetailMitra = document.getElementById("id_detail_mitra").value;
            window.location="/home/profile-mitra/edit/"+idDetailMitra;
     };
</script>

