@include('inc-mitra.header')
<style>
#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	100% { 
		transform: rotate(360deg); 
	}
}
.is-hide{
	display:none;
}
</style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/js/materialize.min.js"></script>
         
<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">SIMULATOR M-BANKING PAYMENT</a>
	</div>
</nav>

<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>
<div class="container">

<div class="pv10 text-grey">Masukan Nomor Pembayaran:</div>
    <input type="text" name="external_id" id="external_id">
<a class=" w-100 btn bg-yellow" onclick="simulated()">CEK NOMOR PEMBAYARAN</a>
<br><br>
<label style="color:green;"><i>*Aplikasi ini hanya untuk simulasi pembayaran dengan Virtual Account</i></label>
  <div class="section">
  <div class="gap"></div>
		<div class="col s12 text-grey"> Detail Pembayaran :</div>
  <div class="row section">
		<div class="col s6 text-blue">No. Pembayaran</div>
		<div class="col s6 black-text">: <span id="no_pembayaran"></span></div>
        <input type="hidden" id="hidden_no_pembayaran" name="hidden_no_pembayaran">
		<div class="col s6 text-blue">Expected Amount</div>
        <input type="hidden" id="hidden_expected_amount" name="hidden_expected_amount">
		<div class="col s6 black-text">: <span id="expected_amount"></span></div>
		
</div><div class="divider"></div>
<div class="section">

		
 <a class="btn w-100 bg-red" id="fixedbutton" onclick="bayar()">BAYAR</a>
 </div>   
	 
</div>   

@include('inc-mitra.footer')

<script>

  function simulated(){
	$(document).ajaxSend(function() {
		$("#overlay").fadeIn(300);　
	});

	 var external_id = document.getElementById('external_id').value;

	  if(external_id == '' || external_id == undefined){
		  return alert('nomor pembayaran harus terisi')
	  }

	$.ajaxSetup({
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
                    type:'POST',
                    url:'/xendit-ceksimulation-payment',
                    data:{external_id:external_id},
                    success:function(data){
                        if(data.status == "success"){
                            document.getElementById("no_pembayaran").innerHTML = data.no_pembayaran;
						    document.getElementById("expected_amount").innerHTML = data.expected_amount;
                            document.getElementById("hidden_no_pembayaran").value = data.no_pembayaran;
						    document.getElementById("hidden_expected_amount").value = data.expected_amount;
                        }else{
                            alert(data.message);
                        }

						$("#overlay").fadeOut();
					}
        });
   }

   function bayar(){
       debugger
	$(document).ajaxSend(function() {
		$("#overlay").fadeIn(300);　
	});

	 var no_pembayaran = document.getElementById('hidden_no_pembayaran').value;
     var expected_amount = document.getElementById('hidden_expected_amount').value;

	  if(no_pembayaran == '' || no_pembayaran == undefined){
		  return alert('Harap Cek Nomor Pembayaran Terlebih dahulu')
	  }

	$.ajaxSetup({
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
                    type:'POST',
                    url:'/xendit-simulation-payment',
                    data:{external_id:no_pembayaran, expected_amount:expected_amount},
                    success:function(data){
                        debugger
                        if(data.status == "COMPLETED"){
                            alert('PAYMENT SUCCESS CHECK BALANCE MITRA SADULUR, INFO :'+data.message);
                        }
						$("#overlay").fadeOut();
					}
        });
   }


  


</script>



