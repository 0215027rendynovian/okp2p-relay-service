@include('inc-mitra.header')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">DETAIL ORDERAN</a>


		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="{{url('home')}}" class="sidenav-trigger right"><i class="material-icons">close</i></a>

	</div>
</nav>

<iframe src="https://maps.google.com/maps?q={{$latitude_customers}},{{$longitude_customers}}&hl=es;z=14&amp;output=embed" width="auto" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
<div class="pv5"></div>
<div class="container">

	<div class="row">
		<div class="col s6">Waktu Order :
		</div>
		<div class="col s6 text-right text-blue">{{$DetailService->created_at}}
		</div>
		<div class="col s6">Nomor HP :
		</div>
		<div class="col s6 text-right text-blue">{{$cekDataCusToMitra->nomor_hp}}
		</div>
		
		<div class="col s6">Durasi :
		</div>
		<div class="col s6 text-right text-red">{{$DetailService->description}}
		</div>
		 <div class="col s6">Layanan :
		</div>
		<div class="col s6 text-right text-red">{{$ServiceMitra->category}} {{$ServiceMitra->name_service}}
		</div>
		<div class="col s6">Biaya yang akan diterima:
		</div>
		<div class="col s6 text-right text-red">@currency($DetailService->amount)
		</div> 
		<div class="col s6">Potongan Fee 20 % :
		</div>
		<div class="col s6 text-right text-red">@currency($DetailService->amount * 20/100)
		</div> 
		<div class="col s6">Biaya Transaksi :
		</div>
		<div class="col s6 text-right text-red">@currency(5500) 
		</div> 
		<div class="col s6">Total yang harus dibayar:
		</div>
		<div class="col s6 text-right text-red">@currency(5500 + $DetailService->amount * 20/100) 
		</div> 
	</div>


	@if($boolean == true)
		<a class="waves-effect waves-light btn w-100 bg-blue disabled">MULAI LAYANAN</a>
	@elseif($boolean == false)
		<a class="waves-effect waves-light btn w-100 bg-blue" onclick="Disbursement()">MULAI LAYANAN</a>
		<br><br>
		<a class="btn w-100 bg-success" onclick="whatsapp()">HUBUNGI BY WHATSAPP</a>
	@endif
	<div class="pv5"></div>
	@if($boolean == true)
	<div class="text-grey center">{{$description}}
		<a class="waves-effect waves-light btn w-100 bg-red" id="fixedbutton" href="{{url('home/saldo')}}">ISI SALDO SEKARANG</a>
	</div>
	@endif
</div>



<textarea style="display: none;" class="form-control" rows="5" id="messagewhatsapp" name="messagewhatsapp">Selamat Bpk/Ibu Mohon tunggu ya 
</textarea>


@include('inc-mitra.footer')

<script>


function whatsapp(){
  debugger
  var customersphone = "<?php echo $cekDataCusToMitra->nomor_hp?>";
  var uri =  document.getElementById('messagewhatsapp').value;
  var res = encodeURI(uri);
  window.location.href = "whatsapp://send?phone="+customersphone+"&text="+res;
  location.reload();
	
}



var a = 0;
function Disbursement(){
	debugger
	var status_active = <?php echo Auth::user()->status_active; ?>;
	if(status_active == 0){
		return Swal.fire({
                        title: 'Alert',
                        imageUrl: 'https://www.flaticon.com/svg/static/icons/svg/2055/2055875.svg',
                        imageWidth: 300,
                        imageHeight: 200,
                        showCloseButton: true,
                        showCancelButton: false,
                        showConfirmButton: false,
                        focusConfirm: false,
                        text: 'Mohon Maaf kami sedang meningkatkan layanan transaksi, Coba beberapa saat lagi',
                      })
	}


var stat_disbursement =  localStorage.getItem("stat_disbursement");
if(stat_disbursement !== null){
	alert('Anda sedang memiliki orderan berjalan, Tetap Lanjutkan?')
	return window.location='count-down-mulai-layanan';	
}



localStorage.setItem("stat_disbursement",1);
//Validate Bank Code

var id_detail_service = "<?php echo $id_detail_service?>";
var status_order = "<?php echo $status_order ? $status_order : 0; ?>";
var a =+ 1;

if( a == 2){
	alert('Perhatian, anda menekan pengiriman berulang kali');
	return window.location='count-down-mulai-layanan';	
}

if(status_order == "1"){
	alert('Anda sudah memulai proses layanan');
	return window.location='count-down-mulai-layanan';	
}

$.ajaxSetup({
	 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
	 $.ajax({
				type:'POST',
				url:'/xendit-create-disbursement',
				data:{id_detail_service:id_detail_service,
				uid:<?php echo Auth::user()->id ?>},
				success:function(data){

					if(data.status == "PENDING"){
						window.location='count-down-mulai-layanan';	
					}else{
						alert('Ada Kesalahan Pada saat pengiriman data, harap hubungi administrator')
					}
					
				}
	});
}
</script>
