@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header back-ops-okp2p">Dashboar Mitra</div>

                <div class="card-body">
                   <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/768px-No_image_available.svg.png" width="400" height="200" class="center" alt="Mountain">
                    <ul class="list-group">
                        <a href="{{url('home/orderan-mitra/')}}" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">ORDERAN</h5>
                            </div>
                        </a>
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">AKTIVITAS</h5>
                            </div>
                        </a>
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">AKUN SAYA</h5>
                            </div>
                        </a>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
