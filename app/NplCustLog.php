<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NplCustLog extends Model
{
    protected $fillable = [
        'ktp','json', 'uid','mode_dev'
    ];

    protected $table = 'log_npl_cust';
}
