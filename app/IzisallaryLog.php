<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IzisallaryLog extends Model
{

    protected $fillable = [
        'ktp','json', 'uid','mode_dev'
    ];

    protected $table = 'log_izi_sallary';
}
