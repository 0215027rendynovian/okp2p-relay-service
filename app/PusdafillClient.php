<?php
namespace App;
use App\PusdafillHttpClient;

/**
 * Pusdafill Client
*/
class PusdafillClient {

    /**
     * accessKey
     * @var string
     */
    protected $accessKey = '';
    
    /**
     * secretKey
     * @var string
     */
    protected $secretKey = '';

    /**
     * @param string $accessKey
     * @param string $secretKey
     */
    public function __construct($accessKey, $secretKey){
        $this->accessKey = $accessKey;
        $this->secretKey = $secretKey;
        $this->client = new PusdafillHttpClient();
        $this->proxies = array();
    }


    /**
     * @param int $ms 
     */
    public function setConnectionTimeoutInMillis($ms){
        $this->client->setConnectionTimeoutInMillis($ms);
    }

    /**
     
     * @param int $ms 
     */
    public function setSocketTimeoutInMillis($ms){
        $this->client->setSocketTimeoutInMillis($ms);
    }

    /**
     * 
     * @param array $proxy
     * @return string
     * 
     */
    public function setProxies($proxies){
        $this->client->setConf($proxies);
    } 

    /**
     * Api 
     * @param  string $url
     * @param  mixed $data
     * @return mixed
     */
    public function request($url, $data){
        $params = array();
        $response = $this->client->post($url, $data, $params, $this->accessKey,$this->secretKey);
        return $response['content'];
    }

    /**
     * @param $content string
     * @return mixed
     */
    protected function proccessResult($content){
        return json_decode($content, true);
    }

}
