<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaldoWallet extends Model
{
    //
    protected $table = "saldo_wallet";
    protected $fillable = [
        'debet',
        'credit',
        'description',
        'external_id',
        'saldo',
        'saldo_penghasilan',
        'admin',
        'uid'
    ];

}
