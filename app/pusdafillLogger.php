<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pusdafillLogger extends Model
{
    //
    protected $table="pusdafill_logger";

    protected $fillable = [
        'type', 'response','date_cron'
    ];

}
