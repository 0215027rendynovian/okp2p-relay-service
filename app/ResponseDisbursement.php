<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponseDisbursement extends Model
{

        protected $fillable = ['status','user_id','external_id','amount','bank_code','account_holder_name','disbursement_description'
        ,'id_disbursement'
        ,'uid'];
    
        protected $table = "response_disbursement";
}
