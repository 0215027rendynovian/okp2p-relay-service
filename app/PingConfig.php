<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PingConfig extends Model
{
    protected $table = "pingconfig";

    protected $fillable =  ['server_ip',  'server_port'];
}
