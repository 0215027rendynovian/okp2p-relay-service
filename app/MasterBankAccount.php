<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterBankAccount extends Model
{
    protected $fillable = [
        'bank_code', 'bank_name', 'additional'
    ];
}
