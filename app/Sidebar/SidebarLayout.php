<?php

namespace App\Sidebar;

use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Support\Facades\Auth;

//use App\Wahyu_Model;

class SidebarLayout
{

    // sample
    public static function array_sidebar() {
        dd('Hallo dunia');
    }

    public static function sidebar()
    {
        $parent_menu = ViewMasterMenu::all();
        $child_menu = ViewChildMenu::all();
        $permission = Auth::user()->permission;
        $is_essystem = Auth::user()->es_system;
 
        $filtered_parent_menu = array();
        $parent_menu_count = count($parent_menu);

        $decoded_parent_menu = json_decode($parent_menu);

        for ($x = 0; $x < $parent_menu_count; $x++){
            $current_menu = $decoded_parent_menu[$x];
        
            // Tampilkan menu ES/OPS sesuai dengan user yang login
            // Lihat tabel 'users', kolom 'es_system'
            // ES user: es_syste m= 0
            // OPS user: es_system = 1
            if ($is_essystem){
                if ( $current_menu->name == 'IZI DATA' ||  $current_menu->name == 'Fintech Data Center'){
                    $filtered_parent_menu[] = $current_menu;        
                }
            }
            else {
                if ( $current_menu->name !== 'IZI DATA' &&  $current_menu->name !== 'Fintech Data Center'){
                    $filtered_parent_menu[] = $current_menu;        
                }
            }   
        }

        $dataSidebar = [
            'parent_menu' => $filtered_parent_menu,
            'child_menu' => $child_menu,
            'permission' => $permission,
            'essystem' => $is_essystem
        ];

        return ($dataSidebar);
    }

}
