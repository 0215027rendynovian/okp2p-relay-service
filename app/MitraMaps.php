<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MitraMaps extends Model
{
    //
    protected $table="mitra_maps";
    protected $fillable = ['id_mitra','address','longitude','latitude','status_ready', 'type', 'jenis_kelamin','uid'];

    public function DetailMitra()
    {
         return $this->belongsTo('App\DetailMitra', 'id', 'id_mitra');
    }
}
