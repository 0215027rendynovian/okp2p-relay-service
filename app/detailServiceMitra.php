<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailServiceMitra extends Model
{
    //
    protected $table = 'detail_service_mitra';
    protected $fillable = ['id_service_mitra','duration','amount'];
}
