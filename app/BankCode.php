<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankCode extends Model
{
    //

    protected $table="bank_code";

    protected $fillable = [
        'bank_code', 'bank_name'
    ];

}
