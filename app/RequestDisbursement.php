<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDisbursement extends Model
{
    //
    protected $table = "request_disbursement";
    protected $fillable = ['external_id','amount','bank_code','account_holder_name','account_number','description','uid'];

}

