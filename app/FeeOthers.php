<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeOthers extends Model
{
    protected $table = "fee_others";
    protected $fillable = ['external_id','amount','uid'];
}
