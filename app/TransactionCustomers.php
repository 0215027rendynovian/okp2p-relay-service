<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionCustomers extends Model
{
    //
    protected $table = "transaction_customers";
    protected $fillable = ['payment_method', 'uid_mitra', 'id_customers', 'amount', 'uid','status_bayar'];
}
