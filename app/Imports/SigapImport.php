<?php

namespace App\Imports;

use App\Sigap;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class SigapImport implements ToModel, WithStartRow
{
    public function  __construct($execute_date)
    {
        $this->execute_date = $execute_date;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Sigap([
            'execute_date' => date('Y-m-d H:i:s'),
            'name' => $row[0],
            'nik' => $row[1],
            'expected' => $row[2],
            'code_densus' => $row[3],
            'birth' => $row[4],
            'birth_date' => date('Y-m-d', strtotime($row[5])),
            'citizen' => $row[6],
            'address' => $row[7]
        ]);
    }
}
