<?php

namespace App\Imports;

use App\uploadIzi;
use Maatwebsite\Excel\Concerns\ToModel;

class IziImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new uploadIzi([
            'name' => $row[1],
            'phone_number' => $row[2], 
            'ktp' => $row[3], 
            'batch' => $row[4],
        ]);
    }
}
