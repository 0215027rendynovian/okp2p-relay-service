<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NusaSms extends Model
{
    //

    protected $table="nusa_sms";
    
    protected $fillable = ['json'];

}
