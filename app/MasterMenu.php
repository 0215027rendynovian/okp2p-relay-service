<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterMenu extends Model
{
    //
    protected $fillable = [
        'name', 'status', 'description', 'menuopen', 'icon', 'url', 'code'
    ];

    protected $table = 'master_menu';

}
