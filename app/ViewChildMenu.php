<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewChildMenu extends Model
{
    public $table = 'view_child_menu';
}
