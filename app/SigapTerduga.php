<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SigapTerduga extends Model
{
    protected $table = 'sigap_terdugas';

    protected $fillable = [
        'id_backoffice',
        'nama',
        'nik',
        'tanggal_lahir',
        'tempat_Lahir',
        'warga_negara',
        'alamat',
        'id_ojk_list',
        'status',
        'additional',
    ];
}
