<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildMenu extends Model
{
    protected $fillable = [
        'parent', 'name', 'menuopen', 'icon', 'url', 'status', 'description', 'code'
    ];
}
