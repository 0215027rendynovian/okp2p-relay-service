<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewConfigRoleMenu extends Model
{
    public $table = 'view_config_role_menu';
}
