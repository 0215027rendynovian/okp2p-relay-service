<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceMitra extends Model
{
    //
    protected $table="service_mitra";
    protected $fillable = ['title','category','name_service','description','open_service','reference','duration','type'];
}

