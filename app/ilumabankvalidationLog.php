<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ilumabankvalidationLog extends Model
{
    //
    protected $fillable = [
        'bank_account_number', 'bank_code', 'given_name','uid','json','mode_dev'
    ];

    protected $table = 'log_iluma_bankvaldation';
}
