<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    //
    
    protected $table="rekening";

    protected $fillable = [
        'bank_code', 'account_number', 'uid', 'account_name',
    ];

    public function BankCode()
    {
         return $this->belongsTo('App\BankCode', 'bank_code', 'bank_code');
    }

}

