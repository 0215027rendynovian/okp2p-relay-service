<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableTimer extends Model
{
    //
    protected $table = 'table_mitra';
    protected $fillable = ['uid_customers','uid_mitra', 'id_order', 'timer','status_timer'];
}
