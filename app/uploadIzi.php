<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class uploadIzi extends Model
{
    //
    protected $table="upload_izi";
    protected $fillable = ['ktp','phone_number','name'];

}
