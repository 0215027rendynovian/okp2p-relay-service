<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sigap extends Model
{
    protected $table = 'dt_sigap';

    protected $fillable = [
        'name', 'nik', 'expected', 'code_densus', 'birth', 'birth_date', 'citizen', 'address','execute_date','status'
    ];
}
