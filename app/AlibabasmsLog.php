<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlibabasmsLog extends Model
{
    //

    protected $table = "log_alibabasms";
    protected $fillable = ['content','numberphone','type','mode_dev',
    'vendor','json', 'uid'];
}
