<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MidtransBankValidator extends Model
{

    protected $table = 'midtrans-bank-validator';

    protected $fillable = [
        'account_name', 'account_no', 'bank_name','count_hit', 'json', 'uid'
    ];



}
