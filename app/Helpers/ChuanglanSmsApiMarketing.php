<?php
namespace App\Helpers;

header("Content-type:text/html; charset=UTF-8");

class ChuanglanSmsApiMarketing {

	//Interface URL Used to send SMS
	const API_SEND_URL='http://intapi.253.com/send/json?';
	
	//Interface URL Used to Query SMS balance
	const API_BALANCE_QUERY_URL='http://intapi.253.com/balance/json?';

	const API_ACCOUNT='IM2612695';//Get SMS Account  from  https://zz.253.com/site/login.html 

	const API_PASSWORD='inv7k5dO0';//Get SMS Password  from https://zz.253.com/site/login.html
	/**
	 * @param string $mobile 		
	 * @param string $msg 			
	 */
	public function sendInternational( $mobile, $msg) {
	
		$postArr = array (
			'account'  =>  self::API_ACCOUNT,
			'password' => self::API_PASSWORD,
			'msg' => $msg,
			'mobile' => $mobile
        );

		$result = $this->curlPost( self::API_SEND_URL , $postArr);
		return $result;
	}
	
	
	public function queryBalance() {
		
		$postArr = array ( 
		    'account' => self::API_ACCOUNT,
		    'password' => self::API_PASSWORD,
		);
		$result = $this->curlPost(self::API_BALANCE_QUERY_URL, $postArr);
		return $result;
	}

	/**
	 * @param string $url  
	 * @param array $postFields  
	 * @return mixed
	 */
	private function curlPost($url,$postFields){
		$postFields = json_encode($postFields);
		$ch = curl_init ();
		curl_setopt( $ch, CURLOPT_URL, $url ); 
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=utf-8'
			)
		);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt( $ch, CURLOPT_TIMEOUT,10); 
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
		$ret = curl_exec ( $ch );
        if (false == $ret) {
            $result = curl_error(  $ch);
        } else {
            $rsp = curl_getinfo( $ch, CURLINFO_HTTP_CODE);
            if (200 !== $rsp) {
                $result = "Respose Status ". $rsp . " " . curl_error($ch);
            } else {
                $result = $ret;
            }
        }
		curl_close ( $ch );
		return $result;
	}
	
}