<?php

namespace App\Helpers;

class Menu
{

    public static function getMenu() {

        $izidata = `<li class="nav-item">
                    <a href="{{url('creditfeature')}}"  class="nav-link {{ Request::segment(1) === 'creditfeature' ? 'active' : null }}" >
                        <i class="fas fa-credit-card nav-icon"></i>
                        <p>Credit Feature</p>
                    </a>
                </li>`;


        $fdc = `<li class="nav-item">
                    <a href="{{url('fdc-data')}}"  class="nav-link {{ Request::segment(1) === 'fdc-data' ? 'active' : null }}" >
                        <i class="fas fa-server nav-icon"></i>
                        <p>Fintech Data Center</p>
                    </a>
                </li>`;
        $ojk = `<li class="nav-item">
                    <a href="{{url('displayojk')}}"  class="nav-link {{ Request::segment(1) === 'displayojk' ? 'active' : null }}" >
                        <i class="fas fa-file-invoice-dollar nav-icon"></i>
                        <p>Display OJK</p>
                    </a>
                </li>`;



        $minstamoney = `<li class="nav-item has-treeview {{ isset($instamoney) ? 'menu-open' : null }}">
                    <a class="nav-link {{ isset($instamoney) ? 'active' : null }}" >
                        <i class="nav-icon fas fa-hand-holding-usd"></i>
                        <p>INSTAMONEY
                        <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('BalanceIM')}}"  class="nav-link {{ Request::segment(1) === 'BalanceIM' ? 'active' : null }}" >
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>SALDO INSTAMONEY</p>
                            </a>
                        </li>
                    </ul>
                </li>`;


        $mpefindo = `<li class="nav-item has-treeview {{ isset($pefindo) ? 'menu-open' : null }}">
                    <a class="nav-link {{ isset($pefindo) ? 'active' : null }}" >
                        <i class="nav-icon fas fa-star-half-alt"></i>
                        <p>Pefindo
                        <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('contract')}}"  class="nav-link {{ Request::segment(1) === 'contract' ? 'active' : null }}" >
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>Contract</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('individual')}}"  class="nav-link {{ Request::segment(1) === 'individual' ? 'active' : null }}" >
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>Individual</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('company')}}"  class="nav-link {{ Request::segment(1) === 'company' ? 'active' : null }}" >
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>Company</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('collateral')}}"  class="nav-link {{ Request::segment(1) === 'collateral' ? 'active' : null }}" >
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>Collateral</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('subject-relation')}}"  class="nav-link {{ Request::segment(1) === 'subject-relation' ? 'active' : null }}" >
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>Subject Relation</p>
                            </a>
                        </li>
                    </ul>
                </li>`;



        $user = `<li class="nav-item">
                    <a href="{{url('listusers')}}"  class="nav-link {{ Request::segment(1) === 'listusers' ? 'active' : null }}" >
                        <i class="fas fa-users-cog nav-icon"></i>
                        <p>Users</p>
                    </a>
                </li>`;
    }

}
