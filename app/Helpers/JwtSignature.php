<?php

namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PusdafillClient;
use App\pusdafillLogger;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Sigap;

class JwtSignature
{


    public static function ShowSignature($RequestPayload, $X_API_JWT){

        
    // Create token header as a JSON string
        $header = JSON_encode([
            'typ' => 'JWT',
            'alg' => 'HS256'
        ]);


     // Parse Array To Json   
     $payload = json_encode($RequestPayload, true);


    //Encode Header to Base64Url String
        $base64UrlHeader = str_replace(
            ['+', '/', '='], ['-', '_', ''], base64_encode($header)
        );

    // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(
            ['+', '/', '='], ['-', '_', ''], base64_encode($payload)
        );
    

    // Create Signature Hash
        $signature = hash_hmac(
            'sha256', $base64UrlHeader.".".$base64UrlPayload, $X_API_JWT, true
        );
        
        
    // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(
            ['+', '/', '='], ['-', '_', ''], base64_encode($signature)
        );
       
       
     // Create JWT
        $jwt = $base64UrlHeader.".".$base64UrlPayload.".".$base64UrlSignature;
        return $jwt;
            

    }

    public static function EncodedString($output){


        $string1 = preg_replace( "/<br>|\n/", "", $output );

        $string2 = preg_replace("/\n/", "", $string1 );

        $encodedString = preg_replace("/\\\\n/", "", $string2 );

        return $encodedString;

    }



}
