<?php

namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PusdafillClient;
use App\pusdafillLogger;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Sigap;

class BNIHttpClient
{


    public static function POST($X_API_KEY,$authorization,$data_post,$url){

        $httpheader = array(
            "Content-type: application/json",
            "x-api-key: ".$X_API_KEY,
            "Authorization: Basic ".$authorization
        );

        $ch = curl_init();
                
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheader);

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    
        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

}
