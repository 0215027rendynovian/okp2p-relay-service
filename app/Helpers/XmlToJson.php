<?php

namespace App\Helpers;

use SimpleXMLElement;

class XmlToJson {

    /**
     *  How to Use this function
     *
     *  this function need xml file
     *
     *  write code like this in your controller :
     *
     *  $xmlNode = simplexml_load_file('sample/example.xml');
     *  $arrayData = XmlToJson::xmlToArray($xmlNode);
     *  $resultData = json_encode($arrayData, JSON_PRETTY_PRINT);
     *
     *  INI DIGUNAKAN UNTUK CONVERT DARI FILE .XML
     *
     */
    public static function xmlToArray($xml, $options = array()) {
        $defaults = array(
            'namespaceRecursive' => false,  //setting to true will get xml doc namespaces recursively
            'removeNamespace' => false,     //set to true if you want to remove the namespace from resulting keys (recommend setting namespaceSeparator = '' when this is set to true)
            'namespaceSeparator' => ':',    //you may want this to be something other than a colon
            'attributePrefix' => '@',       //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),       //array of xml tag names which should always become arrays
            'autoArray' => true,            //only create arrays for tags which appear more than once
            'textContent' => '$',           //key used for the text content of elements
            'autoText' => true,             //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,           //optional search and replace on tag and attribute names
            'keyReplace' => false           //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces($options['namespaceRecursive']);
        $namespaces[''] = null; //add base (empty) namespace

        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            if ($options['removeNamespace']) {
                $prefix = '';
            }
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch']) {
                    $attributeName =
                        str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                }
                $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            if ($options['removeNamespace']) {
                $prefix = '';
            }

            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = self::xmlToArray($childXml, $options);
                $childTagName = key($childArray);
                $childProperties = current($childArray);

                //replace characters in tag name
                if ($options['keySearch']) {
                    $childTagName =
                        str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                }

                //add namespace prefix, if any
                if ($prefix) {
                    $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
                }

                if (!isset($tagsArray[$childTagName])) {
                    //only entry with this key
                    //test if tags of this type should always be arrays, no matter the element count
                    $tagsArray[$childTagName] =
                            in_array($childTagName, $options['alwaysArray'], true) || !$options['autoArray']
                            ? array($childProperties) : $childProperties;
                } elseif (
                    is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                    === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    //key already exists and is integer indexed array
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    //key exists so convert to integer indexed array with previous value in position 0
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') {
            $textContentArray[$options['textContent']] = $plainText;
        }

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }

    /**
     *  How to use this function
     *
     *  this function need parameter string xml
     *
     *  write code like this in your controller :
     *
     *  $resJson = XmlToJson::xml2json($stringXml);
     *
     */
    public static function xml2json($stringXml)
    {
        $xml = simplexml_load_string($stringXml); // where $xml_string is the XML data you'd like to use (a well-formatted XML string). If retrieving from an external source, you can use file_get_contents to retrieve the data and populate this variable.
        $json = json_encode($xml); // convert the XML string to JSON

        return $json;
    }

    /**
     *  How to use this function
     *
     *  this function need parameter array
     *
     *  write code like this in your controller :
     *
     *  $resJson = XmlToJson::array2xml($stringArray);
     */
    public static function array2xml( $array, $xml = false) {
        // Test if iteration
        if ( $xml === false ) {
          $xml = new SimpleXMLElement('<result/>');
        }

        // Loop through array
        foreach( $array as $key => $value ) {
            // Another array? Iterate
            if ( is_array( $value ) ) {
              self::array2xml( $value, $xml->addChild( $key ) );
            } else {
              $xml->addChild( $key, $value );
            }
        }

        // Return XML
        return $xml->asXML();
    }

    /**
     *  How to use this function
     *
     *  this function need parameter string json
     *
     *  write code like this in your controller :
     *
     *  $resXml = XmlToJson::json2xml($stringJson);
     */
    public static function json2xml($json)
    {
        // Decode jSON to array
        $jSON = json_decode($json, true);

        // Process array elements to XML
        $xml = self::array2xml( $jSON, false );

        return $xml;
    }

    /**
     *  How to use this function
     *
     *  this function need parameter string xml soap
     *
     *  write code like this in your controller :
     *
     *  $resJson = XmlToJson::soapxml2json($soap);
     */
    public static function soapxml2json($soap)
    {

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $soap);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);

        $data = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1", $json);

        // $encodedString = preg_replace("/\\\\n/", "", json_encode($data));
        // $encodedString = preg_replace( "/\r|\n/", "", $data );
        $string1 = preg_replace( "/<br>|\n/", "", $data );
        $string2 = preg_replace("/\n/", "", $string1 );
        $encodedString = preg_replace("/\\\\n/", "", $string2 );

        $responseArray = json_decode($encodedString,true);

        //$convertedJson = soapxml2json($string);
        $customJson = $responseArray;
        {

            if(isset($customJson["sBody"]["GetCustomReportResponse"]["GetCustomReportResult"]["aCIP"]["bRecordList"]["bRecord"]))
            {
                $bRecord = $customJson["sBody"]["GetCustomReportResponse"]["GetCustomReportResult"]["aCIP"]["bRecordList"]["bRecord"];

                if(!array_key_exists(0,$bRecord))
                {
                    $customJson["sBody"]["GetCustomReportResponse"]["GetCustomReportResult"]["aCIP"]["bRecordList"]["bRecord"] = array($bRecord);
                }
            }



            if(isset($customJson["sBody"]["GetCustomReportResponse"]["GetCustomReportResult"]["aContractOverview"]["bContractList"]["bContract"]))
            {
                $bContract = $customJson["sBody"]["GetCustomReportResponse"]["GetCustomReportResult"]["aContractOverview"]["bContractList"]["bContract"];
                if(!array_key_exists(0,$bContract))
                {
                    $customJson["sBody"]["GetCustomReportResponse"]["GetCustomReportResult"]["aContractOverview"]["bContractList"]["bContract"] = array($bContract);
                }
            }
        }

        return json_encode($customJson);
    }



    public static function getPefindoId($soap)
    {

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $soap);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);

        // Get Detail Pefindo Id
        $getTree =
            $xml
                ->sBody
                ->SmartSearchIndividualResponse
                ->SmartSearchIndividualResult
                ->aIndividualRecords
                ->aSearchIndividualRecord
                ->aPefindoId;

        $pefindoId = json_decode($getTree,true);

        return $pefindoId;
    }
}

?>
