<?php

namespace App\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\MidtransBankValidator;
use App\ilumabankvalidationLog;
use App\creditFeaturedDetail;
use App\PefindoCustomeReport;
use App\AdvancescoreLog;
use App\CreaditFeatured;
use App\AlibabasmsLog;
use App\IzisallaryLog;
use App\FdcFeatured;
use App\IziCallback;
use App\IziClient;
use App\TigsmsLog;
use App\User;
use App\LogBniModel;
use App\NplCustLog;



use Exception;

class StoreServices {

    public static function storeFdc($request,$response){
        $store = FdcFeatured::create([
            'ktp' => $request->ktp,
            'reason' => $request->reason,
            'count_hit' => 1,
            'json' => $response,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }

    public static function storeTigsms($request,$response){
        $store = TigsmsLog::create([
            'content' => $request->content,
            'numberphone' => $request->numberphone,
            'type'  => $request->type,
            'vendor' => $request->vendor,
            'uid' => 1,
            'json' => $response,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }


    public static function storeAlibabasms($request,$response){
        $store = AlibabasmsLog::create([
            'content' => $request->content,
            'numberphone' => $request->numberphone,
            'type'  => $request->type,
            'vendor' => $request->vendor,
            'uid' => 1,
            'json' => $response,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }


    public static function storeIziCreditFeature($request,$response){
        $store = CreaditFeatured::create([
            'phone' =>$request->phone,
            'nama' => $request->name,
            'ktp' => $request->ktp,
            'json' => $response,
            'count_hit' => 1,
            "uid" => 1,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }

    
    public static function storeIziSallary($request,$response){
        $store = IzisallaryLog::create([
            'ktp' => $request->id,
            'json' => $response,
            "uid" => 1,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }


    public static function storeIluma($request, $response){
        $store = ilumabankvalidationLog::create([
            'bank_account_number' => $request->bank_account_number,
            'bank_code' => $request->bank_code,
            'given_name' => $request->given_name,
            'json' => $response,
            "uid" => 1,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }

 
    public static function storePefindo($ktp, $fullname, $DateOfBirth, $InquiryReason,$idpefindo,$reportdate,$status,$jsonDat,$mode_dev){
        $store = PefindoCustomeReport::create([
            'ktp' => $ktp,
            'fullname' => $fullname,
            'date_of_birth' => $DateOfBirth,
            'inquiry_reason' => $InquiryReason,
            'pefindo_id' => $idpefindo,
            'report_date' => $reportdate,
            'uid' => 1,
            'status' => $status,
            'json' => $jsonDat,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }


    public static function storeAdvanceScore($request, $response){
        $store = AdvancescoreLog::create([
            'name' => $request->name,
            'idNumber' => $request->idNumber,
            'phoneNumber' => $request->phoneNumber,
            'json' => $response,
            "uid" => 1,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);
        return $store;
    }


    public static function storeNplCust($request,$response){
        $store = NplCustLog::create([
            'ktp' => $request->ktp,
            'uid' => 1,
            'json' => $response,
            'mode_dev' => $request->header('mode_dev') === "true"? true:false
        ]);

        return $store;
    }




    // public static function storeMidtransValidation($data){}

    // public static function storeNusasms($data){}


    // =================================BNI Storage Service=================================//

    public static function storeLogBni($token,$dev_mode,$data_post,$response,$type,$url,$requestedUuid,$uid){
        
        $store = LogBniModel::create([
            'token' => $token,
            'mode_dev' => $dev_mode,
            'request' => $data_post,
            'response' => $response,
            'type' => $type,
            'url' =>$url,
            'requestedUuid' => $requestedUuid,
            'uid'=> $uid
        ]);
        
        return $store;
    }
    
}

?>
