<?php

namespace App\Helpers;

define('AES_256_CBC', 'aes-256-cbc');

class AES256 {
    function encrypt($plaintext, $encryptionKey){
        // initialization vector
        $IV = openssl_random_pseudo_bytes(openssl_cipher_iv_length(AES_256_CBC));

        // enkripsi AES
        $encrypted = openssl_encrypt($plaintext, AES_256_CBC, $encryptionKey, 0, $IV);

        // concat hasil enkripsi dengan '::' IV
        $encrypted = $encrypted . '::' . $IV;

        // base_64 hasil sebelumnya
        $encrypted = base64_encode($encrypted);
        return $encrypted;
    }

    public static function encrypt_with_iv($plaintext, $encryptionKey, $IV){

        // enkripsi AES
        $encrypted0 = openssl_encrypt($plaintext, AES_256_CBC, $encryptionKey, 0, $IV);

        // concat hasil enkripsi dengan '::' dan IV
        $encrypted1 = $encrypted0 . '::' . $IV;

        // base_64 hasil sebelumnya
        $encrypted2 = base64_encode($encrypted1);

        return $encrypted2;
    }

    public static function decrypt($encrypted, $encryptionKey){
        $parts = explode('::', base64_decode($encrypted));
        $decrypted = openssl_decrypt($parts[0], AES_256_CBC, $encryptionKey, 0, $parts[1]);
        return $decrypted;
    }
}

?>
