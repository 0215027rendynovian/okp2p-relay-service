<?php

namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PusdafillClient;
use App\creditFeaturedDetail;
use App\CreaditFeatured;
use App\Http\Requests;
use App\FdcFeatured;
use App\IziCallback;
use App\IziClient;
use App\MidtransBankValidator;
use App\User;
use Exception;
use Illuminate\Support\Facades\DB;

class RelayServices {



    public static function getSmartSearch($ktp,$DateOfBirth,$fullname,$InquiryReason,$reportdate)
    {
        $ktp;

        $DateOfBirth;

        $fullname;

        $InquiryReason;
        
        $reportdate; 

        $base_url = 'http://182.253.0.228:8000';

        $url = $base_url."/api/relay/requestSmartSearch";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_POSTFIELDS,"ktp=".$ktp."&DateOfBirth=".$DateOfBirth."&fullname=".$fullname."&InquiryReason=".$InquiryReason."&reportdate=".$reportdate."");


        curl_setopt($ch, CURLOPT_URL, $url);


        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HEADER, false);

        $output = curl_exec($ch);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return $output;

    }


    
    public static function Getfdcdata($ktp, $reason){

        $base_url = 'http://182.253.0.228:8000';


        $url = $base_url."/api/api-post";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_POSTFIELDS,"ktp=".$ktp."&reason=".$reason."");


        curl_setopt($ch, CURLOPT_URL, $url);


        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HEADER, false);

        $output = curl_exec($ch);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $fdcFeatured = FdcFeatured::where('ktp', $ktp)->first();

        
        if($code === 0){

            throw new Exception(curl_error($ch));

        }else{
            FdcFeatured::create([
                'ktp' => $ktp,
                'reason' => $reason,
                'count_hit' => 1,
                'json' => $output
            ]);
        }

        curl_close($ch);

        return $output;

    }
}

?>
