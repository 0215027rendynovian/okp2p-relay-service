<?php

namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PusdafillClient;
use App\pusdafillLogger;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Sigap;

class ListHelper
{

    /**
     * validate Api Pusdafil.
     * @return \Illuminate\Http\Response
    */

    public static function request_pusdafil($param, $type){


        $url = "https://pusdafil.ojk.go.id/PusdafilAPI/pelaporanharian";

        $username = 'dev@okp2p.co.id';

        $password = 'admin123';

        $data[$type] = $param;

        $client = new PusdafillClient($username, $password);


        $res = $client->request($url, $data);

        $date_cron = date("Ymd", time() - 3600);

        $pusdafillLogger = pusdafillLogger::create([
        "response" => $res,"type" => $type, "date_cron" => $date_cron]);

        return $res;

    }


   /**
     * Get list of all available category group
     *
     * @return array
     */
    public static function borrowerDataLst($name,$ktp,$pengajuan)
    {
        return DB::connection('sqlsrv')->select("
        select
        INSP_NO as id_backoffice,
        CUS_NM as nama,
        KTP as nik,
        BIRTHDAY as tanggal_lahir,
        BIRTH as tempat_Lahir,
        'Indonesia' as warga_negara
        ,DBO.FN_GET_PT_ADDRRESS(PT_LNC001TM.INSP_NO,'120001') as alamat
        from PT_LNC001TM where
        CUS_NM LIKE '%$name%'
        AND KTP LIKE '%$ktp%'
        AND INSP_NO LIKE '%$pengajuan%'
        AND LN_STA_CD NOT IN('900010', '900020','900030','900040','900050','900801','900901','900902','900903','900909')");
    }


    /**
     * Get list of this theroist*
     * @return array
     */

    public static function thisTerorist($param,$name, $ktp, $pengajuan)
    {

        // Check Status 0 Default

        $checking = DB::table('dt_sigap')->whereIn('status', [0])->where('name',$param->nama)->orWhere('nik',$param->nik)->orderBy('name', 'asc')->first();

        if(!empty($checking)){

            Sigap::where('id', $checking->id)->update(['status' => 1]);

            return DB::connection('sqlsrv')->select("
            select
            INSP_NO as id_backoffice,
            CUS_NM as nama,
            KTP as nik,
            BIRTHDAY as tanggal_lahir,
            BIRTH as tempat_Lahir,
            'Indonesia' as warga_negara
            ,DBO.FN_GET_PT_ADDRRESS(PT_LNC001TM.INSP_NO,'120001') as alamat,
            ,'$checking->id' as id_ojk_list
            from PT_LNC001TM where
            CUS_NM LIKE '%$name%'
            AND KTP LIKE '%$ktp%'
            AND INSP_NO LIKE '%$pengajuan%'
            AND LN_STA_CD NOT IN('900010', '900020','900030','900040','900050','900801','900901','900902','900903','900909')
            AND PT_LNC001TM.CUS_NM = '$checking->name'");



        }else{

            return $var = [];
        }

    }


    /**
     * Find all Sigap
     *
     * @return array
     */
    public static function check_data_sigap($name, $ktp, $pengajuan)
    {

        $list = [];

        foreach (self::borrowerDataLst($name, $ktp, $pengajuan) as $key => $value){



            foreach (self::thisTerorist($value,$name, $ktp, $pengajuan) as $key2 => $value2){
                if(!in_array($value2, $list, true)){
                    array_push($list, $value2);
                }
            }

        }
        $dtTherorist = array_unique($list,SORT_REGULAR);

        return $dtTherorist;
    }


    // ------------------------------------------------------------------------------




    /**
     * Get list of all available category group
     *
     * @return array
     */
    public static function borrowerDataLstTerduga($name,$ktp, $pengajuan)
    {
        return DB::connection('sqlsrv')->select("
        select
        INSP_NO as id_backoffice,
        CUS_NM as nama,
        KTP as nik,
        BIRTHDAY as tanggal_lahir,
        BIRTH as tempat_Lahir,
        'Indonesia' as warga_negara
        ,DBO.FN_GET_PT_ADDRRESS(PT_LNC001TM.INSP_NO,'120001') as alamat
        from PT_LNC001TM where
        CUS_NM LIKE '%$name%'
        AND KTP LIKE '%$ktp%'
        AND INSP_NO LIKE '%$pengajuan%'
        AND LN_STA_CD NOT IN('900010', '900020','900030','900040','900050','900801','900901','900902','900903','900909')");
    }



    /**
     * Get list of this theroist*
     * @return array
     */

    public static function thisTeroristTerduga($param,$name, $ktp, $pengajuan)
    {
        // Check Status 1 == Terduga
        $checking = DB::table('dt_sigap')->whereIn('status', [0,1])->where('name',$param->nama)->orWhere('nik',$param->nik)->orderBy('name', 'asc')->first();

        if(!empty($checking)){

            Sigap::where('id', $checking->id)->update(['status' => 1]);

            return DB::connection('sqlsrv')->select("
            select
            INSP_NO as id_backoffice,
            CUS_NM as nama,
            KTP as nik,
            BIRTHDAY as tanggal_lahir,
            BIRTH as tempat_Lahir,
            'Indonesia' as warga_negara
            ,DBO.FN_GET_PT_ADDRRESS(PT_LNC001TM.INSP_NO,'120001') as alamat
            ,'$checking->id' as id_ojk_list
            from PT_LNC001TM where
            CUS_NM LIKE '%$name%'
            AND KTP LIKE '%$ktp%'
            AND INSP_NO LIKE '%$pengajuan%'
            AND LN_STA_CD NOT IN('900010', '900020','900030','900040','900050','900801','900901','900902','900903','900909')
            AND PT_LNC001TM.CUS_NM = '$checking->name'");

        }else{

            return $var = [];
        }
    }


    /**
     * Find all Sigap
     *
     * @return array
     */
    public static function check_data_sigap_terduga($name, $ktp, $pengajuan)
    {

        $list = [];

        foreach (self::borrowerDataLstTerduga($name, $ktp, $pengajuan) as $key => $value){



            foreach (self::thisTeroristTerduga($value,$name, $ktp, $pengajuan) as $key2 => $value2){
                if(!in_array($value2, $list, true)){
                    array_push($list, $value2);
                }
            }

        }
        $dtTherorist = array_unique($list,SORT_REGULAR);

        return $dtTherorist;
    }



    // ================= Pembuatan Data SIGAP OJK ================

    /**
     * Find all Sigap
     *
     * @return array
     */
    public static function creationData()
    {

        $list = [];

        foreach (self::borrowerCreationData() as $key => $value){

            foreach (self::thisCreationDataTerduga($value) as $key2 => $value2){
                if(!in_array($value2, $list, true)){
                    array_push($list, $value2);
                }
            }

        }
        $dtTherorist = array_unique($list,SORT_REGULAR);

        return $dtTherorist;
    }

    /**
     * Get list of all available category group
     *
     * @return array
     */
    public static function borrowerCreationData()
    {
        return DB::connection('sqlsrv')->select("
        select
        INSP_NO as id_backoffice,
        CUS_NM as nama,
        KTP as nik,
        BIRTHDAY as tanggal_lahir,
        BIRTH as tempat_Lahir,
        'Indonesia' as warga_negara
        ,DBO.FN_GET_PT_ADDRRESS(PT_LNC001TM.INSP_NO,'120001') as alamat
        from PT_LNC001TM where
        LN_STA_CD NOT IN('900010', '900020','900030','900040','900050','900801','900901','900902','900903','900909')");
    }

    /**
     * Get list of this theroist*
     * @return array
     */

    public static function thisCreationDataTerduga($param)
    {
        // Check Status 1 == Terduga
        $checking = DB::table('dt_sigap')->whereIn('status', [0,1])->where('name',$param->nama)->orWhere('nik',$param->nik)->orderBy('name', 'asc')->first();

        DB::table('dt_sigap')->update(['compare' => 1]);

        if(!empty($checking)){

            Sigap::where('id', $checking->id)->update(['status' => 1]);

            return DB::connection('sqlsrv')->select("
                SELECT
                    INSP_NO as id_backoffice,
                    CUS_NM as nama,
                    KTP as nik,
                    BIRTHDAY as tanggal_lahir,
                    BIRTH as tempat_Lahir,
                    'Indonesia' as warga_negara,
                    DBO.FN_GET_PT_ADDRRESS(PT_LNC001TM.INSP_NO,'120001') as alamat,
                    '$checking->id' as id_ojk_list,
                    '$checking->execute_date' as execute_date,
                    '$checking->expected' as expected,
                    '$checking->code_densus' as code_densus
                FROM PT_LNC001TM where
                    LN_STA_CD NOT IN('900010', '900020','900030','900040','900050','900801','900901','900902','900903','900909')
                    AND PT_LNC001TM.CUS_NM = '$checking->name'
            ");

        }else{

            return $var = [];
        }
    }

        /**
     * validate Api Pusdafil.
     * @return \Illuminate\Http\Response
    */

    public static function EncodedString($output){


        $string1 = preg_replace( "/<br>|\n/", "", $output );

        $string2 = preg_replace("/\n/", "", $string1 );

        $encodedString = preg_replace("/\\\\n/", "", $string2 );

        return $encodedString;

    }

    // MOCK DATA
    public static function MockDataFDC(){
        $data = '{ "noIdentitas": "3175062302970007", "userId": "riski.firdaus@okp2p.co.id", "userName": "OK!P2P", "inquiryReason": "1 - Applying loan via Platform", "memberId": "820140", "memberName": "OK!P2P", "refferenceId": "123", "inquiryDate": "2021-09-14", "status": "Found", "pinjaman": [ { "id_penyelenggara": "5", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "000000000000000", "tgl_perjanjian_borrower": "2021-07-14", "tgl_penyaluran_dana": "2021-07-14", "nilai_pendanaan": 1200000.0, "tgl_pelaporan_data": "2021-07-28", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-08-11", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "5", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "000000000000000", "tgl_perjanjian_borrower": "2021-07-23", "tgl_penyaluran_dana": "2021-07-23", "nilai_pendanaan": 1200000.0, "tgl_pelaporan_data": "2021-08-21", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-08-20", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "5", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "000000000000000", "tgl_perjanjian_borrower": "2021-06-26", "tgl_penyaluran_dana": "2021-06-26", "nilai_pendanaan": 600000.0, "tgl_pelaporan_data": "2021-07-05", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-07-24", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "4", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "758252100006000", "tgl_perjanjian_borrower": "2019-07-05", "tgl_penyaluran_dana": "2019-07-05", "nilai_pendanaan": 400000.0, "tgl_pelaporan_data": "2019-12-17", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2019-08-04", "kualitas_pinjaman": "2", "dpd_terakhir": 30, "dpd_max": 30, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Tidak Lancar (30 sd 90 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "4", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-05-25", "tgl_penyaluran_dana": "2021-05-25", "nilai_pendanaan": 400000.0, "tgl_pelaporan_data": "2021-06-05", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-06-24", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "4", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2020-08-13", "tgl_penyaluran_dana": "2020-08-13", "nilai_pendanaan": 500000.0, "tgl_pelaporan_data": "2020-09-09", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2020-09-12", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "3", "jenis_pengguna": 1, "nama_borrower": "Hidayatulloh", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-07-08", "tgl_penyaluran_dana": "2021-07-08", "nilai_pendanaan": 2800020.0, "tgl_pelaporan_data": "2021-07-18", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-10-08", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "4", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-06-08", "tgl_penyaluran_dana": "2021-06-08", "nilai_pendanaan": 400000.0, "tgl_pelaporan_data": "2021-06-20", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-07-08", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "6", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-06-29", "tgl_penyaluran_dana": "2021-06-29", "nilai_pendanaan": 500000.0, "tgl_pelaporan_data": "2021-07-11", "sisa_pinjaman_berjalan": 500000.0, "tgl_jatuh_tempo_pinjaman": "2021-07-27", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "5", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "000000000000000", "tgl_perjanjian_borrower": "2021-07-07", "tgl_penyaluran_dana": "2021-07-07", "nilai_pendanaan": 600000.0, "tgl_pelaporan_data": "2021-07-13", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-08-04", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "4", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2020-07-17", "tgl_penyaluran_dana": "2020-07-17", "nilai_pendanaan": 600000.0, "tgl_pelaporan_data": "2020-08-11", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2020-08-16", "kualitas_pinjaman": "2", "dpd_terakhir": 30, "dpd_max": 30, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Tidak Lancar (30 sd 90 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "4", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-06-23", "tgl_penyaluran_dana": "2021-06-23", "nilai_pendanaan": 400000.0, "tgl_pelaporan_data": "2021-07-02", "sisa_pinjaman_berjalan": 0.0, "tgl_jatuh_tempo_pinjaman": "2021-07-23", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "L", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Fully Paid" }, { "id_penyelenggara": "4", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-05-06", "tgl_penyaluran_dana": "2021-05-06", "nilai_pendanaan": 400000.0, "tgl_pelaporan_data": "2021-05-23", "sisa_pinjaman_berjalan": 400000.0, "tgl_jatuh_tempo_pinjaman": "2021-06-05", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "O", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Outstanding" }, { "id_penyelenggara": "3", "jenis_pengguna": 1, "nama_borrower": "Hidayatulloh", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-07-15", "tgl_penyaluran_dana": "2021-07-15", "nilai_pendanaan": 3900550.0, "tgl_pelaporan_data": "2021-09-13", "sisa_pinjaman_berjalan": 2699050.0, "tgl_jatuh_tempo_pinjaman": "2021-10-15", "kualitas_pinjaman": "1", "dpd_terakhir": 0, "dpd_max": 0, "status_pinjaman": "O", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Outstanding" }, { "id_penyelenggara": "2", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-08-07", "tgl_penyaluran_dana": "2021-08-07", "nilai_pendanaan": 800000.0, "tgl_pelaporan_data": "2021-09-13", "sisa_pinjaman_berjalan": 800000.0, "tgl_jatuh_tempo_pinjaman": "2021-09-06", "kualitas_pinjaman": "1", "dpd_terakhir": 7, "dpd_max": 7, "status_pinjaman": "O", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Outstanding" }, { "id_penyelenggara": "2", "jenis_pengguna": 1, "nama_borrower": "HIDAYATULLOH", "no_identitas": "3175062302970007", "no_npwp": "", "tgl_perjanjian_borrower": "2021-07-29", "tgl_penyaluran_dana": "2021-07-29", "nilai_pendanaan": 1000000.0, "tgl_pelaporan_data": "2021-09-13", "sisa_pinjaman_berjalan": 1000000.0, "tgl_jatuh_tempo_pinjaman": "2021-08-28", "kualitas_pinjaman": "1", "dpd_terakhir": 16, "dpd_max": 16, "status_pinjaman": "O", "jenis_pengguna_ket": "Individual", "kualitas_pinjaman_ket": "Lancar (<30 hari)", "status_pinjaman_ket": "Outstanding" } ] }';
        return $data;
    }

    public static function MockDataSMS(){
       $data = '{"ResponseCode":"OK","NumberDetail":{"Country":"Indonesia","Region":"Indonesia","Carrier":"IM3"},"RequestId":"60218E45-8216-331D-B0D2-C8F7C81560AE","ResponseDescription":"152311631612761021^0","Segments":"2","To":"6285624282247","From":"OKP2P","MessageId":"152311631612761021"}';
       return $data;
    }

    public static function MockDataIzi(){
        $data = '{ "status": "OK", "message": { "creditscore": 585, "featurelist": { "A_II_14d": 1, "A_II_180d": 14, "A_II_21d": 2, "A_II_30d": 2, "A_II_360d": 23, "A_II_3d": 0, "A_II_60d": 4, "A_II_7d": 0, "A_II_90d": 8, "A_PI_14d": 2, "A_PI_180d": 15, "A_PI_21d": 2, "A_PI_30d": 2, "A_PI_360d": 23, "A_PI_3d": 0, "A_PI_60d": 2, "A_PI_7d": 0, "A_PI_90d": 6, "B_II_14d": 0, "B_II_180d": 1, "B_II_21d": 0, "B_II_30d": 0, "B_II_360d": 1, "B_II_3d": 0, "B_II_60d": 0, "B_II_7d": 0, "B_II_90d": 0, "B_PI_14d": 0, "B_PI_180d": 0, "B_PI_21d": 0, "B_PI_30d": 0, "B_PI_360d": 0, "B_PI_3d": 0, "B_PI_60d": 0, "B_PI_7d": 0, "B_PI_90d": 0, "C_II_14d": 0, "C_II_180d": 0, "C_II_21d": 0, "C_II_30d": 0, "C_II_360d": 0, "C_II_3d": 0, "C_II_60d": 0, "C_II_7d": 0, "C_II_90d": 0, "C_PI_14d": 0, "C_PI_180d": 0, "C_PI_21d": 0, "C_PI_30d": 0, "C_PI_360d": 0, "C_PI_3d": 0, "C_PI_60d": 0, "C_PI_7d": 0, "C_PI_90d": 0, "II_v4_feature_status": "OK", "PI_v4_feature_status": "OK", "b_activeStatus": 0, "b_activeYear": 4, "b_class": null, "b_isopen": 1, "b_members": 1, "b_membership": 13, "ece": 1, "eco": 1, "ecp": 1, "ect": 1, "identity_address": "KP PANYANDUNGAN", "identity_city": "CIANJUR", "identity_date_of_birth": "24-11-1993", "identity_district": "CAMPAKA", "identity_gender": "LAKI-LAKI", "identity_marital_status": "BELUM KAWIN ", "identity_match": 0, "identity_name": "RENDY NOVIAN", "identity_nationnality": "WNI", "identity_place_of_birth": "BANDUNG", "identity_province": "JAWA BARAT", "identity_religion": "ISLAM", "identity_rt": "003", "identity_rw": "001", "identity_status": "OK", "identity_village": "GIRIMUKTI", "identity_work": "PELAJAR/MAHASISWA", "idinquiries_14d": 1, "idinquiries_180d": 15, "idinquiries_21d": 2, "idinquiries_30d": 2, "idinquiries_360d": 24, "idinquiries_3d": 0, "idinquiries_60d": 4, "idinquiries_7d": 0, "idinquiries_90d": 8, "idinquiries_status": "OK", "idinquiries_total": 30, "idinquiriesuname_14d": 1, "idinquiriesuname_180d": 5, "idinquiriesuname_21d": 1, "idinquiriesuname_30d": 1, "idinquiriesuname_360d": 5, "idinquiriesuname_3d": 0, "idinquiriesuname_60d": 1, "idinquiriesuname_7d": 0, "idinquiriesuname_90d": 1, "idinquiriesuname_status": "OK", "idinquiriesuname_total": 7, "isfacebook": "NO_ACCOUNT", "iswhatsapp": "yes", "multiidinquiries_14d": 0, "multiidinquiries_21d": 0, "multiidinquiries_30d": 0, "multiidinquiries_60d": 0, "multiidinquiries_7d": 0, "multiidinquiries_90d": 0, "multiidinquiries_total": 5, "multiphone_idinfo": "", "multiphone_phoneinfo_id": "3207210104940001", "multiphone_phoneinfo_id_phone": "+6281312555467", "multiphone_status": "OK", "multiphoneinquiries_14d": 0, "multiphoneinquiries_21d": 0, "multiphoneinquiries_30d": 0, "multiphoneinquiries_60d": 0, "multiphoneinquiries_7d": 0, "multiphoneinquiries_90d": 0, "multiphoneinquiries_total": 0, "phoneage": "10-12month", "phoneage_status": "OK", "phonealive_id_num": 7, "phonealive_phone_num": 2, "phonealive_status": "OK", "phoneinquiries_14d": 2, "phoneinquiries_180d": 15, "phoneinquiries_21d": 2, "phoneinquiries_30d": 2, "phoneinquiries_360d": 23, "phoneinquiries_3d": 0, "phoneinquiries_60d": 2, "phoneinquiries_7d": 0, "phoneinquiries_90d": 6, "phoneinquiries_status": "OK", "phoneinquiries_total": 23, "phoneinquiriesuname_14d": 2, "phoneinquiriesuname_180d": 2, "phoneinquiriesuname_21d": 2, "phoneinquiriesuname_30d": 2, "phoneinquiriesuname_360d": 3, "phoneinquiriesuname_3d": 0, "phoneinquiriesuname_60d": 2, "phoneinquiriesuname_7d": 0, "phoneinquiriesuname_90d": 2, "phoneinquiriesuname_status": "OK", "phoneinquiriesuname_total": 3, "phonescore": 32, "phonescore_status": "OK", "phoneverify": "NOT_MATCH", "preference_bank_180d": 11, "preference_bank_270d": 12, "preference_bank_60d": 6, "preference_bank_90d": 7, "preference_ecommerce_180d": 59, "preference_ecommerce_270d": 72, "preference_ecommerce_60d": 18, "preference_ecommerce_90d": 36, "preference_game_180d": 11, "preference_game_270d": 11, "preference_game_60d": 2, "preference_game_90d": 8, "preference_lifestyle_180d": 112, "preference_lifestyle_270d": 180, "preference_lifestyle_60d": 28, "preference_lifestyle_90d": 49, "preference_status": "OK", "topup_0_180_avg": 13, "topup_0_180_max": 150, "topup_0_180_min": 5, "topup_0_180_times": 96, "topup_0_30_avg": 55, "topup_0_30_max": 150, "topup_0_30_min": 5, "topup_0_30_times": 3, "topup_0_360_avg": 16, "topup_0_360_max": 150, "topup_0_360_min": 5, "topup_0_360_times": 160, "topup_0_60_avg": 21, "topup_0_60_max": 150, "topup_0_60_min": 5, "topup_0_60_times": 18, "topup_0_90_avg": 16, "topup_0_90_max": 150, "topup_0_90_min": 5, "topup_0_90_times": 40, "topup_180_360_avg": 20, "topup_180_360_max": 115, "topup_180_360_min": 5, "topup_180_360_times": 64, "topup_30_60_avg": 14, "topup_30_60_max": 150, "topup_30_60_min": 5, "topup_30_60_times": 15, "topup_360_720_avg": 45, "topup_360_720_max": 100, "topup_360_720_min": 10, "topup_360_720_times": 9, "topup_60_90_avg": 11, "topup_60_90_max": 150, "topup_60_90_min": 5, "topup_60_90_times": 22, "topup_90_180_avg": 11, "topup_90_180_max": 150, "topup_90_180_min": 5, "topup_90_180_times": 56, "topup_status": "OK", "whatsapp_avatar": "yes", "whatsapp_company_account": "", "whatsapp_signature": "اللَّهُمَّ اغْفِرْ لِي ، وَارْحَمْنِي ، وَاهْدِني ، وَعَافِني ، وَارْزُقْنِي", "whatsapp_updatestatus_time": 20210608 } } }';
        return $data;
    }

    public static function MockDataSallary(){
        $data = '{"status" : "OK", "message" : {"salary" : 700}}';
        return $data;
    }

    public static function MockDataIluma(){
        $data = '{ "status": "SUCCESS", "bank_account_number": "0559318289", "bank_code": "BCA", "updated": "2021-09-15T01:42:56.745Z", "is_normal_account": true, "name_matching_result": "NOT_MATCH", "id": "61414fa016c9a87bc149711f" }';
        return $data;
    }

    public static function MockDataPefindo(){
        $data = '{ "sBody": { "GetCustomReportResponse": { "GetCustomReportResult": { "aCIP": { "bRecordList": { "bRecord": [ { "bDate": "2021-09-01T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "17.21", "bReasonsList": { "bReason": { "bCode": "NSL1", "bDescription": "Contracts from 3 different subscribers last 6 months" } }, "bScore": "598", "bTrend": "NoChange" }, { "bDate": "2021-08-31T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "17.21", "bReasonsList": { "bReason": { "bCode": "NSL1", "bDescription": [] } }, "bScore": "598", "bTrend": "NoChange" }, { "bDate": "2021-07-31T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "17.21", "bReasonsList": { "bReason": { "bCode": "NSL1", "bDescription": [] } }, "bScore": "598", "bTrend": "NoChange" }, { "bDate": "2021-06-30T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "17.21", "bReasonsList": { "bReason": { "bCode": "NSL1", "bDescription": [] } }, "bScore": "598", "bTrend": "Down" }, { "bDate": "2021-05-31T00:00:00", "bGrade": "D2", "bProbabilityOfDefault": "14.16", "bReasonsList": { "bReason": { "bCode": "NSL2", "bDescription": [] } }, "bScore": "608", "bTrend": "NoChange" }, { "bDate": "2021-04-30T00:00:00", "bGrade": "D2", "bProbabilityOfDefault": "14.16", "bReasonsList": { "bReason": { "bCode": "NSL2", "bDescription": [] } }, "bScore": "608", "bTrend": "Up" }, { "bDate": "2021-03-31T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "25.68", "bReasonsList": { "bReason": { "bCode": "RDT3", "bDescription": [] } }, "bScore": "576", "bTrend": "Down" }, { "bDate": "2021-02-28T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "20.37", "bReasonsList": { "bReason": { "bCode": "RDT3", "bDescription": [] } }, "bScore": "589", "bTrend": "NoChange" }, { "bDate": "2021-01-31T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "20.37", "bReasonsList": { "bReason": { "bCode": "RDT3", "bDescription": [] } }, "bScore": "589", "bTrend": "NoChange" }, { "bDate": "2020-12-31T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "20.37", "bReasonsList": { "bReason": { "bCode": "RDT3", "bDescription": [] } }, "bScore": "589", "bTrend": "Down" }, { "bDate": "2020-11-30T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "17.87", "bReasonsList": { "bReason": { "bCode": "RDT3", "bDescription": [] } }, "bScore": "596", "bTrend": "Up" }, { "bDate": "2020-10-31T00:00:00", "bGrade": "D3", "bProbabilityOfDefault": "18.22", "bReasonsList": { "bReason": { "bCode": "RDT3", "bDescription": [] } }, "bScore": "595", "bTrend": "Down" }, { "bDate": "2020-09-30T00:00:00", "bGrade": "D2", "bProbabilityOfDefault": "13.61", "bReasonsList": [], "bScore": "610", "bTrend": "Up" } ] } }, "aCIQ": [], "aCollaterals": [], "aCompany": [], "aContractOverview": { "bContractList": { "bContract": [ { "bContractStatus": "GrantedAndActivated", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "10668350.00", "cValue": "10668350.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Open", "bRoleOfClient": "MainDebtor", "bSector": "NonBankingFinancialInstitutions", "bStartDate": "2021-03-03T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "10668350.00", "cValue": "10668350.00" }, "bTypeOfContract": "OtherWithLoanAgreement" }, { "bContractStatus": "GrantedAndActivated", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Open", "bRoleOfClient": "MainDebtor", "bSector": "NonBankingFinancialInstitutions", "bStartDate": "2019-11-23T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "600000.00", "cValue": "600000.00" }, "bTypeOfContract": "Installment" }, { "bContractStatus": "GrantedAndActivated", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "900000.00", "cValue": "900000.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Open", "bRoleOfClient": "MainDebtor", "bSector": "Banks", "bStartDate": "2021-07-19T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "900000.00", "cValue": "900000.00" }, "bTypeOfContract": "ChannelingLoan" }, { "bContractStatus": "GrantedAndActivated", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "201298467.00", "cValue": "201298467.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Open", "bRoleOfClient": "MainDebtor", "bSector": "Banks", "bStartDate": "2021-06-14T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "201298467.00", "cValue": "201298467.00" }, "bTypeOfContract": "ChannelingLoan" }, { "bContractStatus": "Settled", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Closed", "bRoleOfClient": "MainDebtor", "bSector": "Banks", "bStartDate": "2018-06-06T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "2106040.00", "cValue": "2106040.00" }, "bTypeOfContract": "OtherWithLoanAgreement" }, { "bContractStatus": "SettledWithDiscount", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Closed", "bRoleOfClient": "MainDebtor", "bSector": "NonBankingFinancialInstitutions", "bStartDate": "2016-06-18T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "20677532.00", "cValue": "20677532.00" }, "bTypeOfContract": "Installment" }, { "bContractStatus": "Settled", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Closed", "bRoleOfClient": "MainDebtor", "bSector": "Banks", "bStartDate": "2020-12-22T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bTypeOfContract": "OtherWithLoanAgreement" }, { "bContractStatus": "Settled", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Closed", "bRoleOfClient": "MainDebtor", "bSector": "Banks", "bStartDate": "2020-12-22T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bTypeOfContract": "OtherWithLoanAgreement" }, { "bContractStatus": "Settled", "bOutstandingAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bPastDueDays": "0", "bPhaseOfContract": "Closed", "bRoleOfClient": "MainDebtor", "bSector": "Banks", "bStartDate": "2021-06-16T00:00:00", "bTotalAmount": { "cCurrency": "IDR", "cLocalValue": "0.00", "cValue": "0.00" }, "bTypeOfContract": "ChannelingLoan" } ] } }, "aContractSummary": [], "aContracts": [], "aCurrentRelations": [], "aDashboard": [], "aDisputes": [], "aFinancials": [], "aIndividual": { "bContact": { "bEmail": "0215027rendynovian@gmail.com", "bFixedLine": "085624282247", "bMobilePhone": "085624282247" }, "bGeneral": { "bAlias": [], "bCitizenship": "ID", "bClassificationOfIndividual": "Individual", "bDateOfBirth": "1993-11-24T00:00:00", "bEducation": "Bachelor", "bEmployerName": "OKE PTOP INDONESIA PT", "bEmployerSector": "ID_9990", "bEmployment": "Computer", "bFullName": "Rendy Novian", "bFullNameLocal": "Rendy Novian", "bGender": "Male", "bMaritalStatus": "Married", "bMotherMaidenName": "Didah Kurnia", "bPlaceOfBirth": "BANDUNG", "bResidency": "Yes", "bSocialStatus": "Employed" }, "bIdentifications": { "bKTP": "3203032411930003", "bNPWP": "858917081406000", "bPassportIssuerCountry": "NotSpecified", "bPassportNumber": [], "bPefindoId": "1239743373" }, "bMainAddress": { "bAddressLine": "KP LIO SANTA NO 26 RT 001 RW 006 KEL GEDONG PANJANG KEC CITAMIANG SUKABUMI, 0193, 43142, GEDONG PANJANG, CITAMIANG, ID", "bCity": "Sukabumi, Kota.", "bCountry": "ID", "bDistrict": "Gedong Panjang", "bParish": "Citamiang", "bPostalCode": "43142", "bStreet": "Kp Lio Santa No 26 Rt 001 Rw 006 Kel Gedong Panjang Kec Citamiang Sukabumi" } }, "aInquiries": [], "aInvolvements": [], "aOtherLiabilities": [], "aParameters": { "aConsent": "true", "aIDNumber": "1239743373", "aIDNumberType": "PefindoId", "aInquiryReason": "ProvidingFacilities", "aInquiryReasonText": [], "aReportDate": "2021-09-01T00:00:00", "aSections": { "bstring": [ "CIP", "SubjectInfo", "SubjectHistory", "ContractList" ] }, "aSubjectType": "Individual" }, "aReportInfo": { "bCreated": "2021-09-01T13:40:27.1792617+07:00", "bReferenceNumber": "203857840-1239743373", "bReportStatus": "ReportGenerated", "bRequestedBy": "Wahyu", "bVersion": "553" }, "aSecurities": [], "aSubjectInfoHistory": { "bAddressList": { "bAddress": [ { "bItem": "MainAddress", "bSubscriber": "B02", "bValidFrom": "2018-06-30T00:00:00", "bValidTo": "2019-11-30T00:00:00", "bValue": "Kp Panyandungan Rt 003 Rw 001, Cianjur, Kab. 43263, Girimukti, Campaka, ID" }, { "bItem": "MainAddressLine", "bSubscriber": "B02", "bValidFrom": "2018-06-30T00:00:00", "bValidTo": "2019-01-31T00:00:00", "bValue": "KP PANYANDUNGAN RT 003 RW 001, 43263, Campaka, Girimukti, ID" }, { "bItem": "MainAddressLine", "bSubscriber": "B02", "bValidFrom": "2019-01-31T00:00:00", "bValidTo": "2019-02-28T00:00:00", "bValue": "KP PANYANDUNGAN RT 003 RW 001, 0110, 43263, Campaka, Girimukti, ID" }, { "bItem": "MainAddressLine", "bSubscriber": "B02", "bValidFrom": "2019-02-28T00:00:00", "bValidTo": "2019-03-31T00:00:00", "bValue": "KP PANYANDUNGAN RT 003 RW 001, 43263, Campaka, Girimukti, ID" }, { "bItem": "MainAddressLine", "bSubscriber": "B02", "bValidFrom": "2019-03-31T00:00:00", "bValidTo": "2019-11-30T00:00:00", "bValue": "KP PANYANDUNGAN RT 003 RW 001, 0110, 43263, Campaka, Girimukti, ID" } ] }, "bContactList": { "bContact": { "bItem": "MobilePhone", "bSubscriber": "B02", "bValidFrom": "2018-06-30T00:00:00", "bValidTo": "2019-11-30T00:00:00", "bValue": "6285795044136" } }, "bGeneralList": { "bGeneral": [ { "bItem": "FullNameLocal", "bSubscriber": "B02", "bValidFrom": "2019-01-31T00:00:00", "bValidTo": "2019-02-28T00:00:00", "bValue": "Rendy Novian" }, { "bItem": "Banking.MaritalStatus", "bSubscriber": "B03", "bValidFrom": "2021-06-30T00:00:00", "bValidTo": "2021-07-31T00:00:00", "bValue": "Single" }, { "bItem": "Banking.MaritalStatus", "bSubscriber": "B02", "bValidFrom": "2019-01-31T00:00:00", "bValidTo": "2019-02-28T00:00:00", "bValue": "Single" }, { "bItem": "Banking.MaritalStatus", "bSubscriber": "B02", "bValidFrom": "2019-03-31T00:00:00", "bValidTo": "2019-11-30T00:00:00", "bValue": "Single" }, { "bItem": "Banking.SocialStatus", "bSubscriber": "B02", "bValidFrom": "2019-01-31T00:00:00", "bValidTo": "2019-02-28T00:00:00", "bValue": "Employed" }, { "bItem": "Banking.Residency", "bSubscriber": "B02", "bValidFrom": "2019-01-31T00:00:00", "bValidTo": "2019-02-28T00:00:00", "bValue": "Yes" } ] }, "bIdentificationsList": [] } } } } }';
        return $data;
    }

    public static function MockDataAdvanceScore(){
       $data = '{     "code": "SUCCESS",     "message": "OK",     "data": {         "score": 515,         "features": {             "GD_C_V3_5": 3.0,             "GD_C_V3_4": 10.775,             "GD_C_V3_19": 7.0,             "GD_C_V3_7": 4.0,             "GD_C_V3_6": 32.56,             "GD_C_V3_9": 100000.0,             "GD_C_V3_16": 5000.0,             "GD_C_V3_8": 1.0,             "GD_C_V3_15": 21.0,             "GD_C_V3_18": 5000.0,             "GD_C_V3_17": 34440.47619047619,             "GD_C_V3_12": 0.0,             "GD_C_V3_11": 22209.408197878663,             "GD_C_V3_14": 2.0,             "GD_C_V3_13": 0.0,             "GD_C_V3_10": 4.0,             "GD_C_V3_20": 0.002075630797171953,             "GD_C_V3_1": 1560.0,             "GD_C_V3_3": 34625.0,             "GD_C_V3_2": 13.0         }     },     "extra": null,     "transactionId": "33348ac8c12f417b",     "pricingStrategy": "PAY" }';
       return $data;
    }

    public static function MockCheckNpl(){
        $data = '{ "success": true, "data": [ { "loan_no": "1000007", "npl_id": 19339, "cust_id_no": "3217082012810012", "cust_name": "hilman setiawan", "bond_seller_id": "ADAPUNDI" } ], "message": "Customer retrieved successfully." }';
        return $data;
    }


}
