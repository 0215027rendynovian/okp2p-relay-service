<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvancescoreLog extends Model
{
    protected $table = "log_advancescore";
    
    protected $fillable = ['name','idNumber','phoneNumber','json', 'uid','mode_dev'];
}
