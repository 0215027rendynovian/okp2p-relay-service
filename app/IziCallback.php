<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IziCallback extends Model
{
    //
    protected $table = 'izi_callback';

    protected $fillable = ['json'];
}
