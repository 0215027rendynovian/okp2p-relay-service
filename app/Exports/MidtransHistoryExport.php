<?php

namespace App\Exports;

use App\Departement;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;

class MidtransHistoryExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function __construct(string $startDate, string $endDate, array $dept_id)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->dept_id = $dept_id;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event){
                $event->writer->getDelegate()->getSecurity()->setLockWindows(true);
                $event->writer->getDelegate()->getSecurity()->setLockStructure(true);
                $event->writer->getDelegate()->getSecurity()->setWorkbookPassword("admin123");
            }
        ];
    }

    public function view(): View
    {
        $fromDate = $this->startDate;
        $toDate = $this->endDate;
        return view('admin.export-history-midtrans', [
            'midtranshistory' => DB::table('users')
                            ->leftJoin('midtrans-bank-validator', function ($join) use ($fromDate, $toDate){
                                $join->on('users.id', '=', 'midtrans-bank-validator.uid')
                                    ->whereBetween('midtrans-bank-validator.updated_at', [$fromDate, $toDate]);
                            })
                            ->select('users.id','users.name','users.email','users.dept_id', DB::raw('sum(count_hit) as COUNT_HIT'))
                            ->whereIn('dept_id', $this->dept_id)
                            ->groupBy('users.id')
                            ->groupBy('users.dept_id')
                            ->get(),
            'departement' => Departement::whereIn('code', $this->dept_id)->get(),
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
        ]);
    }

}
