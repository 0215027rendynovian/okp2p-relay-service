<?php

namespace App\Exports;

use App\Departement;
use App\User;
use App\ViewHistoryHitIziData;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;



class IziDataRowExport implements FromView, ShouldAutoSize, WithEvents,WithColumnFormatting
{
    use Exportable;

    public function __construct(string $startDate, string $endDate, array $dept_id)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->dept_id = $dept_id;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event){
                $event->writer->getDelegate()->getSecurity()->setLockWindows(true);
                $event->writer->getDelegate()->getSecurity()->setLockStructure(true);
                $event->writer->getDelegate()->getSecurity()->setWorkbookPassword("admin123");
            }
        ];
    }

    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
        ];
    }


    public function view(): View
    {
        $fromDate = $this->startDate;
        $toDate = $this->endDate;
        return view('admin.export-row-data-izi', [
            'izihistory' => DB::table('users')
                            ->leftJoin('creadit_featureds', function ($join) {
                                $join->on('users.id', '=', 'creadit_featureds.uid');
                            })
                            ->leftJoin('departements', function ($join){
                                $join->on('users.dept_id', '=', 'departements.id');
                            })
                            ->select('users.id', 'users.name as user', 'users.email', 'departements.name as departement', 'creadit_featureds.nama as cust', 'creadit_featureds.ktp as ktp', 'creadit_featureds.phone as phone', 'creadit_featureds.count_hit as count_hit', 'creadit_featureds.json as json', 'creadit_featureds.updated_at as time_hit')
                            ->whereBetween('creadit_featureds.updated_at', [$fromDate, $toDate])
                            ->where('creadit_featureds.json', 'like', '%creditscore%')
                            ->whereIn('users.dept_id', $this->dept_id)
                            ->get(),
            'departement' => Departement::whereIn('code', $this->dept_id)->get(),
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
        ]);
    }
}
