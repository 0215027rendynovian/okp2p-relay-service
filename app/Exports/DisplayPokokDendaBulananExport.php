<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class DisplayPokokDendaBulananExport implements WithColumnFormatting, FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function __construct(string $startDate, string $endDate, string $status)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->status = $status;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event){
                $event->writer->getDelegate()->getSecurity()->setLockWindows(true);
                $event->writer->getDelegate()->getSecurity()->setLockStructure(true);
                //$event->writer->getDelegate()->getSecurity()->setWorkbookPassword("admin123");
            }
        ];
    }

    public function view(): View 
    {
        return view('admin.export-denda-bulanan' , [
            'datadenda' => DB::connection('sqlsrv2')->select("
            SELECT
                MAST.INSP_NO NoPinjaman,
                COMM.CUS_NM NamaPinjaman,
                HIST.STS_CD stsCd,
                dbo.GT_FT_GETCODENM(HIST.COM_CD, HIST.STS_CD, 'ID') Status,
                HIST.TRN_DT TanggalPersetujuan,
                HIST.RPY_TOT_AM Pokok,
                HIST.NML_INT_AM BungaTetap,
                HIST.DFR_INT_AM Denda
            FROM
                FN_LOAN_HIST HIST
                INNER JOIN FN_LOAN_MAST MAST ON HIST.ACT_NO = MAST.ACT_NO
                LEFT OUTER JOIN GT_CUST_COMM COMM ON MAST.CUS_NO = COMM.CUS_NO
                WHERE
                HIST.RCP_DT BETWEEN {$this->startDate}
                AND {$this->endDate}
                AND HIST.STS_CD='{$this->status}'
                ORDER BY MAST.INSP_NO
            ")
        ]);
    }

    public function columnFormats(): array{
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_TEXT
        ];
    }
    
}
