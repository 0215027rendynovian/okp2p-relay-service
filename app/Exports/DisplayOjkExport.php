<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\BeforeExport;

class DisplayOjkExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function __construct(string $startDate, string $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event){
                $event->writer->getDelegate()->getSecurity()->setLockWindows(true);
                $event->writer->getDelegate()->getSecurity()->setLockStructure(true);
                $event->writer->getDelegate()->getSecurity()->setWorkbookPassword("admin123");
            }
        ];
    }

    public function view(): View
    {
        return view('admin.export-ojk', [
            'dataojk' => DB::connection('sqlsrv')->select("
                    SELECT
                        M.ACT_NO as NOMOR_PINJAMAN,
                        M.CUS_NO as NOMOR_CUSTOMER,
                        (
                        SELECT
                            LN_PRD_NM
                        FROM
                            GT_LOAN_CODE WITH (NOLOCK)
                        WHERE
                            COM_CD = M.COM_CD
                            AND LN_PRD_CD = M.LN_PRD_CD
                        ) as NAMA_PRODUK,
                        P.CUS_NM as NAMA_CUSTOMER,
                        P.KTP as KTP,
                        DBO.FN_FORMAT_DATE(BIRTHDAY) AS TANGGAL_LAHIR,
                        dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') as JENIS_KELAMIN,
                        DBO.gt_ft_GetCodeNm(M.COM_CD, M.LN_STS_CD, 'ID') as STATUS_PENGAJUAN,
                        DBO.FN_FORMAT_DATE(M.LN_DT) as PERIODE_PINJAMAN,
                        DBO.FN_FORMAT_DATE(M.LN_DT) as BULAN_PINJAMAN,
                        DBO.FN_FORMAT_DATE(M.XPR_DT) as JATUH_TEMPO,
                        M.LN_AM as NOMINAL_PINJAMAN,
                        M.LN_AM - M.LN_BAL_AM as NOMINAL_PENGEMBALIAN,
                        M.LN_BAL_AM as SALDO_PINJAMAN,
                        CONFIRM_LN_AM - (
                        SELECT
                            SUM(INT_AMT + WON_AMT)
                        FROM
                            PT_LOAN_PLAN WITH (NOLOCK)
                        WHERE
                            INSP_NO = P.INSP_NO
                            AND SCH_DT = P.LN_DT
                        ) - CONVERT(
                        numeric(18, 0),
                        FLOOR(
                            CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                        ) / 100
                        ) as NOMINAL_PEMBAYARAN,
                        CONVERT(
                        numeric(18, 0),
                        FLOOR(
                            CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                        ) / 100
                        ) as BIAYA_ADMINISTRASI,
                        (
                        SELECT
                            SUM(INT_AMT + WON_AMT)
                        FROM
                            PT_LOAN_PLAN
                        WHERE
                            INSP_NO = M.INSP_NO
                            AND SCH_DT = M.LN_DT
                        ) as BUNGA_AWAL,
                        CONVERT(
                        numeric(18, 0),
                        FLOOR(
                            CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                        ) / 100
                        ) + (
                        SELECT
                            SUM(INT_AMT + WON_AMT)
                        FROM
                            PT_LOAN_PLAN
                        WHERE
                            INSP_NO = M.INSP_NO
                            AND SCH_DT = M.LN_DT
                        ) as ADM_PLUS_AWAL,
                        dbo.gt_ft_GetCodeNm(M.COM_CD, M.LN_DAY_CD, 'ID') as TENOR_PINJAMAN,
                        M.NML_IRT as SUKU_BUNGA_PINJAMAN,
                        M.DFR_IRT as SUKU_BUNGA_TUNGGAKAN,
                        DAY(
                        DBO.FN_FORMAT_DATE(P.XPR_DT)
                        ) as TANGGAL_JATUH_TEMPO_BUNGA,
                        DBO.FN_FORMAT_DATE(INT_END_DT) as TANGGAL_REPAYMENT_TERAKHIR,
                        DBO.FN_FORMAT_DATE(M.LN_DT) as TANGGAL_PERSETUJUAN,
                        ISNULL(M.DFR_DAY, 0) as JUMLAH_HARI_TUNGGAKAN_SAAT_INI,
                        CASE WHEN M.DFR_DAY IS NULL THEN 'Lancar' WHEN M.DFR_DAY = 0 THEN 'Lancar' WHEN M.DFR_DAY < 30 THEN 'Lancar' WHEN M.DFR_DAY BETWEEN 30
                        AND 90 THEN 'Tidak Lancar' ELSE 'Macet' END as KATEGORI_DPD,
                        (
                        SELECT
                            MAX(NAME)
                        FROM
                            GT_POST_1_PROVINSI WITH (NOLOCK)
                        WHERE
                            ID = PTLNC031_120001.PROVINSI
                        ) as PROVINCE,
                        CASE WHEN ISDATE(P.BIRTHDAY) = 1 THEN ISNULL(
                        DBO.FN_AGE_INSP(P.INSP_NO, BIRTHDAY, '2'),
                        0
                        ) ELSE 0 END as USIA,
                        CASE WHEN CONVERT(
                        VARCHAR,
                        ISDATE(P.BIRTHDAY)
                        ) = '1' THEN CASE WHEN CONVERT(
                        VARCHAR,
                        ISNULL(
                            DBO.FN_AGE_INSP(P.INSP_NO, BIRTHDAY, '2'),
                            0
                        )
                        ) BETWEEN '19'
                        AND '34' THEN '19-34' WHEN CONVERT(
                        VARCHAR,
                        ISNULL(
                            DBO.FN_AGE_INSP(P.INSP_NO, BIRTHDAY, '2'),
                            0
                        )
                        ) BETWEEN '35'
                        AND '54' THEN '35-54' ELSE '0' END ELSE '0' END AS KATEGORI_USIA,
                        dbo.gt_ft_GetCodeNm(P.COM_CD, P.BUSN_FLD, 'ID') as AREA_USAHA,
                        '' as KODE_USAHA,
                        'Book off November' as KETERANGAN_TAMBAHAN,
                        dbo.gt_ft_GetCodeNm(M.COM_CD, M.INSP_STA_CD, 'ID') as KLASIFIKASI_PINJAMAN,
                        DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID') as BULAN,
                        DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID') AS JUMLAH_HARI
                    FROM
                        PT_LNC001TM P WITH (NOLOCK)
                        LEFT OUTER JOIN PT_LNC031TM PTLNC031_120001 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120001.INSP_NO
                        AND PTLNC031_120001.AAB_CD = '120001' -- HOME ADDRESS
                        LEFT OUTER JOIN PT_LNC031TM PTLNC031_120003 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120003.INSP_NO
                        AND PTLNC031_120003.AAB_CD = '120003' -- WORKPLACE ADDRESS
                        LEFT OUTER JOIN PT_LNC032TM PTLNC032 WITH (NOLOCK) ON P.INSP_NO = PTLNC032.INSP_NO
                        AND PTLNC032.TED_CD = '130001'
                        LEFT OUTER JOIN FN_LOAN_MAST M WITH (NOLOCK) ON P.INSP_NO = M.INSP_NO
                    WHERE
                        P.COM_CD = '01'
                        AND M.LN_DT BETWEEN '$this->startDate'
                        AND '$this->endDate'
            ")
        ]);
    }

}
