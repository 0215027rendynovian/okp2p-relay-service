<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestVa extends Model
{
    //
    protected $table = "request_va";    
    protected $fillable = ['external_id','bank_code','name','is_closed','expected_amount', 'uid'];
}
