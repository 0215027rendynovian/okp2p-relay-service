<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogBniModel extends Model
{
    //
    protected $fillable = [
        'mode_dev',
        'token',
        'request',
        'response',
        'type',
        'url',
        'requestedUuid',
        'uid'
    ];

    protected $table = 'log_bni';
    
}
