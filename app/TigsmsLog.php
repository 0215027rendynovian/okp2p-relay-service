<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TigsmsLog extends Model
{
    protected $table = "log_tigsms";
    protected $fillable = ['content','content','numberphone','type','mode_dev',
    'vendor','json', 'uid'];
}
