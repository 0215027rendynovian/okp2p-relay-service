<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPrivilage extends Model
{
    protected $fillable = [
        'parent', 'child', 'status', 'description', 'role_id', 'code'
    ];
    protected $table = 'user_privilage';
}
