<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FdcFeatured extends Model
{
    protected $fillable = [
        'ktp', 'reason','count_hit', 'json', 'mode_dev'
    ];

    protected $table = 'fdc_featureds';
}
