<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PefindoCustomeReport extends Model
{
    //table for report counter pefindo

    protected $table = 'pefindo_customreport';

    protected $fillable = [ 
    'ktp',
    'fullname',
    'date_of_birth',
    'inquiry_reason',
    'pefindo_id',
    'report_date',
    'uid',
    'json',
    'status',
    'mode_dev'
    ];


}

