<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponseVa extends Model
{
    //
    protected $fillable = ['status',
    'currency',
    'owner_id',
    'external_id',
    'bank_code',
    'is_closed',
    'name',
    'account_number',
    'is_single_use',
    'expiration_date',
    'id_va',
    'uid'];

    protected $table = "response_va";
}
