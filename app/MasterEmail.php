<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterEmail extends Model
{
    protected $fillable = [
        'email',
        'name',
        'status',
        'keterangan'
    ];

    protected $table = 'master_emails';
}
