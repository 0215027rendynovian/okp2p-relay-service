<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanNihil extends Model
{
    protected $fillable = [
        'ref_id_backoffice',
        'ref_id_pengguna',
        'nama_pt',
        'alamat_pt',
        'nomor',
        'lampiran',
        'nomor_polisi',
        'tanggal_polisi',
        'nomor_dttot',
        'hari_nihil',
        'tanggal_nihil',
        'nama_pjk'
    ];

    protected $table = 't_laporan_nihil';
}
