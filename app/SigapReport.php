<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SigapReport extends Model
{
    //

    protected $table = 'dt_sigap_report';

    protected $fillable = [
        'uid','ref_idsigap', 'ref_idbackoffice','inquiry_date','reason','status_report','name', 'nik', 'expected', 'code_densus', 'birth', 'birth_date', 'citizen', 'address','execute_date'
    ];
}
