<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOrderToMitra extends Model
{
    //
    protected $table = "customer_order_to_mitra";
    
    protected $fillable = [
        'id_customer', 'id_mitra', 'address','created','duration','status_order','id_service','lat_customers','lng_customers','status_mulai_layanan','nomor_hp'
    ];
    public function User()
    {
         return $this->belongsTo('App\User', 'uid_customer', 'id');
    }
    
    public function ServiceMitra()
    {
         return $this->belongsTo('App\ServiceMitra', 'id', 'id_service');
    }
}
