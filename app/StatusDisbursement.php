<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusDisbursement extends Model
{
    
    protected $table = "status_disbursement";

    protected $fillable = ['status','uid','description'];
}
