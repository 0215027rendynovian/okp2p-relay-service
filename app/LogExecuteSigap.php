<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogExecuteSigap extends Model
{
    protected $table = 'log_execute_sigaps';

    protected $fillable = [
        'execute_date',
        'name',
        'nik',
        'expected',
        'code_densus',
        'birth',
        'birth_date',
        'citizen',
        'address',
        'ref_idsigap',
        'ref_idbackoffice',
    ];
}
