<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\UserPrivilage;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','permission','status_active','is_re_password','is_active', 'dept_id', 'es_system'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function CreaditFeatured()
    {
       return $this->hasMany('App\CreaditFeatured', 'uid', 'id');
    }

    public function Roles(){
        return $this->belongsTo('App\Roles','permission', 'id');
    }


    public function Privilage()
    {
        return $this->hasMany(UserPrivilage::class,'role_id');
    }

    public function Access()
    {
        $UserPrivilage = UserPrivilage::where('role_id', '=', Auth::user()->Roles->id)->get();
        return  $UserPrivilage;
    }

    public function Midtrans()
    {
       return $this->hasMany('App\MidtransBankValidator', 'uid', 'id');
    }

    public function countMidtrans()
    {
        return $this->hasMany('App\MidtransBankValidator','id')->selectRaw('midtrans-bank-validator.*,sum(count_hit) as sum')->groupBy('uid');
    }





}
