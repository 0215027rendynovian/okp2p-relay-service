<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreaditFeatured extends Model
{
    protected $fillable = [
        'nama', 'ktp', 'phone','count_hit', 'json', 'uid','mode_dev'
    ];

    protected $table = 'creadit_featureds';
}
