<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{

    protected $fillable = [
        'code','name', 'status', 'description'
    ];

    protected $table = 'roles';


    public function Privilage(){
        return $this->hasMany('App\UserPrivilage', 'role_id', 'id');
    }

}
