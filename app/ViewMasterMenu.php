<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewMasterMenu extends Model
{
    public $table = 'view_master_menu';
}
