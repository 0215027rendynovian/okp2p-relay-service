<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetRekeningTujuan extends Model
{
    //

    protected $table = "rekening_tujuan";
    protected $fillable = [
        'id_rekening',
        'bank_code',
        'uid'
    ];

    public function Rekening()
    {
         return $this->belongsTo('App\Rekening', 'id_rekening', 'id');
    }

}
