<?php

namespace App\Http\Middleware;

use Closure;

class SentryErrorReport
{
    /**
     * Let Sentry logs the exception
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            return $next($request);
        }
        catch (\Throwable $exception){
            \Sentry\captureException($exception);
        }
    }
}
