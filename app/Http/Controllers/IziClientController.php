<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\creditFeaturedDetail;
use App\Imports\IziImport;
use App\CreaditFeatured;
use App\Departement;
use App\Exports\IziDataHistoryExport;
use App\Exports\IziDataRowExport;
use App\Http\Requests;
use App\Helpers\Izi;
use App\FdcFeatured;
use App\IziClient;
use App\User;
use App\uploadIzi;
use App\ViewChildMenu;
use App\ViewHistoryHitIziData;
use App\ViewMasterMenu;
use Illuminate\Support\Facades\DB;
use Session;


class IziClientController extends Controller
{

    public function postArray(Request $request){

        $data = Izi::get_arr_izi($request->data);

        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }


    public function import_excel(Request $request){

        // validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

		// cokot file excel
		$file = $request->file('file');

		// jieun ngaran file
		$name_file = rand().$file->getClientOriginalName();

		// upload ke folder file izi di dalam folder public
		$file->move('file_izi',$name_file);

		// import data
		Excel::import(new IziImport, public_path('/file_izi/'.$name_file));

		// notifikasi  session
		Session::flash('sukses','Data Berhasil Diimport!');

		// alihkan halaman kembali
		return redirect('/creditfeature');

    }



    public function creditFeatured(Request $request){


        $phone_number = $request->phone_number;
        $nama_lengkap = $request->nama_lengkap;
        $KTP = $request->ktp;
        $uid = Auth::user()->id;


        //Development
        //$client = new IziClient("bSNAQPBDZIcoroGpIzva", "DuoRvurhaOCuUBAevMDPeGZVjCFKyprysStzmslz");

        //Production
        $client = new IziClient("dOFMsNrXQrpSHjgUbmXy", "hqzGdBwMjpDHggqhYrfmFeDOwCVNMmOTNqBtdFug");
        $url = "https://api.izi.credit/v2/creditfeature";
        // $data = array(
        //     "phone"=> $phone_number,
        //     "name"=>$nama_lengkap,
        //     "id"=> $KTP,
        //     "callback" => "http://182.253.0.228:8000/api/IziCallBack"
        // );
        $data = array(
            "phone"=> $phone_number,
            "name"=>$nama_lengkap,
            "id"=> $KTP
        );
        $response = $client->request($url, $data);

        CreaditFeatured::create([
            'phone' =>$phone_number,
            'nama' => $nama_lengkap,
            'ktp' => $KTP,
            'json' => $response,
            'count_hit' => 1,
            "uid" => Auth::user()->id
        ]);

        // $CreaditFeatured = CreaditFeatured::where('ktp', $KTP)
        //                                     ->where('uid', $uid)
        //                                     ->first();


        // if(empty($CreaditFeatured)){
        //     CreaditFeatured::create([
        //         'phone' =>$phone_number,
        //         'nama' => $nama_lengkap,
        //         'ktp' => $KTP,
        //         'json' => $response,
        //         'count_hit' => 1,
        //         "uid" => Auth::user()->id
        //     ]);
        // }else{
        //     $index = (1 + $CreaditFeatured->count_hit);
        //     $CreaditFeatured->update([
        //         'phone' => $phone_number,
        //         'nama' => $nama_lengkap,
        //         'ktp' => $KTP,
        //         'json' => $response,
        //         'count_hit' => $index,
        //         "uid" => Auth::user()->id
        //     ]);
        // };


        return $response;
    }


    public function FDCDataPost(Request $request){

        $ktp = $request->ktp;
        $reason = $request->reason;

        // $url = "https://pusdafil.afpi.or.id/api/v3.5/Inquiry/?id=".$ktp."&reason=".$reason."&reffid=123";

        $url = "https://srigala.afpi.or.id/api/v3.5/Inquiry/?id=".$ktp."&reason=".$reason."&reffid=123";

        // ubah string JSON menjadi array
        // $url = json_decode($url, TRUE);

        $username = 'riski.firdaus@okp2p.co.id';

        $password = 'X6P4sxBaYCk9k4gbz3ZGd8';

        // persiapkan curl
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        // set user agent
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HEADER, false);

        // $output contains the output string
        $output = curl_exec($ch);

        // $json = json_encode($output);

        // tutup curl
        curl_close($ch);

        // insert data
        FdcFeatured::create([
            'ktp' => $ktp,
            'reason' => $reason,
            'count_hit' => 1,
            'json' => $output
        ]);
        // $fdcFeatured = FdcFeatured::where('ktp', $ktp)->first();

        // if (empty($fdcFeatured)) {
        //     FdcFeatured::create([
        //         'ktp' => $ktp,
        //         'reason' => $reason,
        //         'count_hit' => 1,
        //         'json' => $output
        //     ]);
        // } else {
        //     $count = (1 + $fdcFeatured->count_hit);
        //     $fdcFeatured->update([
        //         'ktp' => $ktp,
        //         'reason' => $reason,
        //         'count_hit' => $count,
        //         'json' => $output
        //     ]);
        // }

        // mengembalikan hasil curl
        return $output;

    }





    public function creditFeaturedLog(Request $request){

        $KTP = $request->ktp;

        $CreaditFeatured = CreaditFeatured::where('ktp', $KTP)->orderBy('updated_at', 'DESC')->first();

        if(empty($CreaditFeatured)){
            $request = '0000';
        }else{
            $request = $CreaditFeatured->json;
        }


        return response()->json([
            $request
        ]);
    }

    public function FDCDataPostLog(Request $request){

        $KTP = $request->ktp;

        // $fdcFeatured = FdcFeatured::where('ktp', $KTP)->first();
        $fdcFeatured = FdcFeatured::where('ktp', $KTP)->orderBy('updated_at', 'DESC')->first();

        if(empty($fdcFeatured)){
            $request = '0000';
            return response()->json(
                [$request]
            );
        }else{
            return $fdcFeatured->json;
        }
    }

    public function FDCDataExcel(Request $request){

        $KTP = $request->ktp;

        // $fdcFeatured = FdcFeatured::where('ktp', $KTP)->first();
        $fdcFeatured = FdcFeatured::where('ktp', $KTP)->orderBy('updated_at', 'DESC')->first();

        if(empty($fdcFeatured)){
            $request = '0000';
            return response()->json(
                [$request]
            );
        }else{
            return $fdcFeatured->json;
        }
    }

    public function ParseJson(Request $request){

        $KTP = '3203032411930003';

        $CreaditFeatured = CreaditFeatured::where('ktp', $KTP)->first();

        if(empty($CreaditFeatured)){
            $request = '0000';
        }else{
            $request = $CreaditFeatured->json;

             $data = json_decode($request, true);
             creditFeaturedDetail::create([
                'status' => $data['status'],
                'creditscore' => $data['message']['creditscore'],
                'A_II_14d' => $data['message']['featurelist']['A_II_14d'],
                'A_II_180d' => $data['message']['featurelist']['A_II_180d'],
                'A_II_21d' => $data['message']['featurelist']['A_II_21d'],
                'A_II_30d' => $data['message']['featurelist']['A_II_30d'],
                'A_II_360d' => $data['message']['featurelist']['A_II_360d'],
                'A_II_3d' => $data['message']['featurelist']['A_II_3d'],
                'A_II_60d' => $data['message']['featurelist']['A_II_60d'],
                'A_II_7d' => $data['message']['featurelist']['A_II_7d'],
                'A_II_90d' => $data['message']['featurelist']['A_II_90d'],
                'A_PI_14d' => $data['message']['featurelist']['A_PI_14d'],
                'A_PI_180d' => $data['message']['featurelist']['A_PI_180d'],
                'A_PI_21d' => $data['message']['featurelist']['A_PI_21d'],
                'A_PI_30d' => $data['message']['featurelist']['A_PI_30d'],
                'A_PI_360d' => $data['message']['featurelist']['A_PI_360d'],
                'A_PI_3d' => $data['message']['featurelist']['A_PI_3d'],
                'A_PI_60d' => $data['message']['featurelist']['A_PI_60d'],
                'A_PI_7d' => $data['message']['featurelist']['A_PI_7d'],
                'A_PI_90d' => $data['message']['featurelist']['A_PI_90d'],
                'B_II_14d' => $data['message']['featurelist']['B_II_14d'],
                'B_II_180d' => $data['message']['featurelist']['B_II_180d'],
                'B_II_21d' => $data['message']['featurelist']['B_II_21d'],
                'B_II_30d' => $data['message']['featurelist']['B_II_30d'],
                'B_II_360d' => $data['message']['featurelist']['B_II_360d'],
                'B_II_3d' => $data['message']['featurelist']['B_II_3d'],
                'B_II_60d' => $data['message']['featurelist']['B_II_60d'],
                'B_II_7d' => $data['message']['featurelist']['B_II_7d'],
                'B_II_90d' => $data['message']['featurelist']['B_II_90d'],
                'B_PI_14d' => $data['message']['featurelist']['B_PI_14d'],
                'B_PI_180d' => $data['message']['featurelist']['B_PI_180d'],
                'B_PI_21d' => $data['message']['featurelist']['B_PI_21d'],
                'B_PI_30d' => $data['message']['featurelist']['B_PI_30d'],
                'B_PI_360d' => $data['message']['featurelist']['B_PI_360d'],
                'B_PI_3d' => $data['message']['featurelist']['B_PI_3d'],
                'B_PI_60d' => $data['message']['featurelist']['B_PI_60d'],
                'B_PI_7d' => $data['message']['featurelist']['B_PI_7d'],
                'B_PI_90d' => $data['message']['featurelist']['B_PI_90d'],
                'C_II_14d' => $data['message']['featurelist']['C_II_14d'],
                'C_II_180d' => $data['message']['featurelist']['C_II_180d'],
                'C_II_21d' => $data['message']['featurelist']['C_II_21d'],
                'C_II_30d' => $data['message']['featurelist']['C_II_30d'],
                'C_II_360d' => $data['message']['featurelist']['C_II_360d'],
                'C_II_3d' => $data['message']['featurelist']['C_II_3d'],
                'C_II_60d' => $data['message']['featurelist']['C_II_60d'],
                'C_II_7d' => $data['message']['featurelist']['C_II_7d'],
                'C_II_90d' => $data['message']['featurelist']['C_II_90d'],
                'C_PI_14d' => $data['message']['featurelist']['C_PI_14d'],
                'C_PI_180d' => $data['message']['featurelist']['C_PI_180d'],
                'C_PI_21d' => $data['message']['featurelist']['C_PI_21d'],
                'C_PI_30d' => $data['message']['featurelist']['C_PI_30d'],
                'C_PI_360d' => $data['message']['featurelist']['C_PI_360d'],
                'C_PI_3d' => $data['message']['featurelist']['C_PI_3d'],
                'C_PI_60d' => $data['message']['featurelist']['C_PI_60d'],
                'C_PI_7d' =>$data['message']['featurelist']['C_PI_7d'],
                'C_PI_90d' => $data['message']['featurelist']['C_PI_90d'],
                'II_v4_feature_status' => $data['message']['featurelist']['II_v4_feature_status'],
                'PI_v4_feature_status' => $data['message']['featurelist']['PI_v4_feature_status'],
                'b_activeStatus' => $data['message']['featurelist']['b_activeStatus'],
                'b_activeYear' => $data['message']['featurelist']['b_activeYear'],
                'b_class' => $data['message']['featurelist']['b_class'],
                'b_isopen' => $data['message']['featurelist']['b_isopen'],
                'b_members' => $data['message']['featurelist']['b_members'],
                'b_membership' => $data['message']['featurelist']['b_membership'],
                'ece' => $data['message']['featurelist']['ece'],
                'eco' => $data['message']['featurelist']['eco'],
                'ecp' => $data['message']['featurelist']['ecp'],
                'ect' => $data['message']['featurelist']['ect'],
                'identity_address' => $data['message']['featurelist']['identity_address'],
                'identity_city' => $data['message']['featurelist']['identity_city'],
                'identity_date_of_birth' => $data['message']['featurelist']['identity_date_of_birth'],
                'identity_district' => $data['message']['featurelist']['identity_district'],
                'identity_gender' => $data['message']['featurelist']['identity_gender'],
                'identity_marital_status' => $data['message']['featurelist']['identity_marital_status'],
                'identity_match' => $data['message']['featurelist']['identity_match'],
                'identity_name' => $data['message']['featurelist']['identity_name'],
                'identity_nationnality' => $data['message']['featurelist']['identity_nationnality'],
                'identity_place_of_birth' => $data['message']['featurelist']['identity_place_of_birth'],
                'identity_province' => $data['message']['featurelist']['identity_province'],
                'identity_religion' => $data['message']['featurelist']['identity_religion'],
                'identity_rt' => $data['message']['featurelist']['identity_rt'],
                'identity_rw' => $data['message']['featurelist']['identity_rw'],
                'identity_status' => $data['message']['featurelist']['identity_status'],
                'identity_village' => $data['message']['featurelist']['identity_village'],
                'identity_work' => $data['message']['featurelist']['identity_work'],
                'idinquiries_14d' => $data['message']['featurelist']['idinquiries_14d'],
                'idinquiries_180d' => $data['message']['featurelist']['idinquiries_180d'],
                'idinquiries_21d' => $data['message']['featurelist']['idinquiries_21d'],
                'idinquiries_30d' => $data['message']['featurelist']['idinquiries_30d'],
                'idinquiries_360d' => $data['message']['featurelist']['idinquiries_360d'],
                'idinquiries_3d' => $data['message']['featurelist']['idinquiries_3d'],
                'idinquiries_60d' =>$data['message']['featurelist']['idinquiries_60d'],
                'idinquiries_7d' => $data['message']['featurelist']['idinquiries_7d'],
                'idinquiries_90d' => $data['message']['featurelist']['idinquiries_90d'],
                'idinquiries_status' => $data['message']['featurelist']['idinquiries_status'],
                'idinquiries_total' => $data['message']['featurelist']['idinquiries_total'],
                'idinquiriesuname_14d' => $data['message']['featurelist']['idinquiriesuname_14d'],
                'idinquiriesuname_180d' => $data['message']['featurelist']['idinquiriesuname_180d'],
                'idinquiriesuname_21d' => $data['message']['featurelist']['idinquiriesuname_21d'],
                'idinquiriesuname_30d' => $data['message']['featurelist']['idinquiriesuname_30d'],
                'idinquiriesuname_360d' => $data['message']['featurelist']['idinquiriesuname_360d'],
                'idinquiriesuname_3d' => $data['message']['featurelist']['idinquiriesuname_3d'],
                'idinquiriesuname_60d' => $data['message']['featurelist']['idinquiriesuname_60d'],
                'idinquiriesuname_7d' => $data['message']['featurelist']['idinquiriesuname_7d'],
                'idinquiriesuname_90d' => $data['message']['featurelist']['idinquiriesuname_90d'],
                'idinquiriesuname_status' => $data['message']['featurelist']['idinquiriesuname_status'],
                'idinquiriesuname_total' => $data['message']['featurelist']['idinquiriesuname_total'],
                'isfacebook' => $data['message']['featurelist']['isfacebook'],
                'iswhatsapp' => $data['message']['featurelist']['iswhatsapp'],
                'multiidinquiries_14d' => $data['message']['featurelist']['multiidinquiries_14d'],
                'multiidinquiries_21d' => $data['message']['featurelist']['multiidinquiries_21d'],
                'multiidinquiries_30d' => $data['message']['featurelist']['multiidinquiries_30d'],
                'multiidinquiries_60d' => $data['message']['featurelist']['multiidinquiries_60d'],
                'multiidinquiries_7d' => $data['message']['featurelist']['multiidinquiries_7d'],
                'multiidinquiries_90d' => $data['message']['featurelist']['multiidinquiries_90d'],
                'multiidinquiries_total' => $data['message']['featurelist']['multiidinquiries_total'],
                'multiphone_idinfo' => $data['message']['featurelist']['multiphone_idinfo'],
                'multiphone_phoneinfo_id' => $data['message']['featurelist']['multiphone_phoneinfo_id'],
                'multiphone_phoneinfo_id_phone' => $data['message']['featurelist']['multiphone_phoneinfo_id_phone'],
                'multiphone_status' => $data['message']['featurelist']['multiphone_status'],
                'multiphoneinquiries_14d' => $data['message']['featurelist']['multiphoneinquiries_14d'],
                'multiphoneinquiries_21d' => $data['message']['featurelist']['multiphoneinquiries_21d'],
                'multiphoneinquiries_30d' => $data['message']['featurelist']['multiphoneinquiries_30d'],
                'multiphoneinquiries_60d' => $data['message']['featurelist']['multiphoneinquiries_60d'],
                'multiphoneinquiries_7d' => $data['message']['featurelist']['multiphoneinquiries_7d'],
                'multiphoneinquiries_90d' =>$data['message']['featurelist']['multiphoneinquiries_90d'],
                'multiphoneinquiries_total' => $data['message']['featurelist']['multiphoneinquiries_total'],
                'phoneage' => $data['message']['featurelist']['phoneage'],
                'phoneage_status' => $data['message']['featurelist']['phoneage_status'],
                'phonealive_id_num' => $data['message']['featurelist']['phonealive_id_num'],
                'phonealive_phone_num' => $data['message']['featurelist']['phonealive_phone_num'],
                'phonealive_status' => $data['message']['featurelist']['phonealive_status'],
                'phoneinquiries_14d' => $data['message']['featurelist']['phoneinquiries_14d'],
                'phoneinquiries_180d' => $data['message']['featurelist']['phoneinquiries_180d'],
                'phoneinquiries_21d' => $data['message']['featurelist']['phoneinquiries_21d'],
                'phoneinquiries_30d' => $data['message']['featurelist']['phoneinquiries_30d'],
                'phoneinquiries_360d' => $data['message']['featurelist']['phoneinquiries_360d'],
                'phoneinquiries_3d' => $data['message']['featurelist']['phoneinquiries_3d'],
                'phoneinquiries_60d' => $data['message']['featurelist']['phoneinquiries_60d'],
                'phoneinquiries_7d' => $data['message']['featurelist']['phoneinquiries_7d'],
                'phoneinquiries_90d' => $data['message']['featurelist']['phoneinquiries_90d'],
                'phoneinquiries_status' => $data['message']['featurelist']['phoneinquiries_status'],
                'phoneinquiries_total' => $data['message']['featurelist']['phoneinquiries_total'],
                'phoneinquiriesuname_14d' => $data['message']['featurelist']['phoneinquiriesuname_14d'],
                'phoneinquiriesuname_180d' => $data['message']['featurelist']['phoneinquiriesuname_180d'],
                'phoneinquiriesuname_21d' => $data['message']['featurelist']['phoneinquiriesuname_21d'],
                'phoneinquiriesuname_30d' => $data['message']['featurelist']['phoneinquiriesuname_30d'],
                'phoneinquiriesuname_360d' => $data['message']['featurelist']['phoneinquiriesuname_360d'],
                'phoneinquiriesuname_3d' => $data['message']['featurelist']['phoneinquiriesuname_3d'],
                'phoneinquiriesuname_60d' => $data['message']['featurelist']['phoneinquiriesuname_60d'],
                'phoneinquiriesuname_7d'  => $data['message']['featurelist']['phoneinquiriesuname_7d'],
                'phoneinquiriesuname_90d' => $data['message']['featurelist']['phoneinquiriesuname_90d'],
                'phoneinquiriesuname_status' => $data['message']['featurelist']['phoneinquiriesuname_status'],
                'phoneinquiriesuname_total' => $data['message']['featurelist']['phoneinquiriesuname_total'],
                'phonescore' => $data['message']['featurelist']['phonescore'],
                'phonescore_status' => $data['message']['featurelist']['phonescore_status'],
                'phoneverify' => $data['message']['featurelist']['phoneverify'],
                'preference_bank_180d' => $data['message']['featurelist']['preference_bank_180d'],
                'preference_bank_270d' => $data['message']['featurelist']['preference_bank_270d'],
                'preference_bank_60d' => $data['message']['featurelist']['preference_bank_60d'],
                'preference_bank_90d' => $data['message']['featurelist']['preference_bank_90d'],
                'preference_ecommerce_180d' => $data['message']['featurelist']['preference_ecommerce_180d'],
                'preference_ecommerce_270d' => $data['message']['featurelist']['preference_ecommerce_270d'],
                'preference_ecommerce_60d' => $data['message']['featurelist']['preference_ecommerce_60d'],
                'preference_ecommerce_90d' => $data['message']['featurelist']['preference_ecommerce_90d'],
                'preference_game_180d' => $data['message']['featurelist']['preference_game_180d'],
                'preference_game_270d' => $data['message']['featurelist']['preference_game_270d'],
                'preference_game_60d' => $data['message']['featurelist']['preference_game_60d'],
                'preference_game_90d' => $data['message']['featurelist']['preference_game_90d'],
                'preference_lifestyle_180d' => $data['message']['featurelist']['preference_lifestyle_180d'],
                'preference_lifestyle_270d' => $data['message']['featurelist']['preference_lifestyle_270d'],
                'preference_lifestyle_60d' => $data['message']['featurelist']['preference_lifestyle_60d'],
                'preference_lifestyle_90d' => $data['message']['featurelist']['preference_lifestyle_90d'],
                'preference_status' => $data['message']['featurelist']['preference_status'],
                'topup_0_180_avg' => $data['message']['featurelist']['topup_0_180_avg'],
                'topup_0_180_max' => $data['message']['featurelist']['topup_0_180_max'],
                'topup_0_180_min' => $data['message']['featurelist']['topup_0_180_min'],
                'topup_0_180_times' => $data['message']['featurelist']['topup_0_180_times'],
                'topup_0_30_avg' => $data['message']['featurelist']['topup_0_30_avg'],
                'topup_0_30_max' => $data['message']['featurelist']['topup_0_30_max'],
                'topup_0_30_min' => $data['message']['featurelist']['topup_0_30_min'],
                'topup_0_30_times' => $data['message']['featurelist']['topup_0_30_times'],
                'topup_0_360_avg' => $data['message']['featurelist']['topup_0_360_avg'],
                'topup_0_360_max' => $data['message']['featurelist']['topup_0_360_max'],
                'topup_0_360_min' =>  $data['message']['featurelist']['topup_0_360_min'],
                'topup_0_360_times' => $data['message']['featurelist']['topup_0_360_times'],
                'topup_0_60_avg' => $data['message']['featurelist']['topup_0_60_avg'],
                'topup_0_60_max' => $data['message']['featurelist']['topup_0_60_max'],
                'topup_0_60_min' => $data['message']['featurelist']['topup_0_60_min'],
                'topup_0_60_times' => $data['message']['featurelist']['topup_0_60_times'],
                'topup_0_90_avg' => $data['message']['featurelist']['topup_0_90_avg'],
                'topup_0_90_max' => $data['message']['featurelist']['topup_0_90_max'],
                'topup_0_90_min' => $data['message']['featurelist']['topup_0_90_min'],
                'topup_0_90_times' => $data['message']['featurelist']['topup_0_90_times'],
                'topup_180_360_avg' => $data['message']['featurelist']['topup_180_360_avg'],
                'topup_180_360_max' => $data['message']['featurelist']['topup_180_360_max'],
                'topup_180_360_min' => $data['message']['featurelist']['topup_180_360_min'],
                'topup_180_360_times' => $data['message']['featurelist']['topup_180_360_times'],
                'topup_30_60_avg' => $data['message']['featurelist']['topup_30_60_avg'],
                'topup_30_60_max' => $data['message']['featurelist']['topup_30_60_max'],
                'topup_30_60_min' => $data['message']['featurelist']['topup_30_60_min'],
                'topup_30_60_times' => $data['message']['featurelist']['topup_30_60_times'],
                'topup_360_720_avg' => $data['message']['featurelist']['topup_360_720_avg'],
                'topup_360_720_max' => $data['message']['featurelist']['topup_360_720_max'],
                'topup_360_720_min' => $data['message']['featurelist']['topup_360_720_min'],
                'topup_360_720_times' => $data['message']['featurelist']['topup_360_720_times'],
                'topup_60_90_avg' => $data['message']['featurelist']['topup_60_90_avg'],
                'topup_60_90_max' => $data['message']['featurelist']['topup_60_90_max'],
                'topup_60_90_min' => $data['message']['featurelist']['topup_60_90_min'],
                'topup_60_90_times' => $data['message']['featurelist']['topup_60_90_times'],
                'topup_90_180_avg' => $data['message']['featurelist']['topup_90_180_avg'],
                'topup_90_180_max' => $data['message']['featurelist']['topup_90_180_max'],
                'topup_90_180_min' => $data['message']['featurelist']['topup_90_180_min'],
                'topup_90_180_times' => $data['message']['featurelist']['topup_90_180_times'],
                'topup_status' => $data['message']['featurelist']['topup_status'],
                'whatsapp_avatar' => $data['message']['featurelist']['whatsapp_avatar'],
                'whatsapp_company_account' => $data['message']['featurelist']['whatsapp_company_account'],
                'whatsapp_signature' => $data['message']['featurelist']['whatsapp_signature'],
                'whatsapp_updatestatus_time' => $data['message']['featurelist']['whatsapp_updatestatus_time']
             ]);
        }

    }

    public function showHistory(Request $request){
        $listDept = Departement::select('id', 'name')->pluck('name','id');

        return view('admin.izi-history', ['activeSidebar' => 'iziData', 'listDept' => $listDept]);
    }

    public function getHistoryHit(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get dept_id
        $temp_dept_id = $request->get('dept_id');
        $dept_id = $temp_dept_id == "" ? [1,2,3] : [$temp_dept_id];

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = User::select('count(*) as allcount')->whereIn('dept_id', $dept_id)->count();
        $totalRecordswithFilter = User::select('count(*) as allcount')->where('name', 'like', '%' .$searchValue . '%')->whereIn('dept_id', $dept_id)->count();

        // Fetch records
        $records = DB::table('users')
                        ->leftJoin('creadit_featureds', function ($join) use ($fromDate, $toDate){
                            $join->on('users.id', '=', 'creadit_featureds.uid')
                                ->whereBetween('creadit_featureds.updated_at', [$fromDate, $toDate]);
                        })
                        ->select('users.id','users.name','users.email','users.dept_id', DB::raw('sum(count_hit) as COUNT_HIT'))
                        ->where('users.name', 'like', '%' .$searchValue . '%')
                        ->whereIn('users.dept_id', $dept_id)
                        ->orderBy($columnName,$columnSortOrder)
                        ->groupBy('users.id')
                        ->groupBy('users.dept_id')
                        ->skip($start)
                        ->take($rowperpage)
                        ->get();

        $data_arr = array();

        foreach($records as $record){
            $id = $record->id;
            $name = $record->name;
            $COUNT_HIT = $record->COUNT_HIT;

            $data_arr[] = array(
                "id" => $id,
                "name" => $name,
                "COUNT_HIT" => $COUNT_HIT ?? 0,
                "log" => '<a class="btn back-ops-okp2p"  data-toggle="modal" data-target="#myModal2" onclick="onRowClicked('.$id.')"><li class="fas fa-sort" style="color:white; font-size:11px;">  Detil</li></a>'
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr,
        );

        echo json_encode($response);
        exit;
    }

    public function getCountHistory(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get dept_id
        $temp_dept_id = $request->get('dept_id');
        $dept_id = $temp_dept_id == "" ? [1,2,3] : [$temp_dept_id];

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        $countHit = DB::table('creadit_featureds')
                        ->leftJoin('users', function ($join) use ($dept_id){
                            $join->on('creadit_featureds.uid', '=', 'users.id');
                        })
                        ->whereBetween('creadit_featureds.updated_at', [$fromDate, $toDate])
                        ->whereIn('users.dept_id', $dept_id)
                        ->sum('count_hit');

        $countHitOK = DB::table('creadit_featureds')
                        ->leftJoin('users', function ($join) use ($dept_id){
                            $join->on('creadit_featureds.uid', '=', 'users.id');
                        })
                        ->whereBetween('creadit_featureds.updated_at', [$fromDate, $toDate])
                        ->whereIn('users.dept_id', $dept_id)
                        ->where('creadit_featureds.json', 'like', '%creditscore%')
                        ->sum('count_hit');

        $countHitInvalid = DB::table('creadit_featureds')
                        ->leftJoin('users', function ($join) use ($dept_id){
                            $join->on('creadit_featureds.uid', '=', 'users.id');
                        })
                        ->whereBetween('creadit_featureds.updated_at', [$fromDate, $toDate])
                        ->whereIn('users.dept_id', $dept_id)
                        ->where('creadit_featureds.json', 'not like', '%creditscore%')
                        ->sum('count_hit');

        // Total records
        $totalRecords = Departement::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Departement::select('count(*) as allcount')->count();

        // Fetch records
        $records = DB::table('users')
            ->leftJoin('creadit_featureds', function ($join) use ($fromDate, $toDate){
                $join->on('users.id', '=', 'creadit_featureds.uid')
                    ->whereBetween('creadit_featureds.updated_at', [$fromDate, $toDate]);
            })
            ->leftJoin('departements', function ($join){
                $join->on('users.dept_id', '=', 'departements.id');
            })
            ->select('users.dept_id', 'departements.name', 'departements.description', DB::raw('sum(count_hit) as sum'), DB::raw('sum(case when json like "%creditscore%" then count_hit END) as ok'), DB::raw('sum(case when json not like "%creditscore%" then count_hit END) as invalid'))
            ->whereIn('dept_id', $dept_id)
            ->groupBy('dept_id')
            ->get();

        $data_arr = array();

        foreach($records as $record){
            $dept_id = $record->dept_id;
            $name = $record->name;
            $description = $record->description;
            $sum = $record->sum;
            $ok = $record->ok;
            $invalid = $record->invalid;

            $data_arr[] = array(
                "dept_id" => '#',
                "name" => '<a style="font-size: 17px">
                                '.$name.'
                            </a>
                            <br/>
                            <small style="font-size: 11px">
                                '.$description.'
                            </small>',
                "description" => '<div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="'.number_format(($sum!=0)?($sum/$countHit)*100:0,2).'" aria-volumemin="0" aria-volumemax="100" style="width: '.number_format(($sum!=0)?($sum/$countHit)*100:0,2).'%">
                                    </div>
                                </div>
                                <small style="font-size: 11px">
                                    '.number_format(($sum!=0)?($sum/$countHit)*100:0,2).'% Berhasil
                                </small>',
                "sum" => '<span class="badge badge-primary" style="font-size: 20px">'.number_format($sum,0,',','.').'</span>',
                "ok" => '<span class="badge badge-success" style="font-size: 20px">'.number_format($ok,0,',','.').'</span>',
                "invalid" => '<span class="badge badge-danger" style="font-size: 20px">'.number_format($invalid,0,',','.').'</span>',
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr,
            "countHit" => number_format($countHit,0,',','.'),
            "countHitOK" => number_format($countHitOK,0,',','.'),
            "countHitInvalid" => number_format($countHitInvalid,0,',','.'),
        );

        echo json_encode($response);
        exit;
    }

    public function showHistoryDetail(Request $request, $uid, $start_date, $end_date){
        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $start_date == "undefined" ? $fromDateDefault : $start_date;
        $toDate = $end_date == "undefined" ? $toDateDefault : $end_date;
        $data =  User::with(['CreaditFeatured' => function($query) use ($fromDate, $toDate){
                    $query->select('*')
                            ->whereBetween('creadit_featureds.updated_at', [$fromDate, $toDate]);
                }])->where('id', $uid)->first();

        return view('admin.izi-history-detail', ['User' => $data->CreaditFeatured, 'activeSidebar' => 'iziData']);
    }

    public function exportDataIziHistory($type, Request $request)
    {
        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get dept_id
        $temp_dept_id = $request->get('dept_id', "-");
        if ($temp_dept_id == "undefined" || $temp_dept_id == null) {
            $dept_id = [1,2,3];
        } else {
            $dept_id = [$temp_dept_id];
        }

        return (new IziDataHistoryExport($fromDate, $toDate, $dept_id))->download('IziDataHistoryReport.'.$type);
    }

    public function exportRawData($type, Request $request)
    {
        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get dept_id
        $temp_dept_id = $request->get('dept_id', "-");
        if ($temp_dept_id == "undefined" || $temp_dept_id == null) {
            $dept_id = [1,2,3];
        } else {
            $dept_id = [$temp_dept_id];
        }

        return (new IziDataRowExport($fromDate, $toDate, $dept_id))->download('exportRawData.'.$type);
    }
}
