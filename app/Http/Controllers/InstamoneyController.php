<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstamoneyPHPClient;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\DisplayPokokDendaBulananExport;

define('SECRET_API_KEY', 'sk_live_HXDc3gVdcJpBoMKSqXF6217Mn8AxrqQBkhXJB5gdBnjguXvfzYJtIVE4Y2oa7BG');
class InstamoneyController extends Controller
{

    public function apiBalanceInstamoney(Request $request){

        $options['secret_api_key'] = SECRET_API_KEY;

        $instamoneyPHPClient = new InstamoneyPHPClient($options);

        $okp2p_customers = 'c314e0a0-2243-11ea-a987-33f8a1367269';

        $okp2pFeeGetBalance = $instamoneyPHPClient->okp2pGetFeeBalance($okp2p_customers);

        $okp2pFeeGetBalance = $instamoneyPHPClient->okp2pGetBalance($okp2p_customers);

        return response()->json(['response'=> '200']);

    }

    public function apiOkp2pWithdrawlRepayment(Request $request){

        $options['secret_api_key'] = SECRET_API_KEY;

        $instamoneyPHPClient = new InstamoneyPHPClient($options);


        if(empty($dataRepayment)){
            return response()->json(['response'=> 'Gagal', 'data'=>NULL, 'status' => '000']);
        }else{

            $dataRepayment = $request->data;
            $okp2pFeeGetBalance = $instamoneyPHPClient->okp2pWithdrawlRepaymemt($okp2p_customers);
            return response()->json(['response'=> '', 'data'=>$dataRepayment, 'status' => '200']);
        }

    }

    public function getRdlBalance()
    {
        $options['secret_api_key'] = SECRET_API_KEY;
        $instamoneyPHPClient = new InstamoneyPHPClient($options);
        $okp2p_customers = 'f5859700-3747-11ea-aabb-91be1c6f9203';
        $getRdl = $instamoneyPHPClient->okp2pGetRdlBalance($okp2p_customers);

        return $getRdl;
    }

    public function getBalanceAccountLender()
    {
        $options['secret_api_key'] = SECRET_API_KEY;
        $instamoneyPHPClient = new InstamoneyPHPClient($options);
        $okp2p_customers = 'a05593a0-376c-11ea-b488-81e965a6b825';
        $getBal = $instamoneyPHPClient->okp2pGetFeeBalance($okp2p_customers);

        return $getBal;
    }

    public function BalanceInstamoney(){

        // dd($this->getRdlBalance());
        $getRdl = $this->getRdlBalance();
        $getAccLender = $this->getBalanceAccountLender();

        return view('admin.instamoney-balance', ['activeSidebar' => 'instamoney', 'getRdl' => $getRdl, 'getAccLender' => $getAccLender]);
    }

    public function getDataBungaTetapAdminFee(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 days', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd', strtotime('-1 days', strtotime( date("Ymd") )));
        $fromDateDefault = date('Ymd', strtotime("now" ));
        $toDateDefault = date('Ymd', strtotime('-30 days', strtotime( date("Ymd"))));

        // $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
            FROM
            FN_LOAN_MAST M,
            GT_CUST_COMM C,
            PT_LNC001TM P
            WHERE
            M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
            FROM
            FN_LOAN_MAST M,
            GT_CUST_COMM C,
            PT_LNC001TM P
            WHERE
            M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (M.INSP_NO LIKE '%$searchValue%' OR
                P.CUS_NM LIKE '%$searchValue%' OR
                M.LN_DT LIKE '%$searchValue%')
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
            M.INSP_NO as NO_PINJAMAN,
            P.CUS_NM as NAMA_PEMINJAM,
            (
                SELECT
                SUM(INT_AMT + WON_AMT)
                FROM
                PT_LOAN_PLAN
                WHERE
                INSP_NO = M.INSP_NO
                AND SCH_DT = M.LN_DT
            ) BUNGA_TETAP,
            CONVERT(
                numeric(18, 0),
                FLOOR(
                CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
            ) ADMIN_FEE,
            M.LN_DT as TANGGAL_PINJAM
            FROM
            FN_LOAN_MAST M,
            GT_CUST_COMM C,
            PT_LNC001TM P
            WHERE
            M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (M.INSP_NO LIKE '%$searchValue%' OR
                P.CUS_NM LIKE '%$searchValue%' OR
                M.LN_DT LIKE '%$searchValue%')
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $totalBungaTetap = DB::connection('sqlsrv')->select("
            SELECT
            SUM(L.INT_AMT + L.WON_AMT) AS total
            FROM
            FN_LOAN_MAST M,
            GT_CUST_COMM C,
            PT_LNC001TM P,
            PT_LOAN_PLAN L
            WHERE
            M.CUS_NO = C.CUS_NO
            AND L.INSP_NO = M.INSP_NO
            AND L.SCH_DT = M.LN_DT
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        $totalAdminFee = DB::connection('sqlsrv')->select("
            SELECT
            SUM(CONVERT(
                numeric(18, 0),
                FLOOR(
                CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
            )) AS total
            FROM
            FN_LOAN_MAST M,
            GT_CUST_COMM C,
            PT_LNC001TM P
            WHERE
            M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;
        $sumBungaTetap = 0;
        $sumAdminFee = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach ($totalBungaTetap as $totalBunga) {
            $sumBungaTetap = $totalBunga->total;
        }

        foreach ($totalAdminFee as $totalAdmin) {
            $sumAdminFee = $totalAdmin->total;
        }

        foreach($records as $record){
            $NO_PINJAMAN = $record->NO_PINJAMAN;
            $NAMA_PEMINJAM = $record->NAMA_PEMINJAM;
            $BUNGA_TETAP = $record->BUNGA_TETAP;
            $ADMIN_FEE = $record->ADMIN_FEE;
            $TANGGAL_PINJAM = $record->TANGGAL_PINJAM;

            $data_arr[] = array(
                "NO_PINJAMAN" => $NO_PINJAMAN,
                "NAMA_PEMINJAM" => $NAMA_PEMINJAM,
                "BUNGA_TETAP" => "Rp " . number_format($BUNGA_TETAP,2,',','.'),
                "ADMIN_FEE" => "Rp " . number_format($ADMIN_FEE,2,',','.'),
                "TANGGAL_PINJAM" => date('d-m-Y', strtotime($TANGGAL_PINJAM)),
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr,
            "sumBungaTetap" => "Rp " . number_format($sumBungaTetap,2,',','.'),
            "sumAdminFee" => "Rp " . number_format($sumAdminFee,2,',','.')
        );

        echo json_encode($response);
        exit;
    }


    public function PokokDenda(Request $request){
        return view('admin.instamoney-pokok-denda', ['activeSidebar' => 'instamoney']);
    }

    public function getDataRepayment(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 days', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd', strtotime('-1 days', strtotime( date("Ymd") )));
        // $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
            FROM
            FN_NAPV_MAST N,
            FN_LOAN_MAST M,
            GT_CUST_COMM C
            WHERE
            N.ACT_NO = M.ACT_NO
            AND C.CUS_NO = M.CUS_NO
            AND N.ACC_CD = '499001'
            AND N.STS_CD = '405001'
            AND N.COM_CD = '01'
            AND N.TRN_DT >= '$fromDate'
            AND N.TRN_DT <= '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
            FROM
            FN_NAPV_MAST N,
            FN_LOAN_MAST M,
            GT_CUST_COMM C
            WHERE
            N.ACT_NO = M.ACT_NO
            AND C.CUS_NO = M.CUS_NO
            AND N.ACC_CD = '499001'
            AND N.STS_CD = '405001'
            AND N.COM_CD = '01'
            AND N.TRN_DT >= '$fromDate'
            AND N.TRN_DT <= '$toDate'
            AND (DBO.GT_FT_GETCODENM(N.COM_CD, N.VIR_BANK_CD, 'ID') + ' - ' + N.VIR_ACNT_NO LIKE '%$searchValue%' OR
                C.CUS_NO LIKE '%$searchValue%' OR
                N.IN_AM LIKE '%$searchValue%' OR
                N.RGS_TRN_DH LIKE '%$searchValue%' OR
                INSTAR_CUST_ID LIKE '%$searchValue%' OR
                C.CUS_NM LIKE '%$searchValue%')
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
            DBO.GT_FT_GETCODENM(N.COM_CD, N.VIR_BANK_CD, 'ID') + ' - ' + N.VIR_ACNT_NO AS VIRTUAL_ACCOUNT_NO,
            C.CUS_NO AS NOMOR_CUSTOMER,
            C.CUS_NM AS NAMA_CUSTOMER,
            N.IN_AM AS NOMINAL,
            N.RGS_TRN_DH as rgsTrnDh,
            INSTAR_CUST_ID
            FROM
            FN_NAPV_MAST N,
            FN_LOAN_MAST M,
            GT_CUST_COMM C
            WHERE
            N.ACT_NO = M.ACT_NO
            AND C.CUS_NO = M.CUS_NO
            AND N.ACC_CD = '499001'
            AND N.STS_CD = '405001'
            AND N.COM_CD = '01'
            AND N.TRN_DT >= '$fromDate'
            AND N.TRN_DT <= '$toDate'
            AND (DBO.GT_FT_GETCODENM(N.COM_CD, N.VIR_BANK_CD, 'ID') + ' - ' + N.VIR_ACNT_NO LIKE '%$searchValue%' OR
                C.CUS_NO LIKE '%$searchValue%' OR
                N.IN_AM LIKE '%$searchValue%' OR
                N.RGS_TRN_DH LIKE '%$searchValue%' OR
                INSTAR_CUST_ID LIKE '%$searchValue%' OR
                C.CUS_NM LIKE '%$searchValue%')
            ORDER BY N.RGS_TRN_DH DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $totalPayment = DB::connection('sqlsrv')->select("
            SELECT
            SUM(N.IN_AM) AS total
            FROM
            FN_NAPV_MAST N,
            FN_LOAN_MAST M,
            GT_CUST_COMM C
            WHERE
            N.ACT_NO = M.ACT_NO
            AND C.CUS_NO = M.CUS_NO
            AND N.ACC_CD = '499001'
            AND N.STS_CD = '405001'
            AND N.COM_CD = '01'
            AND N.TRN_DT >= '$fromDate'
            AND N.TRN_DT <= '$toDate'
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;
        $totalPay = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach ($totalPayment as $totalP) {
            $totalPay = $totalP->total;
        }

        foreach($records as $record){
            $VIRTUAL_ACCOUNT_NO = $record->VIRTUAL_ACCOUNT_NO;
            $NOMOR_CUSTOMER = $record->NOMOR_CUSTOMER;
            $NAMA_CUSTOMER = $record->NAMA_CUSTOMER;
            $NOMINAL = $record->NOMINAL;
            $rgsTrnDh = $record->rgsTrnDh;
            $INSTAR_CUST_ID = $record->INSTAR_CUST_ID;

            $data_arr[] = array(
                "VIRTUAL_ACCOUNT_NO" => $VIRTUAL_ACCOUNT_NO,
                "NOMOR_CUSTOMER" => $NOMOR_CUSTOMER,
                "NAMA_CUSTOMER" => $NAMA_CUSTOMER,
                "NOMINAL" => "Rp. " . number_format($NOMINAL,2,',','.'),
                "rgsTrnDh" => date('d-M-Y H:i:s', strtotime($rgsTrnDh)),
                "INSTAR_CUST_ID" => $INSTAR_CUST_ID,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr,
            "totalPay" => "Rp " . number_format($totalPay,2,',','.')
        );

        echo json_encode($response);
        exit;
    }

    public function exportData($fromDate, $toDate, $status)
    {
        // get range date
       // $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        //$toDateDefault = date('Ymd');
        //$fromDate = $request->get('fromDate', $fromDateDefault);
       // $toDate = $request->get('toDate', $toDateDefault);
       // $status = $request->get('status');
        // get password
        //$password = $request->get('password', null);

        //$notification = array(
        //    'message' => 'The password you entered does not match our records. Please check back and try again!',
        //    'alert-type' => 'error'
        //);

       // if ($password == 'admin123') {
            return (new DisplayPokokDendaBulananExport($fromDate, $toDate, $status))->download('PokokDendaBulanan.xls');
       // } else {
        //    return Redirect::to('/displayojk')->with($notification);
       // }



    }

}
