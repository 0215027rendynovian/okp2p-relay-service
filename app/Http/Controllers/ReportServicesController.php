<?php

namespace App\Http\Controllers;

use App\Mail\ReportServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ReportServicesController extends Controller
{
    public function reportMail()
    {
        $get_mail = DB::connection('mysql')->select("
            SELECT email FROM `master_emails` WHERE status = '1';
        ");

        $im_mail = implode(",", array_map(function($obj) { foreach ($obj as $p => $v) { return $v;} }, $get_mail));

        $ex_mail = explode (",", $im_mail);

        // $emails = ['wehaye94@gmail.com', 'wahyu@okp2p.co.id','rendy@okp2p.co.id','andre@okp2p.co.id','rad@okp2p.co.id'];
        $emails = $ex_mail;

        $t=time();

        echo($t . "<br>");

        $date_time = date("Y-m-d h:i:sa",$t);

        $details = [
            'title' => 'Peringatan Untuk SISTEM OPS OKP2P',
            'body' => 'Telah Terjadi pemadaman listrik pada pukul '.$date_time
        ];

        Mail::to($emails)->send(new ReportServices($details));
    }
}
