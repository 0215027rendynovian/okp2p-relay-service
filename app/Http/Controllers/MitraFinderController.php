<?php

namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Message;
use App\Events\NewMessageNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\MitraMaps;
use Illuminate\Support\Facades\DB;

class MitraFinderController extends Controller
{
    //REFENSI
    //https://afrijaldzuhri.com/mengambil-data-berdasarkan-latitude-dan-longitude-dengan-radius-tertentu-di-mysql/




    public function findmitra()
    {
        return view('findmitra');
    }

    public function __construct() {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $user_id = Auth::user()->id;
        $data = array('user_id' => $user_id);
 
        return view('orderan', ['broadcast'=> $data, 'user_id' => $user_id]);
    }
 

    public function send()
    {
            // message is being sent
            $message = new Message;
            $message->setAttribute('from', 1);
            $message->setAttribute('to', 15);
            $message->setAttribute('message', 'Demo message from user 1 to user 2');
            $message->save();
            
            event(new NewMessageNotification($message));
    }

    public function cekJarak(Request $request) {
        $latitude       =       "-7.785973";
        $longitude      =       "110.399957";
        $mitra_maps          =  DB::table("mitra_maps");
        $mitra_maps->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                  * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                               + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
        $mitra_maps->having('distance', '<=',1);
        return $mitra_maps = $mitra_maps->get();
  
    }

    public function searchAlamat(Request $request) {
        return view('searchalamat');
  
    }

}
