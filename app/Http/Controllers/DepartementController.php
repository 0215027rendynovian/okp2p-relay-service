<?php

namespace App\Http\Controllers;

use App\Departement;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class DepartementController extends Controller
{
    public function index()
    {
        $masterDept = Departement::all();

        $code = Departement::max('id');

        return view('admin.departement', ['listdepartement' => $masterDept, 'activeSidebar' => 'configuration', 'code' => $code]);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Master Departement berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'code' => 'required',
                'name' => 'required',
            ]);

            Departement::create([
                'code' => $request->code,
                'name' => $request->name,
                'status' => 1,
                'description' => $request->description,
            ]);

            return Redirect::to('/departement')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Departement gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/departement')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $masterDept = Departement::find($request->id);
            $masterDept->code = $request->code;
            $masterDept->name = $request->name;
            $masterDept->status = $request->status;
            $masterDept->description = $request->description;
            $masterDept->save();

            $notification = array(
                'message' => 'Master Departement berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/departement')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Departement gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/departement')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            Departement::find($id)->delete();

            $notification = array(
                'message' => 'Master Departement berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/departement')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Departement gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/departement')->with($notification);
        }
    }
}
