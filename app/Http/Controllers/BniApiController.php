<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\JwtSignature;
use App\Helpers\BNIHttpClient;
use App\Helpers\StoreServices;


class BniApiController extends Controller
{

        /**
         * This authorization static basic for key BNI.
         * have 2 const mode : SANDBOX and Production
         */

        // Authorization BNI Request
        const DEV_AUTHORIZATION='NWEyOGM3MTMtMDYxZC00NzE5LWE0ZDEtNGE5ZjI5ZTM4NTg2OmIyYmEwODE3LTZiYWEtNDZiOS04MzlkLTc0ZjY3MTFiOWY1Mg==';

        const PROD_AUTHORIZATION='ZTBmYjI2NDctYmQzYi00Yjc1LTk3YzctOTc4YjExMmVhYTgzOmU3MzU5YmNmLTVkZTEtNDMxMS1iZDNjLWM2ZmQ5MDlmOTlmMw==';


        // End Poin BNI Request
        const END_POINT_PROD = 'https://api.bni.co.id';
        const END_POINT_DEV = 'https://apidev.bni.co.id:8067';

        
        // End Poin X-API-KEY 
        const X_API_KEY_PROD = 'ae6bc341-543a-455b-b691-b22465088e7e';
        const X_API_KEY_DEV = '7d7f281b-a72d-4c6d-afba-d02d68116095';


        // End Poin JWT SECRET KEY 
        const X_API_JWT_PROD = '3f6ef456-1b5a-454a-a01c-a2eedac80bac';
        const X_API_JWT_DEV = '76a682ea-ebe2-42cb-a465-1a74ea6ae7f3';

        private $RequestPayload;
        
        

     function getToken($request){
         
            $dev_mode = $request->header('mode_dev') === 'true'? true: false;

            if($dev_mode == true){
                $authorization = self::DEV_AUTHORIZATION;
                $url = self::END_POINT_DEV;
            }else{
                $authorization = self::PROD_AUTHORIZATION;
                $url = self::END_POINT_PROD;
            } 

            $headers = array(
                "Content-type: application/x-www-form-urlencoded",
                "Authorization: Basic ".$authorization
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url."/api/oauth/token");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials");
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec ($ch);
            $final_output = json_decode($server_output, true);
            $access_token = $final_output['access_token'];
            curl_close ($ch);

            return $access_token;

    }


    public function InquiryGetBalance(Request $request){

        $token = $this->getToken($request);

        $dev_mode = $request->header('mode_dev') === 'true'? true: false;

        $uri = '/p2pl/inquiry/account/balance';

        $uid = 1;

        $this->RequestPayload;


        // Create Condition Development
        if($dev_mode == true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        } 
        

        // Create Exception Failed..
        try {

                $payload = array();

                $requestHeader = array(
                            "companyId" => "OKP2P",
                            "parentCompanyId" => "",
                            "requestUuid" => $request->requestUuid
                );

                $payload['header'] = $requestHeader;

                $payload['accountNumber'] = $request->accountNumber;

                $RequestPayload = array('request' => $payload);

                $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);

                $RequestPayload['request']['header']['signature'] = $JwtSignature;

                $data_post = json_encode($RequestPayload, true);
                
                $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);

                StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Inquiry Get Ballance',$uri,$request->requestUuid,$uid);

                return $response;

        }catch (\Throwable $th) {
            
            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );

            return $notification;
        
        }

        
    }




    public function InquiryGetHistory(Request $request){

        $token = $this->getToken($request);

        $dev_mode = $request->header('mode_dev') === 'true'? true: false;

        
        $uri = '/p2pl/inquiry/account/history';

        $uid = 1;

        if($dev_mode == true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        } 

        try{ 

            $payload = array();

            $requestHeader = array(
                        "companyId" => "OKP2P",
                        "parentCompanyId" => "",
                        "requestUuid" => $request->requestUuid
            );

            $payload['header'] = $requestHeader;

            $payload['accountNumber'] = $request->accountNumber;

            $RequestPayload = array('request' => $payload);

            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);

            $RequestPayload['request']['header']['signature'] = $JwtSignature;

            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);

            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Inquiry Get History',$uri,$request->requestUuid,$uid);

            return $response;

        }catch (\Throwable $th) {
            
            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );

            return $notification;
        
        }

        
    }

    public function InquiryAccountInfo(Request $request){

        $token = $this->getToken($request);

        $dev_mode = $request->header('mode_dev') === 'true'? true: false;

        $uri = '/p2pl/inquiry/account/info';

        $uid = 1;

        if($dev_mode == true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        } 

        try {

                $payload = array();

                $requestHeader = array(
                            "companyId" => "OKP2P",
                            "parentCompanyId" => "",
                            "requestUuid" => $request->requestUuid
                );

                $payload['header'] = $requestHeader;

                $payload['accountNumber'] = $request->accountNumber;

                $RequestPayload = array('request' => $payload);

                $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);

                $RequestPayload['request']['header']['signature'] = $JwtSignature;

                $data_post = json_encode($RequestPayload, true);
                
                $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);

                StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Inquiry Account Info',$uri,$request->requestUuid,$uid);

                return $response;
            
        } catch (\Throwable $th) {

                $notification = array(
                    'message' => 'Error Access Relay Server atau kesalahan pada request server',
                    'success' => 'false'
                );

                return $notification;
        }


        
    }


    public function InquiryInterbank(Request $request){

        $token = $this->getToken($request);

        $dev_mode = $request->header('mode_dev') === 'true'? true: false;

        $uri = '/p2pl/inquiry/interbank/account';

        $uid = 1;

        if($dev_mode == true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        } 

        try {
            $payload = array();

            $requestHeader = array(
                        "companyId" => "OKP2P",
                        "parentCompanyId" => "",
                        "requestUuid" => $request->requestUuid
            );

            $payload['header'] = $requestHeader;

            $payload['accountNumber'] = $request->accountNumber;

            $payload['beneficiaryBankCode'] = $request->beneficiaryBankCode;
            
            $payload['beneficiaryAccountNumber'] = $request->beneficiaryAccountNumber;

            $RequestPayload = array('request' => $payload);

            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);

            $RequestPayload['request']['header']['signature'] = $JwtSignature;

            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);

            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Inquiry Interbank',$uri,$request->requestUuid,$uid);

            return $response;

        } catch (\Throwable $th) {

            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );

            return $notification;
        }
        
    }


    public function PaymentStatus(Request $request){
        
        $token = $this->getToken($request);

        $dev_mode = $request->header('mode_dev') === "true" ? true: false;

        $uri = '/p2pl/inquiry/payment/status';

        $uid = 1;

        if($dev_mode === true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        }

        try {
            $payload = array();

            $requestHeader = array(
                "companyId" => "OKP2P",
                "parentCompanyId" => "",
                "requestUuid" => $request->requestUuid
            );

            $payload['header'] = $requestHeader;

            $payload['requestedUuid'] = $request->requestedUuid;

            $RequestPayload = array('request' => $payload);

            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);

            $RequestPayload['request']['header']['signature'] = $JwtSignature;

            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);

            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Payment Status',$uri,$request->requestUuid,$uid);

            return $response;

        } catch (\Throwable $th) {

            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );

            return $notification;
        }
        
    }


    public function PaymentClearing(Request $request){
        
        $token = $this->getToken($request);

        $dev_mode = $request->header('mode_dev') === "true"? true:false;

        $uri = '/p2pl/payment/clearing';

        $uid = 1;

        if($dev_mode === true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        }

        try {
            $payload = array();

            $requestHeader = array(
                "companyId" => "OKP2P",
                "parentCompanyId" => "",
                "requestUuid" => $request->requestUuid
            );
    
            $payload['header'] = $requestHeader;
    
            $payload['accountNumber'] = $request->accountNumber;
    
            $payload['beneficiaryAccountNumber'] = $request->beneficiaryAccountNumber;
    
            $payload['beneficiaryAddress1'] = $request->beneficiaryAddress1;
            
            $payload['beneficiaryAddress2'] = $request->beneficiaryAddress2;
    
            $payload['beneficiaryBankCode'] = $request->beneficiaryBankCode;
    
            $payload['beneficiaryName'] = $request->beneficiaryName;
    
            $payload['currency'] = $request->currency;
    
            $payload['amount'] = $request->amount;
    
            $payload['remark'] = $request->remark;
    
            $payload['chargingType'] = $request->chargingType;
    
            $RequestPayload = array('request' => $payload);
    
            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);
    
            $RequestPayload['request']['header']['signature'] = $JwtSignature;
    
            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);
    
            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Payment Clearing',$uri,$request->requestUuid,$uid);
    
            return $response;

        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );
            
            return $notification;
        }
    }

    public function PaymentInterbank(Request $request){

        $token = $this->getToken($request);

        $dev_mode = $request->header('mode_dev') === "true"? true:false;

        $uri = '/p2pl/payment/interbank';

        $uid = 1;
        
        if($dev_mode === true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        }


        try {
            $payload = array();

            $requestHeader = array(
                "companyId" => "OKP2P",
                "parentCompanyId" => "",
                "requestUuid" => $request->requestUuid
            );
    
            $payload['header'] = $requestHeader;
    
            $payload['accountNumber'] = $request->accountNumber;
            
            $payload['beneficiaryAccountNumber'] =  $request->beneficiaryAccountNumber;
            
            $payload['beneficiaryAccountName'] =  $request->beneficiaryAccountName;
            
            $payload['beneficiaryBankCode'] = $request->beneficiaryBankCode;
            
            $payload['beneficiaryBankName'] =  $request->beneficiaryBankName;
            
            $payload['amount'] =  $request->amount;
    
            $RequestPayload = array('request' => $payload);
    
            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);
    
            $RequestPayload['request']['header']['signature'] = $JwtSignature;
    
            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);
    
            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Payment Interbank',$uri,$request->requestUuid,$uid);
    
            return $response;

        } catch (\Throwable $th) {

            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );
            
            return $notification;
        }

    }

    public function PaymentRTGS(Request $request){

        $token = $this->getToken($request);
        
        $dev_mode = $request->header('mode_dev') === "true"? true:false;

        $uri = '/p2pl/payment/rtgs';

        $uid = 1;

        if($dev_mode === true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        }   

        try {

            $payload = array();

            $requestHeader = array(
                "companyId" => "OKP2P",
                "parentCompanyId" => "",
                "requestUuid" => $request->requestUuid
            );
    
            $payload['header'] = $requestHeader;
    
            $payload['accountNumber'] = $request->accountNumber;
    
            $payload['beneficiaryAccountNumber'] = $request->beneficiaryAccountNumber;
    
            $payload['beneficiaryAddress1'] = $request->beneficiaryAddress1;
    
            $payload['beneficiaryAddress2'] = $request->beneficiaryAddress2;
    
            $payload['beneficiaryBankCode'] = $request->beneficiaryBankCode;
    
            $payload['beneficiaryName'] = $request->beneficiaryName;
    
            $payload['currency'] = $request->currency;
    
            $payload['amount'] = $request->amount;
    
            $payload['remark'] = $request->remark;
    
            $payload['chargingType'] = $request->chargingType;
    
            $RequestPayload = array('request' => $payload);
    
            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);
    
            $RequestPayload['request']['header']['signature'] = $JwtSignature;
    
            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);
    
            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Payment RTGS',$uri,$request->requestUuid,$uid);
    
            return $response;

        } catch (\Throwable $th) {

            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );
            
            return $notification;
        }
    }

    public function PaymentTransfer(Request $request){

        $token = $this->getToken($request);
        
        $dev_mode = $request->header('mode_dev') === "true"? true:false;

        $uri = '/p2pl/payment/transfer';

        $uid = 1;


        if($dev_mode === true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        }   




            $payload = array();

            $requestHeader = array(
                "companyId" => "OKP2P",
                "parentCompanyId" => "INSTAMONEY",
                "requestUuid" => $request->requestUuid
            );
    
            $payload['header'] = $requestHeader;
    
            $payload['accountNumber'] = $request->accountNumber;
            
            $payload['beneficiaryAccountNumber'] = $request->beneficiaryAccountNumber;
    
            $payload['currency'] = $request->currency;
    
            $payload['amount'] = $request->amount;
    
            $payload['remark'] = $request->remark;
            
            $RequestPayload = array('request' => $payload);
    
            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);
    
            $RequestPayload['request']['header']['signature'] = $JwtSignature;
    
            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);
    
            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Payment RTGS',$uri,$request->requestUuid,$uid);
    
            return $response;

        
    }

    public function RegisterInvestor(Request $request){

        $token = $this->getToken($request);
        
        $dev_mode = $request->header('mode_dev') === "true"? true:false;

        $uri = '/p2pl/register/investor';

        $uid = 1;

        if($dev_mode === true){
            $authorization = self::DEV_AUTHORIZATION;
            $url = self::END_POINT_DEV.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_DEV;
            $X_API_KEY = self::X_API_KEY_DEV;
        }else{
            $authorization = self::PROD_AUTHORIZATION;
            $url = self::END_POINT_PROD.$uri."?access_token=".$token;
            $X_API_JWT = self::X_API_JWT_PROD;
            $X_API_KEY = self::X_API_KEY_PROD;
        }   


        try {
            $payload = array();

            $requestHeader = array(
                "companyId" => "OKP2P",
                "parentCompanyId" => "",
                "requestUuid" => $request->requestUuid
            );
    
            $payload['header'] = $requestHeader;
    
            $payload['title'] = $request->title;
    
            $payload['firstName'] = $request->firstName;
    
            $payload['middleName'] = $request->middleName;
    
            $payload['lastName'] = $request->lastName;
            
            $payload['optNPWP'] = $request->optNPWP;
    
            $payload['NPWPNum'] = $request->NPWPNum;
           
            $payload['nationality'] = $request->nationality;
    
            $payload['domicileCountry'] = $request->domicileCountry;
    
            $payload['religion'] = $request->religion;
            
            $payload['birthPlace'] = $request->birthPlace;
           
            $payload['birthDate'] = $request->birthDate;
    
            $payload['gender'] = $request->gender;
    
            $payload['isMarried'] = $request->isMarried;
    
            $payload['motherMaidenName'] = $request->motherMaidenName;
           
            $payload['jobCode'] = $request->jobCode;
    
            $payload['education'] = $request->education;
    
            $payload['idNumber'] = $request->idNumber;
    
            $payload['idIssuingCity'] = $request->idIssuingCity;
            
            $payload['idExpiryDate'] = $request->idExpiryDate;
    
            $payload['addressStreet'] = $request->addressStreet;
            
            $payload['addressRtRwPerum'] = $request->addressRtRwPerum;
    
            $payload['addressKel'] =  $request->addressKel;
    
            $payload['addressKec'] = $request->addressKec;
    
            $payload['zipCode'] =  $request->zipCode;
    
            $payload['homePhone1'] = $request->homePhone1;
            
            $payload['homePhone2'] = $request->homePhone1;
            
            $payload['officePhone1'] = $request->officePhone1;
            
            $payload['officePhone2'] = $request->officePhone2;
    
            $payload['mobilePhone1'] = $request->mobilePhone1;
            
            $payload['mobilePhone2'] = $request->mobilePhone2;
            
            $payload['faxNum1'] = $request->faxNum1;
            
            $payload['faxNum2'] = $request->faxNum2;
            
            $payload['email'] = $request->email;
            
            $payload['monthlyIncome'] = $request->monthlyIncome;
            
            $payload['branchOpening'] = $request->branchOpening;
    
            $RequestPayload = array('request' => $payload);
    
            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);
    
            $RequestPayload['request']['header']['signature'] = $JwtSignature;
    
            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);
    
            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Register Investor',$uri,$request->requestUuid,$uid);
    
            return $response;

        } catch (\Throwable $th) {

            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );

            return $notification;
        }
       
    }


    public function AccountInvestor(Request $request){
        
        try {
            $token = $this->getToken($request);

            $dev_mode = $request->header('mode_dev') === "true"? true:false;
    
            $uri = '/p2pl/register/investor/account';
    
            $uid = 1;
    
            if($dev_mode === true){
                $authorization = self::DEV_AUTHORIZATION;
                $url = self::END_POINT_DEV.$uri."?access_token=".$token;
                $X_API_JWT = self::X_API_JWT_DEV;
                $X_API_KEY = self::X_API_KEY_DEV;
            }else{
                $authorization = self::PROD_AUTHORIZATION;
                $url = self::END_POINT_PROD.$uri."?access_token=".$token;
                $X_API_JWT = self::X_API_JWT_PROD;
                $X_API_KEY = self::X_API_KEY_PROD;
            }
    
            $payload = array();
    
            $requestHeader = array(
                "companyId" => "OKP2P",
                "parentCompanyId" => "",
                "requestUuid" => $request->requestUuid
            );
    
            $payload['header'] = $requestHeader;
            
            $payload['cifNumber'] = $request->cifNumber;
            
            $payload['accountType'] = $request->accountType;
            
            $payload['currency'] = $request->currency;
            
            $payload['openAccountReason'] = $request->openAccountReason;
            
            $payload['sourceOfFund'] = $request->sourceOfFund;
            
            $payload['branchId'] = $request->branchId;
    
            $RequestPayload = array('request' => $payload);
    
            $JwtSignature = JwtSignature::ShowSignature($RequestPayload, $X_API_JWT);
    
            $RequestPayload['request']['header']['signature'] = $JwtSignature;
    
            $data_post = json_encode($RequestPayload, true);
            
            $response = BNIHttpClient::POST($X_API_KEY,$authorization,$data_post,$url);
    
            StoreServices::storeLogBni($token,$dev_mode,$data_post,$response,'Account Investor',$uri,$request->requestUuid,$uid);
    
            return $response;

        } catch (\Throwable $th) {

            $notification = array(
                'message' => 'Error Access Relay Server atau kesalahan pada request server',
                'success' => 'false'
            );

            return $notification;
        }
    }
}
