<?php

namespace App\Http\Controllers;

use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard', ['activeSidebar' => '']);
    }

    public function clearCache()
    {
        try {
            // system('composer dump-autoload');
            Artisan::call('cache:clear');
            Artisan::call('config:clear');
            Artisan::call('config:cache');

            $notification = array(
                'message' => 'Clear Cache Berhasil!',
                'alert-type' => 'success'
            );

            return Redirect::to('/mba')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Clear Cache Gagal!',
                'alert-type' => 'error'
            );

            return Redirect::to('/mba')->with($notification);
        }

    }
}
