<?php

namespace App\Http\Controllers;

use App\PingConfig;
use Illuminate\Http\Request;

class PingController extends Controller
{

    public function pinging(){
        $config = PingConfig::find(1);
        $ip = 'http://' . $config->server_ip;
        //$port = '8000';
        $url = $ip . ':' . $config->server_port;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $health = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($health) {
            $json = json_encode(['health' => $health, 'status' => '1']);
            return $json;
        } else {
            $json = json_encode(['health' => $health, 'status' => '0']);
            return $json;
        } 
    }
}
