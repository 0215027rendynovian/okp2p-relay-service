<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request)
    {
        $notification_deactivated = array(
            'message' => 'your account has been deactivated!',
            'alert-type' => 'error'
        );

        $notification_login_err_01 = array(
            'message' => 'ES users are not allowed to login to OPS',
            'alert-type' => 'error'
        );

        $notification_login_err_02 = array(
            'message' => 'OPS users are not allowed to login to ES',
            'alert-type' => 'error'
        );

        $credentials = $request->only('email', 'password');

        // Make sure the user is active
        if (Auth::user()->is_active == 0)
        {
            // Increment the failed login attempts and redirect back to the
            // login form with an error message.
            Auth::logout();
            return redirect()
                ->back()
                ->with($notification_deactivated);
        }
        else {
            if (auth()->user()->es_system == 1 && $request->status_login == 'sistem-es') {
                return redirect()->route('creditfeature');
            }
            else if (auth()->user()->es_system == 0 && $request->status_login == 'sistem-es'){
                Auth::logout();
                return Redirect::to('/ops-sistem/login')->with($notification_login_err_02);
            }
            else if (auth()->user()->es_system == 0 && $request->status_login == 'sistem-ops'){
                return redirect()->route('home');
            }
            else if (auth()->user()->es_system == 1 && $request->status_login == 'sistem-ops'){
                Auth::logout();
                return Redirect::to('/es-sistem/login')->with($notification_login_err_01);
                
                //return redirect()
                //    ->back()
                //    ->with($notification_login_err_01);
            }
        }

        /*
        if (auth()->user()->es_system == 1) {
            return redirect()->route('creditfeature');
        }
        else {
            return redirect()->route('home');
        }

        if ((Auth::attempt($credentials)) AND (Auth::user()->is_re_password == 0)) {
            return redirect('change-password');
        }
        */
    }

    public function logout(Request $request){

        if (auth()->user()->es_system == 1) {
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
            return Redirect::to('/es-sistem/login');
        }
        else { 
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
            return Redirect::to('/ops-sistem/login');
        }

    }
    
}
