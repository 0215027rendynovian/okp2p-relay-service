<?php

namespace App\Http\Controllers;

use App\MasterMenu;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class DisplayMenuController extends Controller
{
    public function index()
    {
        $masterDisplayMenu = MasterMenu::all();

        $code = MasterMenu::max('id');

        return view('admin.displaymenu', ['listdisplaymenu' => $masterDisplayMenu, 'activeSidebar' => 'configuration', 'code' => $code]);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Master Menu berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'code' => 'required',
                'name' => 'required',
            ]);

            MasterMenu::create([
                'code' => $request->code,
                'name' => $request->name,
                'menuopen' => $request->menuopen,
                'icon' => $request->icon,
                'url' => $request->url,
                'status' => 1,
                'description' => $request->description,
            ]);

            return Redirect::to('/displaymenu')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Menu gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/displaymenu')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $masterDisplayMenu = MasterMenu::find($request->id);
            $masterDisplayMenu->code = $request->code;
            $masterDisplayMenu->name = $request->name;
            $masterDisplayMenu->menuopen = $request->menuopen;
            $masterDisplayMenu->icon = $request->icon;
            $masterDisplayMenu->url = $request->url;
            $masterDisplayMenu->status = $request->status;
            $masterDisplayMenu->description = $request->description;
            $masterDisplayMenu->save();

            $notification = array(
                'message' => 'Master Menu berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/displaymenu')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Menu gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/displaymenu')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            MasterMenu::find($id)->delete();

            $notification = array(
                'message' => 'Master Menu berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/displaymenu')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Menu gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/displaymenu')->with($notification);
        }
    }
}
