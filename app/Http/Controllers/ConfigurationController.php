<?php

namespace App\Http\Controllers;

use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConfigurationController extends Controller
{
    public function index()
    {
        return view('admin.index-config', ['activeSidebar' => 'configuration']);
    }

    public function roles(){
        //
    }

    public function userPrivilage()
    {
        //
    }

    public function departement()
    {
        //
    }

    public function displayMenu()
    {
        //
    }
}
