<?php

namespace App\Http\Controllers;

use App\Departement;
use App\Exports\MidtransHistoryExport;
use Illuminate\Http\Request;
use App\MidtransBankValidator;
use App\MasterBankAccount;
use App\User;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MidtransController extends Controller
{

    public function index(){
        $uid = Auth::user()->id;
        $MidtransBankValidator = MidtransBankValidator::all();
        $MasterBankAccount = MasterBankAccount::all();

        return view('admin.midtrans-bank-account-validator', ['MidtransBankValidator' => $MidtransBankValidator, 'uid' => $uid, 'MasterBankAccount' => $MasterBankAccount, 'activeSidebar' => 'mitrans']);
    }

    public function ReportMidtrans($account_no){
        $MidtransBankValidator = MidtransBankValidator::all();
        return view('admin.midtrans-data-report', ['MidtransBankValidator' => $MidtransBankValidator, 'account_no' => $account_no]);
    }

    public function showHistory(Request $request)
    {
        $listDept = Departement::select('id', 'name')->pluck('name','id');

        return view('admin.midtrans-history', ['activeSidebar' => 'mitrans', 'listDept' => $listDept]);
    }

    public function getHistoryHit(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get dept_id
        $temp_dept_id = $request->get('dept_id');
        $dept_id = $temp_dept_id == "" ? [1,2,3] : [$temp_dept_id];

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = User::select('count(*) as allcount')->whereIn('dept_id', $dept_id)->count();
        $totalRecordswithFilter = User::select('count(*) as allcount')->where('name', 'like', '%' .$searchValue . '%')->whereIn('dept_id', $dept_id)->count();

        // Fetch records
        $records = DB::table('users')
                        ->leftJoin('midtrans-bank-validator', function ($join) use ($fromDate, $toDate){
                            $join->on('users.id', '=', 'midtrans-bank-validator.uid')
                                ->whereBetween('midtrans-bank-validator.updated_at', [$fromDate, $toDate]);
                        })
                        ->select('users.id','users.name','users.email','users.dept_id', DB::raw('sum(count_hit) as COUNT_HIT'))
                        ->where('users.name', 'like', '%' .$searchValue . '%')
                        ->whereIn('users.dept_id', $dept_id)
                        ->orderBy($columnName,$columnSortOrder)
                        ->groupBy('users.id')
                        ->groupBy('users.dept_id')
                        ->skip($start)
                        ->take($rowperpage)
                        ->get();

        $data_arr = array();

        foreach($records as $record){
            $id = $record->id;
            $name = $record->name;
            $COUNT_HIT = $record->COUNT_HIT;

            $data_arr[] = array(
                "id" => $id,
                "name" => $name,
                "COUNT_HIT" => $COUNT_HIT ?? 0,
                "log" => '<a class="btn back-ops-okp2p"  data-toggle="modal" data-target="#myModal2" onclick="onRowClicked('.$id.')"><li class="fas fa-sort" style="color:white; font-size:11px;">  Detil</li></a>'
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr,
        );

        echo json_encode($response);
        exit;
    }

    public function getCountHistory(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get dept_id
        $temp_dept_id = $request->get('dept_id');
        $dept_id = $temp_dept_id == "" ? [1,2,3] : [$temp_dept_id];

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        $countHit = DB::table('midtrans-bank-validator')
                        ->leftJoin('users', function ($join) use ($dept_id){
                            $join->on('midtrans-bank-validator.uid', '=', 'users.id');
                        })
                        ->whereBetween('midtrans-bank-validator.updated_at', [$fromDate, $toDate])
                        ->whereIn('users.dept_id', $dept_id)
                        ->sum('count_hit');

        // Total records
        $totalRecords = Departement::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Departement::select('count(*) as allcount')->count();

        // Fetch records
        $records = DB::table('users')
            ->leftJoin('midtrans-bank-validator', function ($join) use ($fromDate, $toDate){
                $join->on('users.id', '=', 'midtrans-bank-validator.uid')
                    ->whereBetween('midtrans-bank-validator.updated_at', [$fromDate, $toDate]);
            })
            ->leftJoin('departements', function ($join){
                $join->on('users.dept_id', '=', 'departements.id');
            })
            ->select('users.dept_id', 'departements.name', 'departements.description', DB::raw('sum(count_hit) as sum'))
            ->whereIn('dept_id', $dept_id)
            ->groupBy('dept_id')
            ->get();

        $data_arr = array();

        foreach($records as $record){
            $dept_id = $record->dept_id;
            $name = $record->name;
            $description = $record->description;
            $sum = $record->sum;

            $data_arr[] = array(
                "dept_id" => '#',
                "name" => '<a style="font-size: 17px">
                                '.$name.'
                            </a>
                            <br/>
                            <small style="font-size: 11px">
                                '.$description.'
                            </small>',
                "description" => '<div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="'.number_format(($sum!=0)?($sum/$countHit)*100:0,2).'" aria-volumemin="0" aria-volumemax="100" style="width: '.number_format(($sum!=0)?($sum/$countHit)*100:0,2).'%">
                                    </div>
                                </div>
                                <small style="font-size: 11px">
                                    '.number_format(($sum!=0)?($sum/$countHit)*100:0,2).'% Berhasil
                                </small>',
                "sum" => '<span class="badge badge-primary" style="font-size: 20px">'.number_format($sum,0,',','.').'</span>',
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr,
            "countHit" => number_format($countHit,0,',','.')
        );

        echo json_encode($response);
        exit;
    }

    public function showHistoryDetail(Request $request, $uid, $start_date, $end_date){
        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $start_date == "undefined" ? $fromDateDefault : $start_date;
        $toDate = $end_date == "undefined" ? $toDateDefault : $end_date;
        $data =  User::with(['Midtrans' => function($query) use ($fromDate, $toDate){
                    $query->select('*')
                            ->whereBetween('midtrans-bank-validator.updated_at', [$fromDate, $toDate]);
                }])->where('id', $uid)->first();

        return view('admin.midtrans-history-detail', ['User' => $data->Midtrans, 'activeSidebar' => 'mitrans']);
    }

    public function exportDataMidtransHistory($type, Request $request)
    {
        // get range date
        $fromDateDefault = date('Y-m-d', strtotime('-1 month', strtotime( date("Y-m-d") )));
        $toDateDefault = date('Y-m-d');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get dept_id
        $temp_dept_id = $request->get('dept_id', "-");
        if ($temp_dept_id == "undefined" || $temp_dept_id == null) {
            $dept_id = [1,2,3];
        } else {
            $dept_id = [$temp_dept_id];
        }

        return (new MidtransHistoryExport($fromDate, $toDate, $dept_id))->download('MidtransHistoryReport.'.$type);
    }
}
