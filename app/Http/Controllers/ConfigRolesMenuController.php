<?php

namespace App\Http\Controllers;

use App\ChildMenu;
use App\MasterMenu;
use App\Roles;
use App\UserPrivilage;
use App\ViewConfigRoleMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ConfigRolesMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::all();
        $masterMenu = MasterMenu::all();
        $childMenu = ChildMenu::all();
        $configRole = ViewConfigRoleMenu::all();
        $privilege = UserPrivilage::all();

        return view('admin.config-role-menu', ['activeSidebar' => 'configuration', 'listroles' => $roles, 'masterMenu' => $masterMenu, 'childMenu' => $childMenu, 'configRole' => $configRole, 'no' => 1, 'privilege' => $privilege]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $configRole = ViewConfigRoleMenu::all();
        $privilege = UserPrivilage::all();
        $selectedRole = Roles::find($id);

        return view('admin.change-role-menu', ['activeSidebar' => 'configuration', 'configRole' => $configRole, 'no' => 1, 'privilege' => $privilege, 'idrole' => $id, 'selectedRole' => $selectedRole]);
    }

    /**
     *
     * FUNGSI INI DIGUNAKAN UNTUK UPDATE PRIVILEGE MENU BERDASARKAN ROLE ID, JADI UPDATE NYA WHERE ROLE ID
     *
     *
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $dataSave = $request->checkconfigrole;

            foreach ($dataSave as $key => $value) {
                $dt = explode("+", $value);
                $data[] = [
                    'role_id' => $dt[0],
                    'parent' => $dt[1],
                    'child' => $dt[2],
                    'code' => $dt[3],
                    'isParent' => $dt[4]
                ];
            }

            UserPrivilage::where('role_id', $id)->delete();

            UserPrivilage::insert($data);

            $notification = array(
                'message' => 'Master Roles berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/config-role')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Roles gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/config-role')->with($notification);
        }
    }

    /**
     *
     * FUNGSI INI DIGUNAKAN UNTUK UPDATE PRIVILEGE MENU USER DENGAN CARA MENGOSONGKAN / RESET SEMUA DATA PADA TABEL USER_PRIVILEGE DAN INSERT DENGAN DATA BARU DENGAN BATCH INSERT
     *
     *
     * Reset the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        try {
            $dataSave = $request->checkconfigrole;

            foreach ($dataSave as $key => $value) {
                $dt = explode("+", $value);
                $data[] = [
                    'role_id' => $dt[0],
                    'parent' => $dt[1],
                    'child' => $dt[2],
                    'code' => $dt[3],
                    'isParent' => $dt[4]
                ];
            }

            UserPrivilage::truncate();

            UserPrivilage::insert($data);

            $notification = array(
                'message' => 'Master Roles berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/config-role')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Roles gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/config-role')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
