<?php

namespace App\Http\Controllers;

use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\XmlToJson;
use App\Helpers\RelayServices;
use App\PefindoCustomeReport;

class PefindoController extends Controller
{
    public function index()
    {
        // write code here
    }

    public function dataContract()
    {
        return view('admin.data-contract', ['activeSidebar' => 'pefindo']);
    }

    public function getDataContract(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT count(*) as allcount
            FROM FN_LOAN_MAST M, GT_CUST_COMM C, PT_LNC001TM P
            WHERE M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT count(*) as allcount
            FROM FN_LOAN_MAST M, GT_CUST_COMM C, PT_LNC001TM P
            WHERE M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (P.INSP_NO LIKE '%$searchValue%' OR P.BPJS LIKE '%$searchValue%')
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT  P.INSP_NO as Contract_code
            ,P.BPJS as Branch
            ,1 as Phase_of_contract
            ,2 as Contract_status
            ,1 as Type_of_contract
            ,1 as Credit_Classification
            ,74 as Purspose_Of_Financing
            ,P.BPJS as Name_Of_Insured
            ,205 as Economic_Sector
            ,66 as Currency_Of_Contract
            ,convert(varchar,convert(numeric(5,2),P.CONFIRM_NML_IRT))  Interest_Rate
            ,1 as Type_Of_Interst_Rate
            ,1000000000 as Total_Facility_Amount
            ,M.LN_AM as Total_Amount
            ,0 as Initial_Total_Amount
            ,0 as Total_Taken_Amount
            ,M.LN_BAL_AM + 0 + (SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) as Outstanding_Amount --SECARA RUMUS = Principal Ballance + Pas Due Interest + Penalty
            --,M.LN_AM as Principal_Balance
            ,M.LN_BAL_AM as Pricipal_Balance
            ,(SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) as Past_Due_Amount  -- Principal Arreas + Past Due Interst + Penalty
            --,ISNULL(M.DFR_DAY, 0) AS Past_Due_Days
            ,CASE
                WHEN
                    (SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) = 0 THEN 0
                WHEN
                    (SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) != 0 AND ISNULL(M.DFR_DAY, 0) = 0 THEN 1
                ELSE ISNULL(M.DFR_DAY, 0)
            END AS Past_Due_Days
            ,0 as Past_Due_Interest
            ,NULL as Interest_Arrears
            ,NULL as Interest_Arrears_Frequency
            ,0 as Principal_Arreas
            ,0 as Principal_Arreas_Frequency
            --,0 as Penalty
            ,(SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) Penalty
            ,M.INSP_NO as Initial_Agreement_Number
            ,DBO.FN_FORMAT_DATE(M.LN_DT) as Initial_Agreement_Date --// YYYY-MM-DD
            ,NULL as Last_Agreement_Number
            ,NULL as Last_Agreement_Date
            ,DBO.FN_FORMAT_DATE(M.LN_DT) as Disbursement_Date --// YYYY-MM-DD
            ,DBO.FN_FORMAT_DATE(P.BS_DT) as Start_Date --// YYYY-MM-DD
            ,null as Restructuring_Date
            ,DBO.FN_FORMAT_DATE(P.XPR_DT) as Maturity_Date  --// YYYY-MM-DD
            ,NULL as Real_End_Date
            ,NULL as Condition_Date
            --,0 as Negative_Status_Of_The_Contract
            ,CASE
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 20 THEN 1
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 30 THEN 21
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 40 THEN 22
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 60 THEN 23
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 300 THEN 24

                END AS Negative_Status_Of_The_Contract

            ,NULL as Deliquency_Date
            ,NULL as Default_Date
            ,NULL as Default_Reason
            ,NULL as Default_Reason_Description
            ,NULL as Syndicated_Loan
            FROM FN_LOAN_MAST M, GT_CUST_COMM C, PT_LNC001TM P
            WHERE M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (P.INSP_NO LIKE '%$searchValue%' OR
                P.BPJS LIKE '%$searchValue%')
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $Contract_code = $record->Contract_code;
            $Branch = $record->Branch;
            $Phase_of_contract = $record->Phase_of_contract;
            $Contract_status = $record->Contract_status;
            $Type_of_contract = $record->Type_of_contract;
            $Credit_Classification = $record->Credit_Classification;
            $Purspose_Of_Financing = $record->Purspose_Of_Financing;
            $Name_Of_Insured = $record->Name_Of_Insured;
            $Economic_Sector = $record->Economic_Sector;
            $Currency_Of_Contract = $record->Currency_Of_Contract;
            $Interest_Rate = $record->Interest_Rate;
            $Type_Of_Interst_Rate = $record->Type_Of_Interst_Rate;
            $Total_Facility_Amount = $record->Total_Facility_Amount;
            $Total_Amount = $record->Total_Amount;
            $Initial_Total_Amount = $record->Initial_Total_Amount;
            $Total_Taken_Amount = $record->Total_Taken_Amount;
            $Outstanding_Amount = $record->Outstanding_Amount;
            $Pricipal_Balance = $record->Pricipal_Balance;
            $Past_Due_Amount = $record->Past_Due_Amount;
            $Past_Due_Days = $record->Past_Due_Days;
            $Past_Due_Interest = $record->Past_Due_Interest;
            $Interest_Arrears = $record->Interest_Arrears;
            $Interest_Arrears_Frequency = $record->Interest_Arrears_Frequency;
            $Principal_Arreas = $record->Principal_Arreas;
            $Principal_Arreas_Frequency = $record->Principal_Arreas_Frequency;
            $Penalty = $record->Penalty;
            $Initial_Agreement_Number = $record->Initial_Agreement_Number;
            $Initial_Agreement_Date = empty($record->Initial_Agreement_Date) ? "" : date('d/m/Y', strtotime($record->Initial_Agreement_Date));
            $Last_Agreement_Number = $record->Last_Agreement_Number;
            $Last_Agreement_Date = empty($record->Last_Agreement_Date) ? "" : date('d/m/Y', strtotime($record->Last_Agreement_Date));
            $Disbursement_Date = empty($record->Disbursement_Date) ? "" : date('d/m/Y', strtotime($record->Disbursement_Date));
            $Start_Date = empty($record->Start_Date) ? "" : date('d/m/Y', strtotime($record->Start_Date));
            $Restructuring_Date = empty($record->Restructuring_Date) ? "" : date('d/m/Y', strtotime($record->Restructuring_Date));
            $Maturity_Date = empty($record->Maturity_Date) ? "" : date('d/m/Y', strtotime($record->Maturity_Date));
            $Real_End_Date = empty($record->Real_End_Date) ? "" : date('d/m/Y', strtotime($record->Real_End_Date));
            $Condition_Date = empty($record->Condition_Date) ? "" : date('d/m/Y', strtotime($record->Condition_Date));
            $Negative_Status_Of_The_Contract = $record->Negative_Status_Of_The_Contract;
            $Deliquency_Date = empty($record->Deliquency_Date) ? "" : date('d/m/Y', strtotime($record->Deliquency_Date));
            $Default_Date = empty($record->Default_Date) ? "" : date('d/m/Y', strtotime($record->Default_Date));
            $Default_Reason = $record->Default_Reason;
            $Default_Reason_Description = $record->Default_Reason_Description;
            $Syndicated_Loan = $record->Syndicated_Loan;

            $data_arr[] = array(
                "Contract_code" => $Contract_code,
                "Branch" => $Branch,
                "Phase_of_contract" => $Phase_of_contract,
                "Contract_status" => $Contract_status,
                "Type_of_contract" => $Type_of_contract,
                "Credit_Classification" => $Credit_Classification,
                "Purspose_Of_Financing" => $Purspose_Of_Financing,
                "Name_Of_Insured" => $Name_Of_Insured,
                "Economic_Sector" => $Economic_Sector,
                "Currency_Of_Contract" => $Currency_Of_Contract,
                "Interest_Rate" => $Interest_Rate,
                "Type_Of_Interst_Rate" => $Type_Of_Interst_Rate,
                "Total_Facility_Amount" => $Total_Facility_Amount,
                "Total_Amount" => $Total_Amount,
                "Initial_Total_Amount" => $Initial_Total_Amount,
                "Total_Taken_Amount" => $Total_Taken_Amount,
                "Outstanding_Amount" => $Outstanding_Amount,
                "Pricipal_Balance" => $Pricipal_Balance,
                "Past_Due_Amount" => $Past_Due_Amount,
                "Past_Due_Days" => $Past_Due_Days,
                "Past_Due_Interest" => $Past_Due_Interest,
                "Interest_Arrears" => $Interest_Arrears,
                "Interest_Arrears_Frequency" => $Interest_Arrears_Frequency,
                "Principal_Arreas" => $Principal_Arreas,
                "Principal_Arreas_Frequency" => $Principal_Arreas_Frequency,
                "Penalty" => $Penalty,
                "Initial_Agreement_Number" => $Initial_Agreement_Number,
                "Initial_Agreement_Date" => $Initial_Agreement_Date,
                "Last_Agreement_Number" => $Last_Agreement_Number,
                "Last_Agreement_Date" => $Last_Agreement_Date,
                "Disbursement_Date" => $Disbursement_Date,
                "Start_Date" => $Start_Date,
                "Restructuring_Date" => $Restructuring_Date,
                "Maturity_Date" => $Maturity_Date,
                "Real_End_Date" => $Real_End_Date,
                "Condition_Date" => $Condition_Date,
                "Negative_Status_Of_The_Contract" => $Negative_Status_Of_The_Contract,
                "Deliquency_Date" => $Deliquency_Date,
                "Default_Date" => $Default_Date,
                "Default_Reason" => $Default_Reason,
                "Default_Reason_Description" => $Default_Reason_Description,
                "Syndicated_Loan" => $Syndicated_Loan,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportDataContract(Request $request)
    {
        $fileName = 'ContractData.csv';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // $fromDate = $from;
        // $toDate = $to;

        $getDataContract = DB::connection('sqlsrv')->select("
            SELECT  P.INSP_NO as Contract_code
            ,P.BPJS as Branch
            ,1 as Phase_of_contract
            ,2 as Contract_status
            ,1 as Type_of_contract
            ,1 as Credit_Classification
            ,74 as Purspose_Of_Financing
            ,P.BPJS as Name_Of_Insured
            ,205 as Economic_Sector
            ,66 as Currency_Of_Contract
            ,convert(varchar,convert(numeric(5,2),P.CONFIRM_NML_IRT))  Interest_Rate
            ,1 as Type_Of_Interst_Rate
            ,1000000000 as Total_Facility_Amount
            ,M.LN_AM as Total_Amount
            ,0 as Initial_Total_Amount
            ,0 as Total_Taken_Amount
            ,M.LN_BAL_AM + 0 + (SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) as Outstanding_Amount --SECARA RUMUS = Principal Ballance + Pas Due Interest + Penalty
            --,M.LN_AM as Principal_Balance
            ,M.LN_BAL_AM as Pricipal_Balance
            ,(SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) as Past_Due_Amount  -- Principal Arreas + Past Due Interst + Penalty
            --,ISNULL(M.DFR_DAY, 0) AS Past_Due_Days
            ,CASE
                WHEN
                    (SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) = 0 THEN 0
                WHEN
                    (SELECT ISNULL(SUM(DFR_AMT), 0) as penalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) != 0 AND ISNULL(M.DFR_DAY, 0) = 0 THEN 1
                ELSE ISNULL(M.DFR_DAY, 0)
            END AS Past_Due_Days
            ,0 as Past_Due_Interest
            ,NULL as Interest_Arrears
            ,NULL as Interest_Arrears_Frequency
            ,0 as Principal_Arreas
            ,0 as Principal_Arreas_Frequency
            --,0 as Penalty
            ,(SELECT ISNULL(SUM(DFR_AMT), 0) as Upenalty FROM FN_LOAN_PLAN WHERE ACT_NO = M.ACT_NO) Penalty
            ,M.INSP_NO as Initial_Agreement_Number
            ,DBO.FN_FORMAT_DATE(M.LN_DT) as Initial_Agreement_Date --// YYYY-MM-DD
            ,NULL as Last_Agreement_Number
            ,NULL as Last_Agreement_Date
            ,DBO.FN_FORMAT_DATE(M.LN_DT) as Disbursement_Date --// YYYY-MM-DD
            ,DBO.FN_FORMAT_DATE(P.BS_DT) as Start_Date --// YYYY-MM-DD
            ,null as Restructuring_Date
            ,DBO.FN_FORMAT_DATE(P.XPR_DT) as Maturity_Date  --// YYYY-MM-DD
            ,NULL as Real_End_Date
            ,NULL as Condition_Date
            --,0 as Negative_Status_Of_The_Contract
            ,CASE
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 20 THEN 1
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 30 THEN 21
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 40 THEN 22
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 60 THEN 23
                WHEN
                    ISNULL(M.DFR_DAY, 0) < 300 THEN 24

                END AS Negative_Status_Of_The_Contract

            ,NULL as Deliquency_Date
            ,NULL as Default_Date
            ,NULL as Default_Reason
            ,NULL as Default_Reason_Description
            ,NULL as Syndicated_Loan
            FROM FN_LOAN_MAST M, GT_CUST_COMM C, PT_LNC001TM P
            WHERE M.CUS_NO = C.CUS_NO
            AND M.INSP_NO = P.INSP_NO
            AND M.COM_CD = C.COM_CD
            AND M.COM_CD = P.COM_CD
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Contract_code', 'Branch', 'Phase_of_contract', 'Contract_status', 'Type_of_contract', 'Credit_Classification', 'Purspose_Of_Financing', 'Name_Of_Insured', 'Economic_Sector', 'Currency_Of_Contract', 'Interest_Rate', 'Type_Of_Interst_Rate', 'Total_Facility_Amount', 'Total_Amount', 'Initial_Total_Amount', 'Total_Taken_Amount', 'Outstanding_Amount', 'Pricipal_Balance', 'Past_Due_Amount', 'Past_Due_Days', 'Past_Due_Interest', 'Interest_Arrears', 'Interest_Arrears_Frequency', 'Principal_Arreas', 'Principal_Arreas_Frequency', 'Penalty', 'Initial_Agreement_Number', 'Initial_Agreement_Date', 'Last_Agreement_Number', 'Last_Agreement_Date', 'Disbursement_Date', 'Start_Date', 'Restructuring_Date', 'Maturity_Date', 'Real_End_Date', 'Condition_Date', 'Negative_Status_Of_The_Contract', 'Deliquency_Date', 'Default_Date', 'Default_Reason', 'Default_Reason_Description', 'Syndicated_Loan');

        $callback = function () use ($getDataContract, $columns) {
            $file = fopen('php://output', 'w'); //<-here. name of file is written in headers
            fputcsv($file, $columns, '|', '"');
            foreach ($getDataContract as $res) {
                fputcsv($file, [$res->Contract_code,
                                $res->Branch,
                                $res->Phase_of_contract,
                                $res->Contract_status,
                                $res->Type_of_contract,
                                $res->Credit_Classification,
                                $res->Purspose_Of_Financing,
                                $res->Name_Of_Insured,
                                $res->Economic_Sector,
                                $res->Currency_Of_Contract,
                                $res->Interest_Rate,
                                $res->Type_Of_Interst_Rate,
                                $res->Total_Facility_Amount,
                                $res->Total_Amount,
                                $res->Initial_Total_Amount,
                                $res->Total_Taken_Amount,
                                $res->Outstanding_Amount,
                                $res->Pricipal_Balance,
                                $res->Past_Due_Amount,
                                $res->Past_Due_Days,
                                $res->Past_Due_Interest,
                                $res->Interest_Arrears,
                                $res->Interest_Arrears_Frequency,
                                $res->Principal_Arreas,
                                $res->Principal_Arreas_Frequency,
                                $res->Penalty,
                                $res->Initial_Agreement_Number,
                                $res->Initial_Agreement_Date,
                                $res->Last_Agreement_Number,
                                $res->Last_Agreement_Date,
                                $res->Disbursement_Date,
                                $res->Start_Date,
                                $res->Restructuring_Date,
                                $res->Maturity_Date,
                                $res->Real_End_Date,
                                $res->Condition_Date,
                                $res->Negative_Status_Of_The_Contract,
                                $res->Deliquency_Date,
                                $res->Default_Date,
                                $res->Default_Reason,
                                $res->Default_Reason_Description,
                                $res->Syndicated_Loan],
                        '|', '"');
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function dataIndividual()
    {
        return view('admin.data-individual', ['activeSidebar' => 'pefindo']);
    }

    public function getDataIndividual(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT  COUNT(*) as allcount
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT  COUNT(*) as allcount
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (C.CUS_NO LIKE '%$searchValue%' OR
                P.INSP_NO LIKE '%$searchValue%' OR
                C.CUS_NM LIKE '%$searchValue%' OR
                P.BIRTH LIKE '%$searchValue%' OR
                P.NPWP LIKE '%$searchValue%' OR
                P.KTP LIKE '%$searchValue%' OR
                P.PASSPORT_INFO LIKE '%$searchValue%' OR
                DBO.FN_GET_GT_ADDRRESS(M.CUS_NO,'120001') LIKE '%$searchValue%' OR
                DBO.GT_FT_GETPOSTNM(kabupaten) LIKE '%$searchValue%' OR
                P.CO_TEL_NO LIKE '%$searchValue%')
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT  C.CUS_NO as Customer_Code
            ,P.INSP_NO as Contract_Code
            ,1 as Role_of_Customer
            ,C.CUS_NM as Full_Name
            ,C.CUS_NM as Alias
            ,NULL as Mother_Maiden_Name
            ,1 as Classification_Of_Individual
            ,CASE WHEN C.SEX_CD = '108100' THEN 1 ELSE 2 END as Gender
            ,DBO.FN_FORMAT_DATE(P.BIRTHDAY) as Date_Of_Birth
            ,P.BIRTH as Place_Of_Birth
            ,0 as Social_Status
            ,97 AS Citizenship
            ,CASE
                WHEN P.JOB_CD = '104001' THEN 125
                WHEN P.JOB_CD = '104002' THEN 131
                WHEN P.JOB_CD = '104003' THEN 9999
                WHEN P.JOB_CD = '104004' THEN 127
                WHEN P.JOB_CD = '104005' THEN 122
                WHEN P.JOB_CD = '104006' THEN 9999
                WHEN P.JOB_CD = '104007' THEN 9999
            ELSE 9999 END AS Employment
            ,SUBSTRING(Q.POT_NM, 1, 62) as Employer_Name
            ,205 as Employer_Sector
            ,CASE
                WHEN P.LAST_SCHCAR_CD = '110001' THEN 99
                WHEN P.LAST_SCHCAR_CD = '110002' THEN 99
                WHEN P.LAST_SCHCAR_CD = '110003' THEN 99
                WHEN P.LAST_SCHCAR_CD = '110004' THEN 8
                WHEN P.LAST_SCHCAR_CD = '110005' THEN 9
                WHEN P.LAST_SCHCAR_CD = '110006' THEN 10
                WHEN P.LAST_SCHCAR_CD = '110007' THEN 11
            ELSE 99 END AS  Education
            ,P.NPWP as NPWP
            ,P.KTP as KTP
            ,P.PASSPORT_INFO as Passport_Number
            ,NULL as Passport_Issuer_Country
            ,SUBSTRING(DBO.FN_GET_GT_ADDRRESS(M.CUS_NO,'120001'), 1, 100) as Street
            ,DBO.GT_FT_GETPOSTNM(kabupaten) as City
            ,R.KODE_POS as Postal_Code
            ,NULL as District
            ,NULL as Parish
            ,97 as Country
            ,NULL as Address_Line
            ,P.CO_TEL_NO as Mobile_Phone
            ,NULL as Fixed_Line
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (C.CUS_NO LIKE '%$searchValue%' OR
                P.INSP_NO LIKE '%$searchValue%' OR
                C.CUS_NM LIKE '%$searchValue%' OR
                P.BIRTH LIKE '%$searchValue%' OR
                P.NPWP LIKE '%$searchValue%' OR
                P.KTP LIKE '%$searchValue%' OR
                P.PASSPORT_INFO LIKE '%$searchValue%' OR
                DBO.FN_GET_GT_ADDRRESS(M.CUS_NO,'120001') LIKE '%$searchValue%' OR
                DBO.GT_FT_GETPOSTNM(kabupaten) LIKE '%$searchValue%' OR
                P.CO_TEL_NO LIKE '%$searchValue%')
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $Customer_Code = $record->Customer_Code;
            $Contract_Code = $record->Contract_Code;
            $Role_of_Customer = $record->Role_of_Customer;
            $Full_Name = $record->Full_Name;
            $Alias = $record->Alias;
            $Mother_Maiden_Name = $record->Mother_Maiden_Name;
            $Classification_Of_Individual = $record->Classification_Of_Individual;
            $Gender = $record->Gender;
            $Date_Of_Birth = $record->Date_Of_Birth;
            $Place_Of_Birth = $record->Place_Of_Birth;
            $Social_Status = $record->Social_Status;
            $Citizenship = $record->Citizenship;
            $Employment = $record->Employment;
            $Employer_Name = $record->Employer_Name;
            $Employer_Sector = $record->Employer_Sector;
            $Education = $record->Education;
            $NPWP = $record->NPWP;
            $KTP = $record->KTP;
            $Passport_Number = $record->Passport_Number;
            $Passport_Issuer_Country = $record->Passport_Issuer_Country;
            $Street = $record->Street;
            $City = $record->City;
            $Postal_Code = $record->Postal_Code;
            $District = $record->District;
            $Parish = $record->Parish;
            $Country = $record->Country;
            $Address_Line = $record->Address_Line;
            $Mobile_Phone = $record->Mobile_Phone;
            $Fixed_Line = $record->Fixed_Line;

            $data_arr[] = array(
                "Customer_Code" => $Customer_Code,
                "Contract_Code" => $Contract_Code,
                "Role_of_Customer" => $Role_of_Customer,
                "Full_Name" => $Full_Name,
                "Alias" => $Alias,
                "Mother_Maiden_Name" => $Mother_Maiden_Name,
                "Classification_Of_Individual" => $Classification_Of_Individual,
                "Gender" => $Gender,
                "Date_Of_Birth" => $Date_Of_Birth,
                "Place_Of_Birth" => $Place_Of_Birth,
                "Social_Status" => $Social_Status,
                "Citizenship" => $Citizenship,
                "Employment" => $Employment,
                "Employer_Name" => $Employer_Name,
                "Employer_Sector" => $Employer_Sector,
                "Education" => $Education,
                "NPWP" => $NPWP,
                "KTP" => $KTP,
                "Passport_Number" => $Passport_Number,
                "Passport_Issuer_Country" => $Passport_Issuer_Country,
                "Street" => $Street,
                "City" => $City,
                "Postal_Code" => $Postal_Code,
                "District" => $District,
                "Parish" => $Parish,
                "Country" => $Country,
                "Address_Line" => $Address_Line,
                "Mobile_Phone" => $Mobile_Phone,
                "Fixed_Line" => $Fixed_Line,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportDataIndividual(Request $request)
    {
        $fileName = 'Individual.csv';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $getDataContract = DB::connection('sqlsrv')->select("
            SELECT  C.CUS_NO as Customer_Code
            ,P.INSP_NO as Contract_Code
            ,1 as Role_of_Customer
            ,C.CUS_NM as Full_Name
            ,C.CUS_NM as Alias
            ,NULL as Mother_Maiden_Name
            ,1 as Classification_Of_Individual
            ,CASE WHEN C.SEX_CD = '108100' THEN 1 ELSE 2 END as Gender
            ,DBO.FN_FORMAT_DATE(P.BIRTHDAY) as Date_Of_Birth
            ,P.BIRTH as Place_Of_Birth
            ,0 as Social_Status
            ,97 AS Citizenship
            ,CASE
                WHEN P.JOB_CD = '104001' THEN 125
                WHEN P.JOB_CD = '104002' THEN 131
                WHEN P.JOB_CD = '104003' THEN 9999
                WHEN P.JOB_CD = '104004' THEN 127
                WHEN P.JOB_CD = '104005' THEN 122
                WHEN P.JOB_CD = '104006' THEN 9999
                WHEN P.JOB_CD = '104007' THEN 9999
            ELSE 9999 END AS Employment
            ,SUBSTRING(Q.POT_NM, 1, 62) as Employer_Name
            ,205 as Employer_Sector
            ,CASE
                WHEN P.LAST_SCHCAR_CD = '110001' THEN 99
                WHEN P.LAST_SCHCAR_CD = '110002' THEN 99
                WHEN P.LAST_SCHCAR_CD = '110003' THEN 99
                WHEN P.LAST_SCHCAR_CD = '110004' THEN 8
                WHEN P.LAST_SCHCAR_CD = '110005' THEN 9
                WHEN P.LAST_SCHCAR_CD = '110006' THEN 10
                WHEN P.LAST_SCHCAR_CD = '110007' THEN 11
            ELSE 99 END AS  Education
            ,P.NPWP as NPWP
            ,P.KTP as KTP
            ,P.PASSPORT_INFO as Passport_Number
            ,NULL as Passport_Issuer_Country
            ,SUBSTRING(DBO.FN_GET_GT_ADDRRESS(M.CUS_NO,'120001'), 1, 100) as Street
            ,DBO.GT_FT_GETPOSTNM(kabupaten) as City
            ,R.KODE_POS as Postal_Code
            ,NULL as District
            ,NULL as Parish
            ,97 as Country
            ,NULL as Address_Line
            ,P.CO_TEL_NO as Mobile_Phone
            ,NULL as Fixed_Line
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Customer_Code', 'Contract_Code', 'Role_of_Customer', 'Full_Name', 'Alias', 'Mother_Maiden_Name', 'Classification_Of_Individual', 'Gender', 'Date_Of_Birth', 'Place_Of_Birth', 'Social_Status', 'Citizenship', 'Employment', 'Employer_Name', 'Employer_Sector', 'Education', 'NPWP', 'KTP', 'Passport_Number', 'Passport_Issuer_Country', 'Street', 'City', 'Postal_Code', 'District', 'Parish', 'Country', 'Address_Line', 'Mobile_Phone', 'Fixed_Line');

        $callback = function () use ($getDataContract, $columns) {
            $file = fopen('php://output', 'w'); //<-here. name of file is written in headers
            fputcsv($file, $columns, '|', '"');
            foreach ($getDataContract as $res) {
                fputcsv($file, [$res->Customer_Code,
                                $res->Contract_Code,
                                $res->Role_of_Customer,
                                $res->Full_Name,
                                $res->Alias,
                                $res->Mother_Maiden_Name,
                                $res->Classification_Of_Individual,
                                $res->Gender,
                                $res->Date_Of_Birth,
                                $res->Place_Of_Birth,
                                $res->Social_Status,
                                $res->Citizenship,
                                $res->Employment,
                                $res->Employer_Name,
                                $res->Employer_Sector,
                                $res->Education,
                                $res->NPWP,
                                $res->KTP,
                                $res->Passport_Number,
                                $res->Passport_Issuer_Country,
                                $res->Street,
                                $res->City,
                                $res->Postal_Code,
                                $res->District,
                                $res->Parish,
                                $res->Country,
                                $res->Address_Line,
                                $res->Mobile_Phone,
                                $res->Fixed_Line],
                        '|', '"');
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function dataCompany()
    {
       // $company = DB::connection('sqlsrv')->select("");

        return view("admin.data-company", ['activeSidebar' => 'pefindo']);
    }

    // public function exportDataCompany(Request $request)
    // {
    //     $fileName = 'Company.csv';

    //     // get range date
    //     $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
    //     $toDateDefault = date('Ymd');
    //     $fromDate = $request->get('fromDate', $fromDateDefault);
    //     $toDate = $request->get('toDate', $toDateDefault);

    //     $getDataContract = DB::connection('sqlsrv')->select("

    //     ");

    //     $headers = array(
    //         "Content-type"        => "text/csv",
    //         "Content-Disposition" => "attachment; filename=$fileName",
    //         "Pragma"              => "no-cache",
    //         "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
    //         "Expires"             => "0"
    //     );

    //     $columns = array('Customer_Code', 'Contract_Code', 'Role_Of_Customer', 'Company_Name', 'Company_Alias', 'Group_Name', 'Status', 'Establishment_Date', 'Establishment_Location', 'Category', 'Market_Listed', 'Economic_Sector', 'Registration_Number', 'Latest_Deed_Number', 'Latest_Date_Of_Deed', 'NPWP', 'Street', 'City', 'Postal_Code', 'District', 'Parish', 'Country', 'Address_Line', 'Mobile_Phone', 'Fixed_Line');

    //     $callback = function () use ($getDataContract, $columns) {
    //         $file = fopen('php://output', 'w'); //<-here. name of file is written in headers
    //         fputcsv($file, $columns, '|', '"');
    //         foreach ($getDataContract as $res) {
    //             fputcsv($file, [$res->Customer_Code,
    //                             $res->Contract_Code,
    //                             $res->Role_Of_Customer,
    //                             $res->Company_Name,
    //                             $res->Company_Alias,
    //                             $res->Group_Name,
    //                             $res->Status,
    //                             $res->Establishment_Date,
    //                             $res->Establishment_Location,
    //                             $res->Category,
    //                             $res->Market_Listed,
    //                             $res->Economic_Sector,
    //                             $res->Registration_Number,
    //                             $res->Latest_Deed_Number,
    //                             $res->Latest_Date_Of_Deed,
    //                             $res->NPWP,
    //                             $res->Street,
    //                             $res->City,
    //                             $res->Postal_Code,
    //                             $res->District,
    //                             $res->Parish,
    //                             $res->Country,
    //                             $res->Address_Line,
    //                             $res->Mobile_Phone,
    //                             $res->Fixed_Line],
    //                     '|', '"');
    //         }
    //         fclose($file);
    //     };

    //     return response()->stream($callback, 200, $headers);
    // }

    public function dataCollateral()
    {
        return view('admin.data-collateral', ['activeSidebar' => 'pefindo']);
    }

    public function getDataCollateral(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
            ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
            ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (P.INSP_NO LIKE '%$searchValue%' OR
                C.CUS_NO LIKE '%$searchValue%' OR
                C.CUS_NM LIKE '%$searchValue%')
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
            P.INSP_NO as Collateral_Code
            ,C.CUS_NO as Customer_Code
            --,M.INSP_NO as Contract_code
            ,'' as Branch
            ,99 as Collateral_Type
            ,'SKU' as Collateral_Description
            ,0 as Collateral_Tax_Value
            ,0 as Collateral_Bank_Value
            ,0 as Collateral_Apprasial_Value
            ,null as Valuation_Date
            ,C.CUS_NM as Collateral_Owner_Name
            ,'SKU' as Proof_Of_Ownership
            ,99 as Securty_Assigment_Type
            ,null as Proportion
            ,2 as Collateral_Insurance
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
            ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (P.INSP_NO LIKE '%$searchValue%' OR
                C.CUS_NO LIKE '%$searchValue%' OR
                C.CUS_NM LIKE '%$searchValue%')
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $Collateral_Code = $record->Collateral_Code;
            $Customer_Code = $record->Customer_Code;
            $Branch = $record->Branch;
            $Collateral_Type = $record->Collateral_Type;
            $Collateral_Description = $record->Collateral_Description;
            $Collateral_Tax_Value = $record->Collateral_Tax_Value;
            $Collateral_Bank_Value = $record->Collateral_Bank_Value;
            $Collateral_Apprasial_Value = $record->Collateral_Apprasial_Value;
            $Valuation_Date = $record->Valuation_Date;
            $Collateral_Owner_Name = $record->Collateral_Owner_Name;
            $Proof_Of_Ownership = $record->Proof_Of_Ownership;
            $Securty_Assigment_Type = $record->Securty_Assigment_Type;
            $Proportion = $record->Proportion;
            $Collateral_Insurance = $record->Collateral_Insurance;

            $data_arr[] = array(
                "Collateral_Code" => $Collateral_Code,
                "Customer_Code" => $Customer_Code,
                "Branch" => $Branch,
                "Collateral_Type" => $Collateral_Type,
                "Collateral_Description" => $Collateral_Description,
                "Collateral_Tax_Value" => $Collateral_Tax_Value,
                "Collateral_Bank_Value" => $Collateral_Bank_Value,
                "Collateral_Apprasial_Value" => $Collateral_Apprasial_Value,
                "Valuation_Date" => $Valuation_Date,
                "Collateral_Owner_Name" => $Collateral_Owner_Name,
                "Proof_Of_Ownership" => $Proof_Of_Ownership,
                "Securty_Assigment_Type" => $Securty_Assigment_Type,
                "Proportion" => $Proportion,
                "Collateral_Insurance" => $Collateral_Insurance,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportDataCollateral(Request $request)
    {
        $fileName = 'Collateral.csv';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $getDataContract = DB::connection('sqlsrv')->select("
            SELECT
            P.INSP_NO as Collateral_Code
            ,C.CUS_NO as Customer_Code
            --,M.INSP_NO as Contract_code
            ,'' as Branch
            ,99 as Collateral_Type
            ,'SKU' as Collateral_Description
            ,0 as Collateral_Tax_Value
            ,0 as Collateral_Bank_Value
            ,0 as Collateral_Apprasial_Value
            ,null as Valuation_Date
            ,C.CUS_NM as Collateral_Owner_Name
            ,'SKU' as Proof_Of_Ownership
            ,99 as Securty_Assigment_Type
            ,null as Proportion
            ,2 as Collateral_Insurance
            FROM PT_LNC001TM P
            LEFT OUTER JOIN PT_LNC031TM R
            ON P.INSP_NO = R.INSP_NO
            AND R.AAB_CD = '120001'
            LEFT OUTER JOIN FN_LOAN_MAST M
            ON P.INSP_NO = M.INSP_NO
            LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
            ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Collateral_Code', 'Customer_Code', 'Branch', 'Collateral_Type', 'Collateral_Description', 'Collateral_Tax_Value', 'Collateral_Bank_Value', 'Collateral_Apprasial_Value', 'Valuation_Date', 'Collateral_Owner_Name', 'Proof_Of_Ownership', 'Securty_Assigment_Type', 'Proportion', 'Collateral_Insurance');

        $callback = function () use ($getDataContract, $columns) {
            $file = fopen('php://output', 'w'); //<-here. name of file is written in headers
            fputcsv($file, $columns, '|', '"');
            foreach ($getDataContract as $res) {
                fputcsv($file, [$res->Collateral_Code,
                                $res->Customer_Code,
                                $res->Branch,
                                $res->Collateral_Type,
                                $res->Collateral_Description,
                                $res->Collateral_Tax_Value,
                                $res->Collateral_Bank_Value,
                                $res->Collateral_Apprasial_Value,
                                $res->Valuation_Date,
                                $res->Collateral_Owner_Name,
                                $res->Proof_Of_Ownership,
                                $res->Securty_Assigment_Type,
                                $res->Proportion,
                                $res->Collateral_Insurance],
                        '|', '"');
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function dataSubjectRelation()
    {
        return view('admin.data-subject-relation', ['activeSidebar' => 'pefindo']);
    }

    public function getDataSubjectRelation(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
                FROM PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R
                ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M
                ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
            count(*) as allcount
                FROM PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R
                ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M
                ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (C.CUS_NO LIKE '%$searchValue%' OR
                P.NPWP LIKE '%$searchValue%' OR
                P.KTP LIKE '%$searchValue%' OR
                P.CUS_NM LIKE '%$searchValue%' OR
                DBO.FN_GET_GT_ADDRRESS(C.CUS_NO,'120001') LIKE '%$searchValue%' OR
                DBO.GT_FT_GETPOSTNM(kabupaten) LIKE '%$searchValue%')
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
            C.CUS_NO as Customer_Code_Of_Primary_Subject
            ,99 as Relation_Type
            ,C.CUS_NO as Customer_Code_Of_Secondary_Subject
            ,1 as Subject_Type
            ,P.NPWP as NPWP
            ,P.KTP as KTP
            ,P.CUS_NM as Full_Name
            ,CASE WHEN C.SEX_CD = '108100' THEN 1 ELSE 2 END as Gender
            ,null as Parts_Of_Guranteed
            ,null as Ownership_Share
            ,0 as Category
            ,SUBSTRING(DBO.FN_GET_GT_ADDRRESS(C.CUS_NO,'120001'), 1, 100) as Street
            ,DBO.GT_FT_GETPOSTNM(kabupaten) as City
            ,null as District
            ,null as Parish
            ,null as Adress_Line
                FROM PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R
                ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M
                ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (C.CUS_NO LIKE '%$searchValue%' OR
                P.NPWP LIKE '%$searchValue%' OR
                P.KTP LIKE '%$searchValue%' OR
                P.CUS_NM LIKE '%$searchValue%' OR
                DBO.FN_GET_GT_ADDRRESS(C.CUS_NO,'120001') LIKE '%$searchValue%' OR
                DBO.GT_FT_GETPOSTNM(kabupaten) LIKE '%$searchValue%')
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $Customer_Code_Of_Primary_Subject = $record->Customer_Code_Of_Primary_Subject;
            $Relation_Type = $record->Relation_Type;
            $Customer_Code_Of_Secondary_Subject = $record->Customer_Code_Of_Secondary_Subject;
            $Subject_Type = $record->Subject_Type;
            $NPWP = $record->NPWP;
            $KTP = $record->KTP;
            $Full_Name = $record->Full_Name;
            $Gender = $record->Gender;
            $Parts_Of_Guranteed = $record->Parts_Of_Guranteed;
            $Ownership_Share = $record->Ownership_Share;
            $Category = $record->Category;
            $Street = $record->Street;
            $City = $record->City;
            $District = $record->District;
            $Parish = $record->Parish;
            $Adress_Line = $record->Adress_Line;

            $data_arr[] = array(
                "Customer_Code_Of_Primary_Subject" => $Customer_Code_Of_Primary_Subject,
                "Relation_Type" => $Relation_Type,
                "Customer_Code_Of_Secondary_Subject" => $Customer_Code_Of_Secondary_Subject,
                "Subject_Type" => $Subject_Type,
                "NPWP" => $NPWP,
                "KTP" => $KTP,
                "Full_Name" => $Full_Name,
                "Gender" => $Gender,
                "Parts_Of_Guranteed" => $Parts_Of_Guranteed,
                "Ownership_Share" => $Ownership_Share,
                "Category" => $Category,
                "Street" => $Street,
                "City" => $City,
                "District" => $District,
                "Parish" => $Parish,
                "Adress_Line" => $Adress_Line,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportDataSubjectRelation(Request $request)
    {
        $fileName = 'SubjectRelation.csv';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $getDataContract = DB::connection('sqlsrv')->select("
            SELECT
            C.CUS_NO as Customer_Code_Of_Primary_Subject
            ,99 as Relation_Type
            ,C.CUS_NO as Customer_Code_Of_Secondary_Subject
            ,1 as Subject_Type
            ,P.NPWP as NPWP
            ,P.KTP as KTP
            ,P.CUS_NM as Full_Name
            ,CASE WHEN C.SEX_CD = '108100' THEN 1 ELSE 2 END as Gender
            ,null as Parts_Of_Guranteed
            ,null as Ownership_Share
            ,0 as Category
            ,SUBSTRING(DBO.FN_GET_GT_ADDRRESS(C.CUS_NO,'120001'), 1, 100) as Street
            ,DBO.GT_FT_GETPOSTNM(kabupaten) as City
            ,null as District
            ,null as Parish
            ,null as Adress_Line
                FROM PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R
                ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M
                ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C
            ON P.KTP = C.KTP
            LEFT OUTER JOIN IN_POT001TM Q
                ON R.INSP_NO = Q.POT_NO
            WHERE P.COM_CD = '01' AND  M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Customer_Code_Of_Primary_Subject', 'Relation_Type', 'Customer_Code_Of_Secondary_Subject', 'Subject_Type', 'NPWP', 'KTP', 'Full_Name', 'Gender', 'Parts_Of_Guranteed', 'Ownership_Share', 'Category', 'Street', 'City', 'District', 'Parish', 'Adress_Line');

        $callback = function () use ($getDataContract, $columns) {
            $file = fopen('php://output', 'w'); //<-here. name of file is written in headers
            fputcsv($file, $columns, '|', '"');
            foreach ($getDataContract as $res) {
                fputcsv($file, [$res->Customer_Code_Of_Primary_Subject,
                                $res->Relation_Type,
                                $res->Customer_Code_Of_Secondary_Subject,
                                $res->Subject_Type,
                                $res->NPWP,
                                $res->KTP,
                                $res->Full_Name,
                                $res->Gender,
                                $res->Parts_Of_Guranteed,
                                $res->Ownership_Share,
                                $res->Category,
                                $res->Street,
                                $res->City,
                                $res->District,
                                $res->Parish,
                                $res->Adress_Line],
                        '|', '"');
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function exportAllData(Request $request)
    {
        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $contract = $this->exportDataContract($fromDate, $toDate);

        return [
            'contract' => $contract
        ];
    }





    public function requestSmartSearch(Request $request)
    {
        $ktp  = $request->ktp;
        $DateOfBirth  = $request->DateOfBirth;
        $fullname  = $request->fullname;
        $InquiryReason = $request->InquiryReason;

        // Value Inquiry-----------------------
        // ProvidingFacilities
        // OperationalRiskManagement
        // MonitoringDebtorOrCustomer
        // FulfilRegulationRequirements
        // AnotherReason (Not Recomended)


        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb5="http://creditinfo.com/CB5" xmlns:smar="http://creditinfo.com/CB5/v5.53/SmartSearch">
        <soapenv:Header/>
        <soapenv:Body>
        <cb5:SmartSearchIndividual>
        <cb5:query>
        <smar:InquiryReason>'.$InquiryReason.'</smar:InquiryReason>
        <smar:InquiryReasonText/>
        <smar:Parameters>
        <smar:DateOfBirth>'.$DateOfBirth.'</smar:DateOfBirth>
        <smar:FullName>'.$fullname.'</smar:FullName>
        <smar:IdNumbers>
        <smar:IdNumberPairIndividual>
        <smar:IdNumber>'.$ktp.'</smar:IdNumber>
        <smar:IdNumberType>KTP</smar:IdNumberType>
        </smar:IdNumberPairIndividual>
        </smar:IdNumbers>
        </smar:Parameters>
        </cb5:query>
        </cb5:SmartSearchIndividual>
        </soapenv:Body>
        </soapenv:Envelope>';   // data from the form, e.g. some ID number

        $soapUser = 'wahyu_okp2p';
        $soapPassword = 'Pefindoadmin123';

        $headers = array(
                    "Content-type: text/xml",
                    "Accept: text/xml",
                    "SOAPAction: http://creditinfo.com/CB5/IReportPublicServiceBase/SmartSearchIndividual",
                ); //SOAPAction: your op URL


        // PHP cURL  for https connection with auth
        $ch = curl_init();

        // $url = 'https://cbs5bodemo2.pefindobirokredit.com/WsReport/v5.53/Service.svc';   // DEVELOPMENT
        $url = 'https://bo.pefindobirokredit.com/WsReport/v5.53/service.svc';           // STAGING

        // Check if initialization had gone wrong*
        if ($ch === false) {
            throw new Exception('Gagal Inquiry Pefindo');
        }

        try {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch);
            curl_close($ch);

            // converting
            $response1 = str_replace("<soap:Body>","",$response);
            $response2 = str_replace("</soap:Body>","",$response1);

            return $jsonDat = XmlToJson::soapxml2json($response1);

            // convertingc to XML
            //return $parser = simplexml_load_string($response2);

        } catch(Exception $e) {
            return trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);
        }
    }

    public function requestCustomReport(Request $request)
    {
        $idpefindo  = $request->idpefindo;
        $reportdate  = $request->reportdate;
        $InquiryReason = $request->InquiryReason;

        // Value Inquiry-----------------------
        // ProvidingFacilities
        // OperationalRiskManagement
        // MonitoringDebtorOrCustomer
        // FulfilRegulationRequirements
        // AnotherReason (Not Recomended)


        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb5="http://creditinfo.com/CB5" xmlns:cus="http://creditinfo.com/CB5/v5.53/CustomReport" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
        <soapenv:Header/>
        <soapenv:Body>
        <cb5:GetCustomReport>
        <cb5:parameters>
        <cus:Consent>true</cus:Consent>
        <cus:IDNumber>'.$idpefindo.'</cus:IDNumber>
        <cus:IDNumberType>PefindoId</cus:IDNumberType>
        <cus:InquiryReason>'.$InquiryReason.'</cus:InquiryReason>
        <cus:InquiryReasonText/>
        <cus:ReportDate>'.$reportdate.'</cus:ReportDate>
        <cus:Sections>
        <arr:string>CIP</arr:string>
        <arr:string>SubjectInfo</arr:string>
        <arr:string>SubjectHistory</arr:string>
        <arr:string>ContractList</arr:string>
        </cus:Sections>
        <cus:SubjectType>Individual</cus:SubjectType>
        </cb5:parameters>
        </cb5:GetCustomReport>
        </soapenv:Body>
        </soapenv:Envelope>';   // data from the form, e.g. some ID number

        $soapUser = 'wahyu_okp2p';
        $soapPassword = 'Pefindoadmin123';

        $headers = array(
                    "Content-type: text/xml",
                    "Accept: text/xml",
                    "SOAPAction: http://creditinfo.com/CB5/IReportPublicServiceBase/GetCustomReport",
                ); //SOAPAction: your op URL

        // $url = 'https://cbs5bodemo2.pefindobirokredit.com/WsReport/v5.53/service.svc';      // DEVELOPMENT
        $url = 'https://bo.pefindobirokredit.com/WsReport/v5.53/service.svc';               // STAGING



        // PHP cURL  for https connection with auth
        $ch = curl_init();

        // Check if initialization had gone wrong*
        if ($ch === false) {
            throw new Exception('Gagal Inquiry Pefindo');
        }

        try {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch);
            curl_close($ch);

            // converting
            $response1 = str_replace("<soap:Body>","",$response);
            $response2 = str_replace("</soap:Body>","",$response1);

            return $jsonDat = XmlToJson::soapxml2json($response1);

            // convertingc to XML
           // return $parser = simplexml_load_string($response2);

        } catch(Exception $e) {
            return trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);
        }
    }




}
