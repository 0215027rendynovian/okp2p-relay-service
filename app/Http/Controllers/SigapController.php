<?php

namespace App\Http\Controllers;

use App\BaPemblokiran;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Helpers\ListHelper;
use App\Imports\SigapImport;
use App\LaporanNihil;
use App\LogExecuteSigap;
USE App\SigapReport;
use App\Sigap;
use App\SigapTerduga;
use Exception;
use Illuminate\Support\Facades\DB;
use Session;

class SigapController extends Controller
{
    public function uploadData()
    {
        // write code here
        $listblacklistojk = Sigap::all();
        return view('admin.data-blacklist-ojk', ['activeSidebar' => 'sigapOjk', 'listblacklistojk' => $listblacklistojk]);
    }

    public function findData()
    {
        // write code here
        $checkCreation = Sigap::where('compare', 0)->count();
        return view('admin.cari-data-blacklist-ojk', ['activeSidebar' => 'sigapOjk', 'checkCreation' => $checkCreation]);
    }

    public function getDataSigap(Request $request)
    {
        $draw = $request->get('draw');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);
        $name = $request->get('nama', null);
        $ktp = $request->get('ktp', null);
        $pengajuan = $request->get('pengajuan', null);

        $get_data = ListHelper::check_data_sigap($name,$ktp,$pengajuan);
        $data_arr = array();

        foreach ($get_data as $data) {
            $id_backoffice = $data->id_backoffice;
            $id_ojk_list = $data->id_ojk_list;
            $nama = $data->nama;
            $nik = $data->nik;
            $tanggal_lahir = $data->tanggal_lahir;
            $tempat_Lahir = $data->tempat_Lahir;
            $warga_negara = $data->warga_negara;
            $alamat = $data->alamat;

            $data_arr[] = array(
                'id_backoffice' => $id_backoffice,
                'id_ojk_list' => $id_ojk_list,
                'nama' => $nama,
                'nik' => $nik,
                'tanggal_lahir' => $tanggal_lahir,
                'tempat_Lahir' => $tempat_Lahir,
                'warga_negara' => $warga_negara,
                'alamat' => $alamat,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => $data_arr,
        );

        echo json_encode($response);
        exit;
    }

    public function dataReport()
    {
        // write code here
        return view('admin.data-report-analisis', ['activeSidebar' => 'sigapOjk']);
    }

    public function import_excel(Request $request){
        // delete all
        Sigap::query()->truncate();
        SigapTerduga::query()->truncate();

        $execute_date = date('Y-m-d H:i:s', strtotime($request->execute_date));

        // validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

		// cokot file excel
		$file = $request->file('file');

		// jieun ngaran file
		$name_file = rand().$file->getClientOriginalName();

		// upload ke folder file izi di dalam folder public
		$file->move('file_sigap',$name_file);

		// import data
		Excel::import(new SigapImport($execute_date), public_path('/file_sigap/'.$name_file));

		// notifikasi  session
        $notification = array(
            'message' => 'Data Berhasil Diimport!',
            'alert-type' => 'success'
        );

		// alihkan halaman kembali
        return Redirect::to('/uploadData')->with($notification);
    }

    public function getDataReportTerduga(Request $request)
     {
        $draw = $request->get('draw');

        $name = $request->get('nama', null);
        $ktp = $request->get('ktp', null);
        $pengajuan = $request->get('pengajuan', null);
        $no=1;

        // $get_data = ListHelper::check_data_sigap_terduga($name,$ktp,$pengajuan);
        $get_data = DB::connection('mysql')->select("
                        SELECT * FROM sigap_terdugas
                            WHERE
                                status = 1
                            AND nama LIKE '%$name%'
                            AND nik LIKE '%$ktp%'
                            AND id_backoffice LIKE '%$pengajuan%'
                    ");
        $data_arr = array();

        foreach ($get_data as $data) {
            $id_backoffice = $data->id_backoffice;
            $id_ojk_list = $data->id_ojk_list;
            $nama = $data->nama;
            $nik = $data->nik;
            $tanggal_lahir = $data->tanggal_lahir;
            $tempat_Lahir = $data->tempat_Lahir;
            $warga_negara = $data->warga_negara;
            $alamat = $data->alamat;

            $data_arr[] = array(
                'no' => $no++,
                'id_backoffice' => $id_backoffice,
                'id_ojk_list' => $id_ojk_list,
                'nama' => $nama,
                'nik' => $nik,
                'tanggal_lahir' => $tanggal_lahir,
                'tempat_Lahir' => $tempat_Lahir,
                'warga_negara' => $warga_negara,
                'alamat' => $alamat,
                'aksi' => '<a type="button" href="#" onclick="modalTerdugaSigap('.$id_ojk_list.','.$id_backoffice.')" data-toggle="modal" data-target="#reportTerdugaSigap" class="btn btn-xs btn-warning"> <span><i class="fas fa-share-square"></i>&nbsp; Laporkan</span></a>'
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => $data_arr,
        );

        echo json_encode($response);
        exit;
    }

    public function getCreationData(Request $request)
    {
        try {
            $getData = ListHelper::creationData();
            $dataArray = array();
            $dataLog = array();

            foreach ($getData as $key => $value) {
                $dataArray[] = array(
                    'id_backoffice' => $value->id_backoffice,
                    'id_ojk_list' => $value->id_ojk_list,
                    'nama' => $value->nama,
                    'nik' => $value->nik,
                    'tanggal_lahir' => $value->tanggal_lahir,
                    'tempat_Lahir' => $value->tempat_Lahir,
                    'warga_negara' => $value->warga_negara,
                    'alamat' => $value->alamat,
                );
            }

            // insert to backup data sigap terduga after compare data
            SigapTerduga::insert($dataArray);

            foreach ($getData as $k => $val) {
                $dataLog[] = array(
                    'execute_date' => date('Y-m-d H:i:s', strtotime($val->execute_date)),
                    'name' => $val->nama,
                    'nik' => $val->nik,
                    'expected' => $val->expected,
                    'code_densus' => $val->code_densus,
                    'birth' => $val->tempat_Lahir,
                    'birth_date' => $val->tanggal_lahir,
                    'citizen' => $val->warga_negara,
                    'address' => $val->alamat,
                    'ref_idsigap' => $val->id_ojk_list,
                    'ref_idbackoffice' => $val->id_backoffice,
                );
            }

            // insert to log data execute compare
            LogExecuteSigap::insert($dataLog);

            $notification = array(
                'message' => 'Proses Pembuatan Data berhasil!',
                'alert-type' => 'success'
            );
            return Redirect::to('/findData')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Proses Pembuatan Data gagal',
                'alert-type' => 'error'
            );
            return Redirect::to('/findData')->with($notification);
        }
    }

    public function update_flag_analysisi(Request $request)
    {
        try {

            $request->validate([
                'reason' => 'required',
                'status_report' => 'required',
                'ref_idsigap' => 'required',
                'ref_idbackoffice' => 'required',
            ]);


            // Jika Status Report Pemblokiran;
            if($request->status_report == 2){

                    $dt_sigap = Sigap::where('id', $request->ref_idsigap)->first();

                    SigapReport::create([
                        'execute_date' => date('Y-m-d H:i:s'),
                        'reason' =>  $request->reason,
                        'status_report' =>  $request->status_report,
                        'uid' => Auth::user()->id,
                        'name' => $dt_sigap->name,
                        'nik' => $dt_sigap->nik,
                        'expected' => $dt_sigap->expected,
                        'code_densus' => $dt_sigap->code_densus,
                        'birth' => $dt_sigap->birth,
                        'birth_date' => $dt_sigap->birth_date,
                        'citizen' => $dt_sigap->citizen,
                        'address' => $dt_sigap->address,
                        'inquiry_date' => $dt_sigap->updated_at,
                        'ref_idsigap' => $request->ref_idsigap,
                        'ref_idbackoffice' => $request->ref_idbackoffice
                    ]);

                    // disable data to dt sigap from report success
                    Sigap::where('id', $request->ref_idsigap)->update(['status' => 2]);
                    SigapTerduga::where('id_ojk_list', $request->ref_idsigap)->update(['status' => 2]);

                    $notification = array(
                        'message' => 'Perubahan tersangka berhasil',
                        'alert-type' => 'success'
                    );

            }else{

                // Nihil**(need new Helper)

                $dt_sigap = Sigap::where('id', $request->ref_idsigap)->first();

                SigapReport::create([
                    'execute_date' => date('Y-m-d H:i:s'),
                    'reason' =>  $request->reason,
                    'status_report' =>  $request->status_report,
                    'uid' => Auth::user()->id,
                    'name' => $dt_sigap->name,
                    'nik' => $dt_sigap->nik,
                    'expected' => $dt_sigap->expected,
                    'code_densus' => $dt_sigap->code_densus,
                    'birth' => $dt_sigap->birth,
                    'birth_date' => $dt_sigap->birth_date,
                    'citizen' => $dt_sigap->citizen,
                    'address' => $dt_sigap->address,
                    'inquiry_date' => $dt_sigap->updated_at,
                    'ref_idsigap' => $request->ref_idsigap,
                    'ref_idbackoffice' => $request->ref_idbackoffice
                ]);

                // disable data to dt sigap from report success
                Sigap::where('id', $request->ref_idsigap)->update(['status' => 2]);
                SigapTerduga::where('id_ojk_list', $request->ref_idsigap)->update(['status' => 2]);


                $notification = array(
                    'message' => 'Perubahan terduga berhasil',
                    'alert-type' => 'success'
                );
            }

            return Redirect::to('/dataReport')->with($notification);

        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Perubahaan Gagal',
                'alert-type' => 'warning'
            );

            return Redirect::to('/dataReport')->with($notification);

        }
    }

    public function getDataReportNihil($id, Request $request)
    {
        $dataNihil = json_decode(LaporanNihil::where('ref_id_pengguna', decrypt($id))->first(), true);
        return view('admin.sigap-nihil', ['data' => $dataNihil]);
    }

    public function getDataReportPemblokiran($id, Request $request)
    {
        $dataPemblokiran = json_decode(BaPemblokiran::where('ref_id_pengguna', decrypt($id))->first(), true);
        return view('admin.sigap-terduga', ['data' => $dataPemblokiran]);
    }

    public function downloadTerduga(){

        $wordTest = new \PhpOffice\PhpWord\PhpWord();

        $view_content = View::make('admin.sigap-terduga')->render();

        $newSection = $wordTest->addSection();

        //$desc1 = "The Portfolio details is a very useful feature of the web page. You can establish your archived details and the works to the entire web community. It was outlined to bring in extra clients, get you selected based on this details.";

        $newSection->addText('--');


        $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'Word2007');
        try {
            $objectWriter->save(storage_path('TestWordFile.docx'));
        } catch (Exception $e) {
        }

        return response()->download(storage_path('TestWordFile.docx'));
    }


    public function getReportNihil(Request $request)
     {
        $draw = $request->get('draw');

        $get_data = SigapReport::where('status_report', '1')->get();
        $data_arr = array();
        $no=1;

        foreach ($get_data as $data) {
            $id = $data->id;
            $ref_idsigap = $data->ref_idsigap;
            $ref_idbackoffice = $data->ref_idbackoffice;
            $nama = $data->name;
            $nik = $data->nik;
            $tanggal_lahir = $data->birth_date;
            $tempat_Lahir = $data->birth;
            $warga_negara = $data->citizen;
            $alamat = $data->address;
            $reason = $data->reason;

            $cancel = '<a type="button" href="javascript:void(0)" class="btn btn-xs btn-danger" onclick="deleteReportNihil('.$ref_idsigap.')"> <span><i class="fas fa-trash-alt"></i>  Hapus Laporan</span></a>';
            $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$id.'" data-original-title="Edit" class="edit btn btn-primary btn-xs editProduct"> <span><i class="fas fa-edit"></i>&nbsp; Edit</span></a>&nbsp;';
            $button = '<a type="button" href="#" onclick="modalReportNihil('.$ref_idsigap.','.$ref_idbackoffice.')" data-toggle="modal" data-target="#reportNihil" class="btn btn-xs btn-warning"> <span><i class="fas fa-share-square"></i>&nbsp; Laporkan</span></a>';
            $nihil = LaporanNihil::where('ref_id_pengguna', $ref_idsigap)->first();
            if ($nihil) {
                $button = '<a type="button" target="_blank" href="/report-nihil/'.encrypt($ref_idsigap).'" class="btn btn-xs btn-success"> <span><i class="fas fa-print"></i></i>&nbsp; Print</span></a>&nbsp;'.$cancel;
            }

            $data_arr[] = array(
                'no' => $no++,
                'ref_idsigap' => $ref_idsigap,
                'ref_idbackoffice' => $ref_idbackoffice,
                'nama' => $nama,
                'nik' => $nik,
                'tanggal_lahir' => $tanggal_lahir,
                'tempat_Lahir' => $tempat_Lahir,
                'warga_negara' => $warga_negara,
                'alamat' => $alamat,
                'reason' => $reason,
                'aksi' => $btn.$button
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => $data_arr,
        );

        echo json_encode($response);
        exit;
    }

    public function getReportPemblokiran(Request $request)
     {
        $draw = $request->get('draw');

        $get_data = SigapReport::where('status_report', '2')->get();
        $data_arr = array();
        $no=1;

        foreach ($get_data as $data) {
            $id = $data->id;
            $ref_idsigap = $data->ref_idsigap;
            $ref_idbackoffice = $data->ref_idbackoffice;
            $nama = $data->name;
            $nik = $data->nik;
            $tanggal_lahir = $data->birth_date;
            $tempat_Lahir = $data->birth;
            $warga_negara = $data->citizen;
            $alamat = $data->address;
            $reason = $data->reason;

            $cancel = '<a type="button" href="javascript:void(0)" class="btn btn-xs btn-danger" onclick="deleteReportPemblokiran('.$ref_idsigap.')"> <span><i class="fas fa-trash-alt"></i>  Hapus Laporan</span></a>';
            $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$id.'" data-original-title="Edit" class="edit btn btn-primary btn-xs editProduct"> <span><i class="fas fa-edit"></i>&nbsp; Edit</span></a>&nbsp;';
            $button = '<a type="button" href="#" onclick="modalReportPemblokiran('.$ref_idsigap.','.$ref_idbackoffice.')" data-toggle="modal" data-target="#reportPemblokiran" class="btn btn-xs btn-warning"> <span><i class="fas fa-share-square"></i>&nbsp; Laporkan</span></a>';
            $pemblokiran = BaPemblokiran::where('ref_id_pengguna', $ref_idsigap)->first();
            if ($pemblokiran) {
                $button = '<a type="button" target="_blank" href="/report-pemblokiran/'.encrypt($ref_idsigap).'" class="btn btn-xs btn-success"> <span><i class="fas fa-print"></i></i>&nbsp; Print</span></a>&nbsp;'.$cancel;
            }

            $data_arr[] = array(
                'no' => $no++,
                'ref_idsigap' => $ref_idsigap,
                'ref_idbackoffice' => $ref_idbackoffice,
                'nama' => $nama,
                'nik' => $nik,
                'tanggal_lahir' => $tanggal_lahir,
                'tempat_Lahir' => $tempat_Lahir,
                'warga_negara' => $warga_negara,
                'alamat' => $alamat,
                'reason' => $reason,
                'aksi' => $btn.$button
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => $data_arr,
        );

        echo json_encode($response);
        exit;
    }

    public function form_report_nihil(Request $request)
    {
        try {
            $request->validate([
                'ref_id_backoffice' => 'required',
                'ref_id_pengguna' => 'required',
                'nama_pt' => 'required',
                'alamat_pt' => 'required',
            ]);

            LaporanNihil::create([
                'ref_id_backoffice' => $request->ref_id_backoffice,
                'ref_id_pengguna' => $request->ref_id_pengguna,
                'nama_pt' => $request->nama_pt,
                'alamat_pt' => $request->alamat_pt,
                'nomor' => $request->nomor,
                'lampiran' => $request->lampiran,
                'nomor_polisi' => $request->nomor_polisi,
                'tanggal_polisi' => date('Y-m-d', strtotime($request->tanggal_polisi)),
                'nomor_dttot' => $request->nomor_dttot,
                'hari_nihil' => $request->hari_nihil,
                'tanggal_nihil' => date('Y-m-d', strtotime($request->tanggal_nihil)),
                'nama_pjk' => $request->nama_pjk,
            ]);

            $notification = array(
                'message' => 'Laporkan Data Nihil Berhasil',
                'alert-type' => 'success'
            );

            return Redirect::to('/dataReport')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Laporan Gagal',
                'alert-type' => 'warning'
            );

            return Redirect::to('/dataReport')->with($notification);
        }
    }

    public function form_report_pemblokiran(Request $request)
    {
        try {
            $request->validate([
                'ref_id_backoffice' => 'required',
                'ref_id_pengguna' => 'required',
                'nama_pt' => 'required',
                'nama_pemblokir' => 'required',
                'nomor_rekening' => 'required',
            ]);

            BaPemblokiran::create([
                'ref_id_backoffice' => $request->ref_id_backoffice,
                'ref_id_pengguna' => $request->ref_id_pengguna,
                'nama_pt' => $request->nama_pt,
                'alamat_pt' => $request->alamat_pt,
                'nama_pemblokir' => $request->nama_pemblokir,
                'jabatan_pemblokir' => $request->jabatan_pemblokir,
                'alamat_pemblokir' => $request->alamat_pemblokir,
                'hari_pemblokiran' => $request->hari_pemblokiran,
                'tanggal_pemblokiran' => date('Y-m-d', strtotime($request->tanggal_pemblokiran)),
                'nomor_polisi' => $request->nomor_polisi,
                'tanggal_polisi' => date('Y-m-d', strtotime($request->tanggal_polisi)),
                'no_dttot' => $request->no_dttot,
                'nama_terorist' => $request->nama_terorist,
                'jabatan_terorist' => $request->jabatan_terorist,
                'nama_rekening' => $request->nama_rekening,
                'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
                'pekerjaan' => $request->pekerjaan,
                'alamat' => $request->alamat,
                'nomor_rekening' => $request->nomor_rekening,
                'saldo_terakhir' => $request->saldo_terakhir,
                'jenis_identitas' => $request->jenis_identitas,
                'nama_saksi' => $request->nama_saksi,
                'lampiran' => $request->lampiran,
            ]);

            $notification = array(
                'message' => 'Laporkan Data Pemblokiran Berhasil',
                'alert-type' => 'success'
            );

            return Redirect::to('/dataReport')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Laporan Gagal',
                'alert-type' => 'warning'
            );

            return Redirect::to('/dataReport')->with($notification);
        }
    }

    public function editReasonSigap($id)
    {
        $data = SigapReport::find($id);
        return response()->json($data);
    }

    public function updateReasonSigap(Request $request)
    {
        try {
            $sigap = SigapReport::find($request->id);
            $sigap->reason = $request->reason;
            $sigap->status_report = $request->status_report;
            $sigap->save();

            $notification = array(
                'message' => 'Data berhasil terupdate!',
                'alert-type' => 'success'
            );
            // return Redirect::to('/dataReport')->with($notification);
            return response()->json(['success'=>'Data saved successfully.']);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Data gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/dataReport')->with($notification);
        }
    }

    public function cancelReportNihil($id)
    {
        try {
            $cancel = LaporanNihil::where('ref_id_pengguna', $id)->first();
            $cancel->delete();

            $notification = array(
                'message' => 'Data Laporan Nihil berhasil di cancel!',
                'alert-type' => 'success'
            );
            return Redirect::to('/dataReport')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Data Laporan Nihil gagal di cancel!',
                'alert-type' => 'error'
            );
            return Redirect::to('/dataReport')->with($notification);
        }
    }

    public function cancelReportPemblokiran($id)
    {
        try {
            $cancel = BaPemblokiran::where('ref_id_pengguna', $id)->first();
            $cancel->delete();

            $notification = array(
                'message' => 'Data Laporan Pemblokiran berhasil di cancel!',
                'alert-type' => 'success'
            );
            return Redirect::to('/dataReport')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Data Laporan Pemblokiran gagal di cancel!',
                'alert-type' => 'error'
            );
            return Redirect::to('/dataReport')->with($notification);
        }
    }


}
