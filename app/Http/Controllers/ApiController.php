<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PusdafillClient;
use App\creditFeaturedDetail;
use App\CreaditFeatured;
use App\Http\Requests;
use App\FdcFeatured;
use App\IziCallback;
use App\IziClient;
use App\MidtransBankValidator;
use App\User;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Helpers\RelayServices;
use App\Helpers\StoreServices;
use App\Helpers\ChuanglanSmsApi;
use App\Helpers\ListHelper;
use App\Helpers\ChuanglanSmsApiMarketing;
use App\Alibaba\AlibabaCloud;
use App\Alibaba\Exception\ClientException;
use App\Alibaba\Exception\ServerException;
use App\Helpers\XmlToJson;
use App\PefindoCustomeReport;



class ApiController extends Controller
{

            //Interface URL FDC
            const PROD_FDC_SEND_URL='https://pusdafil.afpi.or.id/api/.5/Inquiry/?id=';
            const DEV_FDC_SEND_URL='https://srigala.afpi.or.id/api/v3.5/Inquiry/?id=';
            const FDC_ACCOUNT='riski.firdaus@okp2p.co.id'; 
            const FDC_PASSWORD='X6P4sxBaYCk9k4gbz3ZGd8';


            //Alibaba SMS Product Key
            const ALI_ACCOUNT='LTAI5tNUGhxYuw8NCcwGb6zF'; 
            const ALI_PASSWORD='8lrMD2bXO37XAqJEgEpoQBhqTbzoCN';


            //Interface URL IZI DATA
            const DEV_IZI_DATA_URL='https://api.izi.credit/';
            const PROD_IZI_DATA_URL='https://api.izi.credit/';
            const IZI_ACCOUNT ='dOFMsNrXQrpSHjgUbmXy';
            const IZI_PASSWORD = 'hqzGdBwMjpDHggqhYrfmFeDOwCVNMmOTNqBtdFug';


            //Interface URL ILUMA
            const URL_ILUMA= "https://api.iluma.ai/";
            const ILUMA_KEY = "iluma_development_JZRz3zzo2DUnjRsCHysO5H9tzqlACPIzFzCfmBHE4BRnycQdHvCHeQsZknQtqN";

            //Interface URL PEFINDO
            const URL_PEFINDO_DEV = 'https://cbs5bodemo2.pefindobirokredit.com/WsReport/v5.53/Service.svc';   
            const URL_PEFINDO_PROD = 'https://bo.pefindobirokredit.com/WsReport/v5.53/service.svc';          
            const PEFINDO_ACCOUNT = 'wahyu_okp2p';
            const PEFINDO_PASSWORD = 'Pefindoadmin123';


            //Interface URL BPS SCORE

            //CONST DEVELOPMENT ADVANCE_KEY = 'db3e651ee73dbb6f';
            CONST ADVANCE_KEY = 'e5a15f5efcbe7748';

            CONST BPS_URL = 'https://api.bpsdata.com/openapi/score/v1/credit3';


            //Interface URL
            CONST NPL_CUST_URL = 'http://34.101.200.112:8000/api/checkCustomer/';
            CONST AUTH_ASSET = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiM2NjNjhmYmMxM2YyNmQ0YmE3NmJhMjVlMDFkNzk4NjAxZTMyNWY3MzE5ZjlhNmM3MTc1MmUxZDJjNjBhZTM3N2ZjNzFlNjU0MjYwOWFlNWMiLCJpYXQiOjE2MzIzOTI1MjIuNTQ1Mjk0LCJuYmYiOjE2MzIzOTI1MjIuNTQ1Mjk4LCJleHAiOjE2NjM5Mjg1MjIuNTM1ODI2LCJzdWIiOiIxMTQiLCJzY29wZXMiOltdfQ.HdpTzt1QIPW1u5li4SoNoRxkKkgA42HlScB8J1_7VBKtlgriVkl8mv5Q-wlWvYWHdtFdDHd_EhLfHXy-36vfaHX7TADIK4oULb4FUl0JU7-7JESWPwGAk5_cvmVKv_9reofafO1amkeyAzcTcQM6VtCRUiK7BmTqPCllDXha9zvV9CZOw0UPNjcvwVS4-oxBxG5ltWbCUzOwTHovYvfXHCo-6sRM6ww8lI-3QjAuVKxZNFlnnqq9Qu-E_yWlmg9LxJsxYYe277vAlRDUvaPy6Mju0W-crrc2JGQlW65PoLHtGPm2iM_wGCxm-5yL1Fjg4jXWfcD2Dt5yeqBeUK2qVSVw5nBKgo0ATBSlt7nIEUryld-d0rvBNqPqXYEK6_vezhKuy8WuCz2FvVTi5ZUdwG6tTK2KjdxtpODacBvY24Jd48i_3dxY0HkjgdYa5IvegwZHK5ins2ztD4Afcuqx2SzpytcvGd3XSmMg9cNEEu6nZ8b2NizJFYnHFzDSLBRfWDegztl6eNwg6O0UQo49WLvnGjyePp-0YwPxpcrCNjdE3EfJv9ScL4Al1WdT79qvpgxipRzyZ3oiw_jww1PRCcbappPgSCdoC1sPU7oBWzOSgdGai7TM-L7C8-meBTkV-pXCFQCl1UVYnPFhfeAJiLyYt8OcujQfHIJ2m2gnATQ';

 
            /** FDC API Relay Service & Sistem OPS API
             * @param string $ktp 	
             * @param string $reason 			
            */
            public function ApiFintechDtCenter(Request $request){

         
                if($request->header('mode_dev') == true){
                    $url = self::DEV_FDC_SEND_URL.$request->ktp."&reason=".$request->reason."&reffid=123";
                    $response = ListHelper::MockDataFDC(); // with mock
                    StoreServices::storeFdc($request,$response);
                    return $response;
                };

                // DISABLE KARENA OJK
                //$url = self::PROD_FDC_SEND_URL.$request->ktp."&reason=".$request->reason."&reffid=123";
                  $url ='';

                try {
                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_USERPWD, self::FDC_ACCOUNT . ":" . self::FDC_PASSWORD);

                        curl_setopt($ch, CURLOPT_URL, $url);

                        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_HEADER, false);

                        $output = curl_exec($ch);

                        $response = ListHelper::EncodedString($output);

                        curl_close($ch);

                        StoreServices::storeFdc($request);

                        return $response;

                } catch (\Throwable $th) {

                        $notification = array(
                            'code' => '501',
                            'message' => 'Gagal Akses Ke Fintech Data Center / Not Impement To Server',
                            'alert-type' => 'error'
                        );

                        return $notification;
                }
                
            }


            /** SMS API TIG & ALIBABA
             * @param string $request->type 	
             * @param number $request->numberphone 	
             * @param string $request->content
             * @param string $request->vendor 
             * type  => Parameter(OTP: verify code,NOTIFY：notification,MKT：marketing)	
            */
            public function SendSMS(Request $request){

                
                if($request->header('mode_dev') == true && $request->vendor == "ALIBABA"){
                    $response = ListHelper::MockDataSMS(); // with mock
                    StoreServices::storeAlibabasms($request,$response);
                    return $response;
                }else if($request->header('mode_dev') == true && $request->vendor == "TIG"){
                    $response = ListHelper::MockDataSMS(); // with mock
                    StoreServices::storeTigsms($request,$response);
                    return $response;  
                };

                if($request->vendor == "TIG"){
                    if($request->type == "OTP"){
                        $clapi  = new ChuanglanSmsApi();
                    }elseif($request->type == "MKT"){
                        $clapi  = new ChuanglanSmsApiMarketing();
                    }else{
                        return $notification = array(
                            'code' => '0001',
                            'message' => 'REQUEST TYPE TIDAK DITEMUKAN',
                            'alert-type' => 'error'
                        );
                    }

                    $content = $request->content;
                    
                    $numberphone = $request->numberphone;

                    $result = $clapi->sendInternational($numberphone, $content);

                    if(!is_null(json_decode($result))){
                        
                        $output=json_decode($result,true);

                        if(isset($output['code'])  && $output['code']=='0'){

                            $notification = array(
                                'code' => '0000',
                                'message' => 'SMS '.$request->type.' Berhasil Dikirim',
                                'alert-type' => 'success',
                                'content' => $content,
                                'phone_number' => $numberphone
                            );

                            StoreServices::storeTigsms($request,json_encode($notification, true));

                            echo json_encode($notification, true);

                        }else{

                            if($output['error']=='手机号码格式错误'){
                                $notification = array(
                                    'code' => '0001',
                                    'message' => 'Format Nomor HP Salah',
                                    'alert-type' => 'error'
                                );
                            }elseif($output['error'] == '短信内容不能为空'){
                                $notification = array(
                                    'code' => '0002',
                                    'message' => 'Content SMS tidak boleh kosong',
                                    'alert-type' => 'error'
                                );
                            }else{
                                $notification = array(
                                    'code' => '0003',
                                    'message' => 'Response Tidak diketahui : ' .$output['error'],
                                    'alert-type' => 'error'
                                );
                            };

                            echo json_encode($notification, true);
                        }
                    }else{
                            echo $result;
                    }

                }elseif($request->vendor == "ALIBABA"){
 
                    AlibabaCloud::accessKeyClient(self::ALI_ACCOUNT, self::ALI_PASSWORD) ->regionId('ap-southeast-1') ->asGlobalClient();
                    try {
                        $result = AlibabaCloud::rpcRequest() 
                        ->product('Dysmsapi') 
                        ->host('dysmsapi.ap-southeast-1.aliyuncs.com') 
                        ->version('2018-05-01') ->action('SendMessageToGlobe') 
                        ->method('POST') ->options([
                        'query' => [ "To" => $request->numberphone,
                        "From" => "OKP2P", 
                        "Message" => $request->content, 
                        "Type" => $request->type
                        ], ]) ->request();
            
                        $output=json_encode($result->toArray(),true);
            
                        StoreServices::storeAlibabasms($request,$output);

                      
                        
                        } catch (ClientException $e) {
                        echo $e->getErrorMessage() . PHP_EOL;
                        } catch (ServerException $e) {
                        echo $e->getErrorMessage() . PHP_EOL;
                        }
                        return $output;

                }else{
                    $notification = array(
                        'code' => '501',
                        'message' => 'Tidak ada vendor yang terdaftar !',
                        'alert-type' => 'error'
                    );
                    return  $notification;
                }
            }



            /** IZI DATA  Credit Featured Service
             * @param string $request->name 	
             * @param number $request->phone 	
            */
            public function CreditFeatured(Request $request){

                if($request->header('mode_dev') == true){
                    $response = ListHelper::MockDataIzi(); // with mock
                    StoreServices::storeIziCreditFeature($request,$response);
                    return $response;
                };

                try{

                    $client = new IziClient(self::IZI_ACCOUNT, self::IZI_PASSWORD);
                    
                    $url = self::PROD_IZI_DATA_URL."v2/creditfeature";
            
                    $data = array(
                        "phone"=> $request->phone,
                        "name"=>$request->name,
                        "id"=> $request->ktp
                    );
                    
                    $response = $client->request($url, $data);
            
                    $response = ListHelper::EncodedString($response);

                    StoreServices::storeIziCreditFeature($request,$response);

                }catch (\Throwable $th) {
                    $notification = array(
                        'message' => 'Kesalahan pada request Izi Data',
                        'code' => '501',
                        'alert-type' => 'error'
                    );
                    return $response = $notification;
                }

                return $response;
            }


            /** IZI DATA Sallary Service
            * @param string $request->id 	
            */
            public function sallary(Request $request){

                if($request->header('mode_dev') == true){
                    $response = ListHelper::MockDataSallary(); // with mock
                    StoreServices::storeIziSallary($request,$response);
                    return $response;
                };

                try{
                    
                    $client = new IziClient(self::IZI_ACCOUNT, self::IZI_PASSWORD);
                    
                    $url = self::PROD_IZI_DATA_URL."v1/salary";
                    
                    $data = array(
                        "id"=> $request->id
                    );
                    
                    $response = $client->request($url, $data);
                    
                    StoreServices::storeIziSallary($request,$response);

                    return $response;

                }catch (\Throwable $th) {
                    $notification = array(
                        'message' => 'Kesalahan pada request sallary',
                        'code' => '501',
                        'alert-type' => 'error'
                    );
                    return $response = $notification;
                }
            }



            /** ILUMA Bank Validation service
             * @param string $request->bank_account_number 	
             * @param string $request->bank_code 	
             * @param string $request->given_name
            */
            public function SendIlumaValidationBank(Request $request){

                if($request->header('mode_dev') == true){
                    $response = ListHelper::MockDataIluma(); // with mock
                    StoreServices::storeIluma($request,$response);
                    return $response;
                };

                try {
                        $iluma_key = self::ILUMA_KEY;

                        $url = self::URL_ILUMA."v2/identity/bank_account_data_requests";

                        $data = array("bank_account_number" => $request->bank_account_number,"bank_code" => $request->bank_code,"given_name" => $request->given_name);                                                                    

                        $data_string = json_encode($data);
                        
                        $ch = curl_init();
                
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        
                        curl_setopt($ch, CURLOPT_USERPWD, $iluma_key);

                        curl_setopt($ch, CURLOPT_URL, $url);

                        curl_setopt($ch, CURLOPT_POST, 1);

                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                    
                        $response = curl_exec($ch);

                        curl_close($ch);

                        StoreServices::storeIluma($request,$response);
                        
                        return $response;

                } catch (\Throwable $th) {

                        $response = array(
                            'error_code' => '501',
                            'message' => 'There was an error with the format submitted to the Relay server or access Iluma API.',
                            'alert-type' => 'error'
                        );
                        return $response;
                }
                
            }



        /** Pefindo service
         * @param string $request->ktp 	
         * @param string $request->DateOfBirth 	
         * @param string $request->fullname
        */
        public function requestSmartSearchCustomReport(Request $request)
        {

            
                // Paramters
                $ktp  = $request->ktp;
                $DateOfBirth  = $request->DateOfBirth;
                $fullname  = $request->fullname;
                $InquiryReason = 'ProvidingFacilities';
                $reportdate = date("Y-m-d"); 


                // If Mode Development
                if($request->header('mode_dev') == true){
                    $response = ListHelper::MockDataPefindo(); // with mock
                    StoreServices::storePefindo($ktp, $fullname, $DateOfBirth, $InquiryReason,'1239743373',$reportdate,'Berhasil',$response,$request->header('mode_dev'));
                    return $response;
                };

                // Basic Auth
                $soapUser = self::PEFINDO_ACCOUNT;
                $soapPassword = self::PEFINDO_PASSWORD;

                // Base Url
                $url = self::URL_PEFINDO_PROD;          

            if(env('APP_URL') == "147.139.165.214"){
                return $RelayServices = RelayServices::getSmartSearch($ktp,$DateOfBirth,$fullname,$InquiryReason,$reportdate);
            }else{

            
                // Payload Body
                $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb5="http://creditinfo.com/CB5" xmlns:smar="http://creditinfo.com/CB5/v5.53/SmartSearch">
                <soapenv:Header/>
                <soapenv:Body>
                <cb5:SmartSearchIndividual>
                <cb5:query>
                <smar:InquiryReason>'.$InquiryReason.'</smar:InquiryReason>
                <smar:InquiryReasonText/>
                <smar:Parameters>
                <smar:DateOfBirth>'.$DateOfBirth.'</smar:DateOfBirth>
                <smar:FullName>'.$fullname.'</smar:FullName>
                <smar:IdNumbers>
                <smar:IdNumberPairIndividual>
                <smar:IdNumber>'.$ktp.'</smar:IdNumber>
                <smar:IdNumberType>KTP</smar:IdNumberType>
                </smar:IdNumberPairIndividual>
                </smar:IdNumbers>
                </smar:Parameters>
                </cb5:query>
                </cb5:SmartSearchIndividual>
                </soapenv:Body>
                </soapenv:Envelope>';   // data from the form, e.g. some ID number

            try {

                // -------------------Get smart search request---------------------------------------- 
                $headers = array(
                    "Content-type: text/xml",
                    "Accept: text/xml",
                    "SOAPAction: http://creditinfo.com/CB5/IReportPublicServiceBase/SmartSearchIndividual",
                );

                $ch = curl_init();
                if ($ch === false) {
                    throw new Exception('Gagal Inquiry Pefindo');
                }

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


                $responseSmartSearch = curl_exec($ch);
                curl_close($ch);

                $resSoapSmartSearch = str_replace("<soap:Body>","",$responseSmartSearch);
                $idpefindo = XmlToJson::getPefindoId($resSoapSmartSearch);



                // -------------------Get Custom Report request----------------------------------------

                $headers = array(
                    "Content-type: text/xml",
                    "Accept: text/xml",
                    "SOAPAction: http://creditinfo.com/CB5/IReportPublicServiceBase/GetCustomReport",
                ); 

                $ch = curl_init();
                if ($ch === false) {
                    throw new Exception('Kesalahan get custom report, connection error');
                }

                $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb5="http://creditinfo.com/CB5" xmlns:cus="http://creditinfo.com/CB5/v5.53/CustomReport" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
                <soapenv:Header/>
                <soapenv:Body>
                <cb5:GetCustomReport>
                <cb5:parameters>
                <cus:Consent>true</cus:Consent>
                <cus:IDNumber>'.$idpefindo.'</cus:IDNumber>
                <cus:IDNumberType>PefindoId</cus:IDNumberType>
                <cus:InquiryReason>'.$InquiryReason.'</cus:InquiryReason>
                <cus:InquiryReasonText/>
                <cus:ReportDate>'.$reportdate.'</cus:ReportDate>
                <cus:Sections>
                <arr:string>CIP</arr:string>
                <arr:string>SubjectInfo</arr:string>
                <arr:string>SubjectHistory</arr:string>
                <arr:string>ContractList</arr:string>
                </cus:Sections>
                <cus:SubjectType>Individual</cus:SubjectType>
                </cb5:parameters>
                </cb5:GetCustomReport>
                </soapenv:Body>
                </soapenv:Envelope>';   // data from the form, e.g. some ID number
        
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


                $responseCustomReport = curl_exec($ch);
                curl_close($ch);

                $GetDataCustomReport = str_replace("<soap:Body>","",$responseCustomReport);

                $jsonDat = XmlToJson::soapxml2json($GetDataCustomReport);


                // Cek id pefindo not found in PBK
                if($idpefindo == null){
                    $idpefindo = 0; // Set menghindari error
                    $status = 'Gagal';
                }else{
                    $status = 'Berhasil';     
                };

                StoreServices::storePefindo($ktp, $fullname, $DateOfBirth, $InquiryReason,$idpefindo,$reportdate,$status,$jsonDat,$request->header('mode_dev'));

                return $jsonDat;

            } catch(Exception $e) {
                return trigger_error(sprintf(
                    'Curl failed with error #%d: %s',
                    $e->getCode(), $e->getMessage()),
                    E_USER_ERROR);
            }
            }
        }



            /** BPS Advance AI service
             * @param string $request->name 	
             * @param string $request->idNumber 	
             * @param string $request->phoneNumber
            */
            public function SendAdvanceAiScore(Request $request){

                if($request->header('mode_dev') == true){
                    $response = ListHelper::MockDataAdvanceScore(); // with mock
                    StoreServices::storeAdvanceScore($request, $response);
                    return $response;
                };

                try {
                    
                        //Paramters :
                        $name = $request->name;
                        
                        $idNumber = $request->idNumber;
                        
                        $phoneNumber = $request->phoneNumber;
    
                        $headers  = ['X-ADVAI-KEY:'. self::ADVANCE_KEY,'Content-Type: application/json'];
            
                        $url = self::BPS_URL;
            
                        $data = array("name" => $name,"idNumber" => $idNumber,"phoneNumber" => $phoneNumber);                                                                    

                        $data_string = json_encode($data);
                        
                        $ch = curl_init();

                        $array_final = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:',$data_string);
        
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                        curl_setopt($ch, CURLOPT_URL, $url);

                        curl_setopt($ch, CURLOPT_POST, 1);
                        
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                    
                        $response = curl_exec($ch);
                   
                        curl_close($ch);

                        StoreServices::storeAdvanceScore($request, $response);
                        
                        return $response;

                } catch (\Throwable $th) {

                        $notification = array(
                            'error_code' => '501',
                            'message' => 'There was an error with the format submitted to the Relay server or access Advance Score V3.',
                            'alert-type' => 'error'
                        );
                        return $notification;
                }
                
            }



        // TESTING MIDTRANS
        public function account_validation(Request $request){

            $bank = 'danamon';
    
            $account_no = '000001137298';

            if($request->header('mode_dev') == true){
                $response = ListHelper::MockDataAdvanceScore(); // with mock
                StoreServices::storeAdvanceScore($request, $response);
                return $response;
            };
    
            $base_url = 'https://app.sandbox.midtrans.com/iris';
    
            $url = $base_url.'/api/v1/account_validation?bank='.$bank.'&account='.$account_no;
    
            $ch = curl_init();
    
            curl_setopt($ch, CURLOPT_USERPWD, 'IRIS-b4edcfd0-13af-4d49-a6eb-1e9ca6f02896');
    
            curl_setopt($ch, CURLOPT_URL, $url);
    
            curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
            curl_setopt($ch, CURLOPT_HEADER, false);
    
            $json_output = curl_exec($ch);
    
            curl_close($ch);
    
            $parsed = json_decode($json_output, true);
    
            return $json_output;
        }



//================================================================END ALL RELAY SERVICE===================================================================================================




    public function getUserCount(Request $request){

        $User =  User::with(['CreaditFeatured' => function($query){
            $query->select('*');
         }])->withCount(['CreaditFeatured as COUNT_HIT' => function($query) {
                              $query->select(DB::raw('SUM(count_hit)'));
                          }])->get();

        return response()->json($User);
    }


    public function getHistoryDetail($uid){

        $User =  User::with(['CreaditFeatured' => function($query){
            $query->select('*');
         }])->withCount(['CreaditFeatured as COUNT_HIT' => function($query) {
                              $query->select(DB::raw('SUM(count_hit)'));
                          }])->where('id', $uid)->get();

        return $User;
    }

    public function getMidtrans(Request $request){

        $bank = $request->bank;

        $account_no = $request->account_no;

        $uid = $request->uid;

        $base_url = 'https://app.sandbox.midtrans.com/iris';

        $url = $base_url.'/api/v1/account_validation?bank='.$bank.'&account='.$account_no;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_USERPWD, 'IRIS-6c09330b-cb8e-4eb6-ba40-1ca17bd108ee');

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HEADER, false);

        $json_output = curl_exec($ch);

        curl_close($ch);

        $parsed = json_decode($json_output, true);

        if (isset($parsed['account_name'])){
            $account_name = $parsed['account_name'];
            $midtransAcc = MidtransBankValidator::where('account_no', $account_no)->first();

            MidtransBankValidator::create([
                'account_name' => $account_name,
                'account_no' => $account_no,
                'bank_name' => $bank,
                'count_hit' => 1,
                'json' => $json_output,
                'uid' => $uid
            ]);
        }

        return $json_output;
    }


    public function getSavedMidtrans(Request $request){

        $midtransAcc = MidtransBankValidator::where('account_no', $request->account_no)->first();

        return json_encode($midtransAcc, true);
    }


    function IziCallBack(Request $request){

        $data = file_get_contents("php://input");

        IziCallback::create([
            'json' => $data
        ]);
        
        $jdata = json_decode($data, true);

        return response()->json($jdata);
    }


    function getReportDendaBulanan(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $status = $request->get("status", "405001");
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get("start_date" ,$fromDateDefault);
        $toDate = $request->get("end_date", $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
        SELECT
            count(*) as allcount
        FROM
            FN_LOAN_HIST HIST
            INNER JOIN FN_LOAN_MAST MAST ON HIST.ACT_NO = MAST.ACT_NO
            LEFT OUTER JOIN GT_CUST_COMM COMM ON MAST.CUS_NO = COMM.CUS_NO
            WHERE
            HIST.RCP_DT BETWEEN {$fromDate}
            AND {$toDate}
            AND HIST.STS_CD='{$status}'
    ");

        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
        SELECT
            count(*) as allcount
        FROM
            FN_LOAN_HIST HIST
            INNER JOIN FN_LOAN_MAST MAST ON HIST.ACT_NO = MAST.ACT_NO
            LEFT OUTER JOIN GT_CUST_COMM COMM ON MAST.CUS_NO = COMM.CUS_NO
            WHERE
            HIST.RCP_DT BETWEEN {$fromDate}
            AND {$toDate}
            AND HIST.STS_CD='{$status}'
            AND (
                MAST.INSP_NO LIKE '%{$searchValue}%'
                OR COMM.CUS_NM LIKE '%{$searchValue}%'
            )
    ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                MAST.INSP_NO NoPinjaman,
                COMM.CUS_NM NamaPinjaman,
                HIST.STS_CD stsCd,
                dbo.GT_FT_GETCODENM(HIST.COM_CD, HIST.STS_CD, 'ID') Status,
                HIST.RCP_DT TanggalPersetujuan,
                HIST.RPY_TOT_AM Pokok,
                HIST.NML_INT_AM BungaTetap,
                HIST.DFR_INT_AM Denda
            FROM
                FN_LOAN_HIST HIST
                INNER JOIN FN_LOAN_MAST MAST ON HIST.ACT_NO = MAST.ACT_NO
                LEFT OUTER JOIN GT_CUST_COMM COMM ON MAST.CUS_NO = COMM.CUS_NO
                WHERE
                HIST.RCP_DT BETWEEN {$fromDate}
                AND {$toDate}
                AND HIST.STS_CD='{$status}'
                AND (
                    MAST.INSP_NO LIKE '%{$searchValue}%'
                    OR COMM.CUS_NM LIKE '%{$searchValue}%'
                )
                ORDER BY MAST.INSP_NO
                OFFSET $start ROWS
                FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $no_pinjaman = $record->NoPinjaman;
            $nama_pinjaman = $record->NamaPinjaman;
            $kode_status = $record->stsCd;
            $deskripsi_status = $record->Status;
            $tgl_persetujuan = $record->TanggalPersetujuan;
            $pokok = $record->Pokok;
            $bunga_tetap = $record->BungaTetap;
            $denda = $record->Denda;

            $data_arr[] = array(
                "NoPinjaman" => $no_pinjaman,
                "NamaPinjaman" => $nama_pinjaman,
                "stsCd" => $kode_status,
                "Status" => $deskripsi_status,
                "TanggalPersetujuan" => $tgl_persetujuan,
                "Pokok" => "RP ".number_format($pokok, 0,',','.'),
                "BungaTetap" => "RP ".number_format($bunga_tetap, 0,',','.'),
                "Denda" => "RP ".number_format($denda, 0,',','.')
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }


        /** Ok Asset Check Customres
        * @param string $ktp 	 			
        */
        public function CheckNPLCust(Request $request){

            $dev_mode = $request->header('mode_dev') === "true"? true:false;

            if($dev_mode == true){
                $response = ListHelper::MockCheckNpl(); // with mock
                StoreServices::storeNplCust($request,$response);
                return $response;
            };

                try {

                        $headers = array(
                        "Accept: application/json",
                        "Authorization: Bearer ".self::AUTH_ASSET);

                        $url = self::NPL_CUST_URL.$request->ktp;

                        $curl = curl_init($url);

                        curl_setopt($curl, CURLOPT_URL, $url);

                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                        $response = curl_exec($curl);
                       
                        curl_close($curl);

                        StoreServices::storeNplCust($request, $response);

                        return $response;

                } catch (\Throwable $th) {

                        $notification = array(
                            'success' => 'false',
                            'message' => 'Gagal Akses Ke Server OK Asset',
                            'alert-type' => 'error'
                        );

                        return $notification;
                }
                
        }
}
