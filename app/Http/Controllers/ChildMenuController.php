<?php

namespace App\Http\Controllers;

use App\ChildMenu;
use App\MasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ChildMenuController extends Controller
{
    public function index()
    {
        $masterchildmenu = ChildMenu::all();
        $code = ChildMenu::max('id');
        $listMenu = MasterMenu::select('id', 'name')->pluck('name','id');

        return view('admin.childmenu', ['listchildmenu' => $masterchildmenu, 'activeSidebar' => 'configuration', 'code' => $code, 'listMenu' => $listMenu]);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Child Menu berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'code' => 'required',
                'parent' => 'required',
                'name' => 'required',
            ]);

            ChildMenu::create([
                'code' => $request->code,
                'parent' => $request->parent,
                'name' => $request->name,
                'menuopen' => $request->menuopen,
                'icon' => $request->icon,
                'url' => $request->url,
                'status' => 1,
                'description' => $request->description,
            ]);

            return Redirect::to('/childmenu')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Child Menu gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/childmenu')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $masterchildmenu = ChildMenu::find($request->id);
            $masterchildmenu->code = $request->code;
            $masterchildmenu->parent = $request->parent;
            $masterchildmenu->name = $request->name;
            $masterchildmenu->menuopen = $request->menuopen;
            $masterchildmenu->icon = $request->icon;
            $masterchildmenu->url = $request->url;
            $masterchildmenu->status = $request->status;
            $masterchildmenu->description = $request->description;
            $masterchildmenu->save();

            $notification = array(
                'message' => 'Child Menu berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/childmenu')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Child Menu gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/childmenu')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            ChildMenu::find($id)->delete();

            $notification = array(
                'message' => 'Child Menu berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/childmenu')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Child Menu gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/childmenu')->with($notification);
        }
    }
}
