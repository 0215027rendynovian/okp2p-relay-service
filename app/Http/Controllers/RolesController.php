<?php

namespace App\Http\Controllers;

use App\Roles;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class RolesController extends Controller
{
    public function index()
    {
        $masterRoles = Roles::all();

        $code = Roles::max('id');

        return view('admin.roles', ['listroles' => $masterRoles, 'activeSidebar' => 'configuration', 'code' => $code]);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Master Roles berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'code' => 'required',
                'name' => 'required',
            ]);

            Roles::create([
                'code' => $request->code,
                'name' => $request->name,
                'status' => 1,
                'description' => $request->description,
            ]);

            return Redirect::to('/roles')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Roles gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/roles')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $masterRoles = Roles::find($request->id);
            $masterRoles->code = $request->code;
            $masterRoles->name = $request->name;
            $masterRoles->status = $request->status;
            $masterRoles->description = $request->description;
            $masterRoles->save();

            $notification = array(
                'message' => 'Master Roles berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/roles')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Roles gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/roles')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            Roles::find($id)->delete();

            $notification = array(
                'message' => 'Master Roles berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/roles')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Roles gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/roles')->with($notification);
        }
    }
}
