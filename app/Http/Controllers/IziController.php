<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\User;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Support\Facades\Auth;

class IziController extends Controller
{


    public function index(){

        return view('admin.index');
    }


     public function identitycheck(){

         return view('admin.identity-check');
     }


     public function facecomparison(){

         return view('admin.face-comparison');
     }



    public function creditfeature(){

        return view('admin.credit-featured', ['activeSidebar' => 'iziData']);

    }



    public function creditfeatureReport($ktp, $numberphone, $namalengkap){

        return view('admin.credit-featured-report',['ktp' => $ktp, 'numberphone' => $numberphone, 'namalengkap' => $namalengkap]);

    }

    public function FDCDataReport($ktp, $reason){
        $reason_ket = ($reason = 1) ? "Pengajuan Pinjaman melalui Platform" : "Monitor Saldo Pinjaman dari Peminjam" ;
        return view('admin.fintech-data-report',['ktp' => $ktp, 'reason' => $reason, 'reason_ket' => $reason_ket]);

    }

    public function PostCreditFeature(Request $request){

        $ktp = $request->ktp;
        $numberphone = $request->numberphone;
        $name = $request->name;

        return response()->json(['response' => 'data berhasil diupdate']);

    }

    public function Fintectdatacentaer(Request $request){

        return view('admin.fintech-data', ['activeSidebar' => '']);
    }

}
