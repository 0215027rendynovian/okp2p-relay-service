<?php

namespace App\Http\Controllers;

use App\User;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $privilege = [
            1 => 'Admin',
            2 => 'Finance',
            3 => 'CSS',
            4 => 'Analyst',
            5 => 'HR',
            6 => 'Super Admin'
        ];


        $departemens = [
            0 => 'None',
            1 => 'OKP2P',
            2 => 'Retail',
            3 => 'Asset'
        ];

        $typeSystem = [
            0 => 'OPS System',
            1 => 'ES System'
        ];

        $uid = Auth::user()->id;

        return view('admin.listusers', ['users' => $users, 'privilege' => $privilege, 'uid' => $uid, 'departemens' => $departemens, 'typeSystem' => $typeSystem, 'activeSidebar' => '']);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Data User berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'name' => 'required',
                'email' => 'required|unique:users|email',
                'permission' => 'required',
                'dept_id' => 'required',
            ]);

            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make('opsokp2p123'),
                'permission' => $request->permission,
                'is_re_password' => 0,
                'is_active' => $request->is_active,
                'dept_id' => $request->dept_id,
                'es_system' => $request->es_system,
            ]);

            return Redirect::to('/users-list')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Data User gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/users-list')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $user = User::find($request->id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->permission = $request->permission;
            $user->dept_id = $request->dept_id;
            $user->is_active = $request->is_active;
            $user->es_system = $request->es_system;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->save();

            $notification = array(
                'message' => 'Data User berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/users-list')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Data User gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/users-list')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            if (Auth::user()->id == $id) {
                $notification = array(
                    'message' => 'Tidak dapat menghapus akun Anda sendiri!',
                    'alert-type' => 'error'
                );
                return Redirect::to('/users-list')->with($notification);
            }
            User::find($id)->delete();

            $notification = array(
                'message' => 'Data User berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/users-list')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Data User gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/users-list')->with($notification);
        }
    }

    public function resetpassword($id)
    {
        try {
            $reset = User::find($id);
            $reset->password = Hash::make('opsokp2p123');
            $reset->is_re_password = 0;
            $reset->save();

            $notification = array(
                'message' => 'Data User berhasil reset password!',
                'alert-type' => 'success'
            );
            return Redirect::to('/users-list')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Data User gagal reset password!',
                'alert-type' => 'error'
            );
            return Redirect::to('/users-list')->with($notification);
        }
    }


    // MAKE TOKEN RELAY SERVICE
    
    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('nApp')->accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }


    public function getToken(){
        if(Auth::attempt(['email' => 'rendy@okp2p.co.id', 'password' => 'admin123'])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
