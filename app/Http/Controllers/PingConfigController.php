<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\PingConfig;
use Illuminate\Support\Facades\Redirect;

class PingConfigController extends Controller {
    
    public function index(){
        try {
           // $config = DB::select('select * from pingconfig')[0]; 
           $config = PingConfig::first();
        }
        catch (\Throwable $th){
           /*
            DB::table('pingconfig')->insert([
                'server_ip' => '0.0.0.0',
                'server_port' => 0
            ]);
            */
           // $config = new PingConfig();
           // $config->server_ip = "0.0.0.0";
           // $config->server_port = 0;
           // $config = DB::select('select * from pingconfig')[0]; 
        }
        //dd($config);
        return view('admin.ping-config-menu',['activeSidebar' => 'configuration', 'config_data' => $config]);
    }
    

    public function setConfig(Request $request){

       try {

            $notification = array(
                'message' => 'Data berhasil disimpan!',
                'alert-type' => 'success'
            );

            //DB::table('pingconfig')->truncate();
            //DB::table('pingconfig')->delete();

            
            $validator = Validator::make($request->all(), [
                'server_ip' => 'required|ip',
                'server_port' => 'required|numeric',
            ]);
        
            if ($validator->fails())
            {
                //return redirect()->back()->withErrors($validator->errors());
                $notification = array(
                    'message' => 'Data gagal tersimpan. Ada format yang salah!',
                    'alert-type' => 'error'
                );
            }
            else {
                $data = PingConfig::updateOrCreate(
                    ['id' => $request->id],
                    ['server_ip' => $request->server_ip, 'server_port' => $request->server_port ]);
            }
                    
            
       } catch (\Throwable $th) {
        $notification = array(
            'message' => 'Data gagal tersimpan! ',
            'alert-type' => 'error'
        );
        

       }
       return Redirect::to('/ipconfig')->with($notification);
    }
}
