<?php

namespace App\Http\Controllers;

use App\Exports\DisplayOjkExport;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class DisplayOjkController extends Controller
{
    public function index()
    {
        // write code here

        return view('admin.display-ojk', ['activeSidebar' => '']);
    }

    public function getData(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) AS allcount
            FROM
                PT_LNC001TM P WITH (NOLOCK)
                LEFT OUTER JOIN PT_LNC031TM PTLNC031_120001 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120001.INSP_NO
                AND PTLNC031_120001.AAB_CD = '120001' -- HOME ADDRESS
                LEFT OUTER JOIN PT_LNC031TM PTLNC031_120003 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120003.INSP_NO
                AND PTLNC031_120003.AAB_CD = '120003' -- WORKPLACE ADDRESS
                LEFT OUTER JOIN PT_LNC032TM PTLNC032 WITH (NOLOCK) ON P.INSP_NO = PTLNC032.INSP_NO
                AND PTLNC032.TED_CD = '130001'
                LEFT OUTER JOIN FN_LOAN_MAST M WITH (NOLOCK) ON P.INSP_NO = M.INSP_NO
            WHERE
                P.COM_CD = '01'
			AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) AS allcount
            FROM
                PT_LNC001TM P WITH (NOLOCK)
                LEFT OUTER JOIN PT_LNC031TM PTLNC031_120001 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120001.INSP_NO
                AND PTLNC031_120001.AAB_CD = '120001' -- HOME ADDRESS
                LEFT OUTER JOIN PT_LNC031TM PTLNC031_120003 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120003.INSP_NO
                AND PTLNC031_120003.AAB_CD = '120003' -- WORKPLACE ADDRESS
                LEFT OUTER JOIN PT_LNC032TM PTLNC032 WITH (NOLOCK) ON P.INSP_NO = PTLNC032.INSP_NO
                AND PTLNC032.TED_CD = '130001'
                LEFT OUTER JOIN FN_LOAN_MAST M WITH (NOLOCK) ON P.INSP_NO = M.INSP_NO
            WHERE
                P.COM_CD = '01'
            AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (
                M.ACT_NO LIKE '%$searchValue%' OR
                M.CUS_NO LIKE '%$searchValue%' OR
                P.CUS_NM LIKE '%$searchValue%' OR
                DBO.gt_ft_GetCodeNm(M.COM_CD, M.LN_STS_CD, 'ID') LIKE '%$searchValue%' OR
                DBO.FN_FORMAT_DATE(M.LN_DT) LIKE '%$searchValue%' OR
                (
                    SELECT
                    MAX(NAME)
                    FROM
                    GT_POST_1_PROVINSI WITH (NOLOCK)
                    WHERE
                    ID = PTLNC031_120001.PROVINSI
                ) LIKE '%$searchValue%' OR
                dbo.gt_ft_GetCodeNm(P.COM_CD, P.BUSN_FLD, 'ID') LIKE '%$searchValue%' OR
                (CASE WHEN M.DFR_DAY IS NULL THEN 'Lancar' WHEN M.DFR_DAY = 0 THEN 'Lancar' WHEN M.DFR_DAY < 30 THEN 'Lancar' WHEN M.DFR_DAY BETWEEN 30
                AND 90 THEN 'Tidak Lancar' ELSE 'Macet' END) LIKE '%$searchValue%' OR
                P.KTP LIKE '%$searchValue%'
                )
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
            M.ACT_NO as NOMOR_PINJAMAN,
            M.CUS_NO as NOMOR_CUSTOMER,
            (
                SELECT
                LN_PRD_NM
                FROM
                GT_LOAN_CODE WITH (NOLOCK)
                WHERE
                COM_CD = M.COM_CD
                AND LN_PRD_CD = M.LN_PRD_CD
            ) as NAMA_PRODUK,
            P.CUS_NM as NAMA_CUSTOMER,
            P.KTP as KTP,
            DBO.FN_FORMAT_DATE(BIRTHDAY) AS TANGGAL_LAHIR,
            dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') as JENIS_KELAMIN,
            DBO.gt_ft_GetCodeNm(M.COM_CD, M.LN_STS_CD, 'ID') as STATUS_PENGAJUAN,
            DBO.FN_FORMAT_DATE(M.LN_DT) as PERIODE_PINJAMAN,
            DBO.FN_FORMAT_DATE(M.LN_DT) as BULAN_PINJAMAN,
            DBO.FN_FORMAT_DATE(M.XPR_DT) as JATUH_TEMPO,
            M.LN_AM as NOMINAL_PINJAMAN,
            M.LN_AM - M.LN_BAL_AM as NOMINAL_PENGEMBALIAN,
            M.LN_BAL_AM as SALDO_PINJAMAN,
            CONFIRM_LN_AM - (
                SELECT
                SUM(INT_AMT + WON_AMT)
                FROM
                PT_LOAN_PLAN WITH (NOLOCK)
                WHERE
                INSP_NO = P.INSP_NO
                AND SCH_DT = P.LN_DT
            ) - CONVERT(
                numeric(18, 0),
                FLOOR(
                CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
            ) as NOMINAL_PEMBAYARAN,
            CONVERT(
                numeric(18, 0),
                FLOOR(
                CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
            ) as BIAYA_ADMINISTRASI,
            (
                SELECT
                SUM(INT_AMT + WON_AMT)
                FROM
                PT_LOAN_PLAN
                WHERE
                INSP_NO = M.INSP_NO
                AND SCH_DT = M.LN_DT
            ) as BUNGA_AWAL,
            CONVERT(
                numeric(18, 0),
                FLOOR(
                CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
            ) + (
                SELECT
                SUM(INT_AMT + WON_AMT)
                FROM
                PT_LOAN_PLAN
                WHERE
                INSP_NO = M.INSP_NO
                AND SCH_DT = M.LN_DT
            ) as ADM_PLUS_AWAL,
            dbo.gt_ft_GetCodeNm(M.COM_CD, M.LN_DAY_CD, 'ID') as TENOR_PINJAMAN,
            M.NML_IRT as SUKU_BUNGA_PINJAMAN,
            M.DFR_IRT as SUKU_BUNGA_TUNGGAKAN,
            DAY(
                DBO.FN_FORMAT_DATE(P.XPR_DT)
            ) as TANGGAL_JATUH_TEMPO_BUNGA,
            DBO.FN_FORMAT_DATE(INT_END_DT) as TANGGAL_REPAYMENT_TERAKHIR,
            DBO.FN_FORMAT_DATE(M.LN_DT) as TANGGAL_PERSETUJUAN,
            ISNULL(M.DFR_DAY, 0) as JUMLAH_HARI_TUNGGAKAN_SAAT_INI,
            CASE WHEN M.DFR_DAY IS NULL THEN 'Lancar' WHEN M.DFR_DAY = 0 THEN 'Lancar' WHEN M.DFR_DAY < 30 THEN 'Lancar' WHEN M.DFR_DAY BETWEEN 30
            AND 90 THEN 'Tidak Lancar' ELSE 'Macet' END as KATEGORI_DPD,
            (
                SELECT
                MAX(NAME)
                FROM
                GT_POST_1_PROVINSI WITH (NOLOCK)
                WHERE
                ID = PTLNC031_120001.PROVINSI
            ) as PROVINCE,
            CASE WHEN ISDATE(P.BIRTHDAY) = 1 THEN ISNULL(
                DBO.FN_AGE_INSP(P.INSP_NO, BIRTHDAY, '2'),
                0
            ) ELSE 0 END as USIA,
            CASE WHEN CONVERT(
                VARCHAR,
                ISDATE(P.BIRTHDAY)
            ) = '1' THEN CASE WHEN CONVERT(
                VARCHAR,
                ISNULL(
                DBO.FN_AGE_INSP(P.INSP_NO, BIRTHDAY, '2'),
                0
                )
            ) BETWEEN '19'
            AND '34' THEN '19-34' WHEN CONVERT(
                VARCHAR,
                ISNULL(
                DBO.FN_AGE_INSP(P.INSP_NO, BIRTHDAY, '2'),
                0
                )
            ) BETWEEN '35'
            AND '54' THEN '35-54' ELSE '0' END ELSE '0' END AS KATEGORI_USIA,
            dbo.gt_ft_GetCodeNm(P.COM_CD, P.BUSN_FLD, 'ID') as AREA_USAHA,
            '' as KODE_USAHA,
            'Book off November' as KETERANGAN_TAMBAHAN,
            dbo.gt_ft_GetCodeNm(M.COM_CD, M.INSP_STA_CD, 'ID') as KLASIFIKASI_PINJAMAN,
            DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID') as BULAN,
            DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID') AS JUMLAH_HARI
            FROM
            PT_LNC001TM P WITH (NOLOCK)
            LEFT OUTER JOIN PT_LNC031TM PTLNC031_120001 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120001.INSP_NO
            AND PTLNC031_120001.AAB_CD = '120001' -- HOME ADDRESS
            LEFT OUTER JOIN PT_LNC031TM PTLNC031_120003 WITH (NOLOCK) ON P.INSP_NO = PTLNC031_120003.INSP_NO
            AND PTLNC031_120003.AAB_CD = '120003' -- WORKPLACE ADDRESS
            LEFT OUTER JOIN PT_LNC032TM PTLNC032 WITH (NOLOCK) ON P.INSP_NO = PTLNC032.INSP_NO
            AND PTLNC032.TED_CD = '130001'
            LEFT OUTER JOIN FN_LOAN_MAST M WITH (NOLOCK) ON P.INSP_NO = M.INSP_NO
            WHERE
            P.COM_CD = '01'
			AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
            AND (
                M.ACT_NO LIKE '%$searchValue%' OR
                M.CUS_NO LIKE '%$searchValue%' OR
                P.CUS_NM LIKE '%$searchValue%' OR
                DBO.gt_ft_GetCodeNm(M.COM_CD, M.LN_STS_CD, 'ID') LIKE '%$searchValue%' OR
                DBO.FN_FORMAT_DATE(M.LN_DT) LIKE '%$searchValue%' OR
                (
                    SELECT
                    MAX(NAME)
                    FROM
                    GT_POST_1_PROVINSI WITH (NOLOCK)
                    WHERE
                    ID = PTLNC031_120001.PROVINSI
                ) LIKE '%$searchValue%' OR
                dbo.gt_ft_GetCodeNm(P.COM_CD, P.BUSN_FLD, 'ID') LIKE '%$searchValue%' OR
                (CASE WHEN M.DFR_DAY IS NULL THEN 'Lancar' WHEN M.DFR_DAY = 0 THEN 'Lancar' WHEN M.DFR_DAY < 30 THEN 'Lancar' WHEN M.DFR_DAY BETWEEN 30
                AND 90 THEN 'Tidak Lancar' ELSE 'Macet' END) LIKE '%$searchValue%' OR
                P.KTP LIKE '%$searchValue%'
                )
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $NOMOR_PINJAMAN = $record->NOMOR_PINJAMAN;
            $NOMOR_CUSTOMER = $record->NOMOR_CUSTOMER;
            $NAMA_PRODUK = $record->NAMA_PRODUK;
            $NAMA_CUSTOMER = $record->NAMA_CUSTOMER;
            $KTP = $record->KTP;
            $TANGGAL_LAHIR = $record->TANGGAL_LAHIR;
            $JENIS_KELAMIN = $record->JENIS_KELAMIN;
            $STATUS_PENGAJUAN = $record->STATUS_PENGAJUAN;
            $PERIODE_PINJAMAN = $record->PERIODE_PINJAMAN;
            $BULAN_PINJAMAN = $record->BULAN_PINJAMAN;
            $JATUH_TEMPO = $record->JATUH_TEMPO;
            $NOMINAL_PINJAMAN = $record->NOMINAL_PINJAMAN;
            $NOMINAL_PENGEMBALIAN = $record->NOMINAL_PENGEMBALIAN;
            $SALDO_PINJAMAN = $record->SALDO_PINJAMAN;
            $NOMINAL_PEMBAYARAN = $record->NOMINAL_PEMBAYARAN;
            $BIAYA_ADMINISTRASI = $record->BIAYA_ADMINISTRASI;
            $BUNGA_AWAL = $record->BUNGA_AWAL;
            $ADM_PLUS_AWAL = $record->ADM_PLUS_AWAL;
            $TENOR_PINJAMAN = $record->TENOR_PINJAMAN;
            $SUKU_BUNGA_PINJAMAN = $record->SUKU_BUNGA_PINJAMAN;
            $SUKU_BUNGA_TUNGGAKAN = $record->SUKU_BUNGA_TUNGGAKAN;
            $TANGGAL_JATUH_TEMPO_BUNGA = $record->TANGGAL_JATUH_TEMPO_BUNGA;
            $TANGGAL_REPAYMENT_TERAKHIR = $record->TANGGAL_REPAYMENT_TERAKHIR;
            $TANGGAL_PERSETUJUAN = $record->TANGGAL_PERSETUJUAN;
            $JUMLAH_HARI_TUNGGAKAN_SAAT_INI = $record->JUMLAH_HARI_TUNGGAKAN_SAAT_INI;
            $KATEGORI_DPD = $record->KATEGORI_DPD;
            $PROVINCE = $record->PROVINCE;
            $USIA = $record->USIA;
            $KATEGORI_USIA = $record->KATEGORI_USIA;
            $AREA_USAHA = $record->AREA_USAHA;
            $KODE_USAHA = $record->KODE_USAHA;
            $KETERANGAN_TAMBAHAN = $record->KETERANGAN_TAMBAHAN;
            $KLASIFIKASI_PINJAMAN = $record->KLASIFIKASI_PINJAMAN;
            $BULAN = $record->BULAN;
            $JUMLAH_HARI = $record->JUMLAH_HARI;

            $data_arr[] = array(
                "NOMOR_PINJAMAN" => $NOMOR_PINJAMAN,
                "NOMOR_CUSTOMER" => $NOMOR_CUSTOMER,
                "NAMA_PRODUK" => $NAMA_PRODUK,
                "NAMA_CUSTOMER" => $NAMA_CUSTOMER,
                "KTP" => $KTP,
                "TANGGAL_LAHIR" => $TANGGAL_LAHIR,
                "JENIS_KELAMIN" => $JENIS_KELAMIN,
                "STATUS_PENGAJUAN" => $STATUS_PENGAJUAN,
                "PERIODE_PINJAMAN" => $PERIODE_PINJAMAN,
                "BULAN_PINJAMAN" => $BULAN_PINJAMAN,
                "JATUH_TEMPO" => $JATUH_TEMPO,
                "NOMINAL_PINJAMAN" => $NOMINAL_PINJAMAN,
                "NOMINAL_PENGEMBALIAN" => $NOMINAL_PENGEMBALIAN,
                "SALDO_PINJAMAN" => $SALDO_PINJAMAN,
                "NOMINAL_PEMBAYARAN" => $NOMINAL_PEMBAYARAN,
                "BIAYA_ADMINISTRASI" => $BIAYA_ADMINISTRASI,
                "BUNGA_AWAL" => $BUNGA_AWAL,
                "ADM_PLUS_AWAL" => $ADM_PLUS_AWAL,
                "TENOR_PINJAMAN" => $TENOR_PINJAMAN,
                "SUKU_BUNGA_PINJAMAN" => $SUKU_BUNGA_PINJAMAN,
                "SUKU_BUNGA_TUNGGAKAN" => $SUKU_BUNGA_TUNGGAKAN,
                "TANGGAL_JATUH_TEMPO_BUNGA" => $TANGGAL_JATUH_TEMPO_BUNGA,
                "TANGGAL_REPAYMENT_TERAKHIR" => $TANGGAL_REPAYMENT_TERAKHIR,
                "TANGGAL_PERSETUJUAN" => $TANGGAL_PERSETUJUAN,
                "JUMLAH_HARI_TUNGGAKAN_SAAT_INI" => $JUMLAH_HARI_TUNGGAKAN_SAAT_INI,
                "KATEGORI_DPD" => $KATEGORI_DPD,
                "PROVINCE" => $PROVINCE,
                "USIA" => $USIA,
                "KATEGORI_USIA" => $KATEGORI_USIA,
                "AREA_USAHA" => $AREA_USAHA,
                "KODE_USAHA" => $KODE_USAHA,
                "KETERANGAN_TAMBAHAN" => $KETERANGAN_TAMBAHAN,
                "KLASIFIKASI_PINJAMAN" => $KLASIFIKASI_PINJAMAN,
                "BULAN" => $BULAN,
                "JUMLAH_HARI" => $JUMLAH_HARI,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportData($type, Request $request)
    {
        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        if ($password == 'admin123') {
            return (new DisplayOjkExport($fromDate, $toDate))->download('DataDisplayOjk.'.$type);
        } else {
            return Redirect::to('/displayojk')->with($notification);
        }



    }
}
