<?php

namespace App\Http\Controllers;

use App\Events\EventVirtualAccount;
use App\Events\EventReadyMitra;
use Illuminate\Support\Facades\Auth;
use App\CustomerOrderToMitra;
use App\CallbackDisbursement;
use Illuminate\Http\Request;
use App\CallbackFva;
use App\DetailMitra;
use App\ServiceMitra;
use App\detailServiceMitra;
use App\MitraMaps;
use App\Message;
use App\BankCode;
use App\Rekening;
use App\RequestDisbursement;
use App\TransactionCustomers;
use App\RequestVa;
use App\ResponseVa;
use Xendit\Xendit;
use App\SaldoWallet;
use App\FeeOthers;
use App\SetRekeningTujuan;
use App\ResponseDisbursement;
use App\StatusDisbursement;



class MitraController extends Controller
{
    
    public function orderan(){
        $data = DetailMitra::where('uid', Auth::user()->id)->first();
        //Cek Data Terakhir Order Yang Berjalan
        $cekDataCusToMitra = CustomerOrderToMitra::with('User')
        ->where('uid_mitra',Auth::user()->id)
        ->orderBy('id', 'desc')->first();

        $getDataMessage = Message::where('to',Auth::user()->id)
        ->orderBy('id', 'desc')->get();

        if(empty($cekDataCusToMitra)){
            $detailServiceMitra = null;
        }else{
            $detailServiceMitra = detailServiceMitra::where('id_service_mitra',$cekDataCusToMitra->id_service)
            ->where('duration', $cekDataCusToMitra->duration)->first();
        }

    


        return view('mitras.orderan',['data' => $data, 
        'cekDataCusToMitra' => $cekDataCusToMitra,
        'DetailServiceMitra' => $detailServiceMitra,
        'getDataMessage' => $getDataMessage]);
    }



    public function ShowToken() {
        echo csrf_token(); 
    }




    public function orderan_detail(){
        
        $data = DetailMitra::where('uid', Auth::user()->id)->first();

        // Cek Data Order dari customer
        $cekDataCusToMitra = CustomerOrderToMitra::with('User')
        ->where('uid_mitra',Auth::user()->id)
        ->where('status_order',1)
        ->orderBy('created_at', 'desc')->first();
     

        $ServiceMitra = ServiceMitra::where('id',$cekDataCusToMitra->id_service)->first();
       
     
        $DetailService = detailServiceMitra::where('id_service_mitra', $cekDataCusToMitra->id_service)
        ->where('duration',$cekDataCusToMitra->duration)
        ->first();

        $id_detail_service = $DetailService->id;

    
        // Cek Saldo Tidak Mencukupi atau saldo Kosong
        $SaldoWallet = SaldoWallet::where('uid', Auth::user()->id)
        ->OrderBy('id', 'DESC')
        ->first();




        $TransactionCustomers = TransactionCustomers::where('id_customers', $cekDataCusToMitra->uid_customer)
        ->OrderBy('id', 'DESC')->first();

        $boolean = false;

        $description = '';

        if(empty($SaldoWallet)){

            $boolean = true;
            $description = 'Maaf, layanan tidak dapat dimulai, lakukan isi saldo sekarang';

        }else{

            if($SaldoWallet->saldo == "0"){
                $boolean = true;
                $description = 'Maaf, layanan tidak dapat dimulai saldo Kosong, lakukan isi saldo sekarang';
                
            }

            if($SaldoWallet->saldo < $TransactionCustomers->amount){
                $boolean = true;
                $description = 'Maaf, layanan tidak dapat dimulai saldo Kurang dari transaksi, lakukan isi saldo sekarang';
            }

        }



        $latitude_customers = $cekDataCusToMitra->lat_customers;
        $longitude_customers =$cekDataCusToMitra->lng_customers;


        // Cek Status yang sedang disbursement
        $StatusDisbursement = StatusDisbursement::where('uid', Auth::user()->id)
        ->OrderBy('id', 'DESC')->first();

        if(empty($StatusDisbursement)){
            $status_order = 0;
        }else{
            $status_order = $StatusDisbursement->status;
        }

       
     
     

        return view('mitras.orderan-detail-1',['data' => $data,
            'cekDataCusToMitra' => $cekDataCusToMitra,
            'boolean' => $boolean,
            'description' => $description,
            'latitude_customers' => $latitude_customers,
            'longitude_customers' => $longitude_customers,
            'ServiceMitra' => $ServiceMitra,
            'DetailService' => $DetailService,
            'id_detail_service' => $id_detail_service,
            'status_order' => $status_order
         ]);
    }


    public function countDownMitra(){

        // Cek Data Order dari customer
        $cekDataCusToMitra = CustomerOrderToMitra::with('User')
        ->where('uid_mitra',Auth::user()->id)
        ->where('status_order',1)
        ->orderBy('created_at', 'desc')->first();

        $detailServiceMitra = detailServiceMitra::where('duration',$cekDataCusToMitra->duration)
        ->where('id_service_mitra', $cekDataCusToMitra->id_service)->first();
        
        $real_time = $detailServiceMitra->real_time;

        $status_mulai_layanan = $cekDataCusToMitra->status_mulai_layanan;

        //Cek Status Status Bayar
        $TransactionCustomers = TransactionCustomers::where('uid_mitra', Auth::user()->id)
        ->OrderBy('id', 'DESC')
        ->first();

 
        $latitude_customers = $cekDataCusToMitra->lat_customers;
        $longitude_customers =$cekDataCusToMitra->lng_customers;

        return view('mitras.orderan-detail-2', ['latitude_customers' => $latitude_customers, 
        'longitude_customers' => $longitude_customers,
        'real_time' => $real_time,
        'status_mulai_layanan' => $status_mulai_layanan]);
    }






    public function saldo(){
        $Bankcode = BankCode::all();
        $SetRekeningTujuan = SetRekeningTujuan::with('Rekening')
        ->where('uid', Auth::user()->id)->first();

        if(empty($SetRekeningTujuan->id_rekening)){
            $RekeningTujuan = null;
            $BankCode = 0;
        }else{
            $RekeningTujuan = $SetRekeningTujuan->id_rekening;
            $BankCode = $SetRekeningTujuan->bank_code;
        }
            



            $SaldoWallet = SaldoWallet::where('uid', Auth::user()->id)
            ->OrderBy('id', 'DESC')
            ->first();

            if(empty($SaldoWallet)){
                $SaldoWallet = 0;
            }else{
                $SaldoWallet = $SaldoWallet->saldo;
            }
           
        
        $Rekening = Rekening::with('BankCode')->where('uid', Auth::user()->id)->get();
   
        return view('mitras.saldo', ['Bankcode' => $Bankcode, 'Rekening' => $Rekening, 'SaldoWallet' => $SaldoWallet,'RekeningTujuan' => $RekeningTujuan,
        'BankCode' => $BankCode]);
    }







    public function aktivitas(){
        return view('mitras.aktivitas');
    }


    public function profileMitra(){
        $DetailMitra = DetailMitra::where('uid', Auth::user()->id)->first();
        return view('mitras.profile-mitra',['DetailMitra' => $DetailMitra]);
    }


    public function edit(){
        $DetailMitra = DetailMitra::where('uid', Auth::user()->id)->first();
        $MitraMaps = MitraMaps::where('id_mitra', $DetailMitra->id)->first();
        if($MitraMaps !== null){
            $latitude = $MitraMaps->latitude;
            $longitude = $MitraMaps->longitude;
            $address = $MitraMaps->address;
        }else{
            $latitude = null;
            $longitude = null;
            $address = null;
        }
 
        return view('mitras.profile-mitra-edit',['DetailMitra' => $DetailMitra,
         'latitude' => $latitude,
         'longitude' => $longitude,
         'address' => $address
         ]);
    }





    public function updateData(Request $Request){
    
        $this->validate($Request, [
            'jenis_kelamin' => 'required',
            'kota' => 'required',
            'alamat_lengkap' => 'required',
            'kecamatan' => 'required',
            'no_hp' => 'required',
            'nik' => 'required',
            'tgl_lahir' => 'required',
            'lat'  => 'required',
            'lng' => 'required',
            'address'  => 'required'
        ]);

        $DetailMitra = DetailMitra::where('uid', Auth::user()->id)->first();
        
        $MitraMaps = MitraMaps::where('id_mitra',$DetailMitra->id)->first();

        $newDate = date("Y-m-d", strtotime($Request->tgl_lahir));

        if($MitraMaps === null){
            MitraMaps::create([
                'id_mitra' => $DetailMitra->id,
                'jenis_kelamin' => $Request->jenis_kelamin,
                "latitude" => $Request->lat,
                "longitude" => $Request->lng,
                "address" => $Request->address,
                "type" => $DetailMitra->type_layanan,
                "uid" => Auth::user()->id
            ]);
        }else{
            $MitraMaps->update([
                "latitude" => $Request->lat,
                "longitude" => $Request->lng,
                "address" => $Request->address,
                "type" => $DetailMitra->type_layanan,
                'jenis_kelamin' => $Request->jenis_kelamin,
                "uid" => Auth::user()->id
            ]);
        };
        
        $DetailMitra->update([
            "jenis_kelamin" => $Request->jenis_kelamin,
            "kota" => $Request->kota,
            "alamat_lengkap" => $Request->alamat_lengkap,
            "kecamatan" => $Request->kecamatan,
            "no_hp" => $Request->no_hp,
            "nik" => $Request->nik,
            "tgl_lahir" => $newDate,
            "chooseFileSertifikat" => $Request->base64Sertifikat,
            "chooseFileKtp" => $Request->base64Ktp,
            "chooseFileSelfie"=> $Request->base64Selfie,
        ]);
        
        return redirect('home/profile-mitra')->with('status', 'Data Berhasil DiUbah');
    }







    public function updateStatusMaps(Request $request){
        $userId_mitra = $request->uid_mitra;
        $statusReady = $request->status_ready;
        $customerId = $request->customer_id;


        // Cek Mitra Maps
        $MitraMaps = MitraMaps::where('uid', $userId_mitra)->first();

        // Cek Data Customers Yang Order
        $UpdateCustomeOrderMitra = CustomerOrderToMitra::where('uid_mitra', $userId_mitra)
        ->orderBy('id', 'desc')->first();


        $MitraMaps->update(["status_ready" => $statusReady]);

        $UpdateCustomeOrderMitra->update(["status_order" => $statusReady]);
        

        event(new EventReadyMitra($customerId, $MitraMaps));
        
      
        return response()->json(['response'=> 'Status Berhasil']);
    }


    

    public function rekeningMitra(Request $request){
        Rekening::create([
            'bank_code' => $request->bank_code,
            "account_number" => $request->account_number,
            "account_name" => $request->account_name,
            "uid" => Auth::user()->id
        ]);
        return response()->json(['response'=> 'Rekening berhasil dibuat']);
    }

    public function ShowVirtualAccount(Request $request){
        return view('mitras.virtual-account');
    }

    public function ShowBerhasilTopUp(){
        return view('mitras.berhasil-topup');
    }


    public function akhiri_mulai_layanan(Request $request){
        $UpdateCustomeOrderMitra = CustomerOrderToMitra::where('uid_mitra', Auth::user()->id)
        ->orderBy('id', 'desc')->first();

        $MitraMaps = MitraMaps::where('uid', Auth::user()->id)->first();

        $MitraMaps->update(['status_ready' => 0]);

        $StatusDisbursement = StatusDisbursement::where('uid', Auth::user()->id)
        ->OrderBy('id', 'DESC')->first();

        $StatusDisbursement->update(['status' => 0, 'description' => 'Orderan Selesai']);

        $UpdateCustomeOrderMitra->update(['status_mulai_layanan' => 1,'status_order' => 2]);

        return view('mitras.berhasil-akhiri-layanan');
    }








    /////======================= XENDIT ======================= /////


    public function createVirtualAccount(Request $request){
        
        Xendit::setApiKey('xnd_development_EoqRii3SrXIHn4eUf3BrWls7w32mV99YFyA7p0ArO7f8xurOnWx38i8OPlkAMv');
        //Xendit::setApiKey('xnd_production_U0Os59HGEB5U3JxPbaB1Z8igbApyudfl9gjImysCYmqenqh2eKF9VkLJG1yevWxo');
        $id_user =  Auth::user()->id;
        $created_user =  Auth::user()->created_at;
        $username = Auth::user()->name;
        $date =date_create($created_user);
        $date = date_format($date,"Ymd");
        $external_id = $username."-".rand()."-".$id_user.$date;

        $biaya_agung = 0;

        // Cek Bank
        if($request->bank_code == 'BNI' || $request->bank_code == 'BRI'){
            $admin_xendit = 4500 + 450 + $biaya_agung;
        }elseif($request->bank_code == 'BCA'){
            $admin_xendit = 2000 + 200 + $biaya_agung;
        }
        
        $params = [ 
          "external_id" => $external_id,
          "bank_code" => $request->bank_code,
          "name" => $username,
          "is_closed" => true,
          "expected_amount" => $request->nominal_saldo+$admin_xendit
        ];

        $createVA = \Xendit\VirtualAccounts::create($params);

                
        // Save Request
        $params["uid"] = Auth::user()->id;
        RequestVa::create($params);

        // Save Response
        $decode = $createVA;
        ResponseVa::create(['status' => $createVA['status'],
        'currency' => $createVA['currency'],
        'owner_id' => $createVA['owner_id'],
        'external_id'=> $createVA['external_id'],
        'bank_code'=> $createVA['bank_code'],
        'is_closed' => $createVA['is_closed'],
        'name' => $createVA['name'],
        'account_number' => $createVA['account_number'],
        'is_single_use' => $decode['is_single_use'],
        'expiration_date' => $decode['expiration_date'],
        'id_va' => $decode['id'],
        'uid' => $id_user]);

        return response()->json($createVA);
    }
    






    
    function FVACallBack(Request $request){
        $data = file_get_contents("php://input");
        $events = json_decode($data, true);

        $cekCallBack = CallbackFva::where('payment_id', $events['payment_id'])->first();
            
            
        if(!empty($cekCallBack)){
            return response()->json('payment_id Sudah Dibayarkan');
        }

        // Cek Bank untuk pengurangan admin pada saldo

        
        $biaya_agung = 0;

        // Keterangan Admin Xendit  : Biaya Transaksi + PPN 10% + Biaya Agung


        if($events['bank_code'] == 'BNI' || $events['bank_code'] == 'BRI'){
            $admin_xendit = 4500 + 450 + $biaya_agung;
        }elseif($events['bank_code'] == 'BCA'){
            $admin_xendit = 2000 + 200 + $biaya_agung;
        }else{
            return response()->json('Bank Code Tidak Di Temukan');
        }


        $CallbackFva = CallbackFva::create([
        "amount" => $events['amount'] - $admin_xendit,
        "callback_virtual_account_id" => $events['callback_virtual_account_id'],
        "payment_id" => $events['payment_id'],
        "external_id" => $events['external_id'],
        "account_number" => $events['account_number'],
        "merchant_code" => $events['merchant_code'],
        "bank_code" => $events['bank_code'],
        "transaction_timestamp" => $events['transaction_timestamp'],
        "currency" => $events['currency'],
        "created" => $events['created'],
        "uid"=>0
        ]);
        

        // Get UID VA
        $ResponseVa = ResponseVa::where('external_id',$events['external_id'])
        ->orderBy('id', 'DESC')
        ->first();

        // Cek Saldo By Uid
        $SaldoWallet = SaldoWallet::where('uid', $ResponseVa->uid)
        ->orderBy('id', 'DESC')
        ->first();
        
        if(empty($SaldoWallet)){
            $saldo =$events['amount'] - $admin_xendit;
        }else{
            $saldo = $SaldoWallet->saldo + $events['amount'] - $admin_xendit;
        }

        $CreateSaldoWallet = SaldoWallet::create([
            'debet' => $events['amount'] - $admin_xendit,
            'credit' => 0,
            'description' => 'Pembayaran Virtual Account pada Virtual Account '.$events['account_number'],
            'external_id' => $events['external_id'],
            'saldo' => $saldo,
            'saldo_penghasilan' => 0,
            'admin'  => $admin_xendit,
            "uid" => $ResponseVa->uid
        ]);
        
        FeeOthers::create(['amount' => $biaya_agung, 'external_id' => $events['external_id'], 'uid' => $ResponseVa->uid]);

        return response()->json($events);
    }





        function DisbursementCallBack(Request $request){
            $data = file_get_contents("php://input");
            $events = json_decode($data, true);

            // Cek Callback Sudah Di Terima
            $cekCallBack = CallbackDisbursement::where('id_disburse', $events['id'])->first();
            
            
            if(!empty($cekCallBack)){
                return response()->json('Id Sudah Disbursement');
            }
           
            
    
            // Get UID VA
            $ResponseDisbursement = ResponseDisbursement::where('external_id',$events['external_id'])
            ->orderBy('id', 'DESC')
            ->first();

            if(empty($ResponseDisbursement)){
                return response()->json('Belum Memiliki Disbursement');
            }
           


        
            $CallbackDisbursement = CallbackDisbursement::create([
                "status" => $events['status'],
                "user_id" => $events['user_id'],
                "external_id" => $events['external_id'],
                "amount"  => $events['amount'],
                "bank_code"  => $events['bank_code'],
                "account_holder_name"  => $events['account_holder_name'],
                "disbursement_description"  => $events['disbursement_description'],
                "created"  => $events['created'],
                "updated"  => $events['updated'],
                "is_instant"  => $events['is_instant'],
                "id_disburse"  => $events['id'],
                "uid"=>$ResponseDisbursement->uid
            ]);

      

            // Cek Saldo By Uid
            $SaldoWallet = SaldoWallet::where('uid', $ResponseDisbursement->uid)
            ->orderBy('id', 'DESC')
            ->first();
            
  
            $saldo = $SaldoWallet->saldo - $events['amount'];

            $CreateSaldoWallet = SaldoWallet::create([
                'debet' => 0,
                'credit' => $events['amount'],
                'description' => 'Disbursement Mitra To Sadulur',
                'external_id' => $events['external_id'],
                'saldo' => $saldo,
                'saldo_penghasilan' => 0,
                'admin'  => 5500,
                "uid" => $ResponseDisbursement->uid
            ]);
            
            FeeOthers::create(['amount' => 2000, 'external_id' => $events['external_id'], 'uid' => $ResponseDisbursement->uid]);
    
            return response()->json($events);
        }
    

    public function createDisbursement(Request $request){
            Xendit::setApiKey('xnd_development_EoqRii3SrXIHn4eUf3BrWls7w32mV99YFyA7p0ArO7f8xurOnWx38i8OPlkAMv');

            $id_user =  Auth::user()->id;
            $created_user =  Auth::user()->created_at;
            $username = Auth::user()->name;
            $date =date_create($created_user);
            $date = date_format($date,"Ymd");
            $external_id = rand()."-".$id_user.$date;

            $DetailService = detailServiceMitra::where('id', $request->id_detail_service)
            ->first();

            // OMAT !!! TRANSAKSI INI HARUS BERDASARKAN UANG SESUAI TANPA ADMIN
            // CONTOH TOTAL YANG DIBAYAR 100.000 - DISBURSEMENT (5.500)
            // REQUEST YANG DIKIRIM HARUS SESUAI DENGAN UANGNYA TANPA ADMIN

            // Disable For Production
            // $biaya_transaksi_xendit = 5500;
            // $ppn = $biaya_transaksi_xendit * (10/100); 
            // $admin_xendit = $biaya_transaksi_xendit + $ppn;
            // $fee_sadulur_20_persen = $DetailService->amount * 20/100;
            // $total_pembayaran = $admin_xendit + $fee_sadulur_20_persen;


            $total_pembayaran =  $DetailService->amount * 20/100;

            $SetRekeningTujuan = SetRekeningTujuan::where('uid', Auth::user()->id)->first();

            $StatusDisbursement = StatusDisbursement::create([
                'status' => 1,
                'uid' => Auth::user()->id,
                'description' => 'Proses Order'
            ]);

        
            $params = [
                'external_id' => $external_id,
                'amount' => $total_pembayaran,
                'bank_code' => 'BNI',
                'account_holder_name' => 'Sadulur Home Care',
                'account_number' => '12213123',
                'description' => 'Transfer Fee Sadulur Home Care'
            ];

 
        $createDisbursements = \Xendit\Disbursements::create($params);
        $params["uid"] = Auth::user()->id;
        $ResponseDisbursement = RequestDisbursement::create($params);

        ResponseDisbursement::create(['status' => $createDisbursements['status'],
        'user_id' => $createDisbursements['user_id'],
        'external_id'=> $createDisbursements['external_id'],
        'amount'=> $createDisbursements['amount'],
        'bank_code' => $createDisbursements['bank_code'],
        'account_holder_name' => $createDisbursements['account_holder_name'],
        'disbursement_description' => $createDisbursements['disbursement_description'],
        'id_disbursement' => $createDisbursements['id'],
        'uid' => Auth::user()->id]);
        
    
        return response()->json($createDisbursements);
    }




    // Ubah semua Posisi Transaksi 3rd party dulu

    public function TarikSaldo(Request $request){
        Xendit::setApiKey('xnd_development_EoqRii3SrXIHn4eUf3BrWls7w32mV99YFyA7p0ArO7f8xurOnWx38i8OPlkAMv');
       
       
        $id_user =  Auth::user()->id;
        $created_user =  Auth::user()->created_at;
        $username = Auth::user()->name;
        $date =date_create($created_user);
        $date = date_format($date,"Ymd");
        $external_id = rand()."-".$id_user.$date;


        $SetRekeningTujuan = SetRekeningTujuan::with('Rekening')->where('uid', $request->uid)->first();
        if(empty($SetRekeningTujuan)){
            SetRekeningTujuan::create([
                'id_rekening' => $request->id_rekening,
                'bank_code' => $request->bank_code,
                'uid' => $request->uid,
            ]);
        }else{
            $SetRekeningTujuan->update(['id_rekening' => $request->id_rekening,
                                        'bank_code' => $request->bank_code,
                                        'uid' => $request->uid
                                        ]);
        }



        $params = [
            'external_id' => $external_id,
            'amount' => $request->amount,
            'bank_code' => $request->bank_code,
            'account_holder_name' => $SetRekeningTujuan->Rekening->account_name,
            'account_number' => $SetRekeningTujuan->Rekening->account_number,
            'description' => 'Tarik Saldo /an '.$SetRekeningTujuan->Rekening->account_name
          ];
          
       
        $createDisbursements = \Xendit\Disbursements::create($params);

        $params["uid"] = Auth::user()->id;
        $ResponseDisbursement = RequestDisbursement::create($params);

        ResponseDisbursement::create(['status' => $createDisbursements['status'],
        'user_id' => $createDisbursements['user_id'],
        'external_id'=> $createDisbursements['external_id'],
        'amount'=> $createDisbursements['amount'],
        'bank_code' => $createDisbursements['bank_code'],
        'account_holder_name' => $createDisbursements['account_holder_name'],
        'disbursement_description' => $createDisbursements['disbursement_description'],
        'id_disbursement' => $createDisbursements['id'],
        'uid' => Auth::user()->id]);

        return response()->json($createDisbursements);
    }


    public function simulator_payment(){
        return view('mitras.simulated-payment');
    }

    public function simulated(Request $request){

        Xendit::setApiKey('xnd_development_EoqRii3SrXIHn4eUf3BrWls7w32mV99YFyA7p0ArO7f8xurOnWx38i8OPlkAMv');

        $external_id = $request->external_id;
        $amount = $request->expected_amount;

        $RequestFva = RequestVa::where('external_id',$external_id)->first();
        
        if(empty($RequestFva)){
            return response()->json('Virtual Account Tidak Ada Bray !');
        }

        if($RequestFva->expected_amount != $amount){
            return response()->json('Jumlah Tidak Sesuai dengan Expected Amount');
        }

        $params = [
            'amount' => $amount
        ];

        $createPayout = \Xendit\VirtualAccounts::simulatedPayment($external_id, $params);
        return response()->json($createPayout);
    }


    public function Ceksimulated(Request $request){
        $external_id = $request->external_id;

        $RequestFva = RequestVa::where('external_id',$external_id)->first();
        
        if(empty($RequestFva)){
            $params = [
                'status' => 'failed',
                'message' => 'Virtual Account Tidak Ada Bray !'
            ];
            return response()->json($params);
        }

        $params = [
            'status' => 'success',
            'expected_amount' => $RequestFva->expected_amount,
            'message' => 'Data Tersedia',
            'no_pembayaran' =>$external_id
        ];

        return response()->json($params);
    }
    
}
