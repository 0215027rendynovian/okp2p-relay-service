<?php

namespace App\Http\Controllers;

use App\Helpers\AES256;
use App\Helpers\ListHelper;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\pusdafillLogger;
use ErrorException;
use JsonException;

use function GuzzleHttp\json_encode;

class SilarasController extends Controller
{
    public function index()
    {
        return view('admin.index-silaras', ['activeSidebar' => 'pusdafil']);
    }

    public function reg_pengguna()
    {
        return view('admin.pusdafil-reg-pengguna', ['activeSidebar' => 'pusdafil']);
    }

    public function getDataRegPengguna(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) AS allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
        ");

        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) AS allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    DBO.FN_FORMAT_DATE(M.LN_DT) LIKE '%$searchValue%' OR
                    CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%' OR
                    P.CUS_NM LIKE '%$searchValue%' OR
                    P.KTP LIKE '%$searchValue%' OR
                    P.NPWP LIKE '%$searchValue%' OR
                    P.BIRTH LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(BIRTHDAY) LIKE '%$searchValue%' OR
                    DBO.FN_GET_GT_ADDRRESS(C.CUS_NO, '120001') LIKE '%$searchValue%' OR
                    (
                        SELECT
                        OJK_CODE
                        FROM
                        GT_POST_2_KABUPATEN WITH (NOLOCK)
                        WHERE
                        ID = KABUPATEN
                    ) LIKE '%$searchValue%' OR
                    (
                        SELECT
                        OJK_CODE
                        FROM
                        GT_POST_1_PROVINSI WITH (NOLOCK)
                        WHERE
                        ID = PROVINSI
                    ) LIKE '%$searchValue%'
                )
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                '1' as jenis_pengguna,
                DBO.FN_FORMAT_DATE(M.LN_DT) as tgl_registrasi,
                P.CUS_NM as nama_pengguna,
                '1' as jenis_identitas,
                P.KTP as no_identitas,
                P.NPWP as no_npwp,
                '5' as id_jenis_badan_hukum,
                P.BIRTH as tempat_lahir,
                DBO.FN_FORMAT_DATE(BIRTHDAY) as tgl_lahir,
                CASE WHEN dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') = 'Pria' THEN 1 WHEN dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') = 'Wanita' THEN 2 END as id_jenis_kelamin,
                SUBSTRING(
                    DBO.FN_GET_GT_ADDRRESS(C.CUS_NO, '120001'),
                    1,
                    100
                ) as alamat,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_POST_2_KABUPATEN WITH (NOLOCK)
                    WHERE
                    ID = KABUPATEN
                ) id_kota,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_POST_1_PROVINSI WITH (NOLOCK)
                    WHERE
                    ID = PROVINSI
                ) as id_provinsi,
                R.KODE_POS as kode_pos,
                CASE WHEN P.AGAMA_CD = '135001' THEN 1 WHEN P.AGAMA_CD = '135002' THEN 2 WHEN P.AGAMA_CD = '135003' THEN 3 WHEN P.AGAMA_CD = '135004' THEN 4 WHEN P.AGAMA_CD = '135005' THEN 5 WHEN P.AGAMA_CD = '135006' THEN 6 WHEN P.AGAMA_CD = '135007' THEN 7 END as id_agama,
                CASE WHEN dbo.gt_ft_GetCodeNm(P.COM_CD, P.WEB_CD, 'ID') = 'Menikah' THEN 1 WHEN dbo.gt_ft_GetCodeNm(P.COM_CD, P.WEB_CD, 'ID') = 'Belum Menikah' THEN 2 END as id_status_perkawinan,
                CASE WHEN P.JOB_CD = '104001' THEN 1 WHEN P.JOB_CD = '104002' THEN 2 WHEN P.JOB_CD = '104003' THEN 3 WHEN P.JOB_CD = '104004' THEN 4 WHEN P.JOB_CD = '104005' THEN 5 WHEN P.JOB_CD = '104006' THEN 6 WHEN P.JOB_CD = '104007' THEN 7 WHEN P.JOB_CD = '104008' THEN 8 ELSE 9999 END as id_pekerjaan,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.BUSN_FLD
                ) as id_bidang_pekerjaan,
                '2' as id_pekerjaan_online,
                CASE WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'UMKM' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_YY_INCOME_CD
                ) WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'PRIBADI CICILAN' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_MM_INCOME_AM_CD
                ) WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'PRIBADI JANGKA PENDEK' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_MM_INCOME_AM_CD
                ) END as pendapatan,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_YY_CD
                ) as pengalaman_kerja,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.LAST_SCHCAR_CD
                ) as id_pendidikan,
                '' as nama_perwakilan,
                '' as no_identitas_perwakilan
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(M.LN_DT) LIKE '%$searchValue%' OR
                    P.CUS_NM LIKE '%$searchValue%' OR
                    P.KTP LIKE '%$searchValue%' OR
                    P.NPWP LIKE '%$searchValue%' OR
                    P.BIRTH LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(BIRTHDAY) LIKE '%$searchValue%' OR
                    DBO.FN_GET_GT_ADDRRESS(C.CUS_NO, '120001') LIKE '%$searchValue%' OR
                    (
                        SELECT
                        OJK_CODE
                        FROM
                        GT_POST_2_KABUPATEN WITH (NOLOCK)
                        WHERE
                        ID = KABUPATEN
                    ) LIKE '%$searchValue%' OR
                    (
                        SELECT
                        OJK_CODE
                        FROM
                        GT_POST_1_PROVINSI WITH (NOLOCK)
                        WHERE
                        ID = PROVINSI
                    ) LIKE '%$searchValue%'
                )
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $id_penyelenggara = $record->id_penyelenggara;
            $id_pengguna = $record->id_pengguna;
            $jenis_pengguna = $record->jenis_pengguna;
            $tgl_registrasi = $record->tgl_registrasi;
            $nama_pengguna = $record->nama_pengguna;
            $jenis_identitas = $record->jenis_identitas;
            $no_identitas = $record->no_identitas;
            $no_npwp = $record->no_npwp;
            $id_jenis_badan_hukum = $record->id_jenis_badan_hukum;
            $tempat_lahir = $record->tempat_lahir;
            $tgl_lahir = $record->tgl_lahir;
            $id_jenis_kelamin = $record->id_jenis_kelamin;
            $alamat = $record->alamat;
            $id_kota = $record->id_kota;
            $id_provinsi = $record->id_provinsi;
            $kode_pos = $record->kode_pos;
            $id_agama = $record->id_agama;
            $id_status_perkawinan = $record->id_status_perkawinan;
            $id_pekerjaan = $record->id_pekerjaan;
            $id_bidang_pekerjaan = $record->id_bidang_pekerjaan;
            $id_pekerjaan_online = $record->id_pekerjaan_online;
            $pendapatan = $record->pendapatan;
            $pengalaman_kerja = $record->pengalaman_kerja;
            $id_pendidikan = $record->id_pendidikan;
            $nama_perwakilan = $record->nama_perwakilan;
            $no_identitas_perwakilan = $record->no_identitas_perwakilan;

            $data_arr[] = array(
                "id_penyelenggara" => $id_penyelenggara,
                "id_pengguna" => $id_pengguna,
                "jenis_pengguna" => $jenis_pengguna,
                "tgl_registrasi" => date('d M Y', strtotime($tgl_registrasi)),
                "nama_pengguna" => $nama_pengguna,
                "jenis_identitas" => $jenis_identitas,
                "no_identitas" => $no_identitas,
                "no_npwp" => $no_npwp,
                "id_jenis_badan_hukum" => $id_jenis_badan_hukum,
                "tempat_lahir" => $tempat_lahir,
                "tgl_lahir" => date('d M Y', strtotime($tgl_lahir)),
                "id_jenis_kelamin" => $id_jenis_kelamin,
                "alamat" => $alamat,
                "id_kota" => $id_kota,
                "id_provinsi" => $id_provinsi,
                "kode_pos" => $kode_pos,
                "id_agama" => $id_agama,
                "id_status_perkawinan" => $id_status_perkawinan,
                "id_pekerjaan" => $id_pekerjaan,
                "id_bidang_pekerjaan" => $id_bidang_pekerjaan,
                "id_pekerjaan_online" => $id_pekerjaan_online,
                "pendapatan" => $pendapatan,
                "pengalaman_kerja" => $pengalaman_kerja,
                "id_pendidikan" => $id_pendidikan,
                "nama_perwakilan" => $nama_perwakilan,
                "no_identitas_perwakilan" => $no_identitas_perwakilan,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportRegPengguna($type, Request $request)
    {
        $redirect = 'reg-pengguna';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        $queryData = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                '1' as jenis_pengguna,
                DBO.FN_FORMAT_DATE(M.LN_DT) as tgl_registrasi,
                P.CUS_NM as nama_pengguna,
                '1' as jenis_identitas,
                P.KTP as no_identitas,
                P.NPWP as no_npwp,
                '5' as id_jenis_badan_hukum,
                P.BIRTH as tempat_lahir,
                DBO.FN_FORMAT_DATE(BIRTHDAY) as tgl_lahir,
                CASE WHEN dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') = 'Pria' THEN 1 WHEN dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') = 'Wanita' THEN 2 END as id_jenis_kelamin,
                SUBSTRING(
                    DBO.FN_GET_GT_ADDRRESS(C.CUS_NO, '120001'),
                    1,
                    100
                ) as alamat,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_POST_2_KABUPATEN WITH (NOLOCK)
                    WHERE
                    ID = KABUPATEN
                ) id_kota,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_POST_1_PROVINSI WITH (NOLOCK)
                    WHERE
                    ID = PROVINSI
                ) as id_provinsi,
                R.KODE_POS as kode_pos,
                CASE WHEN P.AGAMA_CD = '135001' THEN 1 WHEN P.AGAMA_CD = '135002' THEN 2 WHEN P.AGAMA_CD = '135003' THEN 3 WHEN P.AGAMA_CD = '135004' THEN 4 WHEN P.AGAMA_CD = '135005' THEN 5 WHEN P.AGAMA_CD = '135006' THEN 6 WHEN P.AGAMA_CD = '135007' THEN 7 END as id_agama,
                CASE WHEN dbo.gt_ft_GetCodeNm(P.COM_CD, P.WEB_CD, 'ID') = 'Menikah' THEN 1 WHEN dbo.gt_ft_GetCodeNm(P.COM_CD, P.WEB_CD, 'ID') = 'Belum Menikah' THEN 2 END as id_status_perkawinan,
                CASE WHEN P.JOB_CD = '104001' THEN 1 WHEN P.JOB_CD = '104002' THEN 2 WHEN P.JOB_CD = '104003' THEN 3 WHEN P.JOB_CD = '104004' THEN 4 WHEN P.JOB_CD = '104005' THEN 5 WHEN P.JOB_CD = '104006' THEN 6 WHEN P.JOB_CD = '104007' THEN 7 WHEN P.JOB_CD = '104008' THEN 8 ELSE 9999 END as id_pekerjaan,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.BUSN_FLD
                ) as id_bidang_pekerjaan,
                '2' as id_pekerjaan_online,
                CASE WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'UMKM' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_YY_INCOME_CD
                ) WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'PRIBADI CICILAN' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_MM_INCOME_AM_CD
                ) WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'PRIBADI JANGKA PENDEK' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_MM_INCOME_AM_CD
                ) END as pendapatan,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_YY_CD
                ) as pengalaman_kerja,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.LAST_SCHCAR_CD
                ) as id_pendidikan,
                '' as nama_perwakilan,
                '' as no_identitas_perwakilan
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%'
        ");

        // prepare parameter
        $plaintext = json_encode($queryData);
        $encryptionKey = '4bd393e7a457f9023d9ba95fffb5a2e1';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        // prepare content
        $content = $resultDecrypt;

        // file name that will be used in the download
        $fileName = $redirect.'.'.$type;

        // use headers in order to generate the download
        $headers = [
          'Content-type' => 'text/plain',
          'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];

        if ($password == 'admin123') {
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        } else {
            return Redirect::to('/'.$redirect)->with($notification);
        }

    }

    public function reg_borrower()
    {
        return view('admin.pusdafil-reg-borrower', ['activeSidebar' => 'pusdafil']);
    }

    public function getDataRegBorrower(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
        ");

        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    P.INSP_NO LIKE '%$searchValue%' OR
                    CASE WHEN P.BUSN_OWN_CD = '133001' THEN 1 ELSE 2 END LIKE '%$searchValue%' OR
                    CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%'
                )
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                P.INSP_NO as id_borrower,
                '0' as total_aset,
                CASE WHEN P.BUSN_OWN_CD = '133001' THEN 1 ELSE 2 END as status_kepemilikan_rumah
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    P.INSP_NO LIKE '%$searchValue%' OR
                    M.ACT_NO LIKE '%$searchValue%' OR
                    CASE WHEN P.BUSN_OWN_CD = '133001' THEN 1 ELSE 2 END LIKE '%$searchValue%' OR
                    CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%'
                )
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $id_penyelenggara = $record->id_penyelenggara;
            $id_pengguna = $record->id_pengguna;
            $id_borrower = $record->id_borrower;
            $total_aset = $record->total_aset;
            $status_kepemilikan_rumah = $record->status_kepemilikan_rumah;

            $data_arr[] = array(
                "id_penyelenggara" => $id_penyelenggara,
                "id_pengguna" => $id_pengguna,
                "id_borrower" => $id_borrower,
                "total_aset" => $total_aset,
                "status_kepemilikan_rumah" => $status_kepemilikan_rumah,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportRegBorrower($type, Request $request)
    {
        $redirect = 'reg-borrower';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        $queryData = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                P.INSP_NO as id_borrower,
                '0' as total_aset,
                CASE WHEN P.BUSN_OWN_CD = '133001' THEN 1 ELSE 2 END as status_kepemilikan_rumah
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%'
        ");

        // prepare parameter
        $plaintext = json_encode($queryData);
        $encryptionKey = '4bd393e7a457f9023d9ba95fffb5a2e1';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        // prepare content
        $content = $resultDecrypt;

        // file name that will be used in the download
        $fileName = $redirect.'.'.$type;

        // use headers in order to generate the download
        $headers = [
          'Content-type' => 'text/plain',
          'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];

        if ($password == 'admin123') {
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        } else {
            return Redirect::to('/'.$redirect)->with($notification);
        }

    }

    public function reg_lender()
    {
        return view('admin.pusdafil-reg-lender', ['activeSidebar' => 'pusdafil']);
    }

    public function getDataRegLender(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN IN_LNC001TM WITH (NOLOCK) ON P.INSP_NO = IN_LNC001TM.INSP_NO
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN IN_LNC001TM WITH (NOLOCK) ON P.INSP_NO = IN_LNC001TM.INSP_NO
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%'
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                '2000000046' as id_lender,
                '0' as id_negara_domisili,
                '0' as id_kewarganegaraan,
                DBO.GT_FT_GETCODENM(
                IN_LNC001TM.COM_CD, IN_LNC001TM.SOURCE_MONEY,
                'ID'
                ) + ' ' + ISNULL(IN_LNC001TM.SM_INPUT, ' ') as sumber_dana
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN IN_LNC001TM WITH (NOLOCK) ON P.INSP_NO = IN_LNC001TM.INSP_NO
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%'
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $id_penyelenggara = $record->id_penyelenggara;
            $id_pengguna = $record->id_pengguna;
            $id_lender = $record->id_lender;
            $id_negara_domisili = $record->id_negara_domisili;
            $id_kewarganegaraan = $record->id_kewarganegaraan;
            $sumber_dana = $record->sumber_dana;

            $data_arr[] = array(
                "id_penyelenggara" => $id_penyelenggara,
                "id_pengguna" => $id_pengguna,
                "id_lender" => $id_lender,
                "id_negara_domisili" => $id_negara_domisili,
                "id_kewarganegaraan" => $id_kewarganegaraan,
                "sumber_dana" => $sumber_dana,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportRegLender($type, Request $request)
    {
        $redirect = 'reg-lender';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        $queryData = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                '2000000046' as id_lender,
                '0' as id_negara_domisili,
                '0' as id_kewarganegaraan,
                DBO.GT_FT_GETCODENM(
                IN_LNC001TM.COM_CD, IN_LNC001TM.SOURCE_MONEY,
                'ID'
                ) + ' ' + ISNULL(IN_LNC001TM.SM_INPUT, ' ') as sumber_dana
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN IN_LNC001TM WITH (NOLOCK) ON P.INSP_NO = IN_LNC001TM.INSP_NO
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND CONCAT(M.CUS_NO,'', P.INSP_NO) LIKE '%$searchValue%'
        ");

        // prepare parameter
        $plaintext = json_encode($queryData);
        $encryptionKey = '4bd393e7a457f9023d9ba95fffb5a2e1';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        // prepare content
        $content = $resultDecrypt;

        // file name that will be used in the download
        $fileName = $redirect.'.'.$type;

        // use headers in order to generate the download
        $headers = [
          'Content-type' => 'text/plain',
          'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];

        if ($password == 'admin123') {
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        } else {
            return Redirect::to('/'.$redirect)->with($notification);
        }

    }

    public function pengajuan_pinjaman()
    {
        return view('admin.pusdafil-pengajuan-pinjaman', ['activeSidebar' => 'pusdafil']);
    }

    public function getDataPengajuanPinjaman(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    M.CUS_NO LIKE '%$searchValue%' OR
                    M.INSP_NO LIKE '%$searchValue%' OR
                    P.CUS_NM LIKE '%$searchValue%' OR
                    P.RGS_TRN_DH LIKE '%$searchValue%' OR
                    M.LN_AM LIKE '%$searchValue%' OR
                    DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID') LIKE '%$searchValue%' OR
                    P.LN_LMT_AM LIKE '%$searchValue%' OR
                    P.CONFIRM_LN_AM LIKE '%$searchValue%' OR
                    M.NML_IRT LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.BS_DT) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(M.LN_DT) LIKE '%$searchValue%'
                )
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                M.INSP_NO as id_borrower,
                '2' as id_syariah,
                '5' as id_status_pengajuan_pinjaman,
                P.CUS_NM as nama_pinjaman,
                P.RGS_TRN_DH as tgl_pengajuan_pinjaman,
                M.LN_AM as nilai_permohonan_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 2
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 6
                WHEN '12 bulan' THEN 12
                WHEN '18 bulan' THEN 18
                WHEN '24 bulan' THEN 24
                ELSE 30 END as jangka_waktu_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 3
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 3
                WHEN '12 bulan' THEN 3
                WHEN '18 bulan' THEN 3
                WHEN '24 bulan' THEN 3
                ELSE 1 END as satuan_jangka_waktu_pinjaman,
                'e40' as penggunaan_pinjaman,
                '2' as agunan,
                '8' as jenis_agunan,
                '' as rasio_pinjaman_nilai_agunan,
                -- Tidak Mandatory
                '' as permintaan_jaminan,
                -- Tidak Mandatory
                '' as rasio_pinjaman_aset,
                -- Tidak Mandatory
                '' as cicilan_bulan,
                -- Tidak Mandatory
                ISNULL(P.CSS_SCORE,0) as rating_pengajuan_pinjaman,
                P.LN_LMT_AM as nilai_plafond,
                P.CONFIRM_LN_AM as nilai_pengajuan_pinjaman,
                M.NML_IRT as suku_bunga_pinjaman,
                '3' as satuan_suku_bunga_pinjaman,
                '1' as jenis_bunga,
                DBO.FN_FORMAT_DATE(P.BS_DT) as tgl_mulai_publikasi_pinjaman,
                '1' as rencana_jangka_waktu_publikasi,
                '1' as realisasi_jangka_waktu_publikasi,
                DBO.FN_FORMAT_DATE(M.LN_DT) as tgl_mulai_pendanaan,
                (SELECT COUNT(A.KTP) FROM PT_LNC001TM A WHERE INSP_NO = P.INSP_NO GROUP BY A.KTP) as frekuensi_pinjaman
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    M.ACT_NO LIKE '%$searchValue%' OR
                    M.CUS_NO LIKE '%$searchValue%' OR
                    M.INSP_NO LIKE '%$searchValue%' OR
                    P.CUS_NM LIKE '%$searchValue%' OR
                    P.RGS_TRN_DH LIKE '%$searchValue%' OR
                    M.LN_AM LIKE '%$searchValue%' OR
                    DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID') LIKE '%$searchValue%' OR
                    P.LN_LMT_AM LIKE '%$searchValue%' OR
                    P.CONFIRM_LN_AM LIKE '%$searchValue%' OR
                    M.NML_IRT LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.BS_DT) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(M.LN_DT) LIKE '%$searchValue%'
                )
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $id_penyelenggara = $record->id_penyelenggara;
            $id_pinjaman = $record->id_pinjaman;
            $id_borrower = $record->id_borrower;
            $id_syariah = $record->id_syariah;
            $id_status_pengajuan_pinjaman = $record->id_status_pengajuan_pinjaman;
            $nama_pinjaman = $record->nama_pinjaman;
            $tgl_pengajuan_pinjaman = $record->tgl_pengajuan_pinjaman;
            $nilai_permohonan_pinjaman = $record->nilai_permohonan_pinjaman;
            $jangka_waktu_pinjaman = $record->jangka_waktu_pinjaman;
            $satuan_jangka_waktu_pinjaman = $record->satuan_jangka_waktu_pinjaman;
            $penggunaan_pinjaman = $record->penggunaan_pinjaman;
            $agunan = $record->agunan;
            $jenis_agunan = $record->jenis_agunan;
            $rasio_pinjaman_nilai_agunan = $record->rasio_pinjaman_nilai_agunan;
            $permintaan_jaminan = $record->permintaan_jaminan;
            $rasio_pinjaman_aset = $record->rasio_pinjaman_aset;
            $cicilan_bulan = $record->cicilan_bulan;
            $rating_pengajuan_pinjaman = $record->rating_pengajuan_pinjaman;
            $nilai_plafond = $record->nilai_plafond;
            $nilai_pengajuan_pinjaman = $record->nilai_pengajuan_pinjaman;
            $suku_bunga_pinjaman = $record->suku_bunga_pinjaman;
            $satuan_suku_bunga_pinjaman = $record->satuan_suku_bunga_pinjaman;
            $jenis_bunga = $record->jenis_bunga;
            $tgl_mulai_publikasi_pinjaman = $record->tgl_mulai_publikasi_pinjaman;
            $rencana_jangka_waktu_publikasi  = $record->rencana_jangka_waktu_publikasi ;
            $realisasi_jangka_waktu_publikasi  = $record->realisasi_jangka_waktu_publikasi ;
            $tgl_mulai_pendanaan = $record->tgl_mulai_pendanaan;
            $frekuensi_pinjaman = $record->frekuensi_pinjaman;

            $data_arr[] = array(
                "id_penyelenggara" => $id_penyelenggara,
                "id_pinjaman" => $id_pinjaman,
                "id_borrower" => $id_borrower,
                "id_syariah" => $id_syariah,
                "id_status_pengajuan_pinjaman" => $id_status_pengajuan_pinjaman,
                "nama_pinjaman" => $nama_pinjaman,
                "tgl_pengajuan_pinjaman" => date('d M Y H:i:s', strtotime($tgl_pengajuan_pinjaman)),
                "nilai_permohonan_pinjaman" => $nilai_permohonan_pinjaman,
                "jangka_waktu_pinjaman" => $jangka_waktu_pinjaman,
                "satuan_jangka_waktu_pinjaman" => $satuan_jangka_waktu_pinjaman,
                "penggunaan_pinjaman" => $penggunaan_pinjaman,
                "agunan" => $agunan,
                "jenis_agunan" => $jenis_agunan,
                "rasio_pinjaman_nilai_agunan" => $rasio_pinjaman_nilai_agunan,
                "permintaan_jaminan" => $permintaan_jaminan,
                "rasio_pinjaman_aset" => $rasio_pinjaman_aset,
                "cicilan_bulan" => $cicilan_bulan,
                "rating_pengajuan_pinjaman" => $rating_pengajuan_pinjaman,
                "nilai_plafond" => $nilai_plafond,
                "nilai_pengajuan_pinjaman" => $nilai_pengajuan_pinjaman,
                "suku_bunga_pinjaman" => $suku_bunga_pinjaman,
                "satuan_suku_bunga_pinjaman" => $satuan_suku_bunga_pinjaman,
                "jenis_bunga" => $jenis_bunga,
                "tgl_mulai_publikasi_pinjaman" => date('d M Y', strtotime($tgl_mulai_publikasi_pinjaman)),
                "rencana_jangka_waktu_publikasi" => $rencana_jangka_waktu_publikasi,
                "realisasi_jangka_waktu_publikasi" => $realisasi_jangka_waktu_publikasi,
                "tgl_mulai_pendanaan" => date('d M Y', strtotime($tgl_mulai_pendanaan)),
                "frekuensi_pinjaman" => $frekuensi_pinjaman,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportPengajuanPinjaman($type, Request $request)
    {
        $redirect = 'pengajuan-pinjaman';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        $queryData = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                 M.ACT_NO as id_pinjaman,
                M.INSP_NO as id_borrower,
                '2' as id_syariah,
                '5' as id_status_pengajuan_pinjaman,
                P.CUS_NM as nama_pinjaman,
                P.RGS_TRN_DH as tgl_pengajuan_pinjaman,
                M.LN_AM as nilai_permohonan_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 2
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 6
                WHEN '12 bulan' THEN 12
                WHEN '18 bulan' THEN 18
                WHEN '24 bulan' THEN 24
                ELSE 30 END as jangka_waktu_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 3
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 3
                WHEN '12 bulan' THEN 3
                WHEN '18 bulan' THEN 3
                WHEN '24 bulan' THEN 3
                ELSE 1 END as satuan_jangka_waktu_pinjaman,
                'e40' as penggunaan_pinjaman,
                '2' as agunan,
                '8' as jenis_agunan,
                '' as rasio_pinjaman_nilai_agunan,
                -- Tidak Mandatory
                '' as permintaan_jaminan,
                -- Tidak Mandatory
                '' as rasio_pinjaman_aset,
                -- Tidak Mandatory
                '' as cicilan_bulan,
                -- Tidak Mandatory
                ISNULL(P.CSS_SCORE,0) as rating_pengajuan_pinjaman,
                P.LN_LMT_AM as nilai_plafond,
                P.CONFIRM_LN_AM as nilai_pengajuan_pinjaman,
                M.NML_IRT as suku_bunga_pinjaman,
                '3' as satuan_suku_bunga_pinjaman,
                '1' as jenis_bunga,
                DBO.FN_FORMAT_DATE(P.BS_DT) as tgl_mulai_publikasi_pinjaman,
                '1' as rencana_jangka_waktu_publikasi,
                '1' as realisasi_jangka_waktu_publikasi,
                DBO.FN_FORMAT_DATE(M.LN_DT) as tgl_mulai_pendanaan,
                (SELECT COUNT(A.KTP) FROM PT_LNC001TM A WHERE INSP_NO = P.INSP_NO GROUP BY A.KTP) as frekuensi_pinjaman
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
        ");

        // prepare parameter
        $plaintext = json_encode($queryData);
        $encryptionKey = '4bd393e7a457f9023d9ba95fffb5a2e1';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        // prepare content
        $content = $resultDecrypt;

        // file name that will be used in the download
        $fileName = $redirect.'.'.$type;

        // use headers in order to generate the download
        $headers = [
          'Content-type' => 'text/plain',
          'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];

        if ($password == 'admin123') {
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        } else {
            return Redirect::to('/'.$redirect)->with($notification);
        }

    }

    public function pengajuan_pemberian_pinjaman()
    {
        return view('admin.pusdafil-pengajuan-pemberian-pinjaman', ['activeSidebar' => 'pusdafil']);
    }

    public function getDataPengajuanPemberianPinjaman(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND (
                    M.ACT_NO LIKE '%$searchValue%' OR
                    P.INSP_NO LIKE '%$searchValue%' OR
                    M.CUS_NO LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.CONTRACT_DT) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.LN_DT) LIKE '%$searchValue%' OR
                    P.LN_LMT_AM LIKE '%$searchValue%' OR
                    P.CONFIRM_LN_AM LIKE '%$searchValue%' OR
                    P.VIR_ACNT_NO LIKE '%$searchValue%'
                )
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                P.INSP_NO as no_perjanjian_lender,
                DBO.FN_FORMAT_DATE(P.CONTRACT_DT) as tgl_perjanjian_lender,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_penawaran_pemberian_pinjaman,
                P.LN_LMT_AM as nilai_penawaran_pinjaman,
                P.CONFIRM_LN_AM as nilai_penawaran_disetujui,
                P.VIR_ACNT_NO as no_va_lender
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND (
                    M.ACT_NO LIKE '%$searchValue%' OR
                    P.INSP_NO LIKE '%$searchValue%' OR
                    M.CUS_NO LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.CONTRACT_DT) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.LN_DT) LIKE '%$searchValue%' OR
                    P.LN_LMT_AM LIKE '%$searchValue%' OR
                    P.CONFIRM_LN_AM LIKE '%$searchValue%' OR
                    P.VIR_ACNT_NO LIKE '%$searchValue%'
                )
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $id_penyelenggara = $record->id_penyelenggara;
            $id_pinjaman = $record->id_pinjaman;
            $id_borrower = $record->id_borrower;
            $id_lender = $record->id_lender;
            $no_perjanjian_lender = $record->no_perjanjian_lender;
            $tgl_perjanjian_lender = $record->tgl_perjanjian_lender;
            $tgl_penawaran_pemberian_pinjaman = $record->tgl_penawaran_pemberian_pinjaman;
            $nilai_penawaran_pinjaman = $record->nilai_penawaran_pinjaman;
            $nilai_penawaran_disetujui = $record->nilai_penawaran_disetujui;
            $no_va_lender = $record->no_va_lender;

            $data_arr[] = array(
                "id_penyelenggara" => $id_penyelenggara,
                "id_pinjaman" => $id_pinjaman,
                "id_borrower" => $id_borrower,
                "id_lender" => $id_lender,
                "no_perjanjian_lender" => $no_perjanjian_lender,
                "tgl_perjanjian_lender" => date('d M Y', strtotime($tgl_perjanjian_lender)),
                "tgl_penawaran_pemberian_pinjaman" => date('d M Y', strtotime($tgl_penawaran_pemberian_pinjaman)),
                "nilai_penawaran_pinjaman" => $nilai_penawaran_pinjaman,
                "nilai_penawaran_disetujui" => $nilai_penawaran_disetujui,
                "no_va_lender" => $no_va_lender,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportPengajuanPemberianPinjaman($type, Request $request)
    {
        $redirect = 'pengajuan-pemberian-pinjaman';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        $queryData = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                P.INSP_NO as no_perjanjian_lender,
                DBO.FN_FORMAT_DATE(P.CONTRACT_DT) as tgl_perjanjian_lender,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_penawaran_pemberian_pinjaman,
                P.LN_LMT_AM as nilai_penawaran_pinjaman,
                P.CONFIRM_LN_AM as nilai_penawaran_disetujui,
                P.VIR_ACNT_NO as no_va_lender
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        // prepare parameter
        $plaintext = json_encode($queryData);
        $encryptionKey = '4bd393e7a457f9023d9ba95fffb5a2e1';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        // prepare content
        $content = $resultDecrypt;

        // file name that will be used in the download
        $fileName = $redirect.'.'.$type;

        // use headers in order to generate the download
        $headers = [
          'Content-type' => 'text/plain',
          'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];

        if ($password == 'admin123') {
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        } else {
            return Redirect::to('/'.$redirect)->with($notification);
        }

    }

    public function transaksi_pinjam_meminjam()
    {
        return view('admin.pusdafil-transaksi-pinjam-meminjam', ['activeSidebar' => 'pusdafil']);
    }

    public function getDataTransaksiPinjamMeminjam(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
        ");
        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    P.INSP_NO LIKE '%$searchValue%' OR
                    M.CUS_NO LIKE '%$searchValue%' OR
                    M.ACT_NO LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.CONTRACT_DT) LIKE '%$searchValue%' OR
                    P.LN_DT LIKE '%$searchValue%' OR
                    M.NML_IRT LIKE '%$searchValue%' OR
                    (CONFIRM_LN_AM - (SELECT SUM(INT_AMT + WON_AMT ) FROM PT_LOAN_PLAN WITH (NOLOCK) WHERE INSP_NO = P.INSP_NO AND SCH_DT = P.LN_DT) - CONVERT(numeric(18,0),  FLOOR(CONVERT(FLOAT, CONFIRM_LN_AM) *  CONVERT(FLOAT, FN_FEE_IRT)) /  100 )) LIKE '%$searchValue%' OR
                    DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID' ) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.XPR_DT) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.LN_DT) LIKE '%$searchValue%'
                )
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                M.ACT_NO as id_transaksi,
                P.INSP_NO as no_perjanjian_borrower,
                DBO.FN_FORMAT_DATE(P.CONTRACT_DT)  as tgl_perjanjian_borrower,
                P.LN_DT as nilai_pendanaan,
                M.NML_IRT as suku_bunga_pinjaman,
                '3' as satuan_suku_bunga_pinjaman,
                '1' as id_jenis_pembayaran,  -- 1= Installment, 2=Bullet Payment, 3 = Discount, 4 = Grace Periode, 5 = Lainnya
                '1' as id_frekuensi_pembayaran, -- 1= Harian, 2= Mingguan, 3 = Bulanan, 4 = Triwulan, 5 = Sementara, 6 = Tahunan
                CONFIRM_LN_AM - (SELECT SUM(INT_AMT + WON_AMT ) FROM PT_LOAN_PLAN WITH (NOLOCK) WHERE INSP_NO = P.INSP_NO AND SCH_DT = P.LN_DT) - CONVERT(numeric(18,0),  FLOOR(CONVERT(FLOAT, CONFIRM_LN_AM) *  CONVERT(FLOAT, FN_FEE_IRT)) /  100 ) as nilai_angsuran,
                '' as objek_jaminan,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 2
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 6
                WHEN '12 bulan' THEN 12
                WHEN '18 bulan' THEN 18
                WHEN '24 bulan' THEN 24
                ELSE 30 END as jangka_waktu_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 3
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 3
                WHEN '12 bulan' THEN 3
                WHEN '18 bulan' THEN 3
                WHEN '24 bulan' THEN 3
                ELSE 1 END as satuan_jangka_waktu_pinjaman,
                DBO.FN_FORMAT_DATE(P.XPR_DT) as tgl_jatuh_tempo,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_pendanaan,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_penyaluran_dana,
                '814896477' as no_ea_transaksi,
                1 as frekuensi_pendanaan
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
                AND (
                    P.INSP_NO LIKE '%$searchValue%' OR
                    M.CUS_NO LIKE '%$searchValue%' OR
                    M.ACT_NO LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.CONTRACT_DT) LIKE '%$searchValue%' OR
                    P.LN_DT LIKE '%$searchValue%' OR
                    M.NML_IRT LIKE '%$searchValue%' OR
                    (CONFIRM_LN_AM - (SELECT SUM(INT_AMT + WON_AMT ) FROM PT_LOAN_PLAN WITH (NOLOCK) WHERE INSP_NO = P.INSP_NO AND SCH_DT = P.LN_DT) - CONVERT(numeric(18,0),  FLOOR(CONVERT(FLOAT, CONFIRM_LN_AM) *  CONVERT(FLOAT, FN_FEE_IRT)) /  100 )) LIKE '%$searchValue%' OR
                    DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID' ) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.XPR_DT) LIKE '%$searchValue%' OR
                    DBO.FN_FORMAT_DATE(P.LN_DT) LIKE '%$searchValue%'
                )
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $id_penyelenggara = $record->id_penyelenggara;
            $id_pinjaman = $record->id_pinjaman;
            $id_borrower = $record->id_borrower;
            $id_lender = $record->id_lender;
            $id_transaksi = $record->id_transaksi;
            $no_perjanjian_borrower = $record->no_perjanjian_borrower;
            $tgl_perjanjian_borrower = $record->tgl_perjanjian_borrower;
            $nilai_pendanaan = $record->nilai_pendanaan;
            $suku_bunga_pinjaman = $record->suku_bunga_pinjaman;
            $satuan_suku_bunga_pinjaman = $record->satuan_suku_bunga_pinjaman;
            $id_jenis_pembayaran = $record->id_jenis_pembayaran;
            $id_frekuensi_pembayaran = $record->id_frekuensi_pembayaran;
            $nilai_angsuran = $record->nilai_angsuran;
            $objek_jaminan = $record->objek_jaminan;
            $jangka_waktu_pinjaman = $record->jangka_waktu_pinjaman;
            $satuan_jangka_waktu_pinjaman = $record->satuan_jangka_waktu_pinjaman;
            $tgl_jatuh_tempo = $record->tgl_jatuh_tempo;
            $tgl_pendanaan = $record->tgl_pendanaan;
            $tgl_penyaluran_dana = $record->tgl_penyaluran_dana;
            $no_ea_transaksi = $record->no_ea_transaksi;
            $frekuensi_pendanaan = $record->frekuensi_pendanaan;

            $data_arr[] = array(
                "id_penyelenggara" => $id_penyelenggara,
                "id_pinjaman" => $id_pinjaman,
                "id_borrower" => $id_borrower,
                "id_lender" => $id_lender,
                "id_transaksi" => $id_transaksi,
                "no_perjanjian_borrower" => $no_perjanjian_borrower,
                "tgl_perjanjian_borrower" => $tgl_perjanjian_borrower,
                "nilai_pendanaan" => $nilai_pendanaan,
                "suku_bunga_pinjaman" => $suku_bunga_pinjaman,
                "satuan_suku_bunga_pinjaman" => $satuan_suku_bunga_pinjaman,
                "id_jenis_pembayaran" => $id_jenis_pembayaran,
                "id_frekuensi_pembayaran" => $id_frekuensi_pembayaran,
                "nilai_angsuran" => $nilai_angsuran,
                "objek_jaminan" => $objek_jaminan,
                "jangka_waktu_pinjaman" => $jangka_waktu_pinjaman,
                "satuan_jangka_waktu_pinjaman" => $satuan_jangka_waktu_pinjaman,
                "tgl_jatuh_tempo" => $tgl_jatuh_tempo,
                "tgl_pendanaan" => $tgl_pendanaan,
                "tgl_penyaluran_dana" => $tgl_penyaluran_dana,
                "no_ea_transaksi" => $no_ea_transaksi,
                "frekuensi_pendanaan" => $frekuensi_pendanaan,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportTransaksiPinjamMeminjam($type, Request $request)
    {
        $redirect = 'transaksi-pinjam-meminjam';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        $queryData = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                M.ACT_NO as id_transaksi,
                P.INSP_NO as no_perjanjian_borrower,
                DBO.FN_FORMAT_DATE(P.CONTRACT_DT)  as tgl_perjanjian_borrower,
                P.LN_DT as nilai_pendanaan,
                M.NML_IRT as suku_bunga_pinjaman,
                '3' as satuan_suku_bunga_pinjaman,
                '1' as id_jenis_pembayaran,  -- 1= Installment, 2=Bullet Payment, 3 = Discount, 4 = Grace Periode, 5 = Lainnya
                '1' as id_frekuensi_pembayaran, -- 1= Harian, 2= Mingguan, 3 = Bulanan, 4 = Triwulan, 5 = Sementara, 6 = Tahunan
                CONFIRM_LN_AM - (SELECT SUM(INT_AMT + WON_AMT ) FROM PT_LOAN_PLAN WITH (NOLOCK) WHERE INSP_NO = P.INSP_NO AND SCH_DT = P.LN_DT) - CONVERT(numeric(18,0),  FLOOR(CONVERT(FLOAT, CONFIRM_LN_AM) *  CONVERT(FLOAT, FN_FEE_IRT)) /  100 ) as nilai_angsuran,
                '' as objek_jaminan,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 2
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 6
                WHEN '12 bulan' THEN 12
                WHEN '18 bulan' THEN 18
                WHEN '24 bulan' THEN 24
                ELSE 30 END as jangka_waktu_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 3
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 3
                WHEN '12 bulan' THEN 3
                WHEN '18 bulan' THEN 3
                WHEN '24 bulan' THEN 3
                ELSE 1 END as satuan_jangka_waktu_pinjaman,
                DBO.FN_FORMAT_DATE(P.XPR_DT) as tgl_jatuh_tempo,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_pendanaan,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_penyaluran_dana,
                '814896477' as no_ea_transaksi,
                1 as frekuensi_pendanaan
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND M.BOOK_OFF_YN = 'N'
        ");

        // prepare parameter
        $plaintext = json_encode($queryData);
        $encryptionKey = '4bd393e7a457f9023d9ba95fffb5a2e1';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        // prepare content
        $content = $resultDecrypt;

        // file name that will be used in the download
        $fileName = $redirect.'.'.$type;

        // use headers in order to generate the download
        $headers = [
          'Content-type' => 'text/plain',
          'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];

        if ($password == 'admin123') {
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        } else {
            return Redirect::to('/'.$redirect)->with($notification);
        }

    }

    public function pembayaran_pinjaman()
    {
        return view('admin.pusdafil-pembayaran-pinjaman', ['activeSidebar' => 'pusdafil']);
    }

    public function getDataPembayaranPinjaman(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        $totalRecordswithFilter = DB::connection('sqlsrv')->select("
            SELECT
                COUNT(*) as allcount
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND (
                    P.INSP_NO LIKE '%$searchValue%' OR
                    M.CUS_NO LIKE '%$searchValue%' OR
                    M.ACT_NO LIKE '%$searchValue%' OR
                    M.VIR_ACNT_NO LIKE '%$searchValue%' OR
                    P.XPR_DT LIKE '%$searchValue%' OR
                    P.CONTRACT_DT LIKE '%$searchValue%'
                )
        ");

        // Fetch records
        $records = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                M.ACT_NO as id_transaksi,
                M.VIR_ACNT_NO as id_pembayaran,
                P.XPR_DT as tgl_jatuh_tempo,
                convert(varchar, DATEADD(MONTH, 1, P.XPR_DT), 112) as tgl_jatuh_tempo_selanjutnya,
                P.CONTRACT_DT as tgl_pembayaran_borrower,
                '' as tgl_pembayaran_penyelenggara,
                M.LN_BAL_AM as sisa_pinjaman_berjalan,
                CASE
                WHEN M.DFR_DAY < 30 THEN 1
                WHEN M.DFR_DAY BETWEEN 30 AND 90 THEN 2
                   ELSE
                3 END  as id_status_pinjaman,
                '' as tgl_pelunasan_borrower,
                '' as tgl_pelunasan_penyelenggara,
                (SELECT SUM(MAST.DFR_INT_AM ) FROM FN_LOAN_HIST MAST  WHERE ACT_NO = M.ACT_NO  GROUP BY MAST.ACT_NO)  as denda,
                CONFIRM_LN_AM - (
                SELECT
                    SUM(INT_AMT + WON_AMT)
                FROM
                    PT_LOAN_PLAN WITH (NOLOCK)
                WHERE
                    INSP_NO = P.INSP_NO
                    AND SCH_DT = P.LN_DT
                ) - CONVERT(
                numeric(18, 0),
                FLOOR(
                    CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
                ) as nilai_pembayaran
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
                AND (
                    P.INSP_NO LIKE '%$searchValue%' OR
                    M.CUS_NO LIKE '%$searchValue%' OR
                    M.ACT_NO LIKE '%$searchValue%' OR
                    M.VIR_ACNT_NO LIKE '%$searchValue%' OR
                    P.XPR_DT LIKE '%$searchValue%' OR
                    P.CONTRACT_DT LIKE '%$searchValue%'
                )
            ORDER BY M.LN_DT DESC
            OFFSET $start ROWS
            FETCH NEXT $rowperpage ROWS ONLY
        ");

        $data_arr = array();
        $dataRecord = 0;
        $dataRecordFilter = 0;

        foreach ($totalRecords as $totalRecord) {
            $dataRecord = $totalRecord->allcount;
        }

        foreach ($totalRecordswithFilter as $totalRecordFil) {
            $dataRecordFilter = $totalRecordFil->allcount;
        }

        foreach($records as $record){
            $id_penyelenggara = $record->id_penyelenggara;
            $id_pinjaman = $record->id_pinjaman;
            $id_borrower = $record->id_borrower;
            $id_lender = $record->id_lender;
            $id_transaksi = $record->id_transaksi;
            $id_pembayaran = $record->id_pembayaran;
            $tgl_jatuh_tempo = $record->tgl_jatuh_tempo;
            $tgl_jatuh_tempo_selanjutnya = $record->tgl_jatuh_tempo_selanjutnya;
            $tgl_pembayaran_borrower = $record->tgl_pembayaran_borrower;
            $tgl_pembayaran_penyelenggara = $record->tgl_pembayaran_penyelenggara;
            $sisa_pinjaman_berjalan = $record->sisa_pinjaman_berjalan;
            $id_status_pinjaman = $record->id_status_pinjaman;
            $tgl_pelunasan_borrower = $record->tgl_pelunasan_borrower;
            $tgl_pelunasan_penyelenggara = $record->tgl_pelunasan_penyelenggara;
            $denda = $record->denda;
            $nilai_pembayaran = $record->nilai_pembayaran;

            $data_arr[] = array(
                "id_penyelenggara" => $id_penyelenggara,
                "id_pinjaman" => $id_pinjaman,
                "id_borrower" => $id_borrower,
                "id_lender" => $id_lender,
                "id_transaksi" => $id_transaksi,
                "id_pembayaran" => $id_pembayaran,
                "tgl_jatuh_tempo" => $tgl_jatuh_tempo,
                "tgl_jatuh_tempo_selanjutnya" => $tgl_jatuh_tempo_selanjutnya,
                "tgl_pembayaran_borrower" => $tgl_pembayaran_borrower,
                "tgl_pembayaran_penyelenggara" => $tgl_pembayaran_penyelenggara,
                "sisa_pinjaman_berjalan" => $sisa_pinjaman_berjalan,
                "id_status_pinjaman" => $id_status_pinjaman,
                "tgl_pelunasan_borrower" => $tgl_pelunasan_borrower,
                "tgl_pelunasan_penyelenggara" => $tgl_pelunasan_penyelenggara,
                "denda" => $denda,
                "nilai_pembayaran" => $nilai_pembayaran,
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $dataRecord,
            "iTotalDisplayRecords" => $dataRecordFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function exportPembayaranPinjaman($type, Request $request)
    {
        $redirect = 'pembayaran-pinjaman';

        // get range date
        $fromDateDefault = date('Ymd', strtotime('-1 month', strtotime( date("Ymd") )));
        $toDateDefault = date('Ymd');
        $fromDate = $request->get('fromDate', $fromDateDefault);
        $toDate = $request->get('toDate', $toDateDefault);

        // get password
        $password = $request->get('password', null);

        $notification = array(
            'message' => 'The password you entered does not match our records. Please check back and try again!',
            'alert-type' => 'error'
        );

        $queryData = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                M.ACT_NO as id_transaksi,
                M.VIR_ACNT_NO as id_pembayaran,
                P.XPR_DT as tgl_jatuh_tempo,
                convert(varchar, DATEADD(MONTH, 1, P.XPR_DT), 112) as tgl_jatuh_tempo_selanjutnya,
                P.CONTRACT_DT as tgl_pembayaran_borrower,
                '' as tgl_pembayaran_penyelenggara,
                M.LN_BAL_AM as sisa_pinjaman_berjalan,
                CASE
                WHEN M.DFR_DAY < 30 THEN 1
                WHEN M.DFR_DAY BETWEEN 30 AND 90 THEN 2
                   ELSE
                3 END  as id_status_pinjaman,
                '' as tgl_pelunasan_borrower,
                '' as tgl_pelunasan_penyelenggara,
                (SELECT SUM(MAST.DFR_INT_AM ) FROM FN_LOAN_HIST MAST  WHERE ACT_NO = M.ACT_NO  GROUP BY MAST.ACT_NO)  as denda,
                CONFIRM_LN_AM - (
                SELECT
                    SUM(INT_AMT + WON_AMT)
                FROM
                    PT_LOAN_PLAN WITH (NOLOCK)
                WHERE
                    INSP_NO = P.INSP_NO
                    AND SCH_DT = P.LN_DT
                ) - CONVERT(
                numeric(18, 0),
                FLOOR(
                    CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
                ) as nilai_pembayaran
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$fromDate' AND '$toDate'
        ");

        // prepare parameter
        $plaintext = json_encode($queryData);
        $encryptionKey = '4bd393e7a457f9023d9ba95fffb5a2e1';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        // prepare content
        $content = $resultDecrypt;

        // file name that will be used in the download
        $fileName = $redirect.'.'.$type;

        // use headers in order to generate the download
        $headers = [
          'Content-type' => 'text/plain',
          'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];

        if ($password == 'admin123') {
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        } else {
            return Redirect::to('/'.$redirect)->with($notification);
        }

    }

    public function send_reg_pengguna(Request $request){

            $type = 'reg_pengguna';

            $date_cron = date("Ymd", time() - 3600);

            $param = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                '1' as jenis_pengguna,
                DBO.FN_FORMAT_DATE(M.LN_DT) as tgl_registrasi,
                P.CUS_NM as nama_pengguna,
                '1' as jenis_identitas,
                P.KTP as no_identitas,
                '' as no_npwp,
                '5' as id_jenis_badan_hukum,
                P.BIRTH as tempat_lahir,
                DBO.FN_FORMAT_DATE(BIRTHDAY) as tgl_lahir,
                CASE WHEN dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') = 'Pria' THEN 1 WHEN dbo.gt_ft_GetCodeNm('01', P.SEX_CD, 'ID') = 'Wanita' THEN 2 END as id_jenis_kelamin,
                SUBSTRING(
                    DBO.FN_GET_GT_ADDRRESS(C.CUS_NO, '120001'),
                    1,
                    100
                ) as alamat,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_POST_2_KABUPATEN WITH (NOLOCK)
                    WHERE
                    ID = KABUPATEN
                ) id_kota,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_POST_1_PROVINSI WITH (NOLOCK)
                    WHERE
                    ID = PROVINSI
                ) as id_provinsi,
                R.KODE_POS as kode_pos,
                CASE WHEN P.AGAMA_CD = '135001' THEN 1 WHEN P.AGAMA_CD = '135002' THEN 2 WHEN P.AGAMA_CD = '135003' THEN 3 WHEN P.AGAMA_CD = '135004' THEN 4 WHEN P.AGAMA_CD = '135005' THEN 5 WHEN P.AGAMA_CD = '135006' THEN 6 WHEN P.AGAMA_CD = '135007' THEN 7 END as id_agama,
                CASE WHEN dbo.gt_ft_GetCodeNm(P.COM_CD, P.WEB_CD, 'ID') = 'Menikah' THEN 1 WHEN dbo.gt_ft_GetCodeNm(P.COM_CD, P.WEB_CD, 'ID') = 'Belum Menikah' THEN 2 END as id_status_perkawinan,
                CASE WHEN P.JOB_CD = '104001' THEN 1 WHEN P.JOB_CD = '104002' THEN 2 WHEN P.JOB_CD = '104003' THEN 3 WHEN P.JOB_CD = '104004' THEN 4 WHEN P.JOB_CD = '104005' THEN 5 WHEN P.JOB_CD = '104006' THEN 6 WHEN P.JOB_CD = '104007' THEN 7 WHEN P.JOB_CD = '104008' THEN 8 ELSE 9999 END as id_pekerjaan,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.BUSN_FLD
                ) as id_bidang_pekerjaan,
                '2' as id_pekerjaan_online,
                CASE WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'UMKM' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_YY_INCOME_CD
                ) WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'PRIBADI CICILAN' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_MM_INCOME_AM_CD
                ) WHEN (
                    SELECT
                    LN_PRD_NM
                    FROM
                    GT_LOAN_CODE WITH (NOLOCK)
                    WHERE
                    COM_CD = M.COM_CD
                    AND LN_PRD_CD = M.LN_PRD_CD
                ) = 'PRIBADI JANGKA PENDEK' THEN (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_MM_INCOME_AM_CD
                ) END as pendapatan,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.WORK_YY_CD
                ) as pengalaman_kerja,
                (
                    SELECT
                    OJK_CODE
                    FROM
                    GT_CODE WITH (NOLOCK)
                    WHERE
                    PUB_CD = P.LAST_SCHCAR_CD
                ) as id_pendidikan,
                '' as nama_perwakilan,
                '' as no_identitas_perwakilan
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$date_cron' AND '$date_cron'
                AND M.BOOK_OFF_YN = 'N'
        ");



        // prepare parameter
        $plaintext = json_encode($param);
        $encryptionKey = 'WmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        $sendData = ListHelper::request_pusdafil($resutEncrypt, $type);

        // prepare content
        $content = $resutEncrypt;

    }

    public function send_reg_borrower(Request $request){

        $type = 'reg_borrower';

        $date_cron = date("Ymd", time() - 3600);

        $param = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                P.INSP_NO as id_borrower,
                '0' as total_aset,
                CASE WHEN P.BUSN_OWN_CD = '133001' THEN 1 ELSE 2 END as status_kepemilikan_rumah
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$date_cron' AND '$date_cron'
                AND M.BOOK_OFF_YN = 'N'
        ");


        // prepare parameter
        $plaintext = json_encode($param);
        $encryptionKey = 'WmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        $sendData = ListHelper::request_pusdafil($resutEncrypt, $type);

        // prepare content
        $content = $resutEncrypt;

    }

    public function send_reg_lender(Request $request){

        $type = 'reg_lender';

        $date_cron = date("Ymd", time() - 3600);

        $param = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                CONCAT(M.CUS_NO,'', P.INSP_NO) as id_pengguna,
                '2000000046' as id_lender,
                '0' as id_negara_domisili,
                '0' as id_kewarganegaraan,
                DBO.GT_FT_GETCODENM(
                IN_LNC001TM.COM_CD, IN_LNC001TM.SOURCE_MONEY,
                'ID'
                ) + ' ' + ISNULL(IN_LNC001TM.SM_INPUT, ' ') as sumber_dana
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN IN_LNC001TM WITH (NOLOCK) ON P.INSP_NO = IN_LNC001TM.INSP_NO
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$date_cron' AND '$date_cron'
                AND M.BOOK_OFF_YN = 'N'
        ");



        // prepare parameter
        $plaintext = json_encode($param);
        $encryptionKey = 'WmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        $sendData = ListHelper::request_pusdafil($resutEncrypt, $type);

        // prepare content
        $content = $resutEncrypt;

    }

    public function send_pengajuan_pinjaman(Request $request){

        $type = 'pengajuan_pinjaman';

        $date_cron = date("Ymd", time() - 3600);

        $param = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                M.INSP_NO as id_borrower,
                '2' as id_syariah,
                '5' as id_status_pengajuan_pinjaman,
                P.CUS_NM as nama_pinjaman,
                P.RGS_TRN_DH as tgl_pengajuan_pinjaman,
                M.LN_AM as nilai_permohonan_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 2
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 6
                WHEN '12 bulan' THEN 12
                WHEN '18 bulan' THEN 18
                WHEN '24 bulan' THEN 24
                ELSE 30 END as jangka_waktu_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 3
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 3
                WHEN '12 bulan' THEN 3
                WHEN '18 bulan' THEN 3
                WHEN '24 bulan' THEN 3
                ELSE 1 END as satuan_jangka_waktu_pinjaman,
                'e40' as penggunaan_pinjaman,
                '2' as agunan,
                '8' as jenis_agunan,
                '' as rasio_pinjaman_nilai_agunan,
                -- Tidak Mandatory
                '' as permintaan_jaminan,
                -- Tidak Mandatory
                '' as rasio_pinjaman_aset,
                -- Tidak Mandatory
                '' as cicilan_bulan,
                -- Tidak Mandatory
                ISNULL(P.CSS_SCORE,0) as rating_pengajuan_pinjaman,
                P.LN_LMT_AM as nilai_plafond,
                P.CONFIRM_LN_AM as nilai_pengajuan_pinjaman,
                M.NML_IRT as suku_bunga_pinjaman,
                '3' as satuan_suku_bunga_pinjaman,
                '1' as jenis_bunga,
                DBO.FN_FORMAT_DATE(P.BS_DT) as tgl_mulai_publikasi_pinjaman,
                '1' as rencana_jangka_waktu_publikasi,
                '1' as realisasi_jangka_waktu_publikasi,
                DBO.FN_FORMAT_DATE(M.LN_DT) as tgl_mulai_pendanaan,
                (SELECT COUNT(A.KTP) FROM PT_LNC001TM A WHERE INSP_NO = P.INSP_NO GROUP BY A.KTP) as frekuensi_pinjaman
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$date_cron' AND '$date_cron'
                AND M.BOOK_OFF_YN = 'N'
        ");



        // prepare parameter
        $plaintext = json_encode($param);
        $encryptionKey = 'WmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        $sendData = ListHelper::request_pusdafil($resutEncrypt, $type);

        // prepare content
        $content = $resutEncrypt;

    }

    public function send_pengajuan_pemberian_pinjaman(Request $request){

        $type = 'pengajuan_pemberian_pinjaman';

        $date_cron = date("Ymd", time() - 3600);

        $param = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                P.INSP_NO as no_perjanjian_lender,
                DBO.FN_FORMAT_DATE(P.CONTRACT_DT) as tgl_perjanjian_lender,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_penawaran_pemberian_pinjaman,
                P.LN_LMT_AM as nilai_penawaran_pinjaman,
                P.CONFIRM_LN_AM as nilai_penawaran_disetujui,
                P.VIR_ACNT_NO as no_va_lender
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$date_cron ' AND '$date_cron '
                AND M.BOOK_OFF_YN = 'N'
        ");



        // prepare parameter
        $plaintext = json_encode($param);
        $encryptionKey = 'WmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        $sendData = ListHelper::request_pusdafil($resutEncrypt, $type);

        // prepare content
        $content = $resutEncrypt;

    }

    public function send_transaksi_pinjam_meminjam(Request $request){

        $type = 'transaksi_pinjam_meminjam';

        $date_cron = date("Ymd", time() - 3600);

        $param = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                M.ACT_NO as id_transaksi,
                P.INSP_NO as no_perjanjian_borrower,
                DBO.FN_FORMAT_DATE(P.CONTRACT_DT)  as tgl_perjanjian_borrower,
                P.LN_DT as nilai_pendanaan,
                M.NML_IRT as suku_bunga_pinjaman,
                '3' as satuan_suku_bunga_pinjaman,
                '1' as id_jenis_pembayaran,  -- 1= Installment, 2=Bullet Payment, 3 = Discount, 4 = Grace Periode, 5 = Lainnya
                '1' as id_frekuensi_pembayaran, -- 1= Harian, 2= Mingguan, 3 = Bulanan, 4 = Triwulan, 5 = Sementara, 6 = Tahunan
                CASE
                    WHEN
                    CONFIRM_LN_AM - (SELECT SUM(INT_AMT + WON_AMT ) FROM PT_LOAN_PLAN WITH (NOLOCK) WHERE INSP_NO = P.INSP_NO AND SCH_DT = P.LN_DT) - CONVERT(numeric(18,0),  FLOOR(CONVERT(FLOAT, CONFIRM_LN_AM) *  CONVERT(FLOAT, FN_FEE_IRT)) /  100 ) IS NULL
                    THEN 0
                    ELSE
                    CONFIRM_LN_AM - (SELECT SUM(INT_AMT + WON_AMT ) FROM PT_LOAN_PLAN WITH (NOLOCK) WHERE INSP_NO = P.INSP_NO AND SCH_DT = P.LN_DT) - CONVERT(numeric(18,0),  FLOOR(CONVERT(FLOAT, CONFIRM_LN_AM) *  CONVERT(FLOAT, FN_FEE_IRT)) /  100 )
                END as nilai_angsuran,
                '' as objek_jaminan,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 2
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 6
                WHEN '12 bulan' THEN 12
                WHEN '18 bulan' THEN 18
                WHEN '24 bulan' THEN 24
                ELSE 30 END as jangka_waktu_pinjaman,
                CASE  DBO.GT_FT_GETCODENM(M.COM_CD, M.LN_DAY_CD, 'in_ID')
                WHEN '2 bulan' THEN 3
                WHEN '3 bulan' THEN 3
                WHEN '6 bulan' THEN 3
                WHEN '12 bulan' THEN 3
                WHEN '18 bulan' THEN 3
                WHEN '24 bulan' THEN 3
                ELSE 1 END as satuan_jangka_waktu_pinjaman,
                DBO.FN_FORMAT_DATE(P.XPR_DT) as tgl_jatuh_tempo,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_pendanaan,
                DBO.FN_FORMAT_DATE(P.LN_DT) as tgl_penyaluran_dana,
                '814896477' as no_ea_transaksi,
                1 as frekuensi_pendanaan
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$date_cron' AND '$date_cron'
                AND M.BOOK_OFF_YN = 'N'
                ORDER BY M.ACT_NO DESC

        ");


        // prepare parameter
        $plaintext = json_encode($param);
        $encryptionKey = 'WmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        $sendData = ListHelper::request_pusdafil($resutEncrypt, $type);

        // prepare content
        $content = $resutEncrypt;

    }

    public function send_pembayaran_pinjaman(Request $request){

        $type = 'pembayaran_pinjaman';

        $date_cron = date("Ymd", time() - 3600);

        $param = DB::connection('sqlsrv')->select("
            SELECT
                '820140' as id_penyelenggara,
                M.ACT_NO as id_pinjaman,
                P.INSP_NO as id_borrower,
                '2000000046' as id_lender,
                M.ACT_NO as id_transaksi,
                M.VIR_ACNT_NO as id_pembayaran,
                P.XPR_DT as tgl_jatuh_tempo,
                convert(varchar, DATEADD(MONTH, 1, P.XPR_DT), 112) as tgl_jatuh_tempo_selanjutnya,
                P.CONTRACT_DT as tgl_pembayaran_borrower,
                '' as tgl_pembayaran_penyelenggara,
                M.LN_BAL_AM as sisa_pinjaman_berjalan,
                CASE
                WHEN M.DFR_DAY < 30 THEN 1
                WHEN M.DFR_DAY BETWEEN 30 AND 90 THEN 2
                ELSE
                3 END  as id_status_pinjaman,
                '' as tgl_pelunasan_borrower,
                '' as tgl_pelunasan_penyelenggara,
                (SELECT SUM(MAST.DFR_INT_AM ) FROM FN_LOAN_HIST MAST  WHERE ACT_NO = M.ACT_NO  GROUP BY MAST.ACT_NO)  as denda,
                CASE
                WHEN
                CONFIRM_LN_AM - (
                SELECT
                    SUM(INT_AMT + WON_AMT)
                FROM
                    PT_LOAN_PLAN WITH (NOLOCK)
                WHERE
                    INSP_NO = P.INSP_NO
                    AND SCH_DT = P.LN_DT
                ) - CONVERT(
                numeric(18, 0),
                FLOOR(
                    CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                ) / 100
                ) IS NULL
                THEN 0
                ELSE
                    CONFIRM_LN_AM - (
                    SELECT
                        SUM(INT_AMT + WON_AMT)
                    FROM
                        PT_LOAN_PLAN WITH (NOLOCK)
                    WHERE
                        INSP_NO = P.INSP_NO
                        AND SCH_DT = P.LN_DT
                    ) - CONVERT(
                    numeric(18, 0),
                    FLOOR(
                        CONVERT(FLOAT, CONFIRM_LN_AM) * CONVERT(FLOAT, FN_FEE_IRT)
                    ) / 100
                    )
                END
                as nilai_pembayaran
            FROM
                PT_LNC001TM P
                LEFT OUTER JOIN PT_LNC031TM R ON P.INSP_NO = R.INSP_NO
                AND R.AAB_CD = '120001'
                LEFT OUTER JOIN FN_LOAN_MAST M ON P.INSP_NO = M.INSP_NO
                LEFT OUTER JOIN GT_CUST_COMM C ON P.KTP = C.KTP
                LEFT OUTER JOIN IN_POT001TM Q ON R.INSP_NO = Q.POT_NO
            WHERE
                P.COM_CD = '01'
                AND M.LN_DT BETWEEN '$date_cron' AND '$date_cron'
                AND M.BOOK_OFF_YN = 'N'
        ");


        // prepare parameter
        $plaintext = json_encode($param);
        $encryptionKey = 'WmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!';
        $IV = 'ijzh84t1w9xa56s9';

        // encrypt
        $resutEncrypt = AES256::encrypt_with_iv($plaintext, $encryptionKey, $IV);
        $resultDecrypt = AES256::decrypt($resutEncrypt, $encryptionKey);

        $sendData = ListHelper::request_pusdafil($resutEncrypt, $type);

        // prepare content
        $content = $resutEncrypt;

    }


    public function send_test_scheduller(Request $request){
        $res = 'testing response';
        $type = 'testing type';

        $date_cron = date("Ymd", time() - 3600);


        $pusdafillLogger = pusdafillLogger::create([
        "response" => $res,"type" => $type, "date_cron" => $date_cron]);
    }

    public function get_pusdafil_log(Request $request){
        $param = DB::select('select date_cron, created_at, updated_at from pusdafill_logger group by date_cron');


        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');


        $totalData = count($param);
        $result = array();
       // dd($param);

        foreach ($param as $obj){
            $param2 = DB::select("select type, response from pusdafill_logger where date_cron= '". $obj->date_cron. "'");
            //dd($param2);
            $item = array();
            $item['date_cron'] = $obj->date_cron;
            $item['created_at'] = $obj->created_at;
            $item['updated_at'] = $obj->updated_at;
            /*
            $item['reg_pengguna'] = json_decode($param2[0]->response, true)['request_status'];
            $item['reg_borrower'] = json_decode($param2[1]->response, true)['request_status'];
            $item['reg_lender'] = json_decode($param2[2]->response, true)['request_status'];
            $item['pengajuan_pinjaman'] = json_decode($param2[3]->response, true)['request_status'];
            $item['pengajuan_pemberian_pinjaman'] = json_decode($param2[4]->response, true)['request_status'];
            $item['transaksi_pinjam_meminjam'] = json_decode($param2[5]->response, true)['request_status'];
            $item['pembayaran_pinjaman'] = json_decode($param2[6]->response, true)['request_status'];
            */

            for ($x = 0; $x <= 6; $x++){
                if (isset($param2[$x]->response)){
                    switch($x){
                        case 0:
                            $item['reg_pengguna'] = json_decode($param2[0]->response, true)['request_status'];
                            break;

                        case 1:
                            $item['reg_borrower'] = json_decode($param2[1]->response, true)['request_status'];
                            break;

                        case 2:
                            $item['reg_lender'] = json_decode($param2[2]->response, true)['request_status'];
                            break;

                        case 3:
                            $item['pengajuan_pinjaman'] = json_decode($param2[3]->response, true)['request_status'];
                            break;

                        case 4:
                            $item['pengajuan_pemberian_pinjaman'] = json_decode($param2[4]->response, true)['request_status'];
                            break;

                        case 5:
                            $item['transaksi_pinjam_meminjam'] = json_decode($param2[5]->response, true)['request_status'];
                            break;

                        case 6:
                            $item['pembayaran_pinjaman'] = json_decode($param2[6]->response, true)['request_status'];
                            break;

                    }
                }
                else {
                    switch($x){
                        case 0:
                            $item['reg_pengguna'] = "NOT FOUND...";
                            break;

                        case 1:
                            $item['reg_borrower'] = "NOT FOUND...";
                            break;

                        case 2:
                            $item['reg_lender'] = "NOT FOUND...";
                            break;

                        case 3:
                            $item['pengajuan_pinjaman'] = "NOT FOUND...";
                            break;

                        case 4:
                            $item['pengajuan_pemberian_pinjaman'] = "NOT FOUND...";
                            break;

                        case 5:
                            $item['transaksi_pinjam_meminjam'] = "NOT FOUND...";
                            break;

                        case 6:
                            $item['pembayaran_pinjaman'] = "NOT FOUND...";
                            break;
                    }
                }
            }

            array_push($result, $item);
        }

        $resp = array();
        $resp['draw'] = intval($draw);
        $resp['recordsTotal'] = $totalData;
        $resp['recordsFiltered'] = $totalData;
        $resp['data'] = $result;

        return json_encode($resp);
    }

    public function get_log_detail($theDate){

        $param = DB::select("select id, `type`, response, created_at, updated_at
            from pusdafill_logger where date_cron='$theDate'");

        $result = json_encode($param);

        return $param;
    }

    public function ShowPusdafilLog(Request $request){
        $param = DB::select('select date_cron, created_at, updated_at from pusdafill_logger group by date_cron');

        $totalData = count($param);
        $result = [];

        foreach ($param as $obj){
            $param2 = DB::select("select type, response from pusdafill_logger where date_cron= '". $obj->date_cron. "'");

            $pembayaran_pinjaman = '';

            try {
                $pembayaran_pinjaman = json_decode($param2[6]->response, true)['request_status'];
            }
            catch (JsonException $e) {
                $pembayaran_pinjaman = $e->getMessage();
             }
             catch (ErrorException $ee){
                 $pembayaran_pinjaman = $ee->getMessage();
             }

            $result[] = [
                'date_cron' => $obj->date_cron,
                'created_at' => $obj->created_at,
                'updated_at' => $obj->updated_at,
                'reg_pengguna' => json_decode($param2[0]->response, true)['request_status'],
                'reg_borrower' => json_decode($param2[1]->response, true)['request_status'],
                'reg_lender' => json_decode($param2[2]->response, true)['request_status'],
                'pengajuan_pinjaman' => json_decode($param2[3]->response, true)['request_status'],
                'pengajuan_pemberian_pinjaman' => json_decode($param2[4]->response, true)['request_status'],
                'transaksi_pinjam_meminjam' => json_decode($param2[5]->response, true)['request_status'],
                'pembayaran_pinjaman' => $pembayaran_pinjaman
            ];

        }

        $resp = array();
        $resp['draw'] = 1;
        $resp['recordsTotal'] = $totalData;
        $resp['recordsFiltered'] = $totalData;
        $resp['data'] = $result;

       // dd($resp['data']);
        //return view('admin.pusdafill-logger', ['activeSidebar' => 'pusdafil', 'history_data' => $resp['data']]);
        return view('admin.pusdafill-logger', ['activeSidebar' => 'pusdafil']);
    }

}
