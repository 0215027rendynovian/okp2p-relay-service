<?php

namespace App\Http\Controllers;

use App\ChildMenu;
use App\MasterMenu;
use App\Roles;
use App\UserPrivilage;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserPrivilegeController extends Controller
{
    public function index()
    {
        $masterPrivilege = UserPrivilage::all();

        $listRoles = Roles::select('code', 'name')->pluck('name','code');
        $listMenu = MasterMenu::select('id', 'name')->pluck('name','id');
        $listChildMenu = ChildMenu::select('id', 'name')->pluck('name','id');

        return view('admin.privilege', ['listprivilege' => $masterPrivilege, 'listRoles' => $listRoles, 'listMenu' => $listMenu, 'listChildMenu' => $listChildMenu, 'activeSidebar' => 'configuration']);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Master Privilege berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'role_id' => 'required',
                'code' => 'required',
            ]);

            $getMenu = MasterMenu::find($request->code);

            UserPrivilage::create([
                'code' => $request->code,
                'role_id' => $request->role_id,
                'parent' => $request->parent,
                'child' => $request->child,
                'status' => 1,
                'description' => $request->description,
            ]);

            return Redirect::to('/privilege')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Privilege gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/privilege')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $getMenu = MasterMenu::find($request->code);

            $masterPrivilege = UserPrivilage::find($request->id);
            $masterPrivilege->code = $request->code;
            $masterPrivilege->role_id = $request->role_id;
            $masterPrivilege->parent = $request->parent;
            $masterPrivilege->child = $request->child;
            // $masterPrivilege->status = $request->status;
            $masterPrivilege->description = $request->description;
            $masterPrivilege->save();

            $notification = array(
                'message' => 'Master Privilege berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/privilege')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Privilege gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/privilege')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            UserPrivilage::find($id)->delete();

            $notification = array(
                'message' => 'Master Privilege berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/privilege')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Privilege gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/privilege')->with($notification);
        }
    }
}
