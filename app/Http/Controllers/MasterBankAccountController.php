<?php

namespace App\Http\Controllers;

use App\MasterBankAccount;
use App\ViewChildMenu;
use App\ViewMasterMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class MasterBankAccountController extends Controller
{
    public function index()
    {
        $masterBank = MasterBankAccount::all();

        return view('admin.mba', ['listmba' => $masterBank, 'activeSidebar' => 'mitrans']);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Master Data Bank Account berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'bank_code' => 'required',
                'bank_name' => 'required',
            ]);

            MasterBankAccount::create([
                'bank_code' => $request->bank_code,
                'bank_name' => $request->bank_name,
                // 'additional' => $request->additional,
            ]);

            return Redirect::to('/mba')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Data Bank Account gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/mba')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $masterBank = MasterBankAccount::find($request->id);
            $masterBank->bank_code = $request->bank_code;
            $masterBank->bank_name = $request->bank_name;
            // $masterBank->additional = $request->additional;
            $masterBank->save();

            $notification = array(
                'message' => 'Master Data Bank Account berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/mba')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Data Bank Account gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/mba')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            MasterBankAccount::find($id)->delete();

            $notification = array(
                'message' => 'Master Data Bank Account berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/mba')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Data Bank Account gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/mba')->with($notification);
        }
    }
}
