<?php

namespace App\Http\Controllers;

use App\MasterEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class MasterEmailController extends Controller
{
    public function index()
    {
        $masterMails = MasterEmail::all();

        return view('admin.master-emails', ['listemails' => $masterMails, 'activeSidebar' => 'configuration']);
    }

    public function store(Request $request)
    {
        try {
            $notification = array(
                'message' => 'Master Emails berhasil tersimpan!',
                'alert-type' => 'success'
            );

            $request->validate([
                'email' => 'required',
                'name' => 'required',
            ]);

            MasterEmail::create([
                'email' => $request->email,
                'name' => $request->name,
                'status' => 1,
                'keterangan' => $request->keterangan,
            ]);

            return Redirect::to('/mails')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Emails gagal tersimpan! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/mails')->with($notification);
        }
    }

    public function edit(Request $request)
    {
        try {
            $masterMails = MasterEmail::find($request->id);
            $masterMails->email = $request->email;
            $masterMails->name = $request->name;
            $masterMails->status = $request->status;
            $masterMails->keterangan = $request->keterangan;
            $masterMails->save();

            $notification = array(
                'message' => 'Master Emails berhasil terupdate!',
                'alert-type' => 'success'
            );
            return Redirect::to('/mails')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Emails gagal terupdate! ',
                'alert-type' => 'error'
            );
            return Redirect::to('/mails')->with($notification);
        }
    }

    public function destroy($id)
    {
        try {
            MasterEmail::find($id)->delete();

            $notification = array(
                'message' => 'Master Emails berhasil terhapus!',
                'alert-type' => 'success'
            );
            return Redirect::to('/mails')->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Master Emails gagal terhapus!',
                'alert-type' => 'error'
            );
            return Redirect::to('/mails')->with($notification);
        }
    }
}
