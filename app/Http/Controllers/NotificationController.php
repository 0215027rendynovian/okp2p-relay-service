<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function send(Request $request)
    {
        $fcm_token = "dk4YFr8SQhq_5CktL-1EXw:APA91bH5su74-f3TPIf0FCRSoW1nu7u9gLuwjWTnvrBkQSBfoIdnPnmL7eueGJpwNzskw_NQeh7YGhGTmAYw35M96zwjTIsbjM7L6saMtC4PFM_ZZ7pr58PimmFlPn_u075GJHJ_Uzmj"; 
        //$fcm_token = $request->device_token;
        
        return $this->sendNotification($fcm_token, array(
          "title" => "Testing", 
          "body" => "Hallo Testing"
        ));
    }

    public function send_multi(Request $request)
    {
        return $this->sendMultiNotification(array(
          $request->device_token1, 
          $request->device_token2,
          $request->device_token3
          //..
        ), array(
          "title" => "Sample Message", 
          "body" => "This is Test message body"
        ));
    }
  


    public function sendNotification($device_token, $message)
    {
        $SERVER_API_KEY = 'AAAAmclLUZI:APA91bEJquHbfa1C7xcLYj0mgWOxNH4y6YLASGfJ_JSmuK6RvJgj850wgdIac2mvrYwdqiAvVTSwrrr26Bdy-5VFYGNArwk8Dkl178IZoqh8P9CLfRGgTmhrFEhqkDEp_cpXqRoPfyGa';
        // payload data, it will vary according to requirement
        $data = [
            "to" => $device_token, // for single device id
            "data" => $message
        ];
        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
      
        curl_close($ch);
      
        return $response;
    }

    public function sendMultiNotification($device_tokens, $message)
    {
        $SERVER_API_KEY = 'AAAAmclLUZI:APA91bEJquHbfa1C7xcLYj0mgWOxNH4y6YLASGfJ_JSmuK6RvJgj850wgdIac2mvrYwdqiAvVTSwrrr26Bdy-5VFYGNArwk8Dkl178IZoqh8P9CLfRGgTmhrFEhqkDEp_cpXqRoPfyGa';
  
        // payload data, it will vary according to requirement
        $data = [
            "registration_ids" => $device_tokens, // for multiple device ids
            "data" => $message
        ];
        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
      
        curl_close($ch);
      
        return $response;
    }
}
