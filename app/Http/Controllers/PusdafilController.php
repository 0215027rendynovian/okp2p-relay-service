<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

define('AES_256_CBC', 'aes-256-cbc');

class PusdafilController extends Controller
{

    function encrypt($plaintext, $encryptionKey){
        // initialization vector
        $IV = openssl_random_pseudo_bytes(openssl_cipher_iv_length(AES_256_CBC));

        // enkripsi AES
        $encrypted = openssl_encrypt($plaintext, AES_256_CBC, $encryptionKey, 0, $IV);
        //echo "Enkripsi: $encrypted". PHP_EOL;
        
        // concat hasil enkripsi dengan '::' IV
        $encrypted = $encrypted . '::' . $IV;
        //echo "concat: $encrypted". PHP_EOL;

        // base_64 hasil sebelumnya
        $encrypted = base64_encode($encrypted);
    // echo "base64: $encrypted".PHP_EOL;
        return $encrypted;
    }

    function encrypt_with_iv($plaintext, $encryptionKey, $IV){

        // enkripsi AES
        $encrypted = openssl_encrypt($plaintext, AES_256_CBC, $encryptionKey, 0, $IV);
        
        // concat hasil enkripsi dengan '::' dan IV
        $encrypted = $encrypted . '::' . $IV;

        // base_64 hasil sebelumnya
        $encrypted = base64_encode($encrypted);
        return $encrypted;
    }

    function decrypt($encrypted, $encryptionKey){
        $parts = explode('::', base64_decode($encrypted));
        $decrypted = openssl_decrypt($parts[0], AES_256_CBC, $encryptionKey, 0, $parts[1]);
        return $decrypted;
    }
}
