<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallbackDisbursement extends Model
{
    //
    protected $table = "callback_disbursement";
    protected $fillable = ['id_disburse',
    'user_id',
    'external_id',
    'amount',
    'bank_code',
    'account_holder_name',
    'disbursement_description',
    'is_instant',
    'created',
    'updated',
    'status',
    'uid'];
}
