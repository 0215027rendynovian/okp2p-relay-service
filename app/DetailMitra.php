<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailMitra extends Model
{
    //
    protected $table = "detail_mitra";
    protected $fillable = ['kota','alamat_lengkap','kecamatan','no_hp','nik','tgl_lahir',
    'question_profesi','question_institusi','status_kerabat','question_kerabat','question_lainnya',
    'question_aplikasi_lain','question_peraturan','question_promosi','chooseFileSertifikat','chooseFileKtp',
    'chooseFileSelfie','uid', 'type_layanan','jenis_layanan','jenis_kelamin', 'status_active'];

}
