<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaPemblokiran extends Model
{
    protected $fillable = [
        'ref_id_backoffice',
        'ref_id_pengguna',
        'nama_pt',
        'alamat_pt',
        'nama_pemblokir',
        'jabatan_pemblokir',
        'alamat_pemblokir',
        'hari_pemblokiran',
        'tanggal_pemblokiran',
        'nomor_polisi',
        'tanggal_polisi',
        'no_dttot',
        'nama_terorist',
        'jabatan_terorist',
        'nama_rekening',
        'tempat_tanggal_lahir',
        'pekerjaan',
        'alamat',
        'nomor_rekening',
        'saldo_terakhir',
        'jenis_identitas',
        'nama_saksi',
        'lampiran'
    ];

    protected $table = 't_ba_pemblokiran';
}
