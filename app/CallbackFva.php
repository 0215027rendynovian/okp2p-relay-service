<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallbackFva extends Model
{
    //
    protected $table = "callback_fva";
    protected $fillable = ['amount',
    'callback_virtual_account_id',
    'payment_id','external_id',
    'account_number',
    'merchant_code',
    'bank_code',
    'transaction_timestamp',
    'currency',
    'created',
    'uid'];
}
