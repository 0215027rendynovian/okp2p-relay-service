<?php
$title = "BERHASIL";
include( 'inc/header.php' )
?>


<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title"></a>


		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="home.php" class="sidenav-trigger right"><i class="material-icons">close</i></a>
	</div>
</nav>


<div class="container">
	<div class="gap"></div>
	<div class="row">
		<div class="col s12 center">
			<img class="mitra-img" src="img/berhasil-img.png">
		</div>
		<div class="col s12 center">
			<h6 class="bold text-blue">Penarikan Saldo Anda diproses</h6>
			<div class="text-yellow f12">Penarikan saldo sedang diproses,<br>harap tunggu maks. 1x24 jam</div>
		</div>
	</div>

	<div class="divider"></div>
	<div class="gap"></div>
	<div class="row">
		<div class="col s12 text-grey"> Detail Penarikan Saldo</div>
	</div>

	<div class="row">
		<div class="col s12 text-blue">Rekening Tujuan
			<div class="black-text bold">PT. BCA (BANK CENTRAL ASIA) TBK</div>
			<div class="grey-text f12">02146564 - AGUNG HAKIKI</div>
		</div>

	</div>

	<div class="row">
		<div class="col s12 text-blue">Total Penarikan
			<div class="black-text bold">Rp 100.000</div>

		</div>
	</div>
	<a class="waves-effect waves-light btn w-100 bg-yellow" href="saldo.php">KEMBALI KE HALAMAN SALDO</a>
</div>





</div>

<?php
include( 'inc/footer.php' )
?>
<script>
</script>