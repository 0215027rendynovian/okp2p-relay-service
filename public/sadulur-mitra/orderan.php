<?php
$title = "ORDERAN";
include( 'inc/header.php' )
?>



<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">ORDERAN</a>
		<ul class="right hide-on-med-and-down">
			<li><a href="#">Navbar Link</a>
			</li>
		</ul>

		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="#" onclick="goBack()" class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>
	</div>
</nav>

<div class="container">


	<div class="row">
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s3"><a class="active" href="#inbox">Berlangsung</a>
				</li>
				<li class="tab col s3"><a href="#unread">Diterima</a>
				</li>

			</ul>
		</div>

		<div id="inbox" class="col s12">


			<div class="card-panel">
				<div class="cardh1">
					Asep Suhendra
					<span>Jl. Cikahuripan, Rt 07/08, No 22</span>
				</div>

				<div class="row mb-0">


					<div class="pv2"></div>
					<div class="col s6 text-blue">
						<i class="material-icons small pr-5">access_time</i><span class="top">18:00</span>
					</div>

					<div class="col s6 text-red text-right">
						<i class="material-icons f22 pr-5">alarm</i><span class="top">60 menit</span>
					</div>
				</div>
				<div class="divider"></div>
				<div class="pv5"></div>
				<div class="row mb-0">
					<a class="col s6 waves-effect waves-light btn bg-red" href="#">TOLAK</a>
					<a class="col s6 waves-effect waves-light btn bg-yellow" href="orderan-detail-1.php">TERIMA</a>
				</div>
			</div>

			<div class="divider"></div>
			<div class="pv10"></div>
			<div class="text-yellow ">1 minggu lalu :</div>
			<a href="">
				<div class="row mb-5" id="activity-list">
					<div class="col s12 text-blue" id="small-dates">
						13 Jun
					</div>
					<div class="col s6" id="small-service">
						Massage
					</div>
					<span class="right-arrow material-icons">keyboard_arrow_right</span>
				</div>
			</a>
			<div class="divider"></div>
			<a href="">
				<div class="row mb-5" id="activity-list">
					<div class="col s12 text-blue" id="small-dates">
						13 Jun
					</div>
					<div class="col s6" id="small-service">
						Massage
					</div>
					<span class="right-arrow material-icons">keyboard_arrow_right</span>
				</div>
			</a>
			<div class="divider"></div>
			<a href="">
				<div class="row mb-5" id="activity-list">
					<div class="col s12 text-blue" id="small-dates">
						13 Jun
					</div>
					<div class="col s6" id="small-service">
						Massage
					</div>
					<span class="right-arrow material-icons">keyboard_arrow_right</span>
				</div>
			</a>
			<div class="divider"></div>
			<a href="">
				<div class="row mb-5" id="activity-list">
					<div class="col s12 text-blue" id="small-dates">
						13 Jun
					</div>
					<div class="col s6" id="small-service">
						Massage
					</div>
					<span class="right-arrow material-icons">keyboard_arrow_right</span>
				</div>
			</a>
			<div class="divider"></div>

		</div>

		<div id="unread" class="col s12">

			<div class="card-panel">
				<div class="cardh1">
					Asep Suhendra
					<span>Jl. Cikahuripan, Rt 07/08, No 22</span>
				</div>

				<div class="row mb-0">


					<div class="pv2"></div>
					<div class="col s6 text-blue">
						<i class="material-icons f22 pr-5">access_time</i><span class="top">18:00</span>
					</div>

					<div class="col s6 text-red text-right">
						<i class="material-icons f22 pr-5">alarm</i><span class="top">60 menit</span>
					</div>
				</div>
				<div class="divider"></div>
				<div class="pv5"></div>
				<div class="row mb-0">
					<a class="waves-effect waves-light btn w-100 bg-yellow" href="#">MULAI LAYANAN</a>

					<div class="pv5"></div>
					<a class="waves-effect waves-light btn w-100 bg-blue" href="#">KONFIRMASI SELESAI</a>

				</div>
			</div>
			<div class="divider"></div>
		</div>

	</div>



</div>





<?php
include( 'inc/footer.php' )
?>
<script>
</script>