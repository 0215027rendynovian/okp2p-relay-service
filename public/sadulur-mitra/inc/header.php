<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

<!-- title -->
<title><?php echo $title; ?></title>

<!-- custom -->
<link href="./css/bottom-tab.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="./css/font-awesome.css" rel="stylesheet">

<!--  Scripts ---------- -->
<script type = "text/javascript"  src = "./js/jquery-3.2.1.min.js"></script>   
<script type = "text/javascript"  src = "./js/materialize.min.js"></script>
<script type = "text/javascript"  src = "./js/materialize-other.min.js"></script>


<!-- CSS  -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="./css/material-icon.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="./css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="./css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>


<!-- GRecaptcha  -->	
<script src='https://www.google.com/recaptcha/api.js'></script>
	
<!-- browser color  -->
	
<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#223887">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#223887">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#223887">		
	
</head>

	