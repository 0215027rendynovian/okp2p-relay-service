<?php
$title = "PROFIL MITRA";
include( 'inc/header.php' )
?>

<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">PROFIL MITRA</a>
		<ul class="right hide-on-med-and-down">
			<li><a href="#">Navbar Link</a>
			</li>
		</ul>

		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="#" onclick="goBack()" class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>
		<a href="#" class="sidenav-trigger right"><i class="material-icons">settings</i></a>
	</div>
</nav>

<div class="container">

	<div class="section">

		<div class="row">
			<div class="col s4">
				<img class="circle" src="img/mitra-img.png" width="95px" alt=""/>
			</div>
			<div class="col s7">
				<div class="text-blue bold">Tatang Suratang</div>
				<div class="">tatangs@gmail.com</div>
				<div class="text-blue">08571312456</div>

			</div>
		</div>
		<div class="divider"></div>

	</div>

	<div class="section">

		<!--   Konten Info Profil  -->
		<div class="row">
			<div class="col s12 text-blue">Alamat

				<div class="black-text">Jl. Pasar Minggu Raya</div>
			</div>
		</div>

		<div class="row">
			<div class="col s6 text-blue">ID Mitra
				<div class="black-text">MTS0001</div>
			</div>

			<div class="col s6 text-blue text-right">Gender
				<div class="black-text">Pria</div>



			</div>
		</div>
		<div class="divider"></div>
		<div class="gap"></div>

		<div class="row">
			<div class="col s12 text-blue">Layanan Mitra
				<div class="black-text">Massage</div>
				<div class="black-text">Cleaning</div>
			</div>

		</div>
	</div>
	<div class="divider"></div>
	<div class="gap"></div>

	<div class="row">
		<div class="col s12" style="display: block;">
			<div class="pv10 text-grey f12 center">Anda belum memiliki rekening</div>
			<a class="waves-effect waves-light w-100 btn bg-yellow"><i class="material-icons center">add</i>TAMBAH REKENING</a>

		</div>
	</div>

	<a class="waves-effect waves-light btn w-100 bg-red modal-trigger" id="fixedbutton" href="#modal1"><i class="material-icons center">exit_to_app</i>KELUAR</a>

</div>
</div>


<!-- Modal Structure -->
<div id="modal1" class="modal">
	<div class="modal-content">
		<h5>Log Out</h5>
		<p>Anda ingin keluar dari akun ini? </p>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-blue btn bg-red">Batal</a>
		<a href="index.php" class="waves-effect waves-blue btn">Iya</a>
	</div>
</div>

<!---------- Modal ------------>	

<script>
	document.addEventListener( 'DOMContentLoaded', function () {
		var elems = document.querySelectorAll( '.modal' );
		var instances = M.Modal.init( elems, options );
	} );

	// Or with jQuery

	$( document ).ready( function () {
		$( '.modal' ).modal();
	} );
</script>

<?php
include( 'inc/footer.php' )
?>