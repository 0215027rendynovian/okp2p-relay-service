<?php
$title = "SALDO";
include( 'inc/header.php' )
?>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


<!-- nav -->
<nav class="bg-blue" role="navigation">
	<div class="nav-wrapper container"><a href="#" class="" id="nav-title">SALDO</a>
		<ul class="right hide-on-med-and-down">
			<li><a href="#">Navbar Link</a>
			</li>
		</ul>

		<ul id="nav-mobile" class="sidenav">
			<li><a href="#">Reservation List</a>
			</li>
		</ul>
		<a href="#" onclick="goBack()" class="sidenav-trigger"><i class="material-icons">arrow_back</i></a>
	</div>
</nav>


<div class="section">

	<div class="row center">
		<i class="material-icons text-blue">account_balance_wallet</i>
		<div class="text-red">Rp 0
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s3"><a href="#isiSaldo">ISI SALDO</a>
				</li>
				<li class="tab col s3"><a href="#tarikSaldo">
                  TARIK SALDO</a>
				</li>
			</ul>
		</div>

		<div id="isiSaldo" class="col s12">
			<div class="pv10 text-grey">Pilih cara isi saldo :</div>
			<div class="container">
				<a href="virtualaccount.php">
					<div class="card-panel">
						<div class="cardh1">
							VIRTUAL ACCOUNT
						</div><span class="right-arrow material-icons">
keyboard_arrow_right
</span>
					
					</div>
				</a>

			</div>
		</div>

		<div id="tarikSaldo" class="col s12">
			<div class="pv10 text-grey f12 center">Anda belum memiliki rekening</div>
			<div class="container">
				<a class="waves-effect waves-light w-100 btn bg-yellow"><i class="material-icons center">add</i>TAMBAH REKENING</a>




			</div>

			<!-- Jika sudah ada rekening dan ingin narik -->

			<div class="gap"></div>
			<div class="divider"></div>
			<div class="section">
				<div class="row">
					<div class="col s6 bold text-blue">
						Saldo Penghasilan
					</div>
					<div class="col s6 text-red bold text-right">
						Rp 100.000
					</div>
				</div>

				<div class="row">
					<div class="col s6 ">
						Nominal Penarikan
					</div>
					<div class="col s6 text-blue text-right">
						Tarik Semua
					</div>
				</div>

				<div class="row">
					<form class="col s12">

						<div class="input-field">
							<input placeholder="Nominal Penarikan" id="nominaltariksaldio" type="number" class="validate">
							<label for="first_name">Min. penarikan Rp 50.000</label>

					</form>
					</div>




				</div>


				<div class="row">
					<div class="col s12 bold text-blue">
						Rekening Tujuan
					</div>

					<div class="col s12 ">
						<form action="#">
							<p>
								<label>
        <input name="group1" type="radio" checked />
        <span>PT. BCA (BANK CENTRAL ASIA) TBK</span><br>
        <span class="pl10">02146564 - AGUNG HAKIKI</span>
      
      </label>
							
							</p>

						</form>
					</div>

				</div>



				<div class="divider"></div>


				<!-- end -->
				<div class="pv10 text-grey f12 center">Dengan klik tombol tarik saldo, Anda telah setuju dengan <a href="#">ketentuan penarikan saldo</a>
				</div>
				<a class="waves-effect waves-light btn w-100 bg-red" id="fixedbutton" href="berhasil.php">TARIK SALDO</a>



			</div>

		</div>

	</div>


</div>

<?php
include( 'inc/footer.php' )
?>
<script>
</script>