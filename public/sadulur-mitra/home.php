<?php
$title = "HOME MITRA";
include( 'inc/header.php' )
?>

<div class="container">

	<div class="section">

		<div class="row">

			<a href="saldo.php#tarikSaldo">
				<div class="col s8" id="user-info"><i class="material-icons">account_balance_wallet</i><span>Rp 0</span>
				</div>
			</a>


			<div class="col s4 text-right">
				<a id="user-notif" href="saldo.php#isiSaldo" class="sidenav-trigger right">
					<div class="waves-effect waves-light btn-small bg-red">TOPUP</div>
				</a>
			</div>
		</div>

		<div class="row">
			<div class="col s12 center">
				<img class="mitra-img" src="img/mitra-img.png">
			</div>
			<div class="col s12 center">
				<h5 class="bold text-blue">TATANG SURATANG</h5>
				<h6 class="text-yellow">ID SH005</h6>
			</div>
		</div>

		<div class="divider"></div>
		<div class="gap"></div>

		<a href="orderan.php">
			<div class="card-panel">
				<div class="cardh1"> <span class="new badge bg-red">1</span> ORDERAN
				</div><span class="right-arrow material-icons">
keyboard_arrow_right
</span>
			

			</div>
		</a>

		<a href="pesan.php">
			<div class="card-panel">
				<div class="cardh1">
					PESAN
				</div><span class="right-arrow material-icons">
keyboard_arrow_right
</span>
			

			</div>
		</a>

		<a href="aktivitas.php">
			<div class="card-panel">
				<div class="cardh1">
					AKTIVITAS
				</div><span class="right-arrow material-icons">
keyboard_arrow_right
</span>
			

			</div>
		</a>

		<a href="profile-mitra.php">
			<div class="card-panel">
				<div class="cardh1">
					AKUN SAYA
				</div><span class="right-arrow material-icons">
keyboard_arrow_right
</span>
			

			</div>
		</a>


	</div>

	<div class="gap"></div>
	<div class="gap"></div>

</div>


<?php
include( 'inc/footer.php' )
?>