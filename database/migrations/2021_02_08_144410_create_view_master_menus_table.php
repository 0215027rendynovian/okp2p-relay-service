<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewMasterMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_master_menu
            AS
            SELECT
                up.code as code,
                up.role_id as role_id,
                CASE
                    when (SELECT COUNT(parent) FROM izi_data.child_menus where parent = up.parent) > 0 THEN true
                END as isParent,
                up.parent as parent,
                mm.code as code_menu,
                mm.name as name,
                mm.menuopen as menuopen,
                mm.icon as icon,
                mm.url as url,
                up.status as status,
                up.description as description
            FROM
                izi_data.user_privilage up
            left join
                izi_data.master_menu mm on
                (up.parent = mm.code)
             WHERE up.isParent = true
            ORDER BY
                up.role_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS `view_master_menu`;
        ");
    }
}
