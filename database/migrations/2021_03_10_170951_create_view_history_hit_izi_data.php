<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewHistoryHitIziData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_history_hit_izi_data
            AS
            SELECT
                u.id, u.name, u.email, u.dept_id, SUM(count_hit) as COUNT_HIT, MAX(cf.created_at) as created_at, MAX(cf.updated_at) as updated_at
            FROM
                izi_data.users u
            left join
                izi_data.creadit_featureds cf on u.id = cf.uid
            GROUP BY u.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS `view_history_hit_izi_data`;
        ");
    }
}
