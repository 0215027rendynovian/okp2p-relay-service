<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PefindoCustomreport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pefindo_customreport', function (Blueprint $table) {
            $table->id();
            $table->string('ktp');
            $table->string('fullname');
            $table->string('date_of_birth');
            $table->string('inquiry_reason');
            $table->string('pefindo_id');
            $table->string('report_date');
            $table->string('uid');
            $table->string('status');
            $table->json('json');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pefindo_customreport');
    }
}
