<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TLaporanNihil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_laporan_nihil', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id_backoffice')->nullable();
            $table->string('ref_id_pengguna')->nullable();
            $table->string('nama_pt')->nullable();
            $table->string('alamat_pt')->nullable();
            $table->string('nomor')->nullable();
            $table->string('lampiran')->nullable();
            $table->string('nomor_polisi')->nullable();
            $table->string('tanggal_polisi')->nullable();
            $table->string('nomor_dttot')->nullable();
            $table->string('hari_nihil')->nullable();
            $table->string('tanggal_nihil')->nullable();
            $table->string('nama_pjk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('t_laporan_nihil');
    }
}
