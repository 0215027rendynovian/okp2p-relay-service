<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserPrivilage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_privilage', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->integer('role_id')->nullable();
            $table->integer('parent')->nullable();
            $table->integer('child')->nullable();
            $table->integer('status')->default('1');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_privilage');
    }
}
