<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddModeDevToPefindoCustomreport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pefindo_customreport', function (Blueprint $table) {
            //
            $table->string('mode_dev');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pefindo_customreport', function (Blueprint $table) {
            //
            $table->dropColumn('mode_dev');

        });
    }
}
