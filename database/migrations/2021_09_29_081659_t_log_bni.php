<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TLogBni extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_bni', function (Blueprint $table) {
            $table->id();
            $table->string('token');
            $table->boolean('mode_dev');
            $table->string('type');
            $table->json('request');
            $table->string('url');
            $table->json('response');
            $table->string('requestedUuid');
            $table->string('uid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('log_bni');

    }
}
