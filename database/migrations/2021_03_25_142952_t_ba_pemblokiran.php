<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TBaPemblokiran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_ba_pemblokiran', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id_backoffice')->nullable();
            $table->string('ref_id_pengguna')->nullable();
            $table->string('nama_pt')->nullable();
            $table->string('alamat_pt')->nullable();
            $table->string('nama_pemblokir')->nullable();
            $table->string('jabatan_pemblokir')->nullable();
            $table->string('alamat_pemblokir')->nullable();
            $table->string('hari_pemblokiran')->nullable();
            $table->timestamp('tanggal_pemblokiran')->nullable();
            $table->string('nomor_polisi')->nullable();
            $table->date('tanggal_polisi')->nullable();
            $table->string('no_dttot')->nullable();
            $table->string('nama_terorist')->nullable();
            $table->string('jabatan_terorist')->nullable();
            $table->string('nama_rekening')->nullable();
            $table->string('tempat_tanggal_lahir')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('nomor_rekening')->nullable();
            $table->string('saldo_terakhir')->nullable();
            $table->string('jenis_identitas')->nullable();
            $table->string('nama_saksi')->nullable();
            $table->string('lampiran')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('t_ba_pemblokiran');
    }
}
