<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSigapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_sigap', function (Blueprint $table) {
            $table->id();
            $table->timestamp('execute_date')->nullable();
            $table->string('name')->nullable();
            $table->string('nik')->nullable();
            $table->string('expected')->nullable();
            $table->string('code_densus')->nullable();
            $table->string('birth')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('citizen')->nullable();
            $table->text('address')->nullable();
            $table->string('status')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_sigap');
    }
}
