<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepartementIdToUserPrivilageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_privilage', function (Blueprint $table) {
            $table->integer('departement_id')->after('role_id')->nullable();
            $table->boolean('isParent')->after('child')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_privilage', function (Blueprint $table) {
            $table->dropColumn('departement_id');
            $table->dropColumn('isParent');
        });
    }
}
