<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewChildMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_child_menu
            AS
            SELECT
                up.code as code,
                up.role_id as role_id,
                up.parent as parent,
                up.child as child,
                cm.code as code_menu,
                cm.name as name,
                cm.menuopen as menuopen,
                cm.icon as icon,
                cm.url as url,
                up.status as status,
                up.description as description
            FROM
                izi_data.user_privilage up
            left join
                izi_data.child_menus cm on
                (up.child = cm.code)
            ORDER BY
                up.role_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS `view_child_menu`;
        ");
    }
}
