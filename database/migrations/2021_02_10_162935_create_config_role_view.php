<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateConfigRoleView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_config_role_menu
            AS
            SELECT
                mm.code AS code_master,
                cm.code AS code_child,
                cm.parent AS parent,
                CASE
                	when (cm.code=jumlah ) or (cm.code is null) then true
                	else false
                END AS isParent,
                mm.name AS name_master,
                cm.name AS name_child,
                mm.menuopen AS menuopen,
                mm.icon AS icon
            FROM
                izi_data.master_menu mm
            LEFT JOIN izi_data.child_menus cm ON ( mm.code = cm.parent )
            join
                (
	                SELECT
	                mm.code AS code_master,
	                min(cm.code) jumlah
	                FROM
	                izi_data.master_menu mm
	                LEFT JOIN izi_data.child_menus cm ON ( mm.code = cm.parent )
	                GROUP BY
	                mm.code
                ) c on(mm.code=c.code_master)
            order by mm.code
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS `view_config_role_menu`;
        ");
    }
}
