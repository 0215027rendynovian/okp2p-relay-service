<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IlumaBankvalidationLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('log_iluma_bankvaldation', function (Blueprint $table) {
            $table->id();
            $table->string('bank_account_number');
            $table->string('bank_code');
            $table->string('given_name');
            $table->json('json');
            $table->string('uid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('log_iluma_bankvaldation');
    }
}
