<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSigapTerdugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sigap_terdugas', function (Blueprint $table) {
            $table->id();
            $table->string('id_backoffice')->nullable();
            $table->string('nama')->nullable();
            $table->string('nik')->nullable();
            $table->string('tanggal_lahir')->nullable();
            $table->string('tempat_Lahir')->nullable();
            $table->string('warga_negara')->nullable();
            $table->string('alamat')->nullable();
            $table->string('id_ojk_list')->nullable();
            $table->integer('status')->default(1);
            $table->string('additional')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sigap_terdugas');
    }
}
