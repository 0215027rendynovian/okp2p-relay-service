<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DtSigapReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_sigap_report', function (Blueprint $table) {
            $table->id();
            $table->timestamp('execute_date')->nullable();
            $table->string('name')->nullable();
            $table->string('nik')->nullable();
            $table->string('expected')->nullable();
            $table->string('code_densus')->nullable();
            $table->string('birth')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('citizen')->nullable();
            $table->text('address')->nullable();
            $table->text('reason')->nullable();
            $table->string('status_report')->nullable();
            $table->string('uid')->nullable();
            $table->string('ref_idsigap')->nullable();
            $table->string('ref_idbackoffice')->nullable();
            $table->string('inquiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('dt_sigap_report');
    }
}
