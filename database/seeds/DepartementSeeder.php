<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * run in artisan : php artisan db:seed --class=DepartementSeeder
     *
     * @return void
     */
    public function run()
    {
        DB::table('departements')->insert([
            [
                "code" => 1,
                "name" => "OKP2P",
                "description" => "Departement OKP2P",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 2,
                "name" => "Retail",
                "description" => "Departement Retail",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 3,
                "name" => "Asset",
                "description" => "Departement Asset",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ]
        ]);
    }
}
