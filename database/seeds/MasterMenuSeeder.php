<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * run in artisan : php artisan db:seed --class=MasterMenuSeeder
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('master_menu')->insert([
            [
                "code" => "1",
                "name" => "IZI DATA",
                "menuopen" => "iziData",
                "icon" => "fa-university",
                "url" => "creditfeature",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "2",
                "name" => "Fintech Data Center",
                "menuopen" => "",
                "icon" => "fa-server",
                "url" => "fdc-data",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "3",
                "name" => "Midtrans",
                "menuopen" => "mitrans",
                "icon" => "fa-money-check-alt",
                "url" => "",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "4",
                "name" => "Display OJK",
                "menuopen" => "",
                "icon" => "fa-file-invoice-dollar",
                "url" => "displayojk",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "5",
                "name" => "Instamoney",
                "menuopen" => "instamoney",
                "icon" => "fa-hand-holding-usd",
                "url" => "",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "6",
                "name" => "Pefindo",
                "menuopen" => "pefindo",
                "icon" => "fa-star-half-alt",
                "url" => "",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "7",
                "name" => "Silaras",
                "menuopen" => "pusdafil",
                "icon" => "fa-chart-bar",
                "url" => "",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "8",
                "name" => "Users",
                "menuopen" => "",
                "icon" => "fa-users-cog",
                "url" => "users-list",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => "9",
                "name" => "Apps Configuration",
                "menuopen" => "configuration",
                "icon" => "fa-cogs",
                "url" => "",
                "description" => "-",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
        ]);

    }
}
