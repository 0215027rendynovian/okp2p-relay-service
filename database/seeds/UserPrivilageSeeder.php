<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPrivilageSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * run in artisan : php artisan db:seed --class=UserPrivilageSeeder
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_privilage')->insert([
            // ================   Roles Admin

            // IZI Data
            [
                "code" => 1,
                "role_id" => 1,
                "parent" => 1,
                "child" => 1,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 2,
                "role_id" => 1,
                "parent" => 1,
                "child" => 2,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // FDC
            [
                "code" => 3,
                "role_id" => 1,
                "parent" => 2,
                "child" => null,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Midtrans
            [
                "code" => 4,
                "role_id" => 1,
                "parent" => 3,
                "child" => 3,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 5,
                "role_id" => 1,
                "parent" => 3,
                "child" => 4,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Display OJK
            [
                "code" => 6,
                "role_id" => 1,
                "parent" => 4,
                "child" => null,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Instamoney
            [
                "code" => 7,
                "role_id" => 1,
                "parent" => 5,
                "child" => 5,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Pefindo
            [
                "code" => 8,
                "role_id" => 1,
                "parent" => 6,
                "child" => 6,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 9,
                "role_id" => 1,
                "parent" => 6,
                "child" => 7,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 10,
                "role_id" => 1,
                "parent" => 6,
                "child" => 8,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 11,
                "role_id" => 1,
                "parent" => 6,
                "child" => 9,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 12,
                "role_id" => 1,
                "parent" => 6,
                "child" => 10,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Silaras
            [
                "code" => 13,
                "role_id" => 1,
                "parent" => 7,
                "child" => 11,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 14,
                "role_id" => 1,
                "parent" => 7,
                "child" => 12,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 15,
                "role_id" => 1,
                "parent" => 7,
                "child" => 13,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 16,
                "role_id" => 1,
                "parent" => 7,
                "child" => 14,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 17,
                "role_id" => 1,
                "parent" => 7,
                "child" => 15,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 18,
                "role_id" => 1,
                "parent" => 7,
                "child" => 16,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 19,
                "role_id" => 1,
                "parent" => 7,
                "child" => 17,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            //  ================   Roles Finance

            // Instamoney
            [
                "code" => 20,
                "role_id" => 2,
                "parent" => 5,
                "child" => 5,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            //  ================  Roles CSS

            // IZI Data
            [
                "code" => 21,
                "role_id" => 3,
                "parent" => 1,
                "child" => 1,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 22,
                "role_id" => 3,
                "parent" => 1,
                "child" => 2,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            //  ================  Roles Analyst

            // IZI Data
            [
                "code" => 23,
                "role_id" => 4,
                "parent" => 1,
                "child" => 1,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 24,
                "role_id" => 4,
                "parent" => 1,
                "child" => 2,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // FDC
            [
                "code" => 25,
                "role_id" => 4,
                "parent" => 2,
                "child" => null,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Midtrans
            [
                "code" => 26,
                "role_id" => 4,
                "parent" => 3,
                "child" => 3,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 27,
                "role_id" => 4,
                "parent" => 3,
                "child" => 4,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            //  ================  Roles HR

            // User

            [
                "code" => 28,
                "role_id" => 5,
                "parent" => 8,
                "child" => null,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            //  ================   Roles Super Admin

            // IZI Data
            [
                "code" => 29,
                "role_id" => 6,
                "parent" => 1,
                "child" => 1,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 30,
                "role_id" => 6,
                "parent" => 1,
                "child" => 2,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // FDC
            [
                "code" => 31,
                "role_id" => 6,
                "parent" => 2,
                "child" => null,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Midtrans
            [
                "code" => 32,
                "role_id" => 6,
                "parent" => 3,
                "child" => 3,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 33,
                "role_id" => 6,
                "parent" => 3,
                "child" => 4,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Display OJK
            [
                "code" => 34,
                "role_id" => 6,
                "parent" => 4,
                "child" => null,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Instamoney
            [
                "code" => 35,
                "role_id" => 6,
                "parent" => 5,
                "child" => 5,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Pefindo
            [
                "code" => 36,
                "role_id" => 6,
                "parent" => 6,
                "child" => 6,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 37,
                "role_id" => 6,
                "parent" => 6,
                "child" => 7,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 38,
                "role_id" => 6,
                "parent" => 6,
                "child" => 8,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 39,
                "role_id" => 6,
                "parent" => 6,
                "child" => 9,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 40,
                "role_id" => 6,
                "parent" => 6,
                "child" => 10,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Silaras
            [
                "code" => 41,
                "role_id" => 6,
                "parent" => 7,
                "child" => 11,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 42,
                "role_id" => 6,
                "parent" => 7,
                "child" => 12,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 43,
                "role_id" => 6,
                "parent" => 7,
                "child" => 13,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 44,
                "role_id" => 6,
                "parent" => 7,
                "child" => 14,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 45,
                "role_id" => 6,
                "parent" => 7,
                "child" => 15,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 46,
                "role_id" => 6,
                "parent" => 7,
                "child" => 16,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 47,
                "role_id" => 6,
                "parent" => 7,
                "child" => 17,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // User

            [
                "code" => 48,
                "role_id" => 6,
                "parent" => 8,
                "child" => null,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],

            // Configuration
            [
                "code" => 49,
                "role_id" => 6,
                "parent" => 9,
                "child" => 18,
                "isParent" => true,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 50,
                "role_id" => 6,
                "parent" => 9,
                "child" => 19,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 51,
                "role_id" => 6,
                "parent" => 9,
                "child" => 20,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 52,
                "role_id" => 6,
                "parent" => 9,
                "child" => 21,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 53,
                "role_id" => 6,
                "parent" => 9,
                "child" => 22,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 54,
                "role_id" => 6,
                "parent" => 9,
                "child" => 23,
                "isParent" => false,
                "status" => 1,
                "description" => "-",
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ]
        ]);

    }
}
