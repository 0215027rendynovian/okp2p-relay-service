<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * run in artisan : php artisan db:seed --class=RoleSeeder
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            [
                "code" => 1,
                "name" => "Admin",
                "description" => "Akses Admin terkait pengaturan sistem dll",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 2,
                "name" => "Finance",
                "description" => "Akses Finance terkait report pembayaran",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 3,
                "name" => "CSS",
                "description" => "Akses Call Center terkait Score dll",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 4,
                "name" => "Analyst",
                "description" => "Akses Analyst terkait score dll",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 5,
                "name" => "HR",
                "description" => "Menu HR terkait dengan management user dan kepegawaian",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ],
            [
                "code" => 6,
                "name" => "SUPER ADMIN",
                "description" => "Semua menu di buka",
                "status" => 1,
                "created_at" => \Carbon\Carbon::now('Asia/Jakarta'),
                "updated_at" => \Carbon\Carbon::now('Asia/Jakarta'),
            ]
        ]);

    }
}
