<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * Step to run Seeder in this apps :
     *
     * Before you run this Seeder, make sure it's done the mogration with run this :=>  php artisan migrate
     *
     * and then
     *
     * Run terminal in this project directory
     * 1. composer dump-autoload
     * 2. php artisan db:seed
     *
     * @return void
     */
    public function run()
    {
        /**
         * If you add another Seeder, don't forget to add line code in this block {}
         */
        $this->call(ChildMenuSeeder::class);
        $this->call(DepartementSeeder::class);
        $this->call(MasterMenuSeeder::class);
        $this->call(MBASeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserPrivilageSeeder::class);
        $this->call(UserSeeder::class);
    }
}
