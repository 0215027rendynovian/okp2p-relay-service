<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|ApiC
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// BNI Relay Service--------------------------------------------------------------------

// Get Token
Route::get('bni/get-token', 'BniApiController@getToken');

// Get Balance
Route::get('bni/get-balance', 'BniApiController@InquiryGetBalance');

// Get History
Route::get('bni/get-history', 'BniApiController@InquiryGetHistory');

// Get Info
Route::get('bni/account-info', 'BniApiController@InquiryAccountInfo');

//inquiry interbank
Route::get('bni/inquiry-interbank', 'BniApiController@InquiryInterbank');

// Pengecekan Staus Pembayaran 
Route::get('bni/payment-status', 'BniApiController@PaymentStatus');

// Payment Clearing
Route::get('bni/payment-clearing', 'BniApiController@PaymentClearing');

// Payment Interbank
Route::get('bni/payment-interbank', 'BniApiController@PaymentInterbank');

// Payment RTGS
Route::get('bni/payment-rtgs', 'BniApiController@PaymentRTGS');

// Payment Transfer
Route::get('bni/payment-transfer', 'BniApiController@PaymentTransfer');

// Register Investor
Route::get('/bni/register-investor', 'BniApiController@RegisterInvestor');

// Account Investor
Route::get('/bni/account-investor', 'BniApiController@AccountInvestor');



Route::get('api-lender', 'ApiController@getDataRegLender')->name('getRegLender');
Route::post('/midtrans-okp2p-api', 'ApiController@getMidtrans');
Route::post('/saved-midtrans', 'ApiController@getSavedMidtrans');
Route::get('/get-user-count', 'ApiController@getUserCount');
Route::get('/izi-history-detail/{id}', 'ApiController@getHistoryDetail');
Route::get('/division-test/{num}', 'ApiController@doDivision');
Route::get('/report-denda-bulanan/', 'ApiController@getReportDendaBulanan')->name('reportdendabulanan');


// Callback
Route::post('/IziCallBack','ApiController@IziCallBack');

// send data Pusdafil
 Route::get('/send_req_pengguna','SilarasController@send_reg_pengguna');
 Route::get('/send_reg_borrower','SilarasController@send_reg_borrower');
 Route::get('/send_reg_lender','SilarasController@send_reg_lender');
 Route::get('/send_pengajuan_pinjaman','SilarasController@send_pengajuan_pinjaman');
 Route::get('/send_pengajuan_pemberian_pinjaman','SilarasController@send_pengajuan_pemberian_pinjaman');
 Route::get('/send_transaksi_pinjam_meminjam','SilarasController@send_transaksi_pinjam_meminjam');
 Route::get('/send_pembayaran_pinjaman','SilarasController@send_pembayaran_pinjaman');
 Route::get('/send_test_scheduller','SilarasController@send_test_scheduller');
 Route::get('/get-pusdafil-log', 'SilarasController@get_pusdafil_log')->name('pusdafillog');
 Route::get('/get-log-detail/{id}', 'SilarasController@get_log_detail');
 Route::get('/get-ping-config', 'PingConfigController@getConfig');


 Route::group(['middleware' => 'auth:api'], function(){

    //Fintech Data Center
    Route::post('/api-post', 'ApiController@ApiFintechDtCenter');
   
    //All SMS
    Route::post('/api-sendsms', 'ApiController@SendSMS');

    //IZI DATA
    Route::post('/api-izi-post', 'ApiController@CreditFeatured');

    //ILUMA
    Route::post('/api-iluma-bank-validation', 'ApiController@SendIlumaValidationBank');

    //IZI DATA SALLARY
    Route::post('/api-sallary', 'ApiController@sallary');

    //PEFINDO
    Route::post('relay/requestSmartSearch', 'ApiController@requestSmartSearchCustomReport');

    //Advance AI
    Route::post('api-advance-score', 'ApiController@SendAdvanceAiScore');

    //MIDTRANS
    Route::post('/api-midtrans-bank-validation', 'ApiController@account_validation');

    //Check NPL Asset Company
    Route::post('/check-customers-asset', 'ApiController@CheckNPLCust');
 
 });



 
Route::post('/fdc-internserve', 'ApiController@ApiFintechDtCenter');


// Firebase Notification Controller

Route::post('/firebase-sendsms', 'NotificationController@send');




 
 
//Otentikasi
Route::post('login', 'UserController@login');
Route::POST('getToken', 'UserController@getToken');
Route::post('register', 'UserController@register');

?>
