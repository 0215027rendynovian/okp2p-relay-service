<?php

use App\Http\Controllers\PingConfigController;
use FontLib\Table\Type\name;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/okp2p-register', function () {
    return view('auth.register-mitra');
});

Route::get('api-documentation','TechnicalDocumentationController@index');

Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');


Route::get('/ops-sistem/login', function(){
    return view('auth.login-ops');
});

Route::get('/es-sistem/login', function(){
    return view('auth.login-es');
});

Route::get('/clearCache', 'DashboardController@clearCache')->name('clearCache');

Route::group(['middleware'  =>  'es_system'], function(){

     // IZI DATA / CREDIT SCORING
     Route::get('/admin', 'IziController@index');
     Route::get('/creditfeature', 'IziController@creditfeature');
     Route::get('/creditfeature', 'IziController@creditfeature')->name('creditfeature');
     Route::post('izi_import_excel', 'IziClientController@import_excel');
     Route::get('/creditfeatureReport/{ktp}/{phone_number}/{nama_lengkap}', 'IziController@creditfeatureReport');
     Route::post('/creditFeatured', 'IziClientController@creditFeatured');
     Route::post('/creditFeaturedLog', 'IziClientController@creditFeaturedLog');
     Route::get('getCountHistory', 'IziClientController@getCountHistory')->name('getCountHistory');
     Route::get('getHistoryHit', 'IziClientController@getHistoryHit')->name('getHistoryHit');
     Route::get('/history', 'IziClientController@showHistory');
     Route::get('/history-data/{uid}/{start_date}/{end_date}', 'IziClientController@showHistoryDetail');
     Route::get('/exportDataIziHistory/{type}', 'IziClientController@exportDataIziHistory');
     Route::get('/exportRawData/{type}', 'IziClientController@exportRawData');

     // FDC
     Route::get('fdc-data', 'IziController@Fintectdatacentaer');
     Route::get('/FDCDataReport/{ktp}/{reason}', 'IziController@FDCDataReport');
     Route::post('/PostCreditFeature', 'IziController@PostCreditFeature');
     Route::post('/FDCDataPost', 'IziClientController@FDCDataPost');
     Route::post('/FDCDataPostLog', 'IziClientController@FDCDataPostLog');
     Route::post('/FDCDataExcel', 'IziClientController@FDCDataExcel');
});

Route::group( ['middleware' => 'auth' ], function()
{

            // DASHBOARD
            Route::get('/', 'DashboardController@index');
            Route::get('/', 'DashboardController@index')->name('home');

            // Route::get('/', 'IziController@creditfeature');
            Route::get('/index', 'SettingAccountController@index')->name('main');
            Route::get('/identitycheck', 'IziController@identitycheck');
            Route::get('/facecomparison', 'IziController@facecomparison');

            // IZI DATA / CREDIT SCORING
            Route::get('/admin', 'IziController@index');
            Route::get('/creditfeature', 'IziController@creditfeature');
            Route::get('/creditfeature', 'IziController@creditfeature')->name('creditfeature');
            Route::post('izi_import_excel', 'IziClientController@import_excel');
            Route::get('/creditfeatureReport/{ktp}/{phone_number}/{nama_lengkap}', 'IziController@creditfeatureReport');
            Route::post('/creditFeatured', 'IziClientController@creditFeatured');
            Route::post('/creditFeaturedLog', 'IziClientController@creditFeaturedLog');
            Route::get('getCountHistory', 'IziClientController@getCountHistory')->name('getCountHistory');
            Route::get('getHistoryHit', 'IziClientController@getHistoryHit')->name('getHistoryHit');
            Route::get('/history', 'IziClientController@showHistory');
            Route::get('/history-data/{uid}/{start_date}/{end_date}', 'IziClientController@showHistoryDetail');
            Route::get('/exportDataIziHistory/{type}', 'IziClientController@exportDataIziHistory');
            Route::get('/exportRawData/{type}', 'IziClientController@exportRawData');

            // FDC
            Route::get('fdc-data', 'IziController@Fintectdatacentaer');
            Route::get('/FDCDataReport/{ktp}/{reason}', 'IziController@FDCDataReport');
            Route::post('/PostCreditFeature', 'IziController@PostCreditFeature');
            Route::post('/FDCDataPost', 'IziClientController@FDCDataPost');
            Route::post('/FDCDataPostLog', 'IziClientController@FDCDataPostLog');
            Route::post('/FDCDataExcel', 'IziClientController@FDCDataExcel');

            //Pefindo
            Route::get('pefindo', 'PefindoController@index');
            Route::get('contract', 'PefindoController@dataContract');
            Route::get('getcontract','PefindoController@getDataContract')->name('getcontract');
            Route::get('/exportcontract', 'PefindoController@exportDataContract');
            Route::get('individual', 'PefindoController@dataIndividual');
            Route::get('getindividual','PefindoController@getDataIndividual')->name('getindividual');
            Route::get('/exportindividual', 'PefindoController@exportDataIndividual');
            Route::get('company', 'PefindoController@dataCompany');
            Route::get('/exportcompany', 'PefindoController@exportDataCompany');
            Route::get('collateral', 'PefindoController@dataCollateral');
            Route::get('getcollateral','PefindoController@getDataCollateral')->name('getcollateral');
            Route::get('/exportcollateral', 'PefindoController@exportDataCollateral');
            Route::get('subject-relation', 'PefindoController@dataSubjectRelation');
            Route::get('getsubjectrelation','PefindoController@getDataSubjectRelation')->name('getsubjectrelation');
            Route::get('/exportsubjectrelation', 'PefindoController@exportDataSubjectRelation');

            //Midtrans
            Route::get('midtrans-bank-account-validator', 'MidtransController@index');
            Route::get('/midtrans-bank-account-validator', 'MidtransController@index');
            Route::get('/report-midtrans-bank-validator/{account_code}', 'MidtransController@ReportMidtrans');
            Route::get('/midtrans-history', 'MidtransController@showHistory');
            Route::get('/midtrans-history/{uid}', 'MidtransController@showHistoryDetail');

            Route::get('getCountHistoryMidtrans', 'MidtransController@getCountHistory')->name('getCountHistoryMidtrans');
            Route::get('getHistoryHitMidtrans', 'MidtransController@getHistoryHit')->name('getHistoryHitMidtrans');
            Route::get('/midtrans-history', 'MidtransController@showHistory');
            Route::get('/midtrans-history/{uid}/{start_date}/{end_date}', 'MidtransController@showHistoryDetail');
            Route::get('/exportDataMidtransHistory/{type}', 'MidtransController@exportDataMidtransHistory');

            // Master Bank Account
            Route::get('mba', 'MasterBankAccountController@index');
            Route::post('mba/store', 'MasterBankAccountController@store')->name('mba-store');
            Route::put('mba/edit', 'MasterBankAccountController@edit')->name('mba-edit');
            Route::get('mba/delete/{id}', 'MasterBankAccountController@destroy');

            //display ojk
            Route::get('displayojk', 'DisplayOjkController@index');
            Route::get('getdisplayojk','DisplayOjkController@getData')->name('getdisplayojk');
            Route::get('/exportData/{type}', 'DisplayOjkController@exportData');
            Route::get('/exportalldata', 'PefindoController@exportAllData');
            Route::post('/ParseJson', 'IziClientController@ParseJson');

            // instamoney
            Route::get('/BalanceIM', 'InstamoneyController@BalanceInstamoney');
            Route::get('/pokok-denda', 'InstamoneyController@PokokDenda');
            Route::get('/export-pokok-denda/{start}/{end}/{status}','InstamoneyController@exportData');
            Route::post('/apiBalanceInstamoney', 'InstamoneyController@apiBalanceInstamoney');
            Route::get('get-bunga-admin-fee','InstamoneyController@getDataBungaTetapAdminFee')->name('get-bunga-admin-fee');
            Route::get('getrepayment','InstamoneyController@getDataRepayment')->name('getrepayment');

            // manage user
            Route::get('users-list', 'UserController@index');
            Route::post('users-list/store', 'UserController@store')->name('user-store');
            Route::put('users-list/edit', 'UserController@edit')->name('user-edit');
            Route::get('users-list/delete/{id}', 'UserController@destroy');
            Route::get('users-list/resetpassword/{id}', 'UserController@resetpassword');

            // silaras
            Route::get('index-silaras', 'SilarasController@index');

            // pusdafil
            Route::get('reg-pengguna', 'SilarasController@reg_pengguna');
            Route::get('reg-borrower', 'SilarasController@reg_borrower');
            Route::get('reg-lender', 'SilarasController@reg_lender');
            Route::get('pengajuan-pinjaman', 'SilarasController@pengajuan_pinjaman');
            Route::get('pengajuan-pemberian-pinjaman', 'SilarasController@pengajuan_pemberian_pinjaman');
            Route::get('transaksi-pinjam-meminjam', 'SilarasController@transaksi_pinjam_meminjam');
            Route::get('pembayaran-pinjaman', 'SilarasController@pembayaran_pinjaman');
            Route::get('pusdafil-log', 'SilarasController@ShowPusdafilLog');

            // get data pusdafil
            Route::get('getRegPengguna', 'SilarasController@getDataRegPengguna')->name('getRegPengguna');
            Route::get('getRegBorrower', 'SilarasController@getDataRegBorrower')->name('getRegBorrower');
            Route::get('getRegLender', 'SilarasController@getDataRegLender')->name('getRegLender');
            Route::get('getPengajuanPinjaman', 'SilarasController@getDataPengajuanPinjaman')->name('getPengajuanPinjaman');
            Route::get('getPengajuanPemberianPinjaman', 'SilarasController@getDataPengajuanPemberianPinjaman')->name('getPengajuanPemberianPinjaman');
            Route::get('getTransaksiPinjamMeminjam', 'SilarasController@getDataTransaksiPinjamMeminjam')->name('getTransaksiPinjamMeminjam');
            Route::get('getPembayaranPinjaman', 'SilarasController@getDataPembayaranPinjaman')->name('getPembayaranPinjaman');

            // export txt pusdafil silaaras
            Route::get('exportRegPengguna/{type}', 'SilarasController@exportRegPengguna');
            Route::get('exportRegBorrower/{type}', 'SilarasController@exportRegBorrower');
            Route::get('exportRegLender/{type}', 'SilarasController@exportRegLender');
            Route::get('exportPengajuanPinjaman/{type}', 'SilarasController@exportPengajuanPinjaman');
            Route::get('exportPengajuanPemberianPinjaman/{type}', 'SilarasController@exportPengajuanPemberianPinjaman');
            Route::get('exportTransaksiPinjamMeminjam/{type}', 'SilarasController@exportTransaksiPinjamMeminjam');
            Route::get('exportPembayaranPinjaman/{type}', 'SilarasController@exportPembayaranPinjaman');


            // configuration apps
            Route::get('index-config', 'ConfigurationController@index');

            // Master Roles
            Route::get('roles', 'RolesController@index');
            Route::post('roles/store', 'RolesController@store')->name('roles-store');
            Route::put('roles/edit', 'RolesController@edit')->name('roles-edit');
            Route::get('roles/delete/{id}', 'RolesController@destroy');

            // Master Emails
            Route::get('mails', 'MasterEmailController@index');
            Route::post('mails/store', 'MasterEmailController@store')->name('mails-store');
            Route::put('mails/edit', 'MasterEmailController@edit')->name('mails-edit');
            Route::get('mails/delete/{id}', 'MasterEmailController@destroy');

            // Master Departement
            Route::get('departement', 'DepartementController@index');
            Route::post('departement/store', 'DepartementController@store')->name('departement-store');
            Route::put('departement/edit', 'DepartementController@edit')->name('departement-edit');
            Route::get('departement/delete/{id}', 'DepartementController@destroy');

            // Master Menu
            Route::get('displaymenu', 'DisplayMenuController@index');
            Route::post('displaymenu/store', 'DisplayMenuController@store')->name('displaymenu-store');
            Route::put('displaymenu/edit', 'DisplayMenuController@edit')->name('displaymenu-edit');
            Route::get('displaymenu/delete/{id}', 'DisplayMenuController@destroy');

            // Child Menu
            Route::get('childmenu', 'ChildMenuController@index');
            Route::post('childmenu/store', 'ChildMenuController@store')->name('childmenu-store');
            Route::put('childmenu/edit', 'ChildMenuController@edit')->name('childmenu-edit');
            Route::get('childmenu/delete/{id}', 'ChildMenuController@destroy');

            // Master Privilege
            Route::get('privilege', 'UserPrivilegeController@index');
            Route::post('privilege/store', 'UserPrivilegeController@store')->name('privilege-store');
            Route::put('privilege/edit', 'UserPrivilegeController@edit')->name('privilege-edit');
            Route::get('privilege/delete/{id}', 'UserPrivilegeController@destroy');

            // Config Role Menu
            Route::get('config-role', 'ConfigRolesMenuController@index')->name('config-role');
            Route::get('edit-config-role/{code}', 'ConfigRolesMenuController@edit');
            Route::put('config-role/update/{id}', 'ConfigRolesMenuController@update')->name('config-role-update');
            Route::put('config-role/reset', 'ConfigRolesMenuController@reset')->name('config-role-reset');

            // SIGAP
            Route::get('uploadData', 'SigapController@uploadData');
            Route::get('findData', 'SigapController@findData');
            Route::get('dataReport', 'SigapController@dataReport');
            Route::post('sigap_import_excel', 'SigapController@import_excel');
            Route::post('update_flag_analysisi', 'SigapController@update_flag_analysisi');
            Route::get('getDataSigap', 'SigapController@getDataSigap')->name('getDataSigap');
            Route::get('getDataReportTerduga', 'SigapController@getDataReportTerduga')->name('getDataReportTerduga');
            Route::get('report-nihil/{id}', 'SigapController@getDataReportNihil');
            Route::get('report-pemblokiran/{id}', 'SigapController@getDataReportPemblokiran');
            Route::get('downloadTerduga', 'SigapController@downloadTerduga')->name('downloadTerduga');
            Route::get('getReportNihil', 'SigapController@getReportNihil')->name('getReportNihil');
            Route::get('getReportPemblokiran', 'SigapController@getReportPemblokiran')->name('getReportPemblokiran');
            Route::post('form_report_nihil', 'SigapController@form_report_nihil');
            Route::post('form_report_pemblokiran', 'SigapController@form_report_pemblokiran');
            Route::resource('editSigap','SigapController');
            Route::get('editSigap/{id}/editReasonSigap/','SigapController@editReasonSigap');
            Route::put('updateSigap', 'SigapController@updateReasonSigap')->name('updateSigap');
            Route::get('sigap/cancelReportNihil/{id}', 'SigapController@cancelReportNihil');
            Route::get('sigap/cancelReportPemblokiran/{id}', 'SigapController@cancelReportPemblokiran');
            Route::get('getCreationData', 'SigapController@getCreationData')->name('getCreationData');


            // Laravel Pinging Request
            Route::get('laravel-pinging', 'PingController@pinging');

            // MAILER
            Route::get('send-mail', 'ReportServicesController@reportMail');

            Route::get('ipconfig', 'PingConfigController@index');
            Route::post('set-ping-config', 'PingConfigController@setConfig')->name('set-ping-config');

        });


Auth::routes();








